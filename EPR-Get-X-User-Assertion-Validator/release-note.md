---
title: Release note
subtitle: Get-X-User Assertion Validator
toolversion: 66702
releasedate: 2019-03-12
author: Malo TOUDIC
function: Software developer
customer: IHE-Europe
reference: KER1-RNO-IHE-Get_X_User_Assertion_Validator
---
# 66702
_Release date: 2019-03-12_

__Improvement__

* \[[GXUAV-1](https://gazelle.ihe.net/jira/browse/GXUAV-1)\] Implement EPR 1.7 & 1.8
