---
title:  User Manual
subtitle: Get-X-User Assertion Validator
author: Hilary Rabe
function: Software Engineer
date: 10/09/2019
toolversion: 1.1.2
version: 0.01
status: to be reviewed
reference: KER1-MAN-IHE-GET-X-USER_ASSERTION_VALIDATOR_USER
customer: IHE-EUROPE
---

# Accessing
EPD Get-X-User Assertion Validators are available in [EVSClient](https://ehealthsuisse.ihe-europe.net/EVSClient/) from EPR > CH:XUA Get X-User > validate.

# Assertions covered
  * The Get X-User Assertion Request/Response SHOULD be bind to SOAP 1.2 (IHE ITI-TF-vol2x Appendix V).

## CH:XUA Get_X-User_Assertion_Request

ch_xua_get_assertion_common_rule:
  * CH-XUA-091: The Get X-User Assertion Request MUST use the RequestSecurityToken WS-Trust 1.3 (SR 816.111_Ergaenzung 1 Anhang 5 - §Referenced Standard).

Get X-User Assertion Request [...] MUST contain a <wst:Claims> with the following attributes:
  * The Get X-User Assertion Request MUST contain a list of claims.
  * The claim dialect must be http://www.bag.admin.ch/epr/2017/annex/5/amendment/2.
  * The Get X-User Assertion Request MUST contain the SAML Attribute defining the role of the accessing person in the claims.
  * The Get X-User Assertion Request MUST contain the SAML Attribute defining the requested resource id in the claims.
  * The Get X-User Assertion Request MUST contain the SAML Attribute defining the purpose of use in the claims.

Healthcare professional extension:
  * CH-XUA-047: The Get X-User Assertion Request for an healthcare professional MUST contain the SAML Identity Assertion in the Web Service security header.
  * CH-XUA-083: In the healthcare professional extension the role claim ("urn:oasis:names:tc:xacml:2.0:subject:role") attribute MUST be the code HCP from code system 2.16.756.5.30.1.127.3.10.6 of the CH:EPR value set.

Patient extension:
  * CH-XUA-067: The Get X-User Assertion Request for a patient MUST contain the original SAML Identity Assertion in the Web Service Security header.
  * CH-XUA-040: The Get X-User Assertion Request for a patient MUST contain the purpose of use claim attribute with the code NORM from the code system 2.16.756.5.30.1.127.3.10.5 of the CH:EPR value set.
  * CH-XUA-070: There MAY be one Attribute element with name "urn:e-health-suisse:principal-id". If present, the AttributeValue child element MUST convey the EPR-SPID of the patient.

Representative extension:
  * CH-XUA-72: The Get X-User Assertion Request for a representative MUST contain the original SAML Identity Assertion in the Web Service Security header.
  * CH-XUA-040: The Get X-User Assertion Request for a representative MUST contain the purpose of use claim attribute with the code NORM from the code system 2.16.756.5.30.1.127.3.10.5 of the CH:EPR value set.

Assistant extension:
  * CH-XUA-049: The Get X-User Assertion Request for an assistant MUST contain the original SAML Identity Assertion in the Web Service Security header.
  * CH-XUA-050: There MUST be one Attribute element with name "urn:e-health-suisse:principal-id". The AttributeValue child element MUST convey the GLN of the healthcare professional an assistant is acting on behalf of.
  * CH-XUA-051: There MUST be one Attribute element with the attribute name "urn:e-health-suisse:principal-name". The AttributeValue child element MUST convey the name of the healthcare pro-fessional an assistant is acting on behalf of.
  * CH-XUA-033: There MAY be one or more Attribute elements with name "urn:oasis:names:tc:xspa:1.0:subject:organization-id". If present the AttributeValue child element MUST convey the ID of the subject’s organization or group. The ID MUST be an OID in the format of an URN.

Technical user extension:
  * CH-XUA-055: The Get X-User Assertion Request for a technical user MUST contain the original SAML Identity Assertion in the Web Service Security header.
  * CH-XUA-057: There MUST be one Attribute element with name "urn:e-health-suisse:principal-id". The AttributeValue child element MUST convey the GLN of the healthcare professional an assistant is acting on behalf of.
  * CH-XUA-058: There MUST be one or more Attribute elements with name attribute "urn:e-health-suisse:principal-name". The AttributeValue child element MUST convey the name of the legal responsible healthcare professional the technical user is acting on behalf of.
  * CH-XUA-059: The Get X-User Assertion Request for a technical user MUST contain the purpose of use claim attribute with the code AUTO from the code system 2.16.756.5.30.1.127.3.10.5 of the CH:EPR value set.

Document administrator extension:
  * CH-XUA-064: The Get X-User Assertion Request for a document administrator MUST contain the original SAML Identity Assertion in the Web Service Security header.
  * CH-XUA-040: The Get X-User Assertion Request for a document administrator MUST contain the purpose of use claim attribute with the code NORM from the code system 2.16.756.5.30.1.127.3.10.5 of the CH:EPR value set.

Policy administrator extension:
  * CH-XUA-061: The Get X-User Assertion Request for a policy administrator MUST contain the original SAML Identity Assertion in the Web Service Security header.
  * CH-XUA-040: The Get X-User Assertion Request for a policy administrator MUST contain the purpose of use claim attribute with the code NORM from the code system 2.16.756.5.30.1.127.3.10.5 of the CH:EPR value set.



## CH:XUA Get_X-User_Assertion_Response

  * CH-XUA-008: The Get X-User Assertion Response MUST use the RequestSecurityTokenResponse WS-Trust 1.3 (SR 816.111.1_Ergaenzung 1 Anhang 5 - §1.5.3.1.3 Referenced Standards).
  * The Get X-User Assertion Response MUST contain the SAML User Assertion (SR 816.111.1_Ergaenzung 1 Anhang 5 - §1.5.3.2.4.3 Expected Actions).
  * SAML User Assertion presence have been verified but not its structure and content. You MUST use SAML Assertion Validator for that.
  * CH-XUA-002: There MUST be one Attribute element with the name attribute "urn:oasis:names:tc:xspa:1.0:subject:subject-id".
  * CH-XUA-005:There MUST be one Attribute element with the name attribute "urn:oasis:names:tc:xacml:2.0:subject:role".
  * CH-XUA-003: There MUST be one Attribute element with the name attribute "urn:oasis:names:tc:xspa:1.0:subject:organization-id".
  * CH-XUA-004: There MUST be one Attribute element with the name attribute "urn:oasis:names:tc:xspa:1.0:subject:organization".
  * CH-XUA-007: There MUST be one Attribute element with the name attribute "urn:oasis:names:tc:xspa:1.0:subject:purposeofuse".
