---
title: User Manual
subtitle: IUA Simulator
author: Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
releasedate: 2023-04-20
toolversion: 1.0.2
function: Software Engineer
version: 1.00
status: To be reviewed
---

## Table of content

1. [IHE Profile](#ihe)
   1. [ITI-71](#ihe-iti-71)
   2. [ITI-72](#ihe-iti-72)
   3. [ITI-102](#ihe-iti-102)
   4. [ITI-103](#ihe-iti-103)
2. [CH Profile](#ch)
    1. [ITI-71](#ch-iti-71)
    2. [ITI-72](#ch-iti-72)
    3. [ITI-102](#ch-iti-102)
    4. [ITI-103](#ch-iti-103)

## IHE Profile <a name="ihe"></a>

### ITI-71 <a name="ihe-iti-71"></a>

![iti 71 transaction](./resources/iti-71.png)

The ITI-71 transaction is sent by the Authorization Client to the Authorization Server to retrieve an access Token to be used within requests to the Resource server.

Two ways can be used to resolve this transaction, but the one used by this simulator is the Authorization Code grant type.
The request must be sent to the /authorize endpoint, which look like this

``` http
{$simulator.endpoint}/iua-simulator/rest/ch/authorize?
```

The first Http Request is a GET containing the following parameters :

| parameter            |required/optional| description                                                                                                                                                                                                                |
|----------------------|----|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| response_type        |required| The response format of the authorization method. Here must be code                                                                                                                                                         |
| client_id            |required| The ID, the client is registered at the IUA Authorization Server.                                                                                                                                                          |
| redirect_uri         |required| Used as callback URL, the IUA Authorization Server will send the authorization code to. The URL SHALL match one of the client’s pre-registered redirect URIs.                                                              |
| state                |required| An unguessable value used by the client to track the state between the authorization request and the callback.                                                                                                             |
| scope                |required| Attributes the app claims                                                                                                                                                                                                  |
| code_challenge       |required| Transformed version of code_verifier with code_challenge_method                                                                                                                                                            |
| code_chalenge_method |required| SHALL be “S256”.                                                                                                                                                                                                           |

This request must be sent to the /authorize endpoint of the authorization server.
This means the request should look like something like this :

``` http
HTTP GET {$simulator.endpoint}/iua-simulator/rest/ch/authorize?
   response_type=code
   &client_id=ch-iua-client
   &state=helloFromChIua
   &resource=ihe.pixm.net
   &code_challenge=fDH8WRQ-vp-6dU6_llO2Fa7wHYSoiKOWoDNxl_u0TLg
   &code_challenge_method=S256
   &redirect_uri={$simulator.endpoint}/iua-simulator/rest/ch/callback
   &scope=profile
   HTTP 1.1
```
Following this request, you will receive an Authorization Code in a JSON formatted response.

Using the Authorization client, you will then need to do a second Request:

| parameter     | required/optional                | description                                                                                             |
|---------------|----------------------------------|---------------------------------------------------------------------------------------------------------|
| client_id     | required in Authorization header | The ID the client is registered at the IUA Authorization Server.                                        |
| client_secret | required in Authorization header | The secret to access the client                                                                         |
| redirect_uri  | required                         | The URI to redirect the apps user agent to. Must be the same as in the first request                    |
| grant_type    | required                         | Value shall be “authorization_code”.                                                                    |
| code          | required                         | The authorization code we retrieved through the previous request                                        |
| code_verifier | required                         | The original code verifier string, the non-hashed version of the code_challenge from! the first request |

This request is a Post sent to the /token endpoint of the authorization server.
Those arguments must therefore be contained in a body as an *application/x-www-form-urlencoded* content-type, and encoded in UTF-8
Finally both *client_id* and *client_secret* must be encapsulated in the base HTTP Authorization header.

This means the request should like something like this :

``` http
HTTP POST {$simulator.endpoint}/iua-simulator/rest/ch/token HTTP 1.1
Authorization: Basic vsdvZRTYHQS51SgvtsqeGFs62
Content-Type: application/x-www-form-urlencoded

grant_type=authorization_code
&code=13fER1Gqe2FshWDRwg
&redirect_uri={$simulator.endpoint}/iua-simulator/rest/ch/callback
&code_verifier=-5Q635ku8ER2pfGeuDagpsk40
```

As a response to this, you should receive a JSON containing the access_token. You can use resources such as [jwt.io](#https://jwt.io)
to verify the content of your access_token.

This access_token can now be used by the user to access resource servers or for other transactions.
### ITI-72 <a name="ihe-iti-72"></a>
Coming soon
### ITI-102 <a name="ihe-iti-102"></a>
Coming soon
### ITI-103 <a name="ihe-iti-103"></a>

The ITI-103 transaction is usually sent by the Resource Server to the Authorization Server to retrieve information relative to the the endpoints and token format created by the authorization server.

``` http
HTTP GET {$simulator.endpoint}/iua-simulator/rest/ihe/.well-known HTTP 1.1
```

Here is a list of the properties that should be accessible through this call, and their meaning.

| Name   | Required/Optional/Conditional ? | Description |
|-|-|-|
| issuer | required | The Authorization Server's issuer identifier, which is a URL that uses the "https" scheme and has no query or fragment components.|
|authorization_endpoint | required | Authorization Server's Authorization endpoint as used for the "authorization code" flow.|
|token_endpoint | required | Authorization Server's Authorization token endpoint location.|
|jwks_uri | conditional | URL of the Authorization Server's JWK Set |
|scopes_supported | optional | The list of scopes supported by the Authorization Server. Note this transaction does not define the scopes. Authorization Servers may opt to publish a subset of the scopes usable.|
|response_types_supported | required | As the implicit grant flow is not supported in OAuth2.1, the response types should not include the value "token". Authorization Servers shall include the response type "code". Authorization Servers supporting [OpenId Connect] or other standards may include other token types, such as "id_token".|
|grant_types_supported | required | A JSON array listing the type of grants supported toward the token endpoint. The list shall include the values "client_credentials", and "authorization_code". Authorization Servers supporting refresh tokens shall include "refresh-token". Authorization Servers supporting the JWT grant shall include "urn:ietf:params:oauth:grant-type:jwt-bearer".Authorization Servers supporting the SAML grant shall include "urn:ietf:params:oauth:grant-type:saml2-bearer". Authorization Servers supporting other types of grants should provide additional values to this list representing those grant types.|
|token_endpoint_auth_methods_supported | optional | JSON array containing a list of client authentication methods supported by this token endpoint. When provided, this list shall include "client_secret_basic".|
|introspection_endpoint | conditional |URL of the Authorization Server's OAuth 2.0 introspection endpoint. This claim must be provided by Authorization Servers supporting the Token Introspection Option.|
|introspection_endpoint_auth_methods_supported | conditional | JSON array containing a list of client authentication methods supported by the introspection endpoint. This claim must be provided by Authorization Servers supporting the Token Introspection Option. As the introspect option mandates support for Resource Server bearer access tokens, this claim must include the "Bearer" value. Other authentication methods (e.g., "client_secret_basic") are allowed when supported by the Authorization Server.|
|access_token_format | optional | JSON string defining the format of the access token as provided by the Authorization Server. This allows Resource Servers to learn about methods of verification. Authorization Servers supporting the JWT Token Option shall set this claim value to "ihe-jwt". Authorization Servers supporting the SAML Token Option shall set this claim value to "ihe-saml". Authorization Servers providing tokens that are non-parsable shall set this value to "opaque". Authorization Servers supporting access tokens in other structured formats may use alternative format identifiers.|

## CH Profile <a name="ch"></a>

### ITI-71 <a name="ch-iti-71"></a>

![iti 71 transaction](./resources/iti-71.png)

The ITI-71 transaction is sent by the Authorization Client to the Authorization Server to retrieve an access Token to be used within requests to the Resource server.

Two ways can be used to resolve this transaction, but the one used by this simulator is the Authorization Code grant type.
The request must be sent to the /authorize endpoint, which look like this

``` http
{$simulator.endpoint}/iua-simulator/rest/ch/authorize?
```

The first Http Request is a GET containing the following parameters :

| parameter            |required/optional| description                                                                                                                                                                                                                |
|----------------------|----|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| response_type        |required| The response format of the authorization method. Here must be code                                                                                                                                                         |
| client_id            |required| The ID, the client is registered at the IUA Authorization Server.                                                                                                                                                          |
| redirect_uri         |required| Used as callback URL, the IUA Authorization Server will send the authorization code to. The URL SHALL match one of the client’s pre-registered redirect URIs.                                                              |
| state                |required| An unguessable value used by the client to track the state between the authorization request and the callback.                                                                                                             |
| scope                |required| Attributes the app claims                                                                                                                                                                                                  |
| aud                  |required| The audience URL the token will be used for.                                                                                                                                                                               |
| launch               |optional| If present, the launch parameter indicates that the app (or the view) was launched from an EHR or mHealth App context which has already been authorized to access the Swiss EPR (e.g. SMART on FHIR based primary system). |
| code_challenge       |required| Transformed version of code_verifier with code_challenge_method                                                                                                                                                            |
| code_chalenge_method |required| SHALL be “S256”.                                                                                                                                                                                                           |

This request must be sent to the /authorize endpoint of the authorization server.
This means the request should look like something like this :

``` http
HTTP GET {$simulator.endpoint}/iua-simulator/rest/ch/authorize?
   response_type=code
   &client_id=ch-iua-client
   &state=helloFromChIua
   &resource=ihe.pixm.net
   &code_challenge=fDH8WRQ-vp-6dU6_llO2Fa7wHYSoiKOWoDNxl_u0TLg
   &code_challenge_method=S256
   &redirect_uri={$simulator.endpoint}/iua-simulator/rest/ch/callback
   &scope=profile
   &aud=http://pixmResourceServerURL.ch
   &launch=azerty
   HTTP 1.1
```

The scope request inside this request shall only contain permitted arguments :

| scope parameter | Optionnality (Basic/Extended) | description |
|-|-------------------------------|-|
|launch| Optionnal/Required            |Permission to obtain launch context when the app is launched from an EHR. Required for apps or views launched from an EHR or a mHealth App which was authorized before.|
|purpose_of_use| Optionnal/Required            |Value taken from code system 2.16.756.5.30.1.127.3.10.5 of the CH: EPR value set.|
|subject_role| Optionnal/Required            |Only the values for the Role of Healthcare Professionals, Assistants, Patients and Representatives are allowed.|
|person_id| Optionnal/Required            |EPR-SPID identifier of the patient’s record and the patient assigning authority formatted in CX syntax.|
|principal| Optionnal/Optionnal           |Name of the healthcare professional an assistant is acting on behalf of.|
|principal_id| Optionnal/Optionnal           |GLN of the healthcare professional an assistant is acting on behalf of.|
|group| Optionnal/Optionnal           |Name of the organization or group an assistant is acting on behalf of.|
|group_id| Optionnal/Optionnal           |OID of the organization or group an assistant is acting on behalf of.|
|access_token_format| Optionnal/Optionnal           |Either ihe-jwt or ihe-saml as value. Will return this token_flavor. If scope is not provided defaults to ihe-jwt.|

Following this request, you will receive an Authorization Code in a JSON formatted response.

Using the Authorization client, you will then need to do a second Request to the following URL:

``` http
{$simulator.endpoint}/iua-simulator/rest/ch/token?
```

| parameter     | required/optional                | description                                                                                             |
|---------------|----------------------------------|---------------------------------------------------------------------------------------------------------|
| client_id     | required in Authorization header | The ID the client is registered at the IUA Authorization Server.                                        |
| client_secret | required in Authorization header | The secret to access the client                                                                         |
| redirect_uri  | required                         | The URI to redirect the apps user agent to. Must be the same as in the first request                    |
| grant_type    | required                         | Value shall be “authorization_code”.                                                                    |
| code          | required                         | The authorization code we retrieved through the previous request                                        |
| code_verifier | required                         | The original code verifier string, the non-hashed version of the code_challenge from! the first request |

This request is a Post sent to the /token endpoint of the authorization server.
Those arguments must therefore be contained in a body as an *application/x-www-form-urlencoded* content-type, and encoded in UTF-8
Finally both *client_id* and *client_secret* must be encapsulated in the base HTTP Authorization header.

This means the request should like something like this :

``` http
HTTP POST {$simulator.endpoint}/iua-simulator/rest/ch/token HTTP 1.1
Authorization: Basic vsdvZRTYHQS51SgvtsqeGFs62
Content-Type: application/x-www-form-urlencoded

grant_type=authorization_code
&code=13fER1Gqe2FshWDRwg
&redirect_uri={$simulator.endpoint}/iua-simulator/rest/ch/callback
&code_verifier=-5Q635ku8ER2pfGeuDagpsk40
```

As a response to this, you should receive a JSON containing the access_token. You can use resources such as [jwt.io](#https://jwt.io)
to verify the content of your access_token.

This access_token can now be used by the user to access resource servers or for other transactions.
___
## ITI-72 <a name="ch-iti-72"></a>

Coming soon

___
## ITI-102 <a name="ch-iti-102"></a>

Coming soon

___
## ITI-103 <a name="ch-iti-103"></a>

The ITI-103 transaction is usually sent by the Resource Server to the Authorization Server to retrieve information relative to the the endpoints and token format created by the authorization server.

``` http
HTTP GET {$simulator.endpoint}/iua-simulator/rest/ch/.well-known HTTP 1.1
```

Here is a list of properties that should be accessible through this transaction :

| Name   | Required/Optional/Conditional ? | Description |
|-|-|-|
| issuer | required | The Authorization Server's issuer identifier, which is a URL that uses the "https" scheme and has no query or fragment components.|
|authorization_endpoint | required | Authorization Server's Authorization endpoint as used for the "authorization code" flow.|
|token_endpoint | required | Authorization Server's Authorization token endpoint location.|
|jwks_uri | conditional | URL of the Authorization Server's JWK Set |
|scopes_supported | optional | The list of scopes supported by the Authorization Server. Note this transaction does not define the scopes. Authorization Servers may opt to publish a subset of the scopes usable.|
|response_types_supported | required | As the implicit grant flow is not supported in OAuth2.1, the response types should not include the value "token". Authorization Servers shall include the response type "code". Authorization Servers supporting [OpenId Connect] or other standards may include other token types, such as "id_token".|
|grant_types_supported | required | A JSON array listing the type of grants supported toward the token endpoint. The list shall include the values "client_credentials", and "authorization_code". Authorization Servers supporting refresh tokens shall include "refresh-token". Authorization Servers supporting the JWT grant shall include "urn:ietf:params:oauth:grant-type:jwt-bearer".Authorization Servers supporting the SAML grant shall include "urn:ietf:params:oauth:grant-type:saml2-bearer". Authorization Servers supporting other types of grants should provide additional values to this list representing those grant types.|
|token_endpoint_auth_methods_supported | optional | JSON array containing a list of client authentication methods supported by this token endpoint. When provided, this list shall include "client_secret_basic".|
|introspection_endpoint | conditional |URL of the Authorization Server's OAuth 2.0 introspection endpoint. This claim must be provided by Authorization Servers supporting the Token Introspection Option.|
|introspection_endpoint_auth_methods_supported | conditional | JSON array containing a list of client authentication methods supported by the introspection endpoint. This claim must be provided by Authorization Servers supporting the Token Introspection Option. As the introspect option mandates support for Resource Server bearer access tokens, this claim must include the "Bearer" value. Other authentication methods (e.g., "client_secret_basic") are allowed when supported by the Authorization Server.|
|access_token_format | optional | JSON string defining the format of the access token as provided by the Authorization Server. This allows Resource Servers to learn about methods of verification. Authorization Servers supporting the JWT Token Option shall set this claim value to "ihe-jwt". Authorization Servers supporting the SAML Token Option shall set this claim value to "ihe-saml". Authorization Servers providing tokens that are non-parsable shall set this value to "opaque". Authorization Servers supporting access tokens in other structured formats may use alternative format identifiers.|