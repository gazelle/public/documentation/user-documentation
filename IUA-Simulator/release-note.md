---
title: Release note
subtitle: IUA Simulator
toolversion: 1.0.2
releasedate: 2023-04-20
author: Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
function: Software Engineer
customer: IHE Europe
reference: KER1-RNO-IHE-IUA-SIMULATOR
---
# IUA Simulator

# 1.0.2
_Release date: 2024-09-12_

__Bogue__
* \[[IUASIMU-19](https://gazelle.ihe.net/jira/browse/IUASIMU-19)\] [ITI-103] Add a Header parameter & 2 entries in the .well-known
* \[[IUASIMU-20](https://gazelle.ihe.net/jira/browse/IUASIMU-20)\] [Release] GitLab pipeline runners not able to reach docker image

__Tâche__
* \[[IUASIMU-17](https://gazelle.ihe.net/jira/browse/IUASIMU-17)\] Internalize scope handling in Keycloak
* \[[IUASIMU-18](https://gazelle.ihe.net/jira/browse/IUASIMU-18)\] Create a specific docker image for keycloak

# 1.0.1
_Release date: 2023-06-27


__Sous-tâche__
* \[[IUASIMU-8](https://gazelle.ihe.net/jira/browse/IUASIMU-8)\] Client filtering in realm
* \[[IUASIMU-9](https://gazelle.ihe.net/jira/browse/IUASIMU-9)\] Create the project IUA Authorization Server
* \[[IUASIMU-10](https://gazelle.ihe.net/jira/browse/IUASIMU-10)\] Create the project CH:IUA Authorization Server
* \[[IUASIMU-11](https://gazelle.ihe.net/jira/browse/IUASIMU-11)\] SAML Token security in Keycloak

# 1.0.0
_Release date: 2023-04-20_

__Story__
* \[[IUASIMU-1](https://gazelle.ihe.net/jira/browse/IUASIMU-1)\] ITI71 Simulator