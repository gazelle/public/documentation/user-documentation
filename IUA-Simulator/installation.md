---
title:  Installation Manual
subtitle: IUA Simulator
author: Pierre-Marie VAN HOUTEGHEM, CLaude LUSSEAU
function: Software Engineer
date: 2023-04-05
toolversion: 1.0.2
version: 1.00
status: To be reviewed
reference: KER1-MAN-IHE-IUA-SIMULATOR_INSTALLATION-1\_01
customer: IHE-EUROPE
---
# Purpose

Here is a guide to help you install the IUA-Simulator.

# Deployment

## Minimal requirements

* Debian squeeze or ubuntu 12.04 64bits or higher with an Internet access.
* Java virtual machine : JDK 17
* Application server : Wildfly 27
* Keycloak server : Keycloak 20.0.3 and above

To install those requirements you can refer to the documentation of installation of Wildfly : [*General Requirements Wildfly 27*](https://ehealthsuisse.ihe-europe.net/gazelle-documentation/General/wildfly26.html)

## Instructions

Copy the iua-simulator.war into the "/usr/local/wildfly27/standalone/deployments" directory of your Wildfly server.

You'll have to specify Environment variable before starting the server.
Those environment variable are needed for your iua-simulator to know how to access your keycloak installation and the clients to be called through ITI-71 transactions.
Here is a list of everything needed to be setup before launching the application server. Finally, start your server.

Once the application is deployed, open a browser and go to http://yourserver/iua-simulator/rest/ch/ping in order to check of the service is available.

If the deployment is successful, you should receive a 200 Ok Response.

The sources of the projects are available on [Inria's gitlab](https://gitlab.inria.fr/gazelle/public/simulation/iua-simulator.git).

## Keycloak Configuration

In order to use IUA at full capacity, a few things have to be configurated inside keycloak by hand.

### Create a client  

Access Keycloak through the Admin interface by going at http://yourserver/iua-sso
On the top left select the realm in which you wish to create your client.
Go to the clients menu and press "Create Client"

On the first page, verify the value of client type is "OpenID Connect" and put the client id of your new client.
On the next page, turn on the Client Authentication switch, then check "Standard flow", "Direct access grants" and "service accounts roles".
On the last page, add the url of the client, and their redirect and logout redirects uris. You can then click save.

Don't forget to retrieve the client authenticator and client secret for your requests. 

### Dynamic scopes for CH scopes

By default starting from version 1.0.2, Keycloak handles the dynamic variabilty of a few scopes sent in the ITI transactions.
But they need to be added by hand for each client that uses them.

For this, go to the keycloak admin interface, then go the CH IUA realm. 
Find your client in the clients list, select it and go to the Client scopes tab.
In this tab, press "Add client scope". A popup appears where you can select every item. Then press Add then Optional. 

Now your client is ready to recieve dynamic scopes and put them in its access token.