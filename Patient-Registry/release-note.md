---
title: Release note
subtitle: PatientRegistry
toolversion: 2.2.1
releasedate: 2024-01-31
author: Franck Desaize / Alexandre POCINHO
function: Software Engineer
customer: IHE Europe
reference: KER1-RNO-IHE-PATIENT_MANAGER
---

# PIXm Connector 3.0.1
_Release date: 2024-08-07_

__Bug__
* \[[PATREG-225](https://gazelle.ihe.net/jira/browse/PATREG-225)\] [Pixm Connector] hard coded mandatory targetSystem

__Improvement__
* \[[PATREG-226](https://gazelle.ihe.net/jira/browse/PATREG-226)\] [Pixm Connector] Upgrade HAPI FHIR version to 7.2.2


# Patient Registry 2.2.1
_Release date: 2024-06-27_

__Epic__
* \[[PATREG-224](https://gazelle.ihe.net/jira/browse/PATREG-224)\] Implement Search with params


# PIXm Connector 3.0.0
_Release date: 2024-02-02_

__New Feature__
* \[[PATREG-220](https://gazelle.ihe.net/jira/browse/PATREG-220)\] Implementation of ITI-104
* \[[PATREG-221](https://gazelle.ihe.net/jira/browse/PATREG-221)\] Validation Feature w/ Matchbox
* \[[PATREG-222](https://gazelle.ihe.net/jira/browse/PATREG-222)\] Validation Feature w/ HTTP-Validator
* \[[PATREG-223](https://gazelle.ihe.net/jira/browse/PATREG-223)\] Upgrade to Java 17 and Jakarta

# Patient Registry 2.2.0
_Release date: 2024-01-31_
__Epic__
* \[[PATREG-216](https://gazelle.ihe.net/jira/browse/PATREG-216)\] [CH] Adaptation of the new PIXm specifications

# PIXm Connector 2.0.0
_Release date: 2021-09-10_

## Story
* \[[PATREG-169](https://gazelle.ihe.net/jira/browse/PATREG-169)\] [Pixm Connector] Implement Feed requests in new provider
* \[[PATREG-199](https://gazelle.ihe.net/jira/browse/PATREG-199)\] [Patient Registry] Implement GITB Webservice

## Task
* \[[PATREG-166](https://gazelle.ihe.net/jira/browse/PATREG-166)\] [Pixm CH] Implement feed query  Soap Client CH :ITI93
* \[[PATREG-188](https://gazelle.ihe.net/jira/browse/PATREG-188)\] [PIXm Connector] Implement Create method
* \[[PATREG-189](https://gazelle.ihe.net/jira/browse/PATREG-189)\] [PIXm Connector] Implement Update Method#APO
* \[[PATREG-190](https://gazelle.ihe.net/jira/browse/PATREG-190)\] [PIXm Connector] Implement Read Method
* \[[PATREG-191](https://gazelle.ihe.net/jira/browse/PATREG-191)\] [PIXm Connector] Implement Merge method #PVM
* \[[PATREG-192](https://gazelle.ihe.net/jira/browse/PATREG-192)\] [PIXm Connector] Implement Delete Method#FDE
* \[[PATREG-210](https://gazelle.ihe.net/jira/browse/PATREG-210)\] [PIXm Connector] Implement Update Method#PVM

# Patient Registry 2.1.0
_Release date: 2021-09-10_

## Story
* \[[PATREG-168](https://gazelle.ihe.net/jira/browse/PATREG-168)\] [Patient Registry] Implement Feed
* \[[PATREG-194](https://gazelle.ihe.net/jira/browse/PATREG-194)\] [Patient Registry] Implement Feed Services
* \[[PATREG-200](https://gazelle.ihe.net/jira/browse/PATREG-200)\] [Patient Registry] Implement DAO for feed

## Task
* \[[PATREG-193](https://gazelle.ihe.net/jira/browse/PATREG-193)\] [Patient Registry] Add methods to Feed API
* \[[PATREG-195](https://gazelle.ihe.net/jira/browse/PATREG-195)\] [Patient Registry] Service : Implement create method
* \[[PATREG-196](https://gazelle.ihe.net/jira/browse/PATREG-196)\] [Patient Registry] Service : Implement update method
* \[[PATREG-197](https://gazelle.ihe.net/jira/browse/PATREG-197)\] [Patient Registry] Service : Implement merge method
* \[[PATREG-198](https://gazelle.ihe.net/jira/browse/PATREG-198)\] [Patient Registry] Service : Implement delete method
* \[[PATREG-201](https://gazelle.ihe.net/jira/browse/PATREG-201)\] [Patient Registry] Implement XRef DAO
* \[[PATREG-202](https://gazelle.ihe.net/jira/browse/PATREG-202)\] [Patient Registry] Upgrade the Java Feed Client
* \[[PATREG-203](https://gazelle.ihe.net/jira/browse/PATREG-203)\] [Patient Registry] Implement an algorithme to identify XcrossReference
* \[[PATREG-205](https://gazelle.ihe.net/jira/browse/PATREG-205)\] [Patient Registry] Refactor on existing DAO


# PIXm Connector 1.0.0
_Release date: 2021-07-12_
## Epic
* \[[PATREG-154](https://gazelle.ihe.net/jira/browse/PATREG-154)\] [Pixm Hapi Srv]  Implement Pixm FhirServer
* \[[PATREG-156](https://gazelle.ihe.net/jira/browse/PATREG-156)\] [Pixm]  implement CH:Pixm

## Story
* \[[PATREG-146](https://gazelle.ihe.net/jira/browse/PATREG-146)\] [Pixm Hapi Srv] Implement Pixm Simulator (Hapi Fhir) Client and Server
* \[[PATREG-147](https://gazelle.ihe.net/jira/browse/PATREG-147)\] [Pixm Hapi srv] Implement Hapi Server
* \[[PATREG-150](https://gazelle.ihe.net/jira/browse/PATREG-150)\] [Pixm Hapi Srv] Implement Ch:pixm provider
* \[[PATREG-152](https://gazelle.ihe.net/jira/browse/PATREG-152)\] [Pixm Client] Implement a Soap Client (GWT) ITI-83

## Task
* \[[PATREG-148](https://gazelle.ihe.net/jira/browse/PATREG-148)\] [Pixm Hapi Srv]  Implement the Server Hapi Fhir
* \[[PATREG-149](https://gazelle.ihe.net/jira/browse/PATREG-149)\] [Pixm Hapi Srv] Implement Provider for Hapi Server (ITI-83)
* \[[PATREG-165](https://gazelle.ihe.net/jira/browse/PATREG-165)\] [Pixm Client] CH:Pixm implement CH:ITI83
* \[[PATREG-166](https://gazelle.ihe.net/jira/browse/PATREG-166)\] [Pixm CH] Implement feed query  Soap Client CH :ITI93

# Patient Registry 2.0.0
_Release date: 2021-07-12_

## Story
* \[[PATREG-122](https://gazelle.ihe.net/jira/browse/PATREG-122)\] [Patient Registry] Patient X-Ref Search on identifier
* \[[PATREG-123](https://gazelle.ihe.net/jira/browse/PATREG-123)\] [Patient-Registry] Patient X-Ref Search on domain

## Task
* \[[PATREG-124](https://gazelle.ihe.net/jira/browse/PATREG-124)\] [Patient Registry] Patient X-Ref Model and Search API
* \[[PATREG-125](https://gazelle.ihe.net/jira/browse/PATREG-125)\] [Patient Registry] Patient X-Ref Search GITB Web Service
* \[[PATREG-139](https://gazelle.ihe.net/jira/browse/PATREG-139)\] [Patient Registry] Implement the service on X-ref Query with Identifier
* \[[PATREG-157](https://gazelle.ihe.net/jira/browse/PATREG-157)\] [Patient Registry] Add targetDomains Server and Client to Webservice For Search on X-ref
* \[[PATREG-158](https://gazelle.ihe.net/jira/browse/PATREG-158)\] [Patient Registry] Add targetDomains to Service for X-ref search query
* \[[PATREG-159](https://gazelle.ihe.net/jira/browse/PATREG-159)\] [PatientRegistry] Implement DAO For targetDomains
* \[[PATREG-162](https://gazelle.ihe.net/jira/browse/PATREG-162)\] [Patient Registry] Implement Java Webservice Client for X-ref search on Identifier



# PDQm Connector 1.0.0
_Release date: 2020-04-24_

__Story__

* \[[PATREG-37](https://gazelle.ihe.net/jira/browse/PATREG-37)\] [PDQm] PDS query on uuid
* \[[PATREG-41](https://gazelle.ihe.net/jira/browse/PATREG-41)\] [PDQm] PDS retrieve on uuid

# Patient Registry 1.0.0
_Release date: 2020-04-22_

__Story__

* \[[PATREG-31](https://gazelle.ihe.net/jira/browse/PATREG-31)\] Patient Registry patient feed
* \[[PATREG-33](https://gazelle.ihe.net/jira/browse/PATREG-33)\] Patient Registry query on identifier
* \[[PATREG-38](https://gazelle.ihe.net/jira/browse/PATREG-38)\] Patient Registry query on uuid
* \[[PATREG-45](https://gazelle.ihe.net/jira/browse/PATREG-45)\] Improvement on Patient Model
