---
title: User Manual
subtitle: Patient Registry
author: Alexandre POCINHO
releasedate: 2024-02-02
toolversion: 3.0.0
function: Engineer
version: 0.1
status: validated
---

# Patient Registry Tool suite

Outline:

- [PIXm Connector Component](#pixm-connector-component)

  - [Overview](#overview)
  - [Validation Process](#validation-process)
  - [ITI-83 Request cross reference identifiers](#request-a-patient-cross-reference-on-a-specific-target-identifier-iti-83)
  - [ITI-104 Request on Patient Resources](#requests-on-patient-resources-iti-104)
    - [Create/Update](#createupdate)
    - [Merge duplicated patient](#merge-for-resolving-duplicated-patient)
    - [Delete patient](#delete-one-or-more-patients)
  - [Errors returned](#errors-returned)

## PIXm Connector Component

### Overview

Here is a quick overview of the available functionality from PIXm connector

| Operation                      | HTTP Methods | URL to call                                                                                                                    | Entry parameter                                               | Returned value                           |
|--------------------------------|--------------|--------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------|------------------------------------------|
| Create/Update Patient          | PUT          | ```https://example.com/pixm-connector/fhir/Patient?identifier=patient.system%7Cpatient.id```                                          | ITI-104 Patient identifier                                    | ITI-104 FHIR Patient                     |
| Delete Patient                 | DELETE       | ```https://example.com/pixm-connector/fhir/Patient/?identifier=patient.system%7Cpatient.id```                                         | ITI-104 Patient identifier                                    | /                                        |
| Merge Patient                  | PUT          | ```https://example.com/pixm-connector/fhir/Patient/?identifier=patient.system%7Cpatient.id```                                         | ITI-104 Patient identifier w/ patient.link to Patient to keep | ITI-104 FHIR Patient                     |
| Check Cross Referenced Patient | GET          | ```https://example.com/pixm-connector/fhir/Patient/$ihe-pix?sourceIdentifier=patient.system%7Cpatient.id&targetSystem=targetSystem``` | A Patient sourceIdentifier and a TargetDomain                 | ITI-83 FHIR Parameters with X-ref values |

Capability statement of the application can be found with : <https://example.com/pixm-connector/fhir/metadata>

As described in [HAPI FHIR resources](https://hapifhir.io/hapi-fhir/docs/server_plain/rest_operations_operations.html), some strings are automatically escaped when the FHIR server parses URLs:

|Given String|Parsed as|
| :---: | :---: |
|\||%7C|
|=>=|=%3E%3D|
|=<=|=%3C%3D|
|=>|=%3E|
|=<|=%3C|

### Validation process

Each operation implies a validation of requests for both `ITI-104` and `ITI-83` transactions.  
Validation is done by calling:

- [HTTP Validator](https://gitlab.inria.fr/gazelle/applications/test-execution/validator/http-validator) for URL and Headers.
- [Matchbox](https://www.matchbox.health/) for Body with FHIR Resource.

Both validators allow to perform validation and have high customization if specifications changed for both transactions without refactoring pixm-connector application.  
An error during validation process will result with an OperationOutcome with error `400 Bad Request` with issues describing where it failed.

### Request a Patient Cross Reference on a specific Target Identifier (ITI-83)

- [IHE Specifications](https://profiles.ihe.net/ITI/PIXm/ITI-83.html)

Request a cross-referenced Patient is possible thanks to the `$ihe-pixm`.

Parameters allowed are :

- The Patient Identifier and the Target System attributed to this identifier as `sourceIdentfier`
- The Target System you want the cross-reference from as `targetSystem`.
- The format returned as `_format`: **xml** or **json**.

The URL to call is:

```http
  GET https://example.com/pixm-connector/fhir/Patient/$ihe-pix
```

For example :
Given the Patient with the `id=69420` with the `system=urn:oid:1.3.6.1.4.1.21367.13.20.3000` as `sourceIdentifer=system|id`  
And you want the cross-referenced patient in the `targetSystem=1.3.6.1.4.1.21367.13.20.1000`  
And you want the returned response as a `json`.

The corresponding url will be :

```http
    https://example.com/pixm-connector/fhir/Patient/$ihe-pix?sourceIdentifier=urn:oid:1.3.6.1.4.1.21367.13.20.3000|69420&targetSystem=urn:oid:1.3.6.1.4.1.21367.13.20.1000&_format=json
```

---

### Requests on Patient resources (ITI-104)

- [IHE Specifications](https://profiles.ihe.net/ITI/PIXm/ITI-104.html)

---

#### Create/Update

Link: <https://profiles.ihe.net/ITI/PIXm/ITI-104.html#2310441-add-or-revise-patient>

PIXm connector accepts the creation of a Patient in the Patient Manager.
Although a Patient could be created without any information in the HL7 model, PIXm connector will only allow a Patient
to be created with minimum and/or mandatory information to permits cross-reference thanks to validation with Matchbox.

The Resource could not be parsed or failed basic FHIR validation rules.
In the case of an error `400 Bad Request` or `422 Unprocessable Entity` being returned,
please check the following guidelines to verify your query.

Create/Update request is done through a FHIR conditional update mechanism ([cond-update](http://hl7.org/fhir/http.html#cond-update)) where the patient identifier has to be given as following.

```http
    PUT https://example.com/pixm-connector/fhir/Patient/identifier=urn:oid:1.3.6.1.4.1.21367.13.20.1000|IHERED-m94
```

With body :

```json

{
  "resourceType" : "Patient",
  "id" : "Patient-MaidenAlice-Red",
  "meta" : {
    "profile" : [
      "https://profiles.ihe.net/ITI/PIXm/StructureDefinition/IHE.PIXm.Patient"
    ]
  },
  "text" : {
    "status" : "generated",
    "div" : "<div xmlns=\"http://www.w3.org/1999/xhtml\"><p style=\"border: 1px #661aff solid; background-color: #e6e6ff; padding: 10px;\"><b>ALICE MOHR </b> female, DoB: 1958-01-30 ( id:\u00a0IHERED-m94)</p><hr/><table class=\"grid\"><tr><td style=\"background-color: #f3f5da\" title=\"Record is active\">Active:</td><td colspan=\"3\">true</td></tr></table></div>"
  },
  "identifier" : [
    {
      "system" : "urn:oid:1.3.6.1.4.1.21367.13.20.1000",
      "value" : "IHERED-m94"
    }
  ],
  "active" : true,
  "name" : [
    {
      "family" : "MOHR",
      "given" : [
        "ALICE"
      ]
    }
  ],
  "gender" : "female",
  "birthDate" : "1958-01-30"
}
```

If the patient exists then it is updated otherwise it is created.
The response will return the updated/created patient.

---

#### Merge for resolving duplicated patient

Link : <https://profiles.ihe.net/ITI/PIXm/ITI-104.html#2310442-resolve-duplicate-patient>

The merge method allows the user to merge two patients together if two registered patients represent the same people.  
This action is **irreversible** as it deactivates a resource making it only readable and immutable.

The request is a PUT method with the Patient to deactivate with a `activate: false` attribute and a `link` field with identifier for the kept patient resource.

```http
PUT http://example.org/fhir/Patient?identifier=urn:oid:1.3.6.1.4.1.21367.13.20.1000|IHERED-m94 HTTP/1.1
Accept: application/fhir+json
Content-Type: application/fhir+json
```

```json
{
  "resourceType": "Patient",
  "identifier": [
    {
      "system": "urn:oid:1.3.6.1.4.1.21367.13.20.1000",
      "value": "IHERED-m94"
    }
  ],
  "active": false,
  "name": [
    {
      "family": "MOHR",
      "given": [
        "MAIDEN"
      ]
    }
  ],
  "gender": "female",
  "birthDate": "1958-01-30",
  "link": [
    {
      "other": {
        "identifier": {
          "system": "urn:oid:1.3.6.1.4.1.21367.13.20.1000",
          "value": "IHERED-994"
        }
      },
      "type": "replaced-by"
    }
  ]
}
```

#### Delete one or more patient(s)

Link: <https://profiles.ihe.net/ITI/PIXm/ITI-104.html#2310443-remove-patient>

The delete operation allows suppression of the patient with its identifier thanks to a conditional deletion.
This application allows multiple deletion if the identifier returned more than one Patient.

````http
DELETE http://example.org/fhir/Patient?identifier=urn:oid:1.3.6.1.4.1.21367.13.20.1000|IHERED-994 HTTP/1.1
Accept: application/fhir+json
````

if the delete is successful the application returns a `200 OK` response otherwise it would be a `204 No Content`.

### Errors returned

---

Malformed requests can cause different types of error, for now `422 Unprocessable Entity` is mostly returned.  
Future features will allow a better granularity for code returned.
