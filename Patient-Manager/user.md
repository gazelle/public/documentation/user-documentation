---
title:  User Manual
subtitle: Patient Manager
author: Anne-Gaëlle BERGE
releasedate: 2024-07-30
toolversion: 10.X.X
function: Engineer
version: 2.05
status: Approved document
reference: KER1-MAN-IHE_EUROPE-PATIENT_MANAGER_USER-2_05
customer: IHE-EUROPE
---

# Introduction

The Patient Manager tool is developed in conformance with the IHE Technical Framework. This tool is also conformant with the French national extension for the PAM profile. This simulator is expected to act as an initiator or as a responder depending on the emulated actors.

As an initiator, this simulator is aimed to send messages to a responder. Consequently, if your system (named SUT or System Under Test) is ready to listen to an HL7 initiator and reachable from the Internet, you will be able to receive messages from the simulator.

# What is this simulator able to do?

This simulator has been developed with the purpose of helping developers of IHE systems to test their systems with another IHE compliant system for pre-Connectathon testing or during off-connectathon periods. We try to manage most of the cases; that means that, step by step, we planned to offer you all the events defined in the Technical Framework. We also plan to implement national extensions if requested by the different organizations.

For more details regarding an actor in particular, refer to the next sections.

## Simulated actors

The Patient Manager tool emulates the actors involved in the management of the patient demographics and visits. It can act as a test partner that supports the following actors:

| **Integration profile** | **Actor**                                              | **Option**                                | **Affinity Domain** | **development status** |
|-------------------------|--------------------------------------------------------|-------------------------------------------|---------------------|------------------------|
| PAM                     | Patient Demographic Supplier                           | Merge                                     | IHE and IHE France  | available for testing  |
| PAM                     | "                                                      | Link/Unlink                               | IHE                 | available for testing  |
| PAM                     | Patient Demographic Consumer                           | Merge                                     | IHE and IHE France  | available for testing  |
| PAM                     | "                                                      | Link/Unlink                               | IHE                 | available for testing  |
| PAM                     | Patient Encounter Supplier                             | Basic subset                              | IHE and IHE France  | available for testing  |
| PAM                     | "                                                      | Inpatient/Outpatient Encounter Management | IHE and IHE France  | available for testing  |
| PAM                     | "                                                      | Pending Event Management                  | IHE                 | pending                |
| PAM                     | "                                                      | Advanced Encounter Management             | IHE and IHE France  | available for testing  |
| PAM                     | "                                                      | Temporary Patient Transfers Tracking      | IHE                 | pending                |
| PAM                     | "                                                      | Historic Movement Management              | IHE and IHE France  | available for testing  |
| PAM                     | Patient Encounter Consumer                             | basic subset                              | IHE and IHE France  | available for testing  |
| PAM                     | "                                                      | Inpatient/Outpatient Encounter Management | IHE and IHE France  | available for testing  |
| PAM                     | "                                                      | Pending Event Management                  | IHE                 | pending                |
| PAM                     | "                                                      | Advanced Encounter Management             | IHE and IHE France  | available for testing  |
| PAM                     | "                                                      | Temporary Patient Transfers Tracking      | IHE                 | pending                |
| PAM                     | "                                                      | Historic Movement Management              | IHE and IHE France  | available for testing  |
| PDQ                     | Patient Demographics Consumer                          | Patient Demographics and Visit Query      | IHE                 | available for testing  |
| PDQ                     | "                                                      | Pediatric demographics                    | IHE                 | available for testing  |
| PDQ                     | Patient Demographics Supplier                          | Patient Demographics and Visit Query      | IHE                 | available for testing  |
| PDQ                     | "                                                      | Pediatric demographics                    | IHE                 | available for testing  |
| PDQv3                   | Patient Demographics Consumer                          | Pediatric Demographics                    | IHE                 | available for testing  |
| PDQv3                   | "                                                      | Continuation Pointer                      | IHE                 | available for testing  |
| PDQv3                   | Patient Demographics Supplier                          | Pediatric Demographics                    | IHE                 | available for testing  |
| PDQv3                   | "                                                      | Continuation Pointer                      | IHE                 | available for testing  |
| PDQm                    | Patient Demographics Consumer for Mobile               |                                           | IHE                 | available for testing  |
| PDQm                    | Patient Demographics Supplier for Mobile               |                                           |                     | available for testing  |
| PIX                     | Patient Identity Source                                |                                           | IHE                 | available for testing  |
| PIX                     | Patient Identifier Cross-Reference Consumer            | PIX Update Notification                   | IHE                 | available for testing  |
| PIX                     | Patient Identifier Cross-Reference Manager             |                                           | IHE                 | available for testing  |
| PIXV3                   | Patient Identity Source                                |                                           | IHE                 | available for testing  |
| PIXV3                   | Patient Identifier Cross-Reference Consumer            | PIX Update Notification                   | IHE                 | available for testing  |
| PIXV3                   | Patient identifier Cross-Reference Manager             |                                           | IHE                 | available for testing  |
| PIXm                    | Patient identifier Cross-Reference Consumer for Mobile |                                           | IHE                 | available for testing  |
| PIXm                    | Patient identifier Cross-Reference Manager for Mobile  |                                           | IHE                 | available for testing  |
| XCPD                    | Initiating Gateway                      | Deferred response                      | IHE                 | available for testing  |
| XCPD                    | Responding Gateway                      | Deferred response                      | IHE                 | available for testing  |
| XDS.b*                   | Patient Identity Source                                 | Patient Identity Feed (HL7V2)           | IHE                   | available for testing |
| XDS.b*                   | Patient Identity Source                                 | Patient Identity Feed (HL7V3)           | IHE                   | available for testing |

\* There is no specific usage defined for the XDS.b Document Registry. If you want to send feeds to your system, register it as a PIX Manager or PIXV3 Manager for the ITI-8/ITI-44 transaction.

## Note

In the following sections:

* PIX* refers to PIX, PIXV3 and PIXm profiles.
* PDQ* refers to PDQ, PDQV3 and PIXV3 profiles.

For most of the features, the instructions to use the tool are similar for the three versions of the profiles

# Sharing of demographics for connectathon purposes

This feature used to be supported by the Gazelle Test Management tool. In order to increase the number of ways to retrieve the demographics defined for a specific event, we chose to move this feature into Patient Manager which emulates various profiles related to patient demographics.

## Access the list of patients defined for the testing event

Under the *Connectathon > Patient demographics* menu you will access the list of patients specifically created for your testing event.

## Load patients into systems under test

There are four ways to load the patients into your system under test:

* If the system under test supports the __Patient Demographic Consumer__ actor for the __PDQ__, __PDQV3__ or __PDQm__ profile, simply query the Patient Demographic Supplier of Patient Manager. You will obtain its configuration from PDQ* > Patient Demographic Supplier > [HL7V2\|HL7V3\|FHIR] Configuration respectively.
* If the system under test supports the __Patient Identity Consumer__ actor for the __PIX__, __PIXV3__ or __PIXm__ profile, simply query the Patient Identity Cross-Reference Manager of the tool. You will find its configuration from PIX* > Patient Identity Cross-Reference Manager > [HL7V2\|HL7V3\|FHIR] Configuration respectively.
* If the system under test supports the __Initiating Gateway__ actor for the __XCPD__ profile, query the XCPD Initiating Gateway of the tool. Its configuration is available under XCPD > Responding Gateway.
* If the system under test rathers consumer ADT or PRPA_IN201301UV02 messages, Patient Manager will be able to send feeds to your system. Read the [next section](#patient-sharing-steps) for details.

## Sending feeds to systems under test {#patient-sharing-steps}

From the *Connectathon > Patient demographics* page, you will be able to feed a selection of systems under test with a set of selected patients. The sharing icon on top of the table (Actions column) allows you to share all the patients which are currently displayed in the table. If you want to share only one patient, click on the sharing icon located in the row of that patient.

Click on the sharing icon redirects you to the __Demographics Sharing__ page.

The first panel on that page lists the patients that you have selected and which will be sent to the selected systems under test. This panel also contains a link that you can share with partners or keep for later use.

Then, in the "Systems to be fed" panel, select a System Under Test in the list and click on Add. This system will appear in the table located below. If preferences are already defined for this system, the list of assigning authorities for which the system will receive patients' identifiers is displayed. You can set it or edit it by clicking on the pencil icon. You can also select which message to be sent. If only one message type is available based on the system's configuration, it will be automatically selected.
You can also configure those preferences in advanced, follow the steps described [here](#sut-assigning-authorities).

The green play icon in the Actions column allows you to send messages to an individual system.

Once you have selected all the systems you want, click on the "Send messages" button. The list of messages exchanged with the systems under test will be displayed at the bottom of the page. If failures are encountered by the tool, they will appear at the top of the "Sharing report" panel.
For each message, you can see the content of the response by clicking on the magnifying glass icon.

__Note__ that a system configuration (HL7V2 or HL7V3) appears in the list of available systems under test if it meets the following criteria:

* Configuration is marked as available
* List of usages contains at list one of the following transactions: ITI-8, ITI-30, ITI-31, ITI-44, RAD-1.

## Administrator defines demographics

Only users with the administrator permissions are allowed to create patients used by the Demographics Sharing feature.
The administrator users have two ways to define demographics to be used only in a test session. Note that those demographics will never be altered.

* Under *Connectathon > Import patient resources*, you can upload XML or JSON files representing either a FHIR Patient or a FHIR Bundle of Patient resources. ZIP archives containing a mix of XML/JSON files are also allowed.
* Under *Connectathon > Create patients*, you will access the form to generate patients by calling DDS tool. When you do that, make sure that "CONNECTATHON" actor is selected.

If you do not want other users to generate patient's identifiers using specific assigning authorities, you can mark them as reserved for the connectathon purpose. See details on how to [here](#manage-authorities).

# PAM Patient Demographic Consumer

The Patient Manager tool implements the Patient Demographic Consumer actor defined by the IHE technical framework for the PAM profile. This simulated actor implements both the Merge and Link/Unlink options. Consequently, the application is able to receive and integrate the following events for the ITI-30 transaction:

-   Create new Patient (ADT^A28^ADT\_A05)

-   Update patient Information (ADT^A31^ADT\_A05)

-   Change Patient Identifier List (ADT^A47^ADT\_A47)

-   Merge two patients (ADT^A40^ADT\_39)

-   Link Patient Information (ADT^A24^ADT\_A24)

-   Unlink Patient Information (ADT^A37^ADT\_A37)

Three sections (pages) of the Patient Manager Tool application are dedicated to the use of the Patient Demographic Consumer Actor. You can reach them going to Patient Administration Management --&gt; Patient Demographic Consumer. The 3 pages are available through the related icons.

![Patient Demographic Consumer page](./media/image3.png)

The first icon is to access to the configuration and messages

The second icon is to access the received patients page

The third one gives you an access to the patient links page

## Configuration and messages

When the simulator acts as a PDC, it is only a responder; that means that it is listening on a specific port and sends acknowledgements for the messages it receives. As a consequence, you are not expected to give to the simulator the configuration of the PDS part of your SUT. At the contrary, your SUT needs the configuration of the simulator in order to send it messages. When you go to the page "Configuration and Messages" you can see that various configurations are offered. Actually, in order to be able to properly understand the messages it receives, the simulator needs to open a socket using the appropriate encoding character set. The IP address and the receiving application and facility do not differ from a configuration to another, only the port number should change. Note that, if the Character set given in the message (MSH-18) is not the one expected the message is application rejecting. In the same way, if the receiving application or receiving facility does not match the expected one, the message will be rejected with an AR acknowledgment.

In this same page, you can see the list of messages received by the PDC actor. The more recent ones are at the top of the list.

## Received Patients

When the simulator receives a message, it tries to integrate it, if it is not able to do it, it sends back an error message. It means that each time it can, it performs the appropriate action on the patient. The resolution of patients is done on their identifiers.

-   ***Create new Patient***

A new patient is created if none of the given identifiers is already used by another active patient. If one of the identifiers is in use, the PDC application-rejects the message with error code 205 (duplicate key identifier). The creator of the patient is set to sendingFacility\_sendingApplication.

-   ***Update Patient information***

If one of the given identifiers matches an existing patient, the latter is updated with the new values. If the patient does not exist yet, a new patient is created.

-   ***Change Patient identifier***

If more than one identifier is mentioned in PID-3 or in MRG-1 fields, the message is application-rejected. In the contrary, we get different cases:

-   If both correct and incorrect identifiers are used for an active patient; an error message is sent because a merge action should have been performed instead of the change id.

-   If the incorrect identifier identifies an active patient but the correct identifier is unknown, the list of identifiers of the retrieved patient is updated.

-   If both correct and incorrect identifier are unknown, a new patient is created using values given in PID segment.

<!-- -->

-   ***Merge two patients***

If more than one PATIENT group is contained in the message, the latter is application-rejected. Otherwise, we get four cases:

-   Both correct and incorrect patient exist, patients are merged and only the correct one remains.

-   The incorrect patient is known but not the correct one, a change patient id action is performed.

-   Both correct and incorrect patient do not exist, a patient is created using the data contained in PID segment.

-   The incorrect patient is unknown but the correct has been retrieved, we do nothing.


-   ***Link/Unlink patients***

For link case: if both identifiers exist, link them. If one or both of them are missing, create them and link them.

For unlink case: if both identifiers exist and are linked, unlink them. Otherwise nothing is done.

## Received patient links

When displaying the full information about a patient, you can ask the application to show you the possible links between the patient and the other ones. But in some cases, the PDC may have received a message to link two or more identifiers, the ones do not identify patients. In order to check that the messages you have sent have been taken into account, you can go to this page (Received patient links) and you will see the list of links and their creation date. When two identifiers are unlinked, the link between them is deleted so you are not able to view it anymore.

# ITI-30 Initiator {#iti30-init}

The Patient Manager tool implements the Patient Demographic Supplier actor of the PAM integration profile as well as the Patient identity Source actor of the PIX integration profile. Both actors are involved in the ITI-30 transaction. This page of the documentation explains how to send demographics information to your system under test which acts either as a PAM Patient Demographics Consumer or either as a PIX Patient Identity Source.

This simulated actor implements both the Merge and Link/Unlink options. Consequently, the application is able to send the following events for the ITI-30 transaction:

- Create new patient (ADT^A28^ADT\_A05)

- Update patient information (ADT^A31^ADT\_A05)

- Change Patient Identifier List (ADT^A47^ADT\_A30)

- Merge two patients (ADT^A40^ADT\_A39)

- Link Patient Information (ADT^A24^ADT\_A24)

- Unlink Patient Information (ADT^A37^ADT\_A37)

- Remove INS Identifier (Only if French extension is activated)

## Two starting points

![PDC menu](./media/image4.png)

We try, at the most as we can, to keep a consistency between the different sections. As a consequence, the way you select the system under test and the patient to send is almost the same for each event, as well as the display of the test result, although some specificity can appear.

1.  PAM Patient Demographics Supplier

To access the page dedicated to the PAM Patient Demographic Supplier actor, go to Patient Administration Management Patient Demographic Supplier; then you will be able to select which event you want to send to your system under test.

1.  PIX Patient Identity Source

To access the page dedicated to the PAM Patient Demographic Supplier actor, go to PIX* Patient Identity Source \[ITI-30\] Patient Identity Management; then you will be able to select which event you want to send to your system under test.

## Select the system under test

If your system under test has been properly created in the "System Configuration" section; that means that you have associated it to the right actor (Patient Demographic Consumer or Patient identity Source), you should be able to see it in the drop-down menu. As soon as you have selected it, check that it is the right configuration that is displayed in the panel beside the sequence diagram.

## Select the patient

If you are not logged in, the application offers you two ways to choose a patient:

1.  "All patients" will display all the patients created in the context of the PAM/PDS or PIX/PAT\_IDENTITY\_SRC and still active\*. You can apply a filter if you want to restrain the search to a certain period.

2.  "Generate patient" will display a panel which will enable you to create a patient using our DDS application. You are expected to select at least a country from the drop-down list before hitting the "Generate patient" button.

Note that in some instances of the tool which are not linked to the Demographics Data Server, it is not possible to generate new patients. If you have the administration rights, consult the administration section to read how to import patients from a CSV file.

If you are logged in, your patients are automatically displayed by default. However, by clicking on the red cross next to the “Owner” filter criteria, you can show all patients. By picking this choice, only the active\* patient you have created (you were logged in when you create them) are displayed. You can apply a filter if you need to restrain the search to a certain period.

\* A patient is active until he/she is merged with or update to another one.

## Patient history

All the actions performed on a patient are logged into the application. Consequently, for each patient we can say who has created it, to which systems it has been sent (enclosing in which message), whose patient is updated from he/she and so one. To retrieve all the patients created by the simulator or received from a system under test, go to the "All patients" menu and filter on the simulated actor.

## Configure the message to be sent to your system

1.  Create a new patient

In this section of the simulator, you can send a message to your system under test to create a patient. This message can contain a patient you have just created using the DDS generation option or an existing patient.

In the first case, several options are offered for the patient generation; you can modify the generated data of the patient, just hit the "Modify patient data" to edit them. If you need specific patient identifiers, go to the "Patient's Identifiers" tab and edit, add or remove identifiers.

In the second case, you only have to select your system configuration and hit the create button on the line of the patient you want. The message is automatically sent and the result of the test is displayed at the bottom of the page.

To send another message, only hit the "select another patient" button.

1.  Update patient information

In this section of the simulator, you can send a message to your system under test to update a patient. You can either create a new patient using the "generate new patient" option or use an existing one by hitting the update button on the line of the patient you want. In the second case, you will be able to change the information of the patient before sending the update message. Internally, the application creates a new patient with the new values and deactivates the selected patient.

1.  Change patient identifier list

This part of the tool enables you to send a message to your system under test to change the value of one of the identifiers of a patient. You can choose to create a new patient and to change his/her identifiers before sending it or to select an existing one. When you choose the second option, a new patient with the new identifier list is created and the "old" one is deactivated. Note that, according to the IHE technical framework, you can change only one identifier at a time. That means that as soon as you validate the new identifier, you cannot change it again or change another one. If you did a mistake, hit the "select another patient" button.

1.  Merge patients

In this part of the simulator you can send a message to your system under test to notify it about the merging of two patients. In order to create this message, you need to select two patients. The one called "incorrect patient" is used to populate the MRG segment (this patient will be deactivated in the simulator); the other one, called "correct patient" is the patient who remains and is used to populate the PID segment of the message.

Patients can be dragged from the table (using the green frame containing the id) and dropped to the appropriate panel or you can choose to generate one or both patient(s) using DDS.

The message can be sent (button is available) only if two patients are selected.

1.  Link/Unlink patients

The "link/unlink patients" part of the simulator is used to send unlink/unlink messages to your system under test. As for the "merge patients" section, you can drag and drop patients and/or generate them using DDS. Once you have selected two patients, choose if you want to link them or unlink them. The selected option is highlighted in orange and the sequence diagram at the top of the page is updated according this option, as well as the label of the button you have to hit to send the message.

1.  Remove INS Identifier (Only if French extension is activated)

In this part of the simulator you can send a message to your system under test to notify it to remove all INS identifier for the selected patient.

The message can be sent by clicking on the red bin icon next to the patient identifiers.

# PAM Patient Encounter Supplier {#patient-encounter-supplier}

![Patient Encounter Supplier page](./media/image5.png)

The Patient Manager tool implements the Patient Encounter Supplier actor of the PAM profile defined by the IHE technical framework. Currently, the following options are available:

-   Basic subset of messages
-   Inpatient/outpatient encounter management
-   Advanced encounter
-   Historic movement

    Moreover, the French extension is supported, so the specific French events are included in Patient Manager and the user can choose to send messages compliant with this national extension.
    That means that the events listed below are available:

-   Admit patient (ADT^A01^ADT\_A01)
-   Register patient (ADT^A04^ADT\_A01)
-   Cancel admit/register patient (ADT^A11^ADT\_A09)
-   Discharge patient (ADT^A03^ADT\_A03)
-   Cancel discharge (ADT^A13^ADT\_A01)
-   Merge patient identifier list (ADT^A40^ADT\_39)
-   Update patient information (ADT^A08^ADT\_A01)
-   Pre-admit patient (ADT^A05^ADT\_A05)
-   Cancel pre-admit (ADT^A38^ADT\_A38)
-   Change Patient class to inpatient (ADT^A06^ADT\_A06)
-   Change Patient class to outpatient (ADT^A07^ADT\_A06)
-   Transfer patient (ADT^A02^ADT\_A02)
-   Cancel transfert patient (ADT^A12^ADT\_A12)
-   Cancel register patient (ADT^A11^ADT\_A11)
-   Change attending doctor (ADT^A54^ADT\_A54)
-   Cancel Change of attending doctor (ADT^A55^ADT\_A55)
-   Change of conditions of the medico-administrative management (ADT^Z88^ADT\_Z88)
-   Cancel change of conditions of the medico-administrative management (ADT^Z89^ADT\_Z89)
-   Change of medical ward (ADT^Z80^ADT\_Z80)
-   Cancel change of medical ward (ADT^Z81^ADT\_Z81)
-   Change of nursing war (ADT^Z84^ADT\_Z84)
-   Cancel change of nursing war (ADT^Z85^ADT\_Z85)
-   Leave of absence (ADT^A21^ADT\_A21)
-   Cancel leave of absence (ADT^A52^ADT\_A52)
-   Return from leave of absence (ADT^A22^ADT\_A22)
-   Cancel return from leave of absence (ADT^A53^ADT\_A53)
-   Move account information (ADT^A44^ADT\_A44)

A section (page) of the application is dedicated to this actor, to access it go to Patient Administration Management --&gt; Patient Encounter Supplier.

![PAM menu](./media/image6.png)

We have chosen to gather all the events in a same page. In that way, the selection of your SUT, the creation of a new event, its update or its cancellation works in the same way. As soon as a new event is implemented by the simulator it will appear in the drop-down box "Category of event". 

## Select the system under test

If your system under test has been properly created in the "System Configuration" section, which means that you have associated it to the right actor (Patient Encounter Consumer), you should be able to see it and select it in the drop-down menu. As soon as you have selected it, check that it is the right configuration that is displayed in the panel beside the sequence diagram.

*Note that if you are logged in, you are set has the "creator" of the patients you create and in that way, by default, the owner filter will be set with your username to see your patients.*

## Sending a message for notifying a new event

In order to set the appropriate action you want to perform, you have to select first the "Category of event" and then the "Action to perform". In the case of the notification of a new event, the action to perform is "INSERT"; make sure the trigger event mentioned between brackets is the one you want.

The following steps differ depending on the category of event you have chosen.

## Admit/Register a patient

The next step is the selection of the patient:

-   By picking up a patient from the displayed list (this list gathered the patients sent by the PES part of the simulator and ones received by the PDC part of the simulator)
-   Create a patient with random demographics (select the "generate a patient" option.

As described in the Technical Framework, a patient can have only one Inpatient encounter at a time; as a consequence, you will not be able to create a second Inpatient encounter for a given patient until the first encounter is closed (sending of discharge event).

Once the patient is selected, you are asked to fill out the form about the encounter. If you want the application to fill out this form for you, click the "Fill the encounter with random data" button. As soon as you are happy with the values, click on the "Send" button at the bottom of the page to send the message to your SUT.

## Update patient information

According the Technical Framework, this event is only allowed for a patient with an open encounter. 

1.  Select the patient for which you want to update the patient demographics
2.  Update the fields you want
3.  Click on the "Send" button at the bottom of the page.

## Merge patient identifier list

This event requires two patients, the one with incorrect identifiers and a second one which will be the "good" one, this second one will remain. 

1.  To select the incorrect patient, find it in the table and click on the red cross icon, the patient will be displayed into the "Patient with incorrect identifiers" box.
2.  To select the correct pateint, find it in the table and click on the green check icon, the patient will be displayed into the "Target patient" box.
3.  Click on the "Send" button at the bottom of the page.

## Other events

Depending of the event you want to insert, you will be asked to fill out some fields, the ones differ from an event to the other but the main principle remains the same.

1.  Select the patient for which you want to insert a new event. If the new event requires the patient to have an open encounter, you will not be able to select a patient which has no opened encounters.
2.  The list of encounters relative to the patient is displayed; select the encounter for which you want to insert a new event. Note that if you are logged, you will be set as the creator of the encounter and by selecting the "My encounters" option, only "your" encounters will be displayed.
3.  Fill out the form (if asked)
4.  Click on the "Send" button at the bottom of the page.

## Sending a message for notifying the cancellation of an event

According the Technical Framework, not all but some of the events can be cancelled. Only the current (last entered) event can be cancelled. To send a notification to cancel an event, follow the steps given below.

1.  Select the category of event to cancel in the drop-down list.
2.  Select "CANCEL" in the drop-down list entitled "action to perform". Check the trigger event given between brackets is the one you want to send.
3.  Select the movement to cancel (only the current one can be cancelled according to the Technical Framework).
4.  A pop-up raises, check the information given and click on the "Yes" button.

# PAM Patient Encounter Consumer

The Patient Manager tool implements the Patient Encounter Consumer actor of the PAM profile defined by the IHE technical framework. Currently, the following options are available:

-   Basic subset of messages
-   Inpatient/outpatient encounter management
-   Advanced encounter
-   Historic movement

Moreover, the French extension is supported, so the specific French events are included in Patient Manager and the user can choose to send messages compliant with this national extension.
The supported trigger events are:

-   Admit patient (ADT^A01^ADT\_A01)
-   Register patient (ADT^A04^ADT\_A01)
-   Cancel admit/register patient (ADT^A11^ADT\_A09)
-   Discharge patient (ADT^A03^ADT\_A03)
-   Cancel discharge (ADT^A13^ADT\_A01)
-   Merge patient identifier list (ADT^A40^ADT\_39)
-   Update patient information (ADT^A08^ADT\_A01)
-   Pre-admit patient (ADT^A05^ADT\_A05)
-   Cancel pre-admit (ADT^A38^ADT\_A38)
-   Change Patient class to inpatient (ADT^A06^ADT\_A06)
-   Change Patient class to outpatient (ADT^A07^ADT\_A06)
-   Transfer patient (ADT^A02^ADT\_A02)
-   Cancel transfert patient (ADT^A12^ADT\_A12)
-   Cancel register patient (ADT^A11^ADT\_A11)
-   Change attending doctor (ADT^A54^ADT\_A54)
-   Cancel Change of attending doctor (ADT^A55^ADT\_A55)
-   Change of conditions of the medico-administrative management (ADT^Z88^ADT\_Z88)
-   Cancel change of conditions of the medico-administrative management (ADT^Z89^ADT\_Z89)
-   Change of medical ward (ADT^Z80^ADT\_Z80)
-   Cancel change of medical ward (ADT^Z81^ADT\_Z81)
-   Change of nursing war (ADT^Z84^ADT\_Z84)
-   Cancel change of nursing war (ADT^Z85^ADT\_Z85)
-   Leave of absence (ADT^A21^ADT\_A21)
-   Cancel leave of absence (ADT^A52^ADT\_A52)
-   Return from leave of absence (ADT^A22^ADT\_A22)
-   Cancel return from leave of absence (ADT^A53^ADT\_A53)
-   Move account information (ADT^A44^ADT\_A44)

A page of the Patient Manager application is dedicated to the use of the Patient Encounter Consumer actor. You can reach it by going to Patient Administration Management --&gt; Patient Encounter Consumer.

![Patient Encounter Consumer page](./media/image7.png)

## Configuration and messages

When the simulator acts as a PEC, it is only a responder; that means that it is listening on a specific port and sends acknowledgements for the messages it receives. As a consequence, you are not expected to give to the simulator the configuration of the PDC part of your SUT. At the contrary, your SUT needs the configuration of the simulator in order to send it messages. When you go to the page "Configuration and Messages" you can see that various configurations are offered. Actually, in order to be able to properly understand the messages it receives, the simulator needs to open a socket using the appropriate encoding character set. The IP address and the receiving application and facility do not differ from a configuration to another, only the port number should change. Note that if the Character set given in the message (MSH-18) is not the one expected, the message is application rejecting. In the same way, if the receiving application or receiving facility does not match the expected one, the message will be reject with an AR acknowledgment.

In this same page, you can see the list of messages received by the PEC actor. The more recent ones are at the top of the list.

## Received Patients

When the simulator receives a message, it tries to integrate it, if it is not able to do it; it sends back an error message. It means that each time it can, it performs the appropriate action on the patient and its encounter. The resolution of patients is done on their identifiers; the resolution of encounters is done using the visit number (PV1-19). For each patient, the list of encounters and movements received are available under the "Patient's encounter" tab.

# PAM Test Automation

The Patient Manager tool has an automation feature named PAM Test Automation. It is available through the PAM section, under the Automation menu. This automaton aims to handle all events of the ITI-31 transaction, sequentially following an order described by a state diagram. The accepted diagram must be in the graphml format and edited with the yEd software.

![PAM Test Automation Menu](./media/image8.png)

3 pages are defined in this tool:

1.  Execution logs: Display logs results of a graph execution
2.  Available automated tests: graphs that are used in an automaton execution
3.  Run automaton: Graph execution

## Execution logs

![PAM Test Automaton Execution List](./media/image9.png)

The logs page displays the results of the various executions done with the automaton. You can filter with the search criteria on the top of the page.

To display a graph execution, click on the corresponding view icon.

From this page, you can visualize the HL7 message request and response in different views (XML, Tree, ER7, RAW) and display the validation details.

![HL7 message request](./media/image10.png)

## Available automated tests

This page is dedicated to the display and edit of graph. In the list page, you can see all the graphs. You can create a new graph by clicking on the “Create new graph” button or edit an existing one by clicking on the pencil icon.

As an admin, if you click on the green circle, the graph will be disabled, that means that you can’t use it in a new graph execution. If the disabled graph is never used, he can also be deleted by clicking on the trash icon. A disabled graph which has already be used can’t be deleted. If you want to do it, you first need to delete execution logs related to this graph.

![Add graph](./media/image11.png)

When you create a new graph, you need to import a graphml file describing the PAM events you want to support from a list of authorized events which is displayed at the top of the page; basically, they are those supported by the Patient Encounter Supplier section of the tool.

The graph needs to be edited with the yEd software, otherwise it’s not guaranteed that the imported file will work properly.

Moreover, the patients statuses must be named from the following list :

-   No\_Encounter
-   Outpatient
-   Inpatient
-   Preadmit
-   Preadmit\_R
-   Preadmit\_I
-   Preadmit\_O
-   Temporary\_Leave
-   Emergency

The easiest way to create your own working graph is to download an already working one, and edit it with yEd by changing the events.

You also have to add a image to help people with understanding how the state diagram is done. One solution is to take a screenshot of the diagram from the yEd software.

## Run automaton

The last page is devoted to the execution of the automaton. You need to complete these differents steps to run the automaton properly :

1.  Select a graph under the Workflow panel. It defines which events will be executed and from which patient’s statuses they can be processed. When selected, a preview of the automaton is available in the right-sided panel. You can zoom on the preview by clicking on the full-screen icon, By default, the automaton stops running when all the patients statuses are reached at least once. However, you can tick the Full movements coverage checkbox to ensure the automaton only stops when all events are processed.

![Selected graph](./media/image12.png)

1.  The second step is to select the System Under Test (SUT). You can refer to the section 2.3 to configure a SUT.

![Selected SUT](./media/image13.png)

1.  You then need to generate a new patient with the DDS tool. You can select information or let the automaton randomly fill patient data. If you are not satisfied with some information generated by the automaton, you can still click on the “Edit Patient Informations” button to manually change patient data.

![Selected Patient](./media/image14.png)


1.  Select the encounter associated with the patient, as for the patient information, you can either manually fill the encounter or click on the “Fill the encounter with random data” to let the automaton fills the encounter.

2.  Click on the Run automaton button

![Run automaton](./media/image15.png)

The tests results are displayed in real time. When the automaton is processing, you don’t have to stay on the page, you can leave it, the tests results will appear on the “Execution logs” page when the process will be over. This process can be quite long and obviously depends on how many messages are needed to stop the automaton. Moreover, if you use the full movements coverage mode, it is even longer.

![Tests results](./media/image16.png)

For example, this graph above with 9 patient statused and 38 movements needs an average of 400 messages for the automaton to stop with the Full movements coverage mode. The time between 2 messages being process being approximately of 1,75s, you need to wait 12 minutes for the process to be done, in average.

## Editing graph with YED Software

To generate a valid graphml file you need to use Yed. It’s quite simple to edit it. You can add edges from a state to another state. The labels must be named with the event name (ex : A21). The initial event which link the start state to another state must be called “ini”.

![Graph preview](./media/image17.png)

As stated before, the easiest way to make a valid graph is to edit a valid one and change the edges, then save the graph.

Your graph can be oriented with what are called “guards”. Guards are parameters which can be set or evaluated when passing into an edge.

Here is the way of affecting a value to a variable when passing through an edge :

![Variable affectation](./media/image18.png)


Here is the way of saying that the A11 edge can be reached only under conditions :

![Variable evaluation](./media/image19.png)

If your graph is not valid because of unsupported events, a message will be displayed when you try to upload it in Patient manager. However, be careful, it’s not impossible that your graph is invalid for another reason but is accepted by Patient manager and still can be uploaded.

# PDQ*

The Patient Manager tool is also able to act as a Patient Demographic Consumer for the Patient Demographics Query (PDQ), Patient Demographics Query HL7V3 (PDQV3) and Patient Demographics Query for Mobile (PDQm) integration profiles. That means that it can send

-   the message defined by the ITI-21 and ITI-22 transactions

    -   QBP^Q22^QBP\_Q21
    -   QBP^ZV1^QBP\_Q21
    -   QCN^J01^QCN\_J01

-   the messages defined by the ITI-47 transaction

    -   PRPA\_IN201305UV02
    -   QUQI\_IN000003UV01
    -   QUQI\_IN000003UV01\_Cancel

-   the messages defined by the ITI-78 transaction

## Patient Demographics Query (PDQ)

Access the page to create the query to send to your system from menu PDQ* --&gt; Patient Demographic Consumer --&gt; \[ITI-21/ITI-22\] Patient Demographics (and visits) Query.

## Patient Demographics Query HL7V3 (PDQV3)

Access the page to create the query to send to your system from menu PDQ* --&gt; Patient Demographic Consumer --&gt; \[ITI-47\] Patient Demographics Query HL7v3.

## Patient Demographics Query for Mobile (PDQm)

Access the page to create the query to send to your system from menu PDQ* --&gt; Patient Demographic Consumer --&gt; \[ITI-78\] Patient Demographics Query FHIR.

## Step by Step

1. Select your system under test

From the drop-down list "System under test", select your system under test. The sequence diagram at the top of the page is updated with the connection information of your system at right, review them.

2. \[PDQ only\] Select the transaction

From the PDQ PDC page, you can select if you want to test the ITI-21 or ITI-22 transaction. Selected the "Patient demographics and visits query" option will add a panel to configure the query parameter specific to the visit.

![PDQ/PDQV3 - PDC - Visit information criteria panel](./media/image20.png)

3. Configure the query

The screens from PDQ, PDQv3 and PDQm are similar. Only the way to enter the patient identifier is different.

As soon as your query is complete, push the "Send message" button. The query is sent to your system and the exchanged messages are then displayed at the bottom of the page. From there, you will be able to call the Gazelle HL7 Validator tool to check the correctness of the response produced by your system.

The response from the supplier is parsed and you are allowed to ask the tool to store the patients for future use (for instance of ITI-31 transaction), use the 'plus' button. To see the details of a given patient (or encounter in the context of ITI-22 transaction), use the magnifying-glass icon.

![PDQ/PDQV3 - PDC - Returned patients](./media/image21.png)

If you used the "limit value" option, the tool allows you to send the Query continuation message as well as the Query cancellation message.

![PDQ/PDQV3 - PDC - Continuation pointer configuration](./media/image22.png)

First limit the number of hints to be returned by the supplier. The first batch of patients/visits is parsed and displayed.

![PDQ/PDQV3 - PDC - Continuation Query Response](./media/image23.png)

Then you can send the continuation pointer message (Get next results) and send the query cancellation message (Cancel query).

If you choose to cancel the query, the following message is displayed.

![PDQ/PDQV3 - PDC - Query cancellation confirmation](./media/image24.png)

A new button appears which allows you to send the cancellation query again to make sure that your system took the message into account.

![PDQ/PDQV3 - PDC - Query cancellation confirmation (2)](./media/image25.png)

# PDQ Patient Demographics Supplier

The Patient Manager is able to act as a Patient Demographics Supplier for the Patient Demographic Query integration profile. Both the Pediatric Demographics and Patient Demographic and Visit Query options are implemented. As a consequence, the simulator can be used as a responder for the following transactions:

-   ITI-21: Patient Demographics Query

-   ITI-22: Patient Demographics and Visit Query

## ITI-21: Patient Demographics Query

The table below gathers the parameters the simulator is able to map to its database to perform the query and send back the batch of corresponding patients. Note that when several parameters are provided, the AND operator is used to build the database query; the "\*" wildcard is supported to substitute zero or more characters. The returned patients are those owned by the Patient Demographic Supplier actor. To consult the list of available patients, see [*https://gazelle.ihe.net/PatientManager/patient/allPatients.seam?actor=PDS*](https://gazelle.ihe.net/PatientManager/patient/allPatients.seam?actor=PDS) . Note that only the subset of active patients is queried.

| **HL7 FIELD **     | ** ELEMENT NAME**                                                    |  **JAVA OBJECT / ATTRIBUTE (indicative)**  | ** SQL CLAUSE **                                      | ** EXTRA INFO**                |
|--------------------|----------------------------------------------------------------------|--------------------------------------------|-------------------------------------------------------|--------------------------------|
| **PID.3**          | Patient Identifier List                                              | patient.patientIdentifiers                 | like (ignores case)                                   | also filter according to QPD-8 |
| **PID.3.1**        | Patient Identifier List (ID Number)                                  | patientIdentifer.fullPatientId             | like (ignores case), MatchMode = START                |                                |
| **PID.3.4.1**      | Patient Identifier List (Assigning Authority - namespace ID)         |  patientIdentifier.domain.namespaceID      | domain namespaceID like (ignores case)                |                                |
| **PID.3.4.2**      | Patient Identifier List (Assigning Authority - universal ID)         | patientIdentifier.domain.universalID       |  domain universal ID like (ignores case)              |                                |
| **PID.3.4.3**      | Patient Identifier List (Assigning Authority - universal ID Type)    |  patietnIdentifier.domain.universalIDType  |  domain universal ID type  like (ignores case)        |                                |
| **PID.3.5**        | Patient Identifier List (Identifier Type Code)                       | patientIdentifier.identifierTypeCode       | like (ignores case)                                   |                                |
| **PID.5.1.1 **     | Patient Name (family name/surname)                                   | patient.lastName                           | like (ignores case)                                   |                                |
| **PID.5.2**        | Patient Name (given name)                                            | patient.firstName                          | like (ignores case)                                   |                                |
| **PID.7.1**        | Date/Time of Birth                                                   | patient.dateOfBirth                        | between 'date 0:00 am' to 'date 11:59 pm'             | date format : yyyyMMddHHmmss   |
| **PID.8**          | Administrative Sex                                                   | patient.genderCode                         | equals                                                | Gender code (F, M ...)         |
| **PID.11.1**       | Patient Address (Street)                                             | patient.street                             | like (ignores case)                                   |                                |
| **PID.11.3**       | Patient Address (City)                                               | patient.city                               | like (ignores case)                                   |                                |
| **PID.11.4**       | Patient Address (State)                                              | patient.state                              | like (ignores case)                                   |                                |
| **PID.11.5**       | Patient Address (Zip Code)                                           | patient.zipCode                            | like (ignores case)                                   |                                |
| **PID.11.6**       | Patient Address (Country Code)                                       | patient.countryCode                        | like (ignores case)                                   | iso3                           |
| **PID.18**         | Patient Account Number                                               | patient.accountNumber                      | like (ignores case)                                   |                                |
| **PID.18.1**       | Patient Account Number  (ID Number)                                  | patient.accountNumber                      | like (ignores case), MatchMode = START                |                                |
| **PID.18.4.1**     | Patient Account Number  (Assigning Authority - namespace ID)         | patient.accountNumber                      |  like (ignores case) %^^^value, MatchMode = START     |                                |
| **PID.18.4.2**     | Patient Account Number  (Assigning Authority - universal ID)         | patient.accountNumber                      | like (ignores case) %^^^%&value, MatchMode = START    |                                |
| **PID.18.4.3**     | Patient Account Number  (Assigning Authority - universal ID Type)    | patient.accountNumber                      |  like (ignores case) %^^^%&%&value, MatchMode = START |                                |
| **PID.6.1.1**      | Mother's maiden name (last name)                                     | patient.mothersMaidenName                  | like (ignores case)                                   |                                |
| **PID.13.9**       | Phone Number - Home (any text)                                       | patient.phoneNumber                        | like (ignores case)                                   |                                |

Table 1 PDQ - PDS - ITI-21 query parameters

## ITI-22: Patient Demographics and Visit Query

The table below gathers the parameters the simulator is able to map to its database to perform the query and send back the batch of corresponding patients. Note that when several parameters are provided, the AND operator is used to build the database query; the "\*" wildcard is supported to substitute zero or more characters. The returned patients are those owned by the Patient Encounter Supplier actor. To consult the list of available patients, see [*https://gazelle.ihe.net/PatientManager/patient/allPatients.seam?actor=PES*](https://gazelle.ihe.net/PatientManager/patient/allPatients.seam?actor=PES). Note that only the subset of open encounters for active patients is queried.

The parameters gathered in table *Table-1* are also supported for this transaction. In addition, you can provide the following parameters (see *Table-2*).

| HL7 FIELD       |  ELEMENT NAME                                        |  JAVA OBJECT / ATTRIBUTE (indicative)         |  SQL CLAUSE                                         |  EXTRA INFO                   |
|-----------------|------------------------------------------------------|-----------------------------------------------|-----------------------------------------------------|-------------------------------|
| **PV1.2**       | Patient class                                        | encounter.patientClassCode                    | equals                                              | Patient class code (I, O ...) |
| **PV1.3.1**     | Assigned Patient location (Point of care)            | movement.assignedPatientLocation              | like (ignores case), MatchMode = START              |                               |
| **PV1.3.2**     | Assigned Patient location (Room)                     | movement.assignedPatientLocation              |  like (ignores case) %^value, MatchMode = START     |                               |
| **PV1.3.3**     | Assigned Patient location (Bed)                      | movement.assignedPatientLocation              |  like (ignores case) %^%^value, MatchMode = START   |                               |
| **PV1.3.4**     | Assigned Patient location (Facility)                 |  movement.assignedPatientLocation             |  like (ignores case) %^%^%^value, MatchMode = START |                               |
| **PV1.7**       | Attending doctor                                     | encounter.attendingDoctor                     | like (ignores case)                                 |                               |
| **PV1.8 **      | Referring doctor                                     | encounter.referringDoctor                     | like (ignores case)                                 |                               |
| **PV1.10**      | Hospital service                                     | encounter.hospitalServiceCode                 | like (ignores case)                                 |                               |
| **PV1.17**      | Admitting doctor                                     | encounter.admittingDoctor                     | like (ignores case)                                 |                               |
| **PV1.19.1**    | Visit number (ID number)                             | encounter.visitNumber                         | like (ignores case)                                 |                               |
| **PV1.19.4.1 ** | Visit number (Assigning authority namespaceID)       |  encounter.visitNumberDomain.namespaceID      | like (ignores case)                                 |                               |
| **PV1.19.4.2**  | Visit number (Assigning authority universalID)       |  encounter.visitNumberDomain.universalID      | like (ignores case)                                 |                               |
| **PV1.19.4.3**  | Visit number (Assigning authority universal ID Type) |  encounter.visitNumberDomain.universalIDType  | like (ignores case)                                 |                               |

Table 2 PDQ - PDS - ITI-22 query parameters

## QPD-8: What domains returned

The list of the domains known by the Patient Demographics Supplier actor is available under Patient Demographics Query / Patient Demographics Suppliers. It is built up from the list of different assigning authorities for which the simulator owned patient identifiers.

## Continuation pointer persistence and Query Cancellation

As defined in the technical framework, the Patient Demographics Supplier is able to send results in an interactive mode using a continuation pointer. The list of pointers is regularly cleaned up; a pointer for which no request has been received **within the previous hour** is destroyed.

When querying the supplier in interactive mode, the Patient Demographics Consumer can send a cancel query message (QCN^J01^QCN\_J01) to inform the supplier that no more result will be asked. At this point, the supplier destroys the pointer associated to the provided query tag.

# PIX* Patient Identity Source

The Patient Manager tool integrates the Patient Identity Source actor as defined in the context of the Patient Identifier Cross-Referencing (PIX) and Patient Identifier Cross-Referencing HL7V3 (PIXV3) integration profiles.

That means that the tool is able to initiate the following transactions:

-   Patient Identity Management (ITI-30)
-   Patient Identity Feed (ITI-8)
-   Patient Identity Feed HL7V3 (ITI-44)
-   Patient Identity Feed for Mobile (ITI-83)


## Pre-requisites

Before sending your first message to your system under test, do not forget to register it as a Patient Identifier Cross-Reference Manager in the tool. To do so, go to

-   SUT Configurations --&gt; HL7 Responders (for PIX)
-   SUT Configurations --&gt; HL7V3 Responders (for PIXV3)

## Patient Identity Management (ITI-30)

Refer to the section of the documentation related to [ITI-30 initiator](iti30-init).

## Patient Identity Feed (ITI-8)

The pages dedicated to this transaction are available from the menu PIX* Patient Identity Source \[ITI-8\] Patient identity Feed.

For more detailed information on how to generate and send an ADT message, read the page of this user manual dedicated to the **PAM Patient Encounter Supplier** actor. The same page layout is shared by those actors and the process flow is the same. Only the choice of events differs.

## Patient Identity Feed HL7V3 (ITI-44)

The pages dedicated to this transaction are available from the menu PIX* Patient Identity Source \[ITI-44\] Patient identity Feed HL7V3.

![PIXV3 Source Header](./media/image26.png)

For documentation, refer to Patient Manager - ITI-30 initiator with the following differences:

-   Only three events are available

    -   Add Patient Record (= Create new patient)
    -   Revise Patient Record (= Update patient information)
    -   Patient Identity Merge (= Merge patients)

-   Messages sent are those defined in the ITI-44 transaction (HL7V3)

    -   PRPA\_IN201301UV02 (Add Patient Record)
    -   PRPA\_IN201302UV02 (Revise Patient Record)
    -   PRPA\_IN201304UV02 (Patient Identity Merge)

-   Patient Identity Merge

    -   incorrect patient is used to populate the InReplacementOf element of the PRPA\_IN201304UV02 message
    -   Correct patient is the survival patient and is transmitted in the Patient element.

# PIX* Identity Cross-Reference Consumer

The Patient Manager tool integrates the Patient Identifier Cross-Reference Consumer actor defined by the Patient Identifier Cross-Referencing (PIX) and Patient Identifier Cross-Referencing HL7V3 (PIXV3) integration profiles.

That means that it can

-   send Q23 events in the context of the PIX Query (ITI-9) transaction
-   receive A31 events in the context of the PIX Update Notification (ITI-10) transaction
-   send PRPA\_IN201309UV02 messages in the context of the PIXV3 Query (ITI-45) transaction
-   receive PRPA\_IN201302UV02 messages in the context of the PIXV3 Update Notification (ITI-46) transaction

## PIX Update Notification

For this transaction, the Patient Identifier Cross-Reference Consumer actor plays the role of a responder. In this configuration we are not interested in testing the behaviour of the consumer but rather the conformance of the messages sent by the PIX Manager. As a consequence, the PIX Consumer will simply acknowledge the ADT^A31^ADT\_A05 and PRPA\_IN201302UV02 messages and no other action will be taken.

To send PIX Update Notification messages to our Patient Identifier Cross-Reference Consumer actor, review the configuration of this part of the tool available.

-   This page is reachable from thePIX* Patient Identity Consumer HL7v2 Configuration (for ITI-10/PIX)

-   PIX* Patient Identity Consumer HL7v3 Configuration (for ITI-46/PIXV3)

## PIX Query

The Patient Identifier Cross-Reference Consumer actor plays the role of the initiator in the PIX Query (ITI-9), PIXV3 Query (ITI-45) and PIXm Query (ITI-83). In this configuration, the tool needs some information concerning your system under test in order to be able to send messages to it. If it is your first time in this application, do not forget to register your system under test as a PIX Manager under the SUT Configurations HL7 responders or SUT Configurations HL7V3 Responders menus.

1.  **Start your test**

From menu

-   PIX* Patient Identity Consumer \[ITI-9\] PIX Query
-   PIX* Patient Identity Consumer \[ITI-45\] PIXV3 Query
-   PIX* Patient Identity Consumer \[ITI-83\] PIXm Query

1.  **Select your system under test**

Select the system under test to query from the drop-down menu entitled "System under test". Look at the connection information displayed at right of the sequence diagram and make sure they meet your system configuration.

1.  **Configure the query parameters**

PIX, PIXm and PIXV3 screens slightly differ because of the format of the patient identifiers in HL7V2, FHIR and HL7V3 but the main purpose is similar.

Fill out the person identifier you want to query your system for. Optionally add one or more domains to be returned in the response.

Finally hit the send message button.

1.  **Response**

The received response is parsed to extract the identifiers returned by your system (if some).

1.  **Validate the message**

Finally, in the test report section, the messages exchanged for the test are logged and you can ask the tool to validate them; the Gazelle HL7 Validator will be called to do so.

# PIX* Identity Cross-Reference Manager

The Patient Manager tool integrates the Patient Identifier Cross-Reference Manager actor as defined in the PIX (Patient Identifier Cross-Referencing) and PIXV3 (Patient Identifier Cross-Referencing HL7V3) integration profiles.

That means that the tool is able to act

-   as a receiver in the context of the Patient Identity Feed (ITI-8), Patient identity Feed HL7V (ITI-44), and Patient Identity Management (ITI-30) transactions
-   as a responder of the PIX Query (ITI-9), PIXV3 Query (ITI-45) and PIXm (ITI-83) transactions.
-   as a sender of PIX Update Notification (ITI-10) and PIXV3 Update Notification messages

## HL7v2, HL7v3 and FHIR endpoints

The configuration of the HL7V2 endpoint of the tool is available from menu PIX* Patient Identifier Cross-Reference Manager HL7v2 Configuration

The configuration of the HL7V2 endpoint of the tool is available from menu PIX* Patient Identifier Cross-Reference Manager HL7v3 Configuration

The configuration of the FHIR endpoint of the tool is available from menu PIX* Patient Identifier Cross-Reference Manager FHIR Configuration

## Patient Identity Feed / Patient Identity Management

If your system under test is a Patient Identity Source, it can send messages to our Patient Identifier Cross-Reference Manager.

For each new patient received, the tool computes the [*double metaphone*](http://en.wikipedia.org/wiki/Metaphone#Double_Metaphone) for its first name, last name and mother's maiden name. Then, it looks for similar patients. In our cases patients are similar if

-   [*levenstein distance*](http://en.wikipedia.org/wiki/Levenshtein_distance) between first names is strictly lower than 2
-   levenstein distance between last names is strictly lower than 2
-   levenstein distance between mother's maiden names is strictly lower than 2
-   Patients are of the same gender
-   Patients are born the same month in the same year

If all those criteria are met, then, the two patients are cross-referenced.

1.  PIX Manager

On ADT message reception, the tool will perform the following actions:

1.  Parse message and extract patient's demographics data

2.  Create, update, merge ... the patient according to the received event

3.  If necessary, automatically references (or unreferences) patients

4.  Send the acknowledgement

5.  PIXV3 Manager

Currently, the manager only acknowledges the messages received in the context of the ITI-44 transaction. They are not yet integrated; this will come with a future version.

## PIX* Query

The Patient Identifier Cross-Reference manager actor integrated into the Patient Manager implements the responder part of the PIX Query and PIXV3 Query and transactions.

That means that it is able to answer to

-   QBP^Q23^QBP\_Q21
-   PRPA\_IN201309UV02

You can consult the list of available patients [*here*](https://gazelle.ihe.net/PatientManager/pixmanager/referenceManager.seam?manageIdentities=true) (go to PIX* Patient Identity Cross-reference Manager --&gt; Cross-References Management)

## PIX* Update Notification

If this is your first time in the application, you need to register your system under test in the tool.

Go to

-   SUT Configurations --&gt; HL7 responders, for PIX Update Notification (HL7V2)
-   SUT Configurations --&gt; HL7V3 Responders, for PIXV3 Update Notification (HL7V3)

and register your system as a Patient Identifier Cross-Reference Consumer actor.

1.  Starting points

1.  For PIX profile

Go to PIX* --&gt; Patient Identity Cross-Reference Manager --&gt; \[ITI-10\] PIX Update notification

1.  For PIXV3 profile

Go to PIX* --&gt; Patient Identity Cross-Reference Manager --&gt; \[ITI-46\] PIXV3 Update notification

1.  Configure the update notification

First, select your system under test in the drop-down list and check the configuration (at the right of the sequence diagram).

Then, select the list of domains you want to be sent by the tool.

![PIX* - Manager - Configure update notifications](./media/image27.png)

Finally, select the patient you want to receive and hit the ![](./media/image28.png) button. The message will be automatically send to your system, including all the cross-referenced identifiers which match the set of domains you have selected.

## Manually cross-reference patients

Although the tool automatically performs a cross-referencing of patients received from the patient identity source, you may want to complete or correct the cross-references made to a patient. The tool offers a section to manage those references manually.

1.  Starting point

Go to PIX* --&gt; Patient Identity Cross-Reference Manager --&gt; Cross-references management.

1.  Send notifications to a system under test

At the top of the page, you can chose to send PIX* update notifications to a system under test, each time you change the list of identifiers of a patient.

![PIX* - Manager - Configure auto update](./media/image29.png)


Each time you will add create/remove a cross-reference, the sending of a message will be triggered if the domains you have selected are concerned. At the bottom of the page will be displayed the messages exchanged with your system so that you can call the validation service to check the conformance of your acknowledgements with the specifications.

1.  *Cross-reference patients*

Clicking on the magnifying glass icon on a patient row will display that patient's information. The table at right lists all the patients which are referenced together with that patient.

![PIX* - Manager - Manually Cross-reference patients](./media/image30.png)

To cross-reference other patients with the selected one, drag and drop their identifiers to the panel; the new reference will be automatically saved.

To remove the reference between two patients hit on the red minus icon.

# PDQv3 Patient Demographic Supplier

The Patient Manager is able to act as a Patient Demographics Supplier for the Patient Demographic Query HL7v3 integration profile. 

-   ITI-47: Patient Demographics Query HL7v3

# PDQm and PIXm Responders

The Patient Manger is able to act as a simulated responder for FHIR transactions. For this you need to go to the following URL :

[*https://gazelle.ihe.net/PatientManager/fhir/*](https://gazelle.ihe.net/PatientManager/fhir/)

You can then specify parameters specified in PIXm or PDQm transactions :
* PDQm parameters

`GET [base]/[type]{?[parameters]{&_format=[mime-type]}}`

|Parameter name|Cardinality|Parameter Type|Description|
|--------------|-----------|--------------|-----------|
|_id|0..1|String|This parameter of type string, when supplied, represents the resource identifier for the Patient Resource being queried.|
|identifier|0..n|Token|This repeating parameter of type token, when supplied, specifies an identifier associated with the patient whose information is being queried (e.g., a local identifier, account identifier, etc.). See ITI TF-2x: Appendix Z.2.2 for use of the token data type. If multiple instances of this parameter are provided in the query, the query represents a logical AND condition (i.e., all of the associated identifiers must match).|
|_format|0..1|mime-type|The requested format of the response.
|family|0..n|String|This parameter of type string, when supplied, specify the lastname of the person whose information is being queried.|
|given|0..n|String|This parameter of type string, when supplied, specify the firstname of the person whose information is being queried.|
|birthdate|0..1|Date|This parameter of type date, when supplied, specifies the birth date and time of the person whose information is being queried.|
|address|0..n|String|This parameter of type string, when supplied, specifies one or more address parts associated with the person whose information is being queried.|
|gender|0..1|Token|This parameter of type token, when supplied, specifies the administrative gender of the person whose information is being queried. Accepted values : male, female, other, unknown|
|mothersMaidenName.family|0..n|String|This parameter of type string, when supplied, specify the lastname of a patient’s mother|
|mothersMaidenName.given|0..n|String|This parameter of type string, when supplied, specify the firstname of a patient’smother|
|telecom|0..1|String|This parameter of type string, when supplied, specifies the telecommunications address  for the person whose information is being queried|



* PIXm parameters

`GET [base]/Patient/$ihe-pix?sourceIdentifier=[token]{&targetSystem=[uri]}{&_format=[mime-type]}`

|Parameter name|Cardinality|Parameter Type|Description|
|--------------|-----------|--------------|-----------|
|sourceIdentifier|1..1|Token|The Patient identifier search parameter that will be used by the Patient Identifier Cross-reference Manager to find cross matching identifiers associated with the Patient Resource
|targetSystem|0..1|uri|The target Patient Identifier Assigning Authority from which the returned identifiers should be selected.|
|_format|0..1|mime-type|The requested format of the response. Accepted values : JSON and XML|


## ITI-47: Patient Demographics Query

The table below gathers the parameters the simulator is able to map to its database to perform the query and send back the batch of corresponding patients. Note that when several parameters are provided, the AND operator is used to build the database query; the "\*" wildcard is supported to substitute zero or more characters. The returned patients are those owned by the Patient Demographic Supplier actor. To consult the list of available patients, see [*https://gazelle.ihe.net/PatientManager/patient/allPatients.seam?actor=PDS*](https://gazelle.ihe.net/PatientManager/patient/allPatients.seam?actor=PDS) . Note that only the subset of active patients is queried.

*Table-1: Query parameters supported by the PDQv3/PDS simulator for ITI-47 transaction*

| **Parameter**                             |  **JAVA OBJECT / ATTRIBUTE (indicative)**  | ** SQL CLAUSE **                           | ** EXTRA INFO**                           |
|-------------------------------------------|--------------------------------------------|--------------------------------------------|-------------------------------------------|
| **livingSubjectId (extension)**           | patientIdentifer.fullPatientId             | like (ignores case), MatchMode = START     |                                           |
| **livingSubjectId (root)**                | patientIdentifier.domain.universalID       | domain universal ID like (ignores case)    |                                           |
| **livingSubjectName (family)**            | patient.lastName                           | like (ignores case)                        |  by now, only the first occurence is used |
| **livingSubjectName (given)**             | patient.firstName                          | like (ignores case)                        |  by now, only the first occurence is used |
| **livingSubjectBirthTime**                | patient.dateOfBirth                        | between 'date 0:00 am' to 'date 11:59 pm'  | date format : yyyyMMddHHmmss              |
| **livingSubjectAdministrativeGenderCode** | patient.genderCode                         | equals                                     | Gender code (F, M ...)                    |
| **patientAddress (streetAddressLine)**    | patient.street                             | like (ignores case)                        |                                           |
| **patientAddress (city)**                 | patient.city                               | like (ignores case)                        |                                           |
| **patientAddress (state)**                | patient.state                              | like (ignores case)                        |                                           |
| **patientAddress (postalCode)**           | patient.zipCode                            | like (ignores case)                        |                                           |
| **patientAddress (country)**              | patient.countryCode                        | like (ignores case)                        | iso3                                      |
| **mothersMaidenName (family)**            | patient.mothersMaidenName                  | like (ignores case)                        |                                           |
| **patientTelecom**                        | patient.phoneNumber                        | like (ignores case)                        |                                           |

## Other IDs Scoping organizations

If the otherIDsScopingOrganization parameter is transmitted to the supplier, the simulator behaves as stated in the Technical Framework. To list the identifier domains known by the tool, go to PDQ/PDQV3 Patient Demographics Supplier HL7v3 Configuration.

## Continuation pointer persistence and Query Cancellation

The simulator is able to handle the continuation pointer protocol. If no cancellation message is received within the 24 hours, the pointer and the associated results are deleted from the system.

# SWF ADT

The Patient Manager tool integrates the ADT actor as defined in the context of the Scheduled Workflow of Radiology profile. That means that the tool is able to send Patient Registration (RAD-1) and Patient Update (RAD-12) messages to your ADT client.

The following events are available for message sending:

-   Patient Registration (RAD-1)
-   Admission of an in-patient (A01)
-   Registration of an out-patient (A04)
-   Cancellation of admission/registration (A11)
-   Pre-admission of an in-patient (A05)
-   Cancellation of pre-admission (A38)
-   Patient Update (RAD-12)
-   Change Patient Class to in-patient (A06)
-   Change Patient Class to out-patient (A07)
-   Discharge Patient (A03)
-   Cancel discharge (A13)
-   Transfer patient (A02)
-   Cancel transfer (A12)
-   Merge patient - Internal ID (A40)
-   Update patient Information (A08)

Before sending your first message to your system under test, do not forget to register it as an ADT Client in the tool. To do so, go to *SUT Configurations > HL7V2 Responders* page and create a new configuration.

The ADT features are available under the ADT menu.

For more detailed information on how to generate and send an ADT message, read the page of this user manual dedicated to the [*PAM Patient Encounter Supplier*](#patient-encounter-supplier) actor. The same page layout is shared by both actors and the procedures are the same.

# XCPD - Cross-Community Profile Discovery

Our implementation of the XCPD profile is based on what we have done for the PDQV3 profile. That means that you can refer to the PDQV3 actors to:

* understand how the query is processed by the Responding Gateway
* get directions to use the Initiating Gateway part

The patients queried by the Responding Gateway when a PRPA_IN201305UV02 query is received are those held by the Patient Demographics Supplier actor. To list the available patients and configure your query to the tool:

* go to All Patients and filtered based on Simulated actor = PDS
* go to XCPD > Responding Gateway, the patients icon will redirect you to the filtered list of patients.

In addition, the Patient Manager tool implements the Deferred response option.

## Deferred response on Initiating Gateway

If you want to test the implementation of the deferred response option for your Responding Gateway:

1. Access the XCPD > Initiating Gateway > [ITI-55] Cross Gateway Patient Discovery
1. After selecting the System Under Test, check the "Deferred response mode" option
1. Create your query and send it to your system
1. Your system should send back the HL7V3 Accept Acknowledgement
1. Then, move to XCPD > Initiating Gateway > Configuration and Deferred queries
1. A table gathers all the queries sent by the tool for the deferred response mode. Use the filters to find the message you have sent. This table displays the system under test, the status of the request, timestamp of the initial request, the timestamp of the deferred response, and gives access to both exchanges of messages.

![Select the "Deferred response mode"](./media/deferred-response-select.png)

![Accept Acknowloedgement received from your system](./media/deferred-test-report.png)

![Logs of the complete exchange of messages](./media/deferred-response-logs.png)

Check the "Auto-refresh the list of messages" checkbox if you are still waiting for the actual response to reach the tool. The page will be refreshed automatically every second.

__Note__ The status can have the following values:

* WAITING: The tool is still waiting for the actual response
* DONE: The actual response has been received and acknowledged
* OPTION_NOT_SUPPORTED: The system under test does not support the deferred response mode option

## Deferred response on Responding Gateway

If you want to test the implementation of the deferred response optio nfor your Initiating Gateway, the endpoint to contact is the same as for the normal process. You will find the configuration of our XCPD Responding Gateway under XCPD > Responding Gateway.

As soon as you have sent the deferred query to the simulator, you can go to the XCPD Responding Gateway page. A table gathers all the deferred messages received by the tool. Use the filters to find the message you have sent. This table displays the system under test, the status of the request, timestamp of the initial request, the timestamp of the deferred response, and gives access to both exchanges of messages.

__Note__ The status can have the following values:

* WAITING: The message is in the pipe for being processed, waiting for next scanner run
* DONE: The actual response has been sent and the acknowledgement received
* DISCARDED: The tool does not have enough pieces of information to send back the actual response.


__Note__ The XCPD Responding Gateway immediately sends back the accept acknowledgement and stores the query for future processing. A cron scans the list of "WAITING" message at regular intervals. This period is set by the administrator of the tool. Default value is every minute.

![Logs of the complete exchange of messages](./media/deferred-respgw.png)

# XUA over XCPD

The Patient Manager tool offers a feature to send SAML assertion inside the SOAP headers of the XCPD queries.
The tool makes use of the [Gazelle-STS](../Gazelle-STS/user.html) simulator to issue assertions and to validate them.

## XUA X-Service User

If you want the XCPD Initiating Gateway simulator to add an SAML assertion when it sends messages to your system under test:

1. Check the "use XUA?" checkbox
2. Select the kind of assertions to be sent (The Gazelle STS tool can issue valid and non valid assertions)

![Configuration of the SAML Assertion](./media/xua-select-assertion.png)

Access the XUA logs under XUA > X-Service User logs, you will be able to check if the assertion was accepted and if not, the reason why. Note that the assertion might seem accepted since IHE recommands to silently reject invalid assertions.

## XUA X-Service Provider

If a SAML assertion is contained in the SOAP Header of the XCPD message your Initiating Gateway sends to the tool, it will be sent to Gazelle-STS in order to check its validity. A SOAP Fault will be issued in the assertions gets rejected.

The logs of the X-Service User are available from XUA > X-Service Provider logs

# How to add your system as a receiver

The Patient Manager can send HL7v2/HL7v3/FHIR messages to your system under test (e.g. if you are testing PAM/PDC, PIX Manager, Order Placer, Order Filler, and others).

In order to send messages to your system under test, the Patient Manager tool needs the configuration (IP address, listening port, and receiving facility/application) of your receiving system. This configuration has to be stored in the database of the application, so that you can re-use this configuration without creating it each time you need to perform a test. The procedure is different depending on the version of HL7 your system is implementing.

In both cases, if you are logged in when creating the configuration, you will be set as the owner of the configuration. If you do not want other testers to send messages to your SUT you can uncheck the box "Do you want this configuration to be public?" and you will be the only one to be able to select your system in the drop-down list (if logged in !).

## HL7v2 Systems Under Test

Go to System Configurations HL7 Responders and hit the "Create a Configuration" button. You can also copy ![](./media/image31.png)or Edit ![](./media/image32.png) an existing configuration (one of yours!)

In both cases, the simulator needs to know:

-   A name for your configuration (displayed in the drop-down list menus)
-   The receiving facility/application
-   The IP address
-   The port the system is listening on
-   The charset expected by your SUT
-   The list of actor/transaction pairs supported by your system

## HL7V3 Systems Under Test

Go to System Configurations HL7V3 Responders and hit the "Create a Configuration" button. You can also copy or edit an existing configuration.

In both cases, the simulator needs to know:

-   A name for your configuration
-   The name of the tested system
-   Its endpoint location
-   Its device id root OID
-   Its organization OID
-   The list of transactions which are supported by your system

## FHIR Systems Under Test

Go to System Configurations FHIR Responders and hit the "Create a Configuration" button. You can also copy or edit an existing configuration.

In both cases, the simulator needs to know:

-   A name for your configuration
-   The name of the tested system
-   Its endpoint location
-   The list of transactions which are supported by your system

## SUT's assigning authorities {#sut-assigning-authorities}

By default, the ADT and PRPA messages sent to your system to feed it with patient demographics will enclose all the patient's identifiers known by PatientManager for this given patient. If you want to restrict the list to a subset of domain, you can configure the tool to do so.

From *SUT Configurations > SUT's assigning authorities*, you can either update the preferences for your system under test (it is list in the table) or define them for the first time (click on the "Define preferences" button at the bottom of the page).

The preferred message sections refers to the Connectathon feature of the tool which allow sharing of a batch of patients pre-defined by the test session manager.

# How to perform HL7 validation

The simulator communicates with our Gazelle HL7 Validator which performs validation of HL7v2.x messages (based on HL7 message profiles developed by Gazelle team and the NIST), validation of HL7v3 messages and FHIR messages. For each received and sent messages, you can ask the simulator to validate the messages. Below is the meaning of the different icons you can meet in the Test Report section of each page or under the HL7 messages menu (gathers all the messages received and sent by the simulator).

|     |  Open the pop-up containing the received and sent messages beside their validation results. The validation service is automatically called each time you hit this button. Consequently, the validation result you see is always the one matching the newest version of the message profile. |
|-----|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|     | The message has not been validated yet. Hitting this button leads to the same action as the previous icon (magnifying glass).                                                                                                                                                               |
|     | The message has been successfully validated. Hitting this button leads to the same action as the previous ones.                                                                                                                                                                             |
|     | The message has been validated but the message contains errors.                                                                                                                                                                                                                             |
|     | Open a pop-up containing the list of SUT which can receive this message. Enables the user to send again a specific message. Be aware that the simulator can only be asked to replay a message sent by it (not received from another SUT)                                                    |

# RESTful APIs

From version 9.7.0, Patient Manager offers two new RESTful APIs. They allow the user to:

* Trigger the execution of the PAM PES automaton remotely and get the execution status
* Trigger the validation of HL7 messages remotely and get the validation status and report (XML)

## Automaton API

Patient Manager offers an automaton which randomly walks through a state diagram and sends the appropriate ITI-31 messages to change the status of the patient's encounter. The tool can be launch trough the graphical user interface [see documentation here](#pam-test-automation) or using a RESTful query.
Four methods are available from PatientManager/rest/automaton:
* listGraphs returns the list of available sequence diagram that the automaton can execute
* RunAutomaton executes the given sequence diagram against the given system under test with a newly generated patient
* RunAutomatonWithExistingPatient executes the given sequence diagram against the given system under test with the given patient
* StatusExecution returns the status of the given execution

### Pre-requisites

Your system under test shall be recorded into Patient Manager as a PAM / Patient Encounter Consumer actor. [See how here](#hl7v2-systems-under-test). The name of the configuration shall be used as systemundertest parameter of the RunAutomaton and RunAutomatonWithExistingPatient methods.

### listGraphs

Sample request: [/PatientManager/rest/automaton/listGraphs](../../PatientManager/rest/automaton/listGraphs)

This request does not have any parameter. It returns an XML structure (GraphDescriptions) containing the list of available sequence diagrams (GraphDescription) with the following attributes:

* id: the identifier of the graph to use as parameter of the RunAutomaton and RunAutomatonWithExistingPatient methods
* name: the name of the graph
* description: a short description of the content of the sequence diagram

### RunAutomaton

Sample request: /PatientManager/rest/automaton/RunAutomaton?graphmlID=GRAPHID&systemundertest=SUT_NAME[&fullmovementcoverage=BOOLEAN&validatetransactioninstance=BOOLEAN&BP6Mode=BOOLEAN&startwithanA28=BOOLEAN&patientscountrycode=ISO3_COUNTRY_CODE]

The outcome of this request is:

* the identifier of the execution (to be used as executionID parameter of the StatusExecution method)
* the automaton is configured and executed in background

Parameters:

* graphmlID \[Required\]: the identifier of the state diagram to be executed [see listGraphs method](#listgraphs)
* systemundertest \[Required\]: the name of the system under test as registered in Patient Manager [see Pre-requisites](#pre-requisites-2)
* Optional parameters:
  * fullmovementcoverage (default: false) if set to true, the automaton will stop as soon as it has covered all the transitions (default behaviour: stops upon coverage of every states)
  * validatetransactioninstance (default: false) if set to true, each messages will be sent to the Gazelle HL7 Validator to check the conformance
  * BP6Mode (default: false, available only when BP6Mode is enabled in PatientManager): if set to true, the automaton will generate messages compliant with PAM-FR requirements
  * startwithanA28 (default: false), if set to true, the automaton will send a create patient message (ADT^A28^ADT_05) before going through the sequence diagram
  * patientscountrycode (default: random), the ISO3 country code to be used when generating the patient (if not set, the automaton takes one randomly)

### RunAutomatonWithExistingPatient

Sample request: /PatientManager/rest/automaton/RunAutomatonWithExistingPatient?graphmlID=GRAPHID&systemundertest=SUT_NAME&patientID=PATIENT_ID_IN_DB[&fullmovementcoverage=BOOLEAN&validatetransactioninstance=BOOLEAN&BP6Mode=BOOLEAN&startwithanA28=BOOLEAN]

The outcome of this request is:

* the identifier of the execution (to be used as executionID parameter of the StatusExecution method)
* the automaton is configured and executed in background

Parameters:

* graphmlID \[Required\]: the identifier of the state diagram to be executed [see listGraphs method](#listgraphs)
* systemundertest \[Required\]: the name of the system under test as registered in Patient Manager [see Pre-requisites](#pre-requisites)
* patientID \[Required\]: the identifier of the patient to be used. This identifier is available in the permanent link to the patient.
* Optional parameters:
  * fullmovementcoverage (default: false) if set to true, the automaton will stop as soon as it has covered all the transitions (default behaviour: stops upon coverage of every states)
  * validatetransactioninstance (default: false) if set to true, each messages will be sent to the Gazelle HL7 Validator to check the conformance
  * BP6Mode (default: false, available only when BP6Mode is enabled in PatientManager): if set to true, the automaton will generate messages compliant with PAM-FR requirements
  * startwithanA28 (default: false), if set to true, the automaton will send a create patient message (ADT^A28^ADT_05) before going through the sequence diagram

### StatusExecution

Sample request: /PatientManager/rest/automaton/StatusExecution?executionID=ID_RETURNS_IN_RUNAUTOMATON

The outcome of this request is the current status of the execution.

Parameter:

  * executionID: the integer returns by RunAutomaton and RunAutomatonWithExistingPatient methods.

### Handling of errors

If the automaton fails during the configuration phase (before returning the execution ID), an HTTP error code is returned instead of HTTP code 200 and the reason of the failure is given in the Warning header of the HTTP response.

## Validation API

The purpose of the validation API is to trigger the validation of an HL7V2/HL7V3 message remotely, knowing its ID and the issuer. The API also allows to query for the validation status (PASSED vs FAILED) and the validation report (XML formatted).

Three methods are available through this RESTful API at /PatientManager/rest/hl7Messages

* launchValidation: returns the identifier of the transaction instance and execute the validation of both the request and response in background
* validationStatus: returns the outcome of the validation
* validationReport: returns the validation report

All three methods expect 2 mandatory parameters:

* messageId: the unique identifier of the message to validate (see next section for details)
* issuer: the author of the message (see next section for the details)

### Pre-requisites

Messages sent and received by the tool before November, 3rd 2017 cannot be validated using this API (useful informations have not been extracted).

For HL7V2 messages:

* Identifier is the messageControlId (MSH-10)
* The issuer is made of SENDING_FACILITY/SENDING_APPLICATION (MSH-4/MSH-3) like displayed in the list of HL7 messages

For HL7V3 messages:

* Identifier is made of id@root:id@extension
* The issuer is the IP address of the caller or the name of the system under test

For both kind of messages, the messageId is available in the metadata section of the transaction instance details page.

### launchValidation

Sample request: /PatientManager/rest/hl7Messages/launchValidation?messageId=MESSAGEID&issuer=ISSUER

It returns the identifier of the transaction instance with HTTP code 200. In case of error, message not found, or several messages match the criteria, an error code is returned and the reason of the failure is given in the Warning header of the HTTP response.

### validationStatus

Sample request: /PatientManager/rest/hl7Messages/validationStatus?messageId=MESSAGEID&issuer=ISSUER

It returns the status of the validation for the given message: PASSED, FAILED. An empty value (with code 200) means that the message has not been validated yet. An error HTTP status code is returned in case the message is not found or several messages match the criteria. In that latter case, the reason of the failure is given in the Warning header of the HTTP response.

__Note__ that the validation can take a while, you may wait an average time of 3 to 5 seconds after the execution has been triggered to ask for the result.

### validationReport

Sample request: /PatientManager/rest/hl7Messages/validationReport?messageId=MESSAGEID&issuer=ISSUER

It returns the XML report received from Gazelle HL7 Validator. An empty value (with code 200) means that the message has not been validated yet. An error HTTP status code is returned in case the message is not found or several messages match the criteria. In that latter case, the reason of the failure is given in the Warning header of the HTTP response.

# How to create a patient & share it with the Order Manager tool

Patients created within the Patient Manager can be sent to an external SUT.  These patients can also be used with the Order Manager tool, so that a patient in the Patient Manager database can be used by the Order Manager to create HL7 orders and DICOM Modality Worlist.

Here's how:

1.  Create a new patient in the Patient Manager (e.g. in the "ADT" or "PAM--&gt;Patient Demographics Supplier" menu)
2.  Then, select menu "All patients"
3.  Use the filters and column headings on that page to find your patient.
4.  In the "Action" column for that patient, select the  ![](./media/image32.png) icon to 'Create a worklist or order for an existing patient'.  This button will launch the Order Manager application, and you can proceed to create an Order or Worklist.

# Pre-defined queries

In order to save time and to ease the execution of test cases which are defined in Gazelle Test Management (pre-connectathon and conformity assessment tests), Patient Manager has the ability to save queries and to reuse them later on.
Currently, this feature is available for the following simulators:

* XCPD Initiating Gateway
* PDQ* Patient Demographic Consumer

## Creating a pre-defined query (logged in users)

__Note__ A pre-defined query is defined for a particular simulator.

Creating a pre-defined query is done from the simulator page. Access the page you usually use to send queries to your system under test, fill out the form with the option (deferred mode, XUA) and click on "Add query to favorite".
A pop-up will raise which asks you to give a name and to describe your query. If you want this pre-defined query to be public, check "Shared ?".

![Describe your query for future retrieval](./media/favorite-popup.png)

You can know execute your query. The **next time you will access the simulator page**, this new query will be available in the "Pre-defined query" drop-down list.

## Using a pre-defined query

This feature is available to any user (logged or anonymous) in the limit of the pre-defined queries marked as "shared".

When you are configuring the simulator to send a Patient Demographic Query or a Cross-Community Patient Discovery query, you can select a "pre-defined query". When you do so, the form disappear and the content of the query being sent is shown instead.

![Select the pre-defined query in the drop-down menu](./media/pre-defined-select.png)

Simply select your system under test and click "Send message" to ask the simulator to send the message to your system.

## Manage pre-defined queries

Users with the administrator role can access the exhaustive list of pre-defined queries from Administration > Pre-defined test data.
For each entry, all the informations are displayed including a link to the criteria. No edition feature is available from there but the administrator can remove unuseful entries.

# Administration - Management of assigning authorities {#manage-authorities}

The Patient Manager is able to manage several assigning authorities, especially in the context of the ADT actor, PAM profile and PIX(V3) Feed transaction. That means that when the user asks the tool to create a new patient, he/she can choose to have this patient identified in one or several domain.
By default, the patient generated using our Demographic Data Server (DDS) comes with a DDS identifier (domain namespaceID = DDS). For some countries, we have implemented the algorithm to compute the SSN so in some cases, the patient might also be created with a national identifier.

## Managing available assigning authorities

You will access the list of the assigning authorities known by the tool from menu Administration > Assigning Authorities.

The tool distinguishes the assiging authorities in which it is able to generate identifiers from the assigning authorities extracted from the identifiers received from SUT. The one which are managed by the tool are flagged with "owned by tool = true". In the table, you will notice them when the checkbox in the "owned by tool" column is checked.

### Default assigning authorities

The tool is expected to generate the following items:

* patient identifiers
* visit numbers
* movement identifiers
* account numbers

Thus, at the bottom of the administration page, you will notice a panel where to define which is the assigning authority to be used by default for these items.

![Assigning authorities used as default](./media/default-aa.png)

### Creating a new assigning authority

You can define as many assigning authorities as you need. Note that currently, users are only able to select the assigning authorities of their choice for the patient identifiers. In other words, there is no need to defined several assigning authorities for encounters, movements or accounts.

Clicking on "Create an assigning authority" leads you to a new form where to enter:

* The Namespace ID (HL7v2: HD-1 / HLv3: AssigningAuthorityName)
* The universal ID (HL7v2: HD-2 / HL7v3: root)
* The universal ID type (HL7v2 : HD-3)
* Owned by the tool (default value)
* Usage : what this assigning authority is used for
* Shall only be used for connectathon's demographics: Only administrators will be able to generate patients' identifiers for this assigning authority
* Prefix : used to build the number/identifier as Prefix${nextValueOfIndex}
* Whether the assigning authority is a default one for the selected Usage
* Index (can be reset with the red cross): set to 0 and incremented by one

When you have completed the form, click on Save.

__Note__ If you defined an assigning authority but you don't want it to be used by the users anymore, edit it and mark it as **NOT** "owned by the tool".


# Administration - Management of Value sets

The tool makes use of a feature of the SVSSimulator (our Value Set repository) to offer to the user values to populate the messages sent to the SUT. The list of value sets used by the tool is maintained in the database which allows each instance to use different value sets depending on the needs.

To manage the value sets, access the page from Value sets entry in the menu bar.

Above the table, you will notice that the URL of the SVS Simulator which is called is mentionned. It comes from the __svs_repository_url__ application preference. If it not accurate, do not forget to change it.

Then, for each value set you will find:

* the keyword used in the code to access the value set
* the name of the value set
* the OID of the value set (used to query SVS Simulator)
* the Usage column summarizes what this value set is used for
* the magnifying-glass leads you to the value set

All the fields but the keyword are directly editable in the table. As the keyword is used in the code, it is not safe to allow users to change it.

The content of the value sets is cached in the application to avoid a huge number of calls to SVS Simulator. If you change the OID of a value set, or the URL of the SVSSimulator, do not forget to click on the "Reset cache" button so that the new value sets will be loaded upon request.

Currently, the creation feature is not available.

## Check availability of value sets in SVS Simulator

From PatientManager-9.6.0, the administrator has the ability to ask the tool to check that all the value sets referenced in its database are actually available in the SVS repository. There are two ways to trigger this action:

* From the user interface, go to Value sets page and click on "Check availability", the page will be refreshed. Look at the "Accessible ?" column to check the status
* Use the REST webservice available at [YOUR_SERVER_URL]/PatientManager/rest/valueSets/checkAll, an XML is returned and for each entry a boolean attribute "accessible" reports the status of the value set

Note that when you edit an entry, the tool automatically checks that the value set is still accessible (you might have changed the OID for a non-valid one).

# Administration - Management of validators

Under Administration > Validation parameters management, you will find the exhaustive list of validators used in the tool. Actually, Patient Manager exchanged HL7v2, HL7v3 and Fhir messages. To allow you to check the conformance of those messages, the tool calls three different validation services. In each case, the validator needs to know exactly what is to be validated. This is done, passing either with the HL7 message profile's OID for HL7v2 or the validator's name for FHIR and HL7v3.
In another hand, the simulator needs to be able to send the correct OID or validator's name, to do so it uses the information stored along the message (Domain, Transaction, Actor, message type and so on). The validation parameter feature allows the simulator to do so.

You can edit, delete and create new validation parameters.

# Logging in to get more features

As some others applications from the Gazelle testing platform, Patient Manager application includes the CAS system. That means that you can log in into the application using your "European" Gazelle account (the one created on the European instance of *TestManagement*). Once you are logged in, you are set as the "creator" of all the patients you create or modify and then (still logged in) you can choose to see only those patients. Another important feature is that you can decide to be the only one to be allowed to send messages to the SUT you have configure in the application (see next section).

# FAQ

## My SUT configuration does not appear in the drop-down menu

Two things can be the reason of this issue:

-   According the actor you are testing, the listed configurations are not the same. Consequently, if you are testing against the PDS, check that your SUT is set as a PDC and if you are testing against the PES, check that you have configured your SUT as a PEC. **Solution: edit the configuration and select the appropriate actor.**
-   If you are not logged in and your configuration has been set to private (You have uncheck the "Do you want this configuration to be public?"), you are not allowed to see it because the application cannot identify you. **Solution: logged in using CAS.**

## I do not understand the messages received by my SUT

In order to be compliant with the highest number of systems, we have chosen to ask the user which encoding character set is supported by his/her SUT. This option can be chosen in the "SUT configuration" page. If none is given, the default one is UTF-8.

In another hand, if you try to send a patient with European characters using the ASCII character set for example, it is trivial that some characters cannot be "translated" and consequently not understood by your SUT.

## HL7 validation says my message contains errors but I think it's wrong

Two answers:

-   Make sure the validation has been performed in the correct domain (ITI vs ITI-RS)
-   We do our best to maintain the HL7 message profile files as the new versions of the Technical Framework are released; we may have missed some changes, so please, be kind, and report those issue into [*JIRA*](https://gazelle.ihe.net/jira/browse/PAM) under the Patient Manager project so that we can take your remark into account and, if needed, update the message profile.
