---
title:  User Manual
subtitle: Patient Manager
author: Alexandre POCINHO
releasedate: 2022-02-22
toolversion: 1.0.0
function: Software Engineer
version: 1.00
status: Validated document
reference: KER1-MAN-IHE_EUROPE-UPI_SIMULATOR_USER-1_00 
customer: IHE EUROPE
---

## Introduction
___

UPI Simulator is a mock for eCH-0213 and eCH-0214 standards of Swizterland ZAS for managing and querying SPID to/from UPI (Unique Person Identification).
- [General page for UPI](https://www.zas.admin.ch/zas/fr/home/partenaires-et-institutions-/unique-person-identification--upi-/identifiant-du-dossier-electronique-du-patient.html)

Patients used by UPI Sedex Simulator are available here :<br> https://ehealthsuisse.ihe-europe.net/test_data/UPI/testData_20220308_v01.xlsx 

The examples given below work with the [SoapUI software](https://www.soapui.org/).

## eCH-0213 : Managing SPID
___

The eCH-0213 interface standard covers all SPID-based announcements registered in the UPI. This standard also describes the common types (eCH-0213-commons), which occur in the different SPID-related standards.

The eCH-0213 is about managing SPID, there are 3 allowed actions on SPID  :
- generate : to generate a SPID from a submitted NAV13.
- inactivate : to inactivate a SPID
- cancel : to cancel a SPID

Each transaction is basically a SOAP Request which communicates with this WSDL endpoint : 
```
https://{{wildfly18.adress}}/upi-simulator/UPISedexManageWS?wsdl
```

There are 3 possible types of Response from Request :
- a positive Response : the Request succeed and the application returns the Response content regarding the submitted action on Spid.

- a positive Response with Warning (only for **generate**) : same as a "positive Response" but a warning is also returned with content with a warning code and its message.

- a negative Report : the Request failed and a Negative Report is returned with an error code and its message.
___
### Generating SPID
To generate a SPID from a NAV13 for a Person, mandatory elements are :
- actionOnSpid : has to be "generate" (case sensitive)
- a NAV13 : a 13-digit number greater or equal than 7560000000001 <br/>

And the 3 following demographic data :

- First name of the Person 
- Offical name of the Person
- Birthdate of the Person

The table below shows which content are returned regarding the correctness of Demographic Data.

| Number of correct <br>Demographic data | Returned Content                                                        |
|:----------------------------------------:|:------------------------------------------------------------------------|
|                                    1/3 | A Negative Report with code 310402 and its message.<br>                |
|                                    2/3 | A Positive Response with Warning with <br>code 210401 and its message. |
|                                    3/3 | A Positive Response.                                                   |

Example of Request for generate as action on SPID : 

```xml
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:tns="http://www.ech.ch/xmlns/eCH-0213/1"
  xmlns:src="http://www.ech.ch/xmlns/eCH-0058/5"
  xmlns:ns3="http://www.ech.ch/xmlns/eCH-0213-commons/1"
  xmlns:ns5="http://www.ech.ch/xmlns/eCH-0011/8"
  xmlns:ns6="http://www.ech.ch/xmlns/eCH-0044/4"
  xmlns:ns7="http://www.ech.ch/xmlns/eCH-0007/5"
  xmlns:ns8="http://www.ech.ch/xmlns/eCH-0008/3"
  xmlns:ns9="http://www.integic.ch/schema/upi/person/1"
  xmlns:json="urn:emds:json"
  xmlns:ns10="http://www.ech.ch/xmlns/eCH-0021/7">
	<soapenv:Header/>
	<soapenv:Body>
    <tns:request minorVersion="0">
   <tns:header>
    <src:senderId>sedex://T4-562840-2</src:senderId>
    <src:declarationLocalReference>integic AG</src:declarationLocalReference>
    <src:recipientId>sedex://T3-CH-24</src:recipientId>
    <src:messageId>eb265cf32eb94ea2a86d551f24cc0165</src:messageId>
    <src:ourBusinessReferenceId>Orchestra-000022</src:ourBusinessReferenceId>
    <src:uniqueIdBusinessTransaction>9a6c563124b04beab902635c7863ba6a</src:uniqueIdBusinessTransaction>
    <src:messageType>1020</src:messageType>
    <src:sendingApplication>
      <src:manufacturer>x-tention AG</src:manufacturer>
      <src:product>Orchestra</src:product>
      <src:productVersion>2.0.0</src:productVersion>
    </src:sendingApplication>
    <src:messageDate>2021-09-20T11:58:52Z</src:messageDate>
    <src:action>5</src:action>
    <src:testDeliveryFlag>true</src:testDeliveryFlag>
  </tns:header>
  <tns:content>
    <tns:SPIDCategory>EPD-ID.BAG.ADMIN.CH</tns:SPIDCategory>
    <tns:responseLanguage>DE</tns:responseLanguage>
    <tns:actionOnSPID>generate</tns:actionOnSPID>
    <tns:pidsToUPI>
      <ns3:vn>7560000000001</ns3:vn>
    </tns:pidsToUPI>
    <tns:personToUPI>
      <ns3:firstName>Enrique</ns3:firstName>
      <ns3:officialName>Iglesias</ns3:officialName>
      <ns3:sex>1</ns3:sex>
      <ns3:dateOfBirth>
        <ns6:yearMonthDay>1989-03-04</ns6:yearMonthDay>
      </ns3:dateOfBirth>
      <ns3:placeOfBirth>
        <ns5:unknown>0</ns5:unknown>
      </ns3:placeOfBirth>
      <ns3:nationalityData>
        <ns5:nationalityStatus>2</ns5:nationalityStatus>
        <ns5:countryInfo>
            <ns5:country>
              <ns8:countryId>8100</ns8:countryId>
              <ns8:countryNameShort>Switzerland</ns8:countryNameShort>
            </ns5:country>
          </ns5:countryInfo>
        </ns3:nationalityData>
      </tns:personToUPI>
    </tns:content>
  </tns:request>
  </soapenv:Body>
</soapenv:Envelope>
```
The UPI simulator should return **NAV13 from Request, Demographic Data from UPI and generated SPID** as following :<br/>
```
NAVS13 = 7560000000001
SPID = "73575"+NAVS13
SPID = 735757560000000001
```

Example of Positive Response for **generate** :
```xml
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
   <soap:Body>
      <ns10:response minorVersion="0" xmlns="http://www.ech.ch/xmlns/eCH-0058/5" xmlns:ns2="http://www.ech.ch/xmlns/eCH-0213/1" xmlns:ns3="http://www.ech.ch/xmlns/eCH-0213-commons/1" xmlns:ns4="http://www.ech.ch/xmlns/eCH-0044/4" xmlns:ns5="http://www.ech.ch/xmlns/eCH-0011/8" xmlns:ns6="http://www.ech.ch/xmlns/eCH-0007/5" xmlns:ns7="http://www.ech.ch/xmlns/eCH-0008/3" xmlns:ns8="http://www.ech.ch/xmlns/eCH-0021/7" xmlns:ns9="http://www.ech.ch/xmlns/eCH-0010/5" xmlns:ns10="http://www.ech.ch/xmlns/eCH-0213/">
         <ns2:positiveResponse>
            <ns2:SPIDCategory>EPD-ID.BAG.ADMIN.CH</ns2:SPIDCategory>
            <ns2:pids>
               <ns3:vn>7560000000001</ns3:vn>
               <ns3:SPID>735757560000000001</ns3:SPID>
            </ns2:pids>
            <ns2:personFromUPI>
               <ns3:recordTimestamp>2022-02-23T16:13:29Z</ns3:recordTimestamp>
               <ns3:firstName>Enrique</ns3:firstName>
               <ns3:officialName>Iglesias</ns3:officialName>
               <ns3:sex>2</ns3:sex>
               <ns3:dateOfBirth>
                  <ns4:yearMonthDay>1989-03-04</ns4:yearMonthDay>
               </ns3:dateOfBirth>
               <ns3:placeOfBirth/>
               <ns3:nationalityData>
                  <ns5:nationalityStatus>2</ns5:nationalityStatus>
                  <ns5:countryInfo>
                     <ns5:country>
                        <ns7:countryId>8100</ns7:countryId>
                        <ns7:countryNameShort>Switzerland<ns7:countryNameShort>
                     </ns5:country>
                  </ns5:countryInfo>
               </ns3:nationalityData>
            </ns2:personFromUPI>
         </ns2:positiveResponse>
      </ns10:response>
   </soap:Body>
</soap:Envelope>
```

Example of Positive Response with warning for **generate** :
```xml
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
   <soap:Body>
      <ns10:response minorVersion="0" xmlns="http://www.ech.ch/xmlns/eCH-0058/5" xmlns:ns2="http://www.ech.ch/xmlns/eCH-0213/1" xmlns:ns3="http://www.ech.ch/xmlns/eCH-0213-commons/1" xmlns:ns4="http://www.ech.ch/xmlns/eCH-0044/4" xmlns:ns5="http://www.ech.ch/xmlns/eCH-0011/8" xmlns:ns6="http://www.ech.ch/xmlns/eCH-0007/5" xmlns:ns7="http://www.ech.ch/xmlns/eCH-0008/3" xmlns:ns8="http://www.ech.ch/xmlns/eCH-0021/7" xmlns:ns9="http://www.ech.ch/xmlns/eCH-0010/5" xmlns:ns10="http://www.ech.ch/xmlns/eCH-0213/">
         <ns2:positiveResponse>
            <ns2:SPIDCategory>EPD-ID.BAG.ADMIN.CH</ns2:SPIDCategory>
            <ns2:warning>
               <ns3:code>210401</ns3:code>
               <ns3:descriptionLanguage>EN</ns3:descriptionLanguage>
               <ns3:codeDescription>The correspondence between the demographic data and the NAVS leaves some doubt as to the correct identification</ns3:codeDescription>
            </ns2:warning>
            <ns2:pids>
               <ns3:vn>7560520067196</ns3:vn>
               <ns3:SPID>735757560520067196</ns3:SPID>
            </ns2:pids>
            <ns2:personFromUPI>
               <ns3:recordTimestamp>2022-02-24T10:57:09Z</ns3:recordTimestamp>
               <ns3:firstName>Laura Mathilda Cheyenne</ns3:firstName>
               <ns3:officialName>VERBOUX</ns3:officialName>
               <ns3:sex>2</ns3:sex>
               <ns3:dateOfBirth>
                  <ns4:yearMonthDay>2000-09-02</ns4:yearMonthDay>
               </ns3:dateOfBirth>
               <ns3:placeOfBirth/>
               <ns3:nationalityData>
                  <ns5:nationalityStatus>2</ns5:nationalityStatus>
                  <ns5:countryInfo>
                     <ns5:country>
                        <ns7:countryId>8100</ns7:countryId>
                        <ns7:countryNameShort>Switzerland</ns7:countryNameShort>
                     </ns5:country>
                  </ns5:countryInfo>
               </ns3:nationalityData>
            </ns2:personFromUPI>
         </ns2:positiveResponse>
      </ns10:response>
   </soap:Body>
</soap:Envelope>
```
Table of error codes returned in Positive Report with Warning for generate action on SPID.

| Code   | Message                                                                                                            |
|--------|--------------------------------------------------------------------------------------------------------------------|
| 210401 | The correspondence between the demographic data <br>and the NAVS leaves some doubt as to the correct identification 

Example of Negative Report for generate :
```xml
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
   <soap:Body>
      <ns10:response minorVersion="0" xmlns="http://www.ech.ch/xmlns/eCH-0058/5" xmlns:ns2="http://www.ech.ch/xmlns/eCH-0213/1" xmlns:ns3="http://www.ech.ch/xmlns/eCH-0213-commons/1" xmlns:ns4="http://www.ech.ch/xmlns/eCH-0044/4" xmlns:ns5="http://www.ech.ch/xmlns/eCH-0011/8" xmlns:ns6="http://www.ech.ch/xmlns/eCH-0007/5" xmlns:ns7="http://www.ech.ch/xmlns/eCH-0008/3" xmlns:ns8="http://www.ech.ch/xmlns/eCH-0021/7" xmlns:ns9="http://www.ech.ch/xmlns/eCH-0010/5" xmlns:ns10="http://www.ech.ch/xmlns/eCH-0213/">
         <ns2:header>
            <senderId>sedex://T3-CH-24</senderId>
            <recipientId>sedex://T4-237196-8</recipientId>
            <messageId>99fddb13d9ba66776g6a6866b9c1222f</messageId>
            <referenceMessageId>62fdee70d9ea77646f6e8686a3f9332e</referenceMessageId>
            <ourBusinessReferenceId>AQ</ourBusinessReferenceId>
            <yourBusinessReferenceId>service d'admission</yourBusinessReferenceId>
            <uniqueIdBusinessTransaction>74738ff5536759589aee98fffdcd1876</uniqueIdBusinessTransaction>
            <messageType>1020</messageType>
            <sendingApplication>
               <manufacturer>regcent.zas.admin.ch</manufacturer>
               <product>WUPISPID</product>
               <productVersion>1.0</productVersion>
            </sendingApplication>
            <messageDate>2022-02-24T12:01:04Z</messageDate>
            <action>6</action>
            <testDeliveryFlag>true</testDeliveryFlag>
         </ns2:header>
         <ns2:negativeReport>
            <ns3:notice>
               <ns3:code>310402</ns3:code>
               <ns3:descriptionLanguage>EN</ns3:descriptionLanguage>
               <ns3:codeDescription>There is no correspondence between the demographic data and the announced NAVS13.</ns3:codeDescription>
               <ns3:comment/>
            </ns3:notice>
            <ns3:data xmlns:eCH-0007="http://www.ech.ch/xmlns/eCH-0007/5" xmlns:eCH-0008="http://www.ech.ch/xmlns/eCH-0008/3" xmlns:eCH-0011="http://www.ech.ch/xmlns/eCH-0011/8" xmlns:eCH-0021="http://www.ech.ch/xmlns/eCH-0021/7" xmlns:eCH-0044="http://www.ech.ch/xmlns/eCH-0044/4" xmlns:eCH-0058="http://www.ech.ch/xmlns/eCH-0058/5" xmlns:eCH-0213="http://www.ech.ch/xmlns/eCH-0213/1" xmlns:eCH-0213-commons="http://www.ech.ch/xmlns/eCH-0213-commons/1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"></ns3:data>
         </ns2:negativeReport>
      </ns10:response>
   </soap:Body>
</soap:Envelope>
```

Table of error codes returned in Negative Report for generate action on spid.

| Code   | Message                                                                                                            |
|--------|--------------------------------------------------------------------------------------------------------------------|
| 300001 | The structure of the announcement generated <br>from the original announcement is not correct (not valid XSD)<br>  |
| 300501 | The actionOnSPID element contains an unexpected value                                                              |
| 310402 | There is no correspondence between <br>the demographic data and the announced NAVS13                               |
___
### Inactivating SPID
To inactivate a SPID mandatory elements are :
- action on SPID : has to be "inactivate" (case sensitive)
- an active SPID
- a SPID to inactivate

In the case of the UPI simulator, the **SPID to inactive is the same** for any Person : 73575701N4C71V4730.

Example of Request for inactivate action on SPID :

```xml
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:tns="http://www.ech.ch/xmlns/eCH-0213/1"
  xmlns:src="http://www.ech.ch/xmlns/eCH-0058/5"
  xmlns:ns3="http://www.ech.ch/xmlns/eCH-0213-commons/1"
  xmlns:ns5="http://www.ech.ch/xmlns/eCH-0011/8"
  xmlns:ns6="http://www.ech.ch/xmlns/eCH-0044/4"
  xmlns:ns7="http://www.ech.ch/xmlns/eCH-0007/5"
  xmlns:ns8="http://www.ech.ch/xmlns/eCH-0008/3"
  xmlns:ns9="http://www.integic.ch/schema/upi/person/1"
  xmlns:json="urn:emds:json"
  xmlns:ns10="http://www.ech.ch/xmlns/eCH-0021/7">
	<soapenv:Header/>
	<soapenv:Body>
    <tns:request  minorVersion="0">
   <tns:header>
    <src:senderId>sedex://T4-562840-2</src:senderId>
    <src:declarationLocalReference>integic AG</src:declarationLocalReference>
    <src:recipientId>sedex://T3-CH-24</src:recipientId>
    <src:messageId>eb265cf32eb94ea2a86d551f24cc0165</src:messageId>
    <src:ourBusinessReferenceId>Orchestra-000022</src:ourBusinessReferenceId>
    <src:uniqueIdBusinessTransaction>9a6c563124b04beab902635c7863ba6a</src:uniqueIdBusinessTransaction>
    <src:messageType>1020</src:messageType>
    <src:sendingApplication>
      <src:manufacturer>x-tention AG</src:manufacturer>
      <src:product>Orchestra</src:product>
      <src:productVersion>2.0.0</src:productVersion>
    </src:sendingApplication>
    <src:messageDate>2021-09-20T11:58:52Z</src:messageDate>
    <src:action>5</src:action>
    <src:testDeliveryFlag>true</src:testDeliveryFlag>
  </tns:header>
  <tns:content>
    <tns:SPIDCategory>EPD-ID.BAG.ADMIN.CH</tns:SPIDCategory>
    <tns:responseLanguage>DE</tns:responseLanguage>
    <tns:actionOnSPID>inactivate</tns:actionOnSPID>
    <tns:pidsToUPI>
      <ns3:SPID>761337610506643086</ns3:SPID>
    </tns:pidsToUPI>
    <tns:pidsToUPI>
      <ns3:SPID>73575701N4C71V4730</ns3:SPID>
    </tns:pidsToUPI>
    </tns:content>
  </tns:request>
  </soapenv:Body>
</soapenv:Envelope>
```

The Response should return **the active SPID and Demographic Data of the Person from the active SPID**.

Example of Positive Response for **inactivate** action on SPID :

```xml
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
   <soap:Body>
      <ns10:response minorVersion="0" xmlns="http://www.ech.ch/xmlns/eCH-0058/5" xmlns:ns2="http://www.ech.ch/xmlns/eCH-0213/1" xmlns:ns3="http://www.ech.ch/xmlns/eCH-0213-commons/1" xmlns:ns4="http://www.ech.ch/xmlns/eCH-0044/4" xmlns:ns5="http://www.ech.ch/xmlns/eCH-0011/8" xmlns:ns6="http://www.ech.ch/xmlns/eCH-0007/5" xmlns:ns7="http://www.ech.ch/xmlns/eCH-0008/3" xmlns:ns8="http://www.ech.ch/xmlns/eCH-0021/7" xmlns:ns9="http://www.ech.ch/xmlns/eCH-0010/5" xmlns:ns10="http://www.ech.ch/xmlns/eCH-0213/">
         <ns2:positiveResponse>
            <ns2:SPIDCategory>EPD-ID.BAG.ADMIN.CH</ns2:SPIDCategory>
            <ns2:pids>
               <ns3:SPID>761337610506643086</ns3:SPID>
            </ns2:pids>
            <ns2:personFromUPI>
               <ns3:recordTimestamp>2022-02-23T16:53:54Z</ns3:recordTimestamp>
               <ns3:firstName>Cyril René Emmanuel</ns3:firstName>
               <ns3:officialName>SALLES</ns3:officialName>
               <ns3:sex>1</ns3:sex>
               <ns3:dateOfBirth>
                  <ns4:yearMonthDay>1996-02-05</ns4:yearMonthDay>
               </ns3:dateOfBirth>
               <ns3:placeOfBirth/>
               <ns3:nationalityData>
                  <ns5:countryInfo>
                     <ns5:country/>
                  </ns5:countryInfo>
               </ns3:nationalityData>
            </ns2:personFromUPI>
         </ns2:positiveResponse>
      </ns10:response>
   </soap:Body>
</soap:Envelope>
```
Example of Negative Report for **inactivate** : <br/> 
- see example of Negative Report for **generate**

Here are the error codes returned in Negative Report for **inactive** action on SPID.

| Code   | Message                                                                                                            |
|--------|--------------------------------------------------------------------------------------------------------------------|
| 300001 | The structure of the announcement generated <br>from the original announcement is not correct (not valid XSD)<br>  |
| 300501 | The actionOnSPID element contains an unexpected value                                                              |
| 307101 | The presence of SPID is mandatory
___
### Cancelling SPID

To cancel a SPID mandatory elements are :
- action on SPID : has to be "**cancel**" (case sensitive)
- a SPID to cancel

Example of Request for **cancel** action on SPID :

```xml
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:tns="http://www.ech.ch/xmlns/eCH-0213/1"
  xmlns:src="http://www.ech.ch/xmlns/eCH-0058/5"
  xmlns:ns3="http://www.ech.ch/xmlns/eCH-0213-commons/1"
  xmlns:ns5="http://www.ech.ch/xmlns/eCH-0011/8"
  xmlns:ns6="http://www.ech.ch/xmlns/eCH-0044/4"
  xmlns:ns7="http://www.ech.ch/xmlns/eCH-0007/5"
  xmlns:ns8="http://www.ech.ch/xmlns/eCH-0008/3"
  xmlns:ns9="http://www.integic.ch/schema/upi/person/1"
  xmlns:json="urn:emds:json"
  xmlns:ns10="http://www.ech.ch/xmlns/eCH-0021/7">
	<soapenv:Header/>
	<soapenv:Body>
    <tns:request  minorVersion="0">
   <tns:header>
    <src:senderId>sedex://T4-562840-2</src:senderId>
    <src:declarationLocalReference>integic AG</src:declarationLocalReference>
    <src:recipientId>sedex://T3-CH-24</src:recipientId>
    <src:messageId>eb265cf32eb94ea2a86d551f24cc0165</src:messageId>
    <src:ourBusinessReferenceId>Orchestra-000022</src:ourBusinessReferenceId>
    <src:uniqueIdBusinessTransaction>9a6c563124b04beab902635c7863ba6a</src:uniqueIdBusinessTransaction>
    <src:messageType>1020</src:messageType>
    <src:sendingApplication>
      <src:manufacturer>x-tention AG</src:manufacturer>
      <src:product>Orchestra</src:product>
      <src:productVersion>2.0.0</src:productVersion>
    </src:sendingApplication>
    <src:messageDate>2021-09-20T11:58:52Z</src:messageDate>
    <src:action>5</src:action>
    <src:testDeliveryFlag>true</src:testDeliveryFlag>
  </tns:header>
  <tns:content>
    <tns:SPIDCategory>EPD-ID.BAG.ADMIN.CH</tns:SPIDCategory>
    <tns:responseLanguage>DE</tns:responseLanguage>
    <tns:actionOnSPID>cancel</tns:actionOnSPID>
    <tns:pidsToUPI>
      <ns3:SPID>761337610282287788</ns3:SPID>
    </tns:pidsToUPI>
    </tns:content>
  </tns:request>
  </soapenv:Body>
</soapenv:Envelope>
```

The Response should return **all SPID and the NAV13 of the Person and Demographic Data** who own the SPID to cancel.

Example of Positive Response for **cancel** action on SPID :

```xml
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
   <soap:Body>
      <ns10:response minorVersion="0" xmlns="http://www.ech.ch/xmlns/eCH-0058/5" xmlns:ns2="http://www.ech.ch/xmlns/eCH-0213/1" xmlns:ns3="http://www.ech.ch/xmlns/eCH-0213-commons/1" xmlns:ns4="http://www.ech.ch/xmlns/eCH-0044/4" xmlns:ns5="http://www.ech.ch/xmlns/eCH-0011/8" xmlns:ns6="http://www.ech.ch/xmlns/eCH-0007/5" xmlns:ns7="http://www.ech.ch/xmlns/eCH-0008/3" xmlns:ns8="http://www.ech.ch/xmlns/eCH-0021/7" xmlns:ns9="http://www.ech.ch/xmlns/eCH-0010/5" xmlns:ns10="http://www.ech.ch/xmlns/eCH-0213/">
         <ns2:positiveResponse>
            <ns2:SPIDCategory>EPD-ID.BAG.ADMIN.CH</ns2:SPIDCategory>
            <ns2:pids>
               <ns3:vn>7560507216456</ns3:vn>
               <ns3:SPID>761337610328722006</ns3:SPID>
               <ns3:SPID>761337610507216456</ns3:SPID>
            </ns2:pids>
            <ns2:personFromUPI>
               <ns3:recordTimestamp>2022-02-23T17:28:18Z</ns3:recordTimestamp>
               <ns3:firstName>DANIELA BALBINA</ns3:firstName>
               <ns3:officialName>Correia Santos</ns3:officialName>
               <ns3:sex>2</ns3:sex>
               <ns3:dateOfBirth>
                  <ns4:yearMonthDay>1974-01-14</ns4:yearMonthDay>
               </ns3:dateOfBirth>
               <ns3:placeOfBirth/>
               <ns3:nationalityData>
                  <ns5:countryInfo>
                     <ns5:country/>
                  </ns5:countryInfo>
               </ns3:nationalityData>
            </ns2:personFromUPI>
         </ns2:positiveResponse>
      </ns10:response>
   </soap:Body>
</soap:Envelope>
```
Example of Negative Report for **cancel** : <br/> 
- see example of Negative Report for **generate**

Here are the error codes returned in Negative Report for **cancel** action on SPID.

| Code   | Message                                                                                                            |
|--------|--------------------------------------------------------------------------------------------------------------------|
| 300001 | The structure of the announcement generated <br>from the original announcement is not correct (not valid XSD)<br>  |
| 300501 | The actionOnSPID element contains an unexpected value                                                              |
| 307101 | The presence of SPID is mandatory
___
## eCH-0214 : Query SPID
___

The eCH-0214 interface allows to query the attributes stored in the UPI for a specific person. eCH-0214 provides for three query possibilities:
- Searching for the attributes of a person by means of an identifier
- Searching for a person's attributes by demographic attributes
- Comparison with data in the UPI concerning the link between SPID and NAVS

Only ONE of the three queries is possible in ONE request, combinations of queries (for example search by identifier AND by demographic) are NOT allowed.

Each transaction is basically a SOAP Request which communicates with this WSDL endpoint : 
```
https://{{wildfly18.adress}}/upi-simulator/UPISedexQueryWS?wsdl
```

For any query the 3 tags MUST NOT be empty or absent:
- a content
- a SPID Category 
- a response language

The UPI Simulator should return either a Positive Response or a Negative Report.

For simplification, header in messages will be shown only once.
___
### Searching for the attributes of a person by means of an identifier

In this query, the mandatory fields are either:
- NAV13
- SPID

And it should return a Positive Response with Demographic Data of one or several Persons regarding the number of the given identifier in the Request.

Example of Request for searching the attribute of a person by NAV13

```xml
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <soapenv:Header/>
    <soapenv:Body>
        <eCH-0214:request minorVersion="0"
                          xmlns:eCH-0058="http://www.ech.ch/xmlns/eCH-0058/5"
                          xmlns:eCH-0214="http://www.ech.ch/xmlns/eCH-0214/2"
                          xsi:schemaLocation="http://www.ech.ch/xmlns/eCH-0214/2 http://www.ech.ch/xmlns/eCH-0214/2/eCH-0214-2-0.xsd">
            <eCH-0214:header>
                <eCH-0058:senderId>sedex://T4-237196-8</eCH-0058:senderId>
                <eCH-0058:declarationLocalReference>Hôpital XYZ</eCH-0058:declarationLocalReference>
                <eCH-0058:recipientId>sedex://T3-CH-24</eCH-0058:recipientId>
                <eCH-0058:messageId>62fdee70d9ea77646f6e8686a3f9332e</eCH-0058:messageId>
                <eCH-0058:ourBusinessReferenceId>service d'admission</eCH-0058:ourBusinessReferenceId>
                <eCH-0058:uniqueIdBusinessTransaction>74738ff5536759589aee98fffdcd1876</eCH-0058:uniqueIdBusinessTransaction>
                <eCH-0058:messageType>1021</eCH-0058:messageType>
                <eCH-0058:sendingApplication>
                    <eCH-0058:manufacturer>MonEntreprise</eCH-0058:manufacturer>
                    <eCH-0058:product>MonProduit</eCH-0058:product>
                    <eCH-0058:productVersion>1.1</eCH-0058:productVersion>
                </eCH-0058:sendingApplication>
                <eCH-0058:messageDate>2016-11-17T09:30:47Z</eCH-0058:messageDate>
                <eCH-0058:action>5</eCH-0058:action>
                <eCH-0058:testDeliveryFlag>true</eCH-0058:testDeliveryFlag>
            </eCH-0214:header>
            <eCH-0214:content>
                <eCH-0214:SPIDCategory>EPD-ID.BAG.ADMIN.CH</eCH-0214:SPIDCategory>
                <eCH-0214:responseLanguage>FR</eCH-0214:responseLanguage>
                <eCH-0214:getInfoPersonRequest>
                    <eCH-0214:getInfoPersonRequestId>1</eCH-0214:getInfoPersonRequestId>
                    <eCH-0214:detailLevelOfResponse>standard</eCH-0214:detailLevelOfResponse>
                    <eCH-0214:pid>
                        <eCH-0214:vn>7560409928839</eCH-0214:vn>
                    </eCH-0214:pid>
                </eCH-0214:getInfoPersonRequest>
                <eCH-0214:getInfoPersonRequest>
                    <eCH-0214:getInfoPersonRequestId>2</eCH-0214:getInfoPersonRequestId>
                    <eCH-0214:detailLevelOfResponse>standard</eCH-0214:detailLevelOfResponse>
                    <eCH-0214:pid>
                        <eCH-0214:vn>7560409957945</eCH-0214:vn>
                    </eCH-0214:pid>
                </eCH-0214:getInfoPersonRequest>
            </eCH-0214:content>
        </eCH-0214:request>
    </soapenv:Body>
</soapenv:Envelope>
```
Example of Request for searching the attribute of a person by SPID:<br>
Same as searching by NAV13, only tags incuded in parent \<eCH-0214:pid> change from "vn" to "SPID".

```xml
<eCH-0214:pid>
   <eCH-0214:SPID>761337610409928839</eCH-0214:SPID>
</eCH-0214:pid>
```

Example of Positive Response for searching the attribute of a person by either NAV13 or SPID
```xml
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
   <soap:Body>
      <ns2:response minorVersion="0" xmlns="http://www.ech.ch/xmlns/eCH-0058/5" xmlns:ns2="http://www.ech.ch/xmlns/eCH-0214/2" xmlns:ns3="http://www.ech.ch/xmlns/eCH-0213-commons/1" xmlns:ns4="http://www.ech.ch/xmlns/eCH-0044/4" xmlns:ns5="http://www.ech.ch/xmlns/eCH-0011/8" xmlns:ns6="http://www.ech.ch/xmlns/eCH-0007/5" xmlns:ns7="http://www.ech.ch/xmlns/eCH-0008/3" xmlns:ns8="http://www.ech.ch/xmlns/eCH-0021/7" xmlns:ns9="http://www.ech.ch/xmlns/eCH-0010/5">
         <ns2:header/>
         <ns2:positiveResponse>
            <ns2:SPIDCategory>EPD-ID.BAG.ADMIN.CH</ns2:SPIDCategory>
            <ns2:getInfoPersonResponse>
               <ns2:getInfoPersonRequestId>1</ns2:getInfoPersonRequestId>
               <ns2:echoPidRequest>
                  <ns2:SPID>761337610409928839</ns2:SPID>
               </ns2:echoPidRequest>
               <ns2:pids>
                  <ns3:vn>7560409928839</ns3:vn>
                  <ns3:SPID>761337610409928839</ns3:SPID>
               </ns2:pids>
               <ns2:personFromUPI>
                  <ns3:recordTimestamp>2022-02-24T16:38:06Z</ns3:recordTimestamp>
                  <ns3:firstName>FRIEDA MARIE</ns3:firstName>
                  <ns3:officialName>NECIPOGLU</ns3:officialName>
                  <ns3:sex>2</ns3:sex>
                  <ns3:dateOfBirth>
                     <ns4:yearMonthDay>1968-04-09</ns4:yearMonthDay>
                  </ns3:dateOfBirth>
                  <ns3:nationalityData>
                     <ns5:nationalityStatus>2</ns5:nationalityStatus>
                     <ns5:countryInfo>
                        <ns5:country>
                           <ns7:countryId>0</ns7:countryId>
                           <ns7:countryNameShort/>
                        </ns5:country>
                     </ns5:countryInfo>
                  </ns3:nationalityData>
               </ns2:personFromUPI>
            </ns2:getInfoPersonResponse>
         </ns2:positiveResponse>
      </ns2:response>
   </soap:Body>
</soap:Envelope>
```
____
### Searching for the attributes of a person by Demographic Attribute

In this query, the mandatory fields are:
- First Name
- Official Name
- Birthday

And it should return a Positive Response with NAV13 and SPID of the requested Person and its Demographic Data.

Example of Request for searching the attribute of a person by Demographic Attribute

```xml
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <soapenv:Header/>
    <soapenv:Body>
        <eCH-0214:request minorVersion="0"
                          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                          xmlns:eCH-0007="http://www.ech.ch/xmlns/eCH-0007/5"
                          xmlns:eCH-0008="http://www.ech.ch/xmlns/eCH-0008/3"
                          xmlns:eCH-0011="http://www.ech.ch/xmlns/eCH-0011/8"
                          xmlns:eCH-0021="http://www.ech.ch/xmlns/eCH-0021/7"
                          xmlns:eCH-0044="http://www.ech.ch/xmlns/eCH-0044/4"
                          xmlns:eCH-0058="http://www.ech.ch/xmlns/eCH-0058/5"
                          xmlns:eCH-0213-commons="http://www.ech.ch/xmlns/eCH-0213-commons/1"
                          xmlns:eCH-0214="http://www.ech.ch/xmlns/eCH-0214/2"
                          xsi:schemaLocation="http://www.ech.ch/xmlns/eCH-0214/2 http://www.ech.ch/xmlns/eCH-0214/2/eCH-0214-2-0.xsd">
            <eCH-0214:header/>
            <eCH-0214:content>
                <eCH-0214:SPIDCategory>EPD-ID.BAG.ADMIN.CH</eCH-0214:SPIDCategory>
                <eCH-0214:responseLanguage>FR</eCH-0214:responseLanguage>
                <eCH-0214:searchPersonRequest>
                    <eCH-0214:searchPersonRequestId>1</eCH-0214:searchPersonRequestId>
                    <eCH-0214:algorithm>default</eCH-0214:algorithm>
                    <eCH-0214:searchedPerson>
                        <eCH-0213-commons:firstName>FRIEDA MARIE</eCH-0213-commons:firstName>
                        <eCH-0213-commons:officialName>NECIPOGLU</eCH-0213-commons:officialName>
                        <eCH-0213-commons:sex>1</eCH-0213-commons:sex>
                        <eCH-0213-commons:dateOfBirth>
                            <eCH-0044:yearMonthDay>1968-04-09</eCH-0044:yearMonthDay>
                        </eCH-0213-commons:dateOfBirth>
                        <eCH-0213-commons:placeOfBirth>
                            <eCH-0011:swissTown>
                                <eCH-0007:municipalityName>Buchs (ZH)</eCH-0007:municipalityName>
                                <eCH-0007:cantonAbbreviation >SG</eCH-0007:cantonAbbreviation >
                                <eCH-0007:historyMunicipalityId>10080</eCH-0007:historyMunicipalityId>
                            </eCH-0011:swissTown>
                        </eCH-0213-commons:placeOfBirth>
                        <eCH-0213-commons:mothersName>
                            <eCH-0021:firstName>Marie Anna</eCH-0021:firstName>
                            <eCH-0021:officialName>Müller</eCH-0021:officialName>
                        </eCH-0213-commons:mothersName>
                        <eCH-0213-commons:fathersName>
                            <eCH-0021:firstName>Johannes</eCH-0021:firstName>
                            <eCH-0021:officialName>Müller</eCH-0021:officialName>
                        </eCH-0213-commons:fathersName>
                        <eCH-0213-commons:nationalityData>
                            <eCH-0011:nationalityStatus>2</eCH-0011:nationalityStatus>
                            <eCH-0011:countryInfo>
                                <eCH-0011:country>
                                    <eCH-0008:countryId>8100</eCH-0008:countryId>
                                    <eCH-0008:countryNameShort>Suisse</eCH-0008:countryNameShort>
                                </eCH-0011:country>
                            </eCH-0011:countryInfo>
                        </eCH-0213-commons:nationalityData>
                    </eCH-0214:searchedPerson>
                </eCH-0214:searchPersonRequest>
            </eCH-0214:content>
        </eCH-0214:request>
    </soapenv:Body>
</soapenv:Envelope>
```
Example of Postive Response for searching the attribute of a person by Demographic Attribute

```xml
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
   <soap:Body>
      <ns2:response minorVersion="0" xmlns="http://www.ech.ch/xmlns/eCH-0058/5" xmlns:ns2="http://www.ech.ch/xmlns/eCH-0214/2" xmlns:ns3="http://www.ech.ch/xmlns/eCH-0213-commons/1" xmlns:ns4="http://www.ech.ch/xmlns/eCH-0044/4" xmlns:ns5="http://www.ech.ch/xmlns/eCH-0011/8" xmlns:ns6="http://www.ech.ch/xmlns/eCH-0007/5" xmlns:ns7="http://www.ech.ch/xmlns/eCH-0008/3" xmlns:ns8="http://www.ech.ch/xmlns/eCH-0021/7" xmlns:ns9="http://www.ech.ch/xmlns/eCH-0010/5">
         <ns2:header/>
         <ns2:positiveResponse>
            <ns2:SPIDCategory>EPD-ID.BAG.ADMIN.CH</ns2:SPIDCategory>
            <ns2:searchPersonResponse>
               <ns2:searchPersonRequestId>1</ns2:searchPersonRequestId>
               <ns2:notice>
                  <ns2:code>0</ns2:code>
                  <ns2:descriptionLanguage>FR</ns2:descriptionLanguage>
                  <ns2:codeDescription/>
                  <ns2:comment/>
               </ns2:notice>
               <ns2:found>
                  <ns2:pids>
                     <ns3:vn>7560409928839</ns3:vn>
                     <ns3:SPID>761337610409928839</ns3:SPID>
                  </ns2:pids>
                  <ns2:personFromUPI>
                     <ns3:recordTimestamp>2022-02-24T17:04:30Z</ns3:recordTimestamp>
                     <ns3:firstName>FRIEDA MARIE</ns3:firstName>
                     <ns3:officialName>NECIPOGLU</ns3:officialName>
                     <ns3:sex>2</ns3:sex>
                     <ns3:dateOfBirth>
                        <ns4:yearMonthDay>1968-04-09</ns4:yearMonthDay>
                     </ns3:dateOfBirth>
                     <ns3:placeOfBirth>
                        <ns5:swissTown>
                           <ns6:municipalityName/>
                           <ns6:historyMunicipalityId>0</ns6:historyMunicipalityId>
                        </ns5:swissTown>
                     </ns3:placeOfBirth>
                     <ns3:mothersName>
                        <ns8:firstName></ns8:firstName>
                        <ns8:officialName/>
                     </ns3:mothersName>
                     <ns3:fathersName>
                        <ns8:firstName></ns8:firstName>
                        <ns8:officialName/>
                     </ns3:fathersName>
                     <ns3:nationalityData>
                        <ns5:nationalityStatus>2</ns5:nationalityStatus>
                        <ns5:countryInfo>
                           <ns5:country>
                              <ns7:countryId>0</ns7:countryId>
                              <ns7:countryNameShort>UNKNOWN</ns7:countryNameShort>
                           </ns5:country>
                        </ns5:countryInfo>
                     </ns3:nationalityData>
                  </ns2:personFromUPI>
               </ns2:found>
            </ns2:searchPersonResponse>
         </ns2:positiveResponse>

```

___
### Comparison with data in the UPI concerning the link between SPID and NAVS

In this query, the mandatory fields are:
- NAV13
- SPID

And it should return a Positive Response with NAV13 and SPID of the Request with two different state :
- identical Data : when both SPID and NAV13 are active and linked together
- different Data : when either NAV13 or SPID is not active or are not linked together


Example of Request for Comparison with data in the UPI concerning the link between SPID and NAVS
```xml
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <soapenv:Header/>
    <soapenv:Body>
        <eCH-0214:request minorVersion="0"
                          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                          xmlns:eCH-0058="http://www.ech.ch/xmlns/eCH-0058/5"
                          xmlns:eCH-0214="http://www.ech.ch/xmlns/eCH-0214/2"
                          xsi:schemaLocation="http://www.ech.ch/xmlns/eCH-0214/2 http://www.ech.ch/xmlns/eCH-0214/2/eCH-0214-2-0.xsd">
            <eCH-0214:header/>
            <eCH-0214:content>
                <eCH-0214:SPIDCategory>EPD-ID.BAG.ADMIN.CH</eCH-0214:SPIDCategory>
                <eCH-0214:responseLanguage>FR</eCH-0214:responseLanguage>
                <eCH-0214:compareDataRequest>
                    <eCH-0214:compareDataRequestId>1</eCH-0214:compareDataRequestId>
                    <eCH-0214:pids>
                        <eCH-0214:vn>7560409957945</eCH-0214:vn>
                        <eCH-0214:SPID>761337610409957945</eCH-0214:SPID>
                    </eCH-0214:pids>
                </eCH-0214:compareDataRequest>
            </eCH-0214:content>
        </eCH-0214:request>
    </soapenv:Body>
</soapenv:Envelope>
```
Example of positive Response for Comparison with data in the UPI concerning the link between SPID and NAVS
```xml
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
   <soap:Body>
      <ns2:response minorVersion="0" xmlns="http://www.ech.ch/xmlns/eCH-0058/5" xmlns:ns2="http://www.ech.ch/xmlns/eCH-0214/2" xmlns:ns3="http://www.ech.ch/xmlns/eCH-0213-commons/1" xmlns:ns4="http://www.ech.ch/xmlns/eCH-0044/4" xmlns:ns5="http://www.ech.ch/xmlns/eCH-0011/8" xmlns:ns6="http://www.ech.ch/xmlns/eCH-0007/5" xmlns:ns7="http://www.ech.ch/xmlns/eCH-0008/3" xmlns:ns8="http://www.ech.ch/xmlns/eCH-0021/7" xmlns:ns9="http://www.ech.ch/xmlns/eCH-0010/5">
         <ns2:header/>
         <ns2:positiveResponse>
            <ns2:SPIDCategory>EPD-ID.BAG.ADMIN.CH</ns2:SPIDCategory>
            <ns2:compareDataResponse>
               <ns2:compareDataRequestId>1</ns2:compareDataRequestId>
               <ns2:echoPidsRequest>
                  <ns2:vn>7560409957945</ns2:vn>
                  <ns2:SPID>761337610409957945</ns2:SPID>
               </ns2:echoPidsRequest>
               <ns2:identicalData xmlns:eCH-0007="http://www.ech.ch/xmlns/eCH-0007/5" xmlns:eCH-0008="http://www.ech.ch/xmlns/eCH-0008/3" xmlns:eCH-0011="http://www.ech.ch/xmlns/eCH-0011/8" xmlns:eCH-0021="http://www.ech.ch/xmlns/eCH-0021/7" xmlns:eCH-0044="http://www.ech.ch/xmlns/eCH-0044/4" xmlns:eCH-0058="http://www.ech.ch/xmlns/eCH-0058/5" xmlns:eCH-0213="http://www.ech.ch/xmlns/eCH-0213/1" xmlns:eCH-0213-commons="http://www.ech.ch/xmlns/eCH-0213-commons/1" xmlns:eCH-0214="http://www.ech.ch/xmlns/eCH-0214/2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>
            </ns2:compareDataResponse>
         </ns2:positiveResponse>
      </ns2:response>
   </soap:Body>
</soap:Envelope>
```
___
### Negative Report

For any of the three queries, if submitted is not fullfilled with correct fields a Negative Report is returned with an error message.

Example of Negative Report with Null Content inside.

```xml
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
   <soap:Body>
      <ns2:response minorVersion="0" xmlns="http://www.ech.ch/xmlns/eCH-0058/5" xmlns:ns2="http://www.ech.ch/xmlns/eCH-0214/2" xmlns:ns3="http://www.ech.ch/xmlns/eCH-0213-commons/1" xmlns:ns4="http://www.ech.ch/xmlns/eCH-0044/4" xmlns:ns5="http://www.ech.ch/xmlns/eCH-0011/8" xmlns:ns6="http://www.ech.ch/xmlns/eCH-0007/5" xmlns:ns7="http://www.ech.ch/xmlns/eCH-0008/3" xmlns:ns8="http://www.ech.ch/xmlns/eCH-0021/7" xmlns:ns9="http://www.ech.ch/xmlns/eCH-0010/5">
         <ns2:header/>
         <ns2:negativeReport>
            <ns3:notice>
               <ns3:code>0</ns3:code>
               <ns3:descriptionLanguage/>
               <ns3:codeDescription>Le contenu de la requête est null</ns3:codeDescription>
               <ns3:comment/>
            </ns3:notice>
            <ns3:data xmlns:eCH-0007="http://www.ech.ch/xmlns/eCH-0007/5" xmlns:eCH-0008="http://www.ech.ch/xmlns/eCH-0008/3" xmlns:eCH-0011="http://www.ech.ch/xmlns/eCH-0011/8" xmlns:eCH-0021="http://www.ech.ch/xmlns/eCH-0021/7" xmlns:eCH-0044="http://www.ech.ch/xmlns/eCH-0044/4" xmlns:eCH-0058="http://www.ech.ch/xmlns/eCH-0058/5" xmlns:eCH-0213="http://www.ech.ch/xmlns/eCH-0213/1" xmlns:eCH-0213-commons="http://www.ech.ch/xmlns/eCH-0213-commons/1" xmlns:eCH-0214="http://www.ech.ch/xmlns/eCH-0214/2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>
         </ns2:negativeReport>
      </ns2:response>
   </soap:Body>
</soap:Envelope>
```
___
## Bibliography

- [General documentation for EPD-ID standard with terms description](https://www.zas.admin.ch/dam/zas/fr/dokumente/Partenaires%20et%20institutions/UPI/R%C3%A8glement%20de%20traitement%20EPD-ID%20-%20v1.3%20-%20F%20-%20Pub.pdf.download.pdf/R%C3%A8glement%20de%20traitement%20EPD-ID%20-%20v1.3%20-%20F%20-%20Pub.pdf)

- [eCH-0213 specifications](https://www.ech.ch/fr/standards/60607)
- [eCH-0214 specifications](https://www.ech.ch/fr/standards/60275)
- [eCH-0215 specifications](https://www.ech.ch/fr/standards/60154)








