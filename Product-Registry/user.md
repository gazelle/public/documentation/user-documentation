---
subtitle:  Product Registry
title: User Manual
author: Guillaume THOMAZON
date: 08/02/2017
toolversion: 5.4.x
function: Engineer
version: 1.02
status: Approved document
reference: KER1-MAN-IHE-PRODUCT_REGISTRY_USER-1_02
customer: IHE-EUROPE
---
# Introduction
## About the IHE Product Registry

The IHE Product Registry is a way to register and search products supporting IHE Profiles with published IHE Integration Statements. IHE Integration Statements are documents prepared and published by vendors to describe the conformance of their products with the IHE Technical Framework. They identify the specific IHE capabilities a given product supports in terms of IHE actors and integration profiles. Users familiar with these concepts can use Integration Statements to determine what level of integration a vendor asserts a product supports with complementary systems and what clinical and operational benefits such integration might provide. IHE provides a process for vendors to test their implementations of IHE profiles. The IHE testing process starts with testing against IHE test Tools and culminates in a multi-party interactive testing event called the Connect-a-thon. The process is not intended to independently evaluate, or ensure, product compliance, but to offer a practical and effective assessment of interoperability. View the [powerpoint presentation](http://gazelle.ihe.net/files/ProductRegistry.pdf) on the product registry.

## How does it work ?

### 1- Register and reference IHE Integration Statement (IIS)

There are 2 ways to register an IHE Integration Statement (IIS). First way, the vendor has already an IIS :  
![](./media/register-IIS-1.png)

1. Vendor has an existing IIS published on the company website, this is often a PDF document
2. Vendor registers the IIS in the Product Registry, indicating the content of the IIS (actors, profiles, ...) and the URL of this document on the company website
3. After the admin verification of the IIS content, the Product Registry references the IIS. This referencement allows any user to find it, performing a search in the Product Registry

Second way, the vendor generates an IIS using the Product Registry and does not wait the admin verification :  
![](./media/register-IIS-2.png)

1. Vendor registers the IIS in the Product Registry, indicating the content of the IIS (actors, profiles, ...)
2. Vendor generates the IIS document (PDF file) and download it on his computer
3. Vendor published the IIS document (PDF file) on the company website, to make it reachable by any one
4. Vendor updates the declaration of the IIS within the Product Registry updating  URL to the published IIS on the company website.
5. Product Registry recognizes the generated IIS and references it automatically. This referencement allows any user to find it, performing a search in the Product Registry

### 2- Search and find IIS depending on search criteria

![](./media/search-IIS.png)

1. A user queries the Product Registry to find IIS depending on criteria (domain, profile, actor, company...)
2. This user gets from the Product Registry the list of existing IIS, with the associated URL
3. User clicks on the URL and go directly on the IIS located on the vendor website. He can view the IIS

## State Diagram

The following diagram describes all the possible states in the process of the publication of an IHE Integration Statement in the Product Registry. ![](./media/pr_state_machine.gif)

* Red lines are actions performed by the PR crawler. It runs every night and checks that the registered URL are reachable and that the content of the IIS online did not change.
* Blue lines are actions performed by the administrator of the PR application (requires human actions and this may take some time)
* Black lines are actions performed by users that are registering IIS in the PR.
* Green nodes indicate states where the IIS are visible in searches.
* Grey node indicates state where the IIS is invisible in searches.
* Blue nodes are transition states that indicates a check for thresholds before performing status change.
