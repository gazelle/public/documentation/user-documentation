# use an old version of Ruby
FROM ruby:2.7.0 as builder

USER root
# non interactive
ARG DEBIAN_FRONTEND=noninteractive

# install utilities (gcc, ...)
RUN apt-get update && apt-get install -y \
        wget curl sudo build-essential git git-svn \
        && rm -rf /var/lib/apt/lists/*

# for gem dependencies
RUN mkdir -p /opt/jekyll-gazelle-documentation
WORKDIR /opt/jekyll-gazelle-documentation
# copy only Gemfile/Gemfile.lock
COPY _templates/jekyll-gazelle-documentation/Gemfile* /opt/jekyll-gazelle-documentation/
# install bundler with version specified in Gemfile.lock
RUN gem install bundler
# install gem dependencies provided in Gemfile.lock
RUN bundle install --deployment

# copy all directory to /opt/gazelle-user-documentation
RUN mkdir -p /opt/gazelle-user-documentation
COPY . /opt/gazelle-user-documentation/
WORKDIR /opt/gazelle-user-documentation/

# build jekyll website
RUN make jekyll

# simple webserver
FROM nginx:latest
# html location
RUN mkdir -p /usr/share/nginx/html/gazelle-documentation
# copy jekyll website
COPY --from=builder /opt/gazelle-user-documentation/target/jekyll-gazelle-documentation/_site /usr/share/nginx/html/gazelle-documentation/
# create an index.html
RUN cp /usr/share/nginx/html/gazelle-documentation/index-default.html /usr/share/nginx/html/gazelle-documentation/index.html
