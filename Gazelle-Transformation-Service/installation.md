---
title:  Installation Manual
subtitle: Gazelle Transformation
author:  Malo Toudic, Cédric Eoche-Duval, Nicolas BAILLIET
function: Engineer
date: 18/10/2024
toolversion: 2.2.X
version: 1.02
status: Approved document
reference: KER1-MAN-IHE-GAZELLE_TRANSFORMATION_INSTALLATION-1_02
customer: IHE-EUROPE
---

# Introduction

# Sources & binaries

Gazelle Transformation Service is an open-source project under Apache License Version 2.0 ([https://gazelle.ihe.net/content/license](https://gazelle.ihe.net/content/license)). Sources are available via git at [https://gitlab.inria.fr/gazelle/public/processing/transformation](https://gitlab.inria.fr/gazelle/public/processing/transformation).

# Gazelle Transformation Service installation and configuration

## Jboss 8 installation

It is necessary to have a jboss8 server. Make sure to read carefully the [general considerations for JBoss8](/gazelle-documentation/General/jboss8.html)

## Jboss 8 configuration

1. Create the directory '/usr/local/jboss8/modules/org/postgresql/main'

2. Add the following 'module.xml' file in '/usr/local/jboss8/modules/org/postgresql/main' directory:

```xml
<?xml version="1.0" ?>
<module xmlns="urn:jboss:module:1.1" name="org.postgresql">
  <resources>
       <resource-root path="postgresql-42.2.1.jar"/>
   </resources>
   <dependencies>
       <module name="javax.api"/>
       <module name="javax.transaction.api"/>
   </dependencies>
</module>
```

3. Download the postgresql driver from: [https://gazelle.ihe.net/jboss8/postgresql-42.2.1.jar](https://gazelle.ihe.net/jboss8/postgresql-42.2.1.jar)

``` bash
wget -nv -O /tmp/postgresql-9.4.1212.jar http://gazelle.ihe.net/jboss8/postgresql-9.4.1212.jar
sudo mv /tmp/postgresql-9.4.1212.jar /usr/local/jboss8/modules/org/postgresql/main
```

4. Edit /usr/local/jboss8/standalone/configuration/standalone.xml
In <subsystem xmlns="urn:jboss:domain:datasources:2.0">, add the following in <datasources> :

```xml
<drivers>
  <driver name="postgresql" module="org.postgresql">
    <driver-class>org.postgresql.Driver</driver-class>
  </driver>
</drivers>
```

Remove the following line:

```xml
<subsystem xmlns="urn:jboss:domain:weld:2.0"/>
```

## Mojarra installation

1. Download the mojarra cli from: [https://gazelle.ihe.net/jboss8/install-mojarra-2.1.19.cli](https://gazelle.ihe.net/jboss8/install-mojarra-2.1.19.cli "https://gazelle.ihe.net/jboss8/install-mojarra-2.1.19.cli")

``` bash
wget -nv -O /tmp/install-mojarra-2.1.19.cli https://gazelle.ihe.net/jboss8/install-mojarra-2.1.19.cli
```
2. Start the Jboss 8 server:

``` bash
sudo /etc/init.d/jboss8 start
```
3. To install Mojarra, it is necessary to know the port of the management interface of the Jboss server. To determine the port, type the following command:

``` bash
sudo grep "Http management interface listening on http" /usr/local/jboss8/standalone/log/server.log
```

And get the port (here: 10490):

```
2017-08-23 12:32:24,351 INFO  [org.jboss.as] (Controller Boot Thread) JBAS015961: Http management interface listening on http://127.0.0.1:10490/management
```

Now, set the port for controller argument and execute the following command:

``` bash
sudo /usr/local/jboss8/bin/jboss-cli.sh --connect --controller=127.0.0.1:10490 --command="deploy install-mojarra-2.1.19.cli"
```

The CLI archive will install mojarra packages in :

```
/usr/local/jboss8/modules/com
/usr/local/jboss8/modules/javax
/usr/local/jboss8/modules/org
```

# Database configuration

## Create 'transformation' database

Type the following command to create the 'transformation' database:

```bash
createdb -U gazelle -E UTF8 transformation
```

## Import the database schema and initiate the date

1. Download the sql scripts from the latest version of Gazelle Transformation Service (transformation-ear-X.X.X-sql.zip) : [https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22transformation-ear%22](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22transformation-ear%22")

2. Unzip the file

3. Type this command to import the schema:

```bash
psql -U gazelle transformation < schema.sql
```

4. Type this command to initiate data:

```bash
psql -U gazelle transformation < init.sql
```

## Daffodil environment configuration

The DFDL file which are referenced in the database are stored in the following directory on your environment in '/opt/DaffodilTransformation/dfdl/'.

Type the following commands:

```bash
mkdir -p /opt/DaffodilTransformation/dfdl/
sudo chown -R jboss:jboss-admin /opt/DaffodilTransformation/
sudo chmod -R 755 /opt/DaffodilTransformation/
```

## CAS properties file

Gazelle Transformation's CAS client is using a dedicated file in /opt/gazelle/cas which MUST be named "gazelle-transformation.properties".
The properties file content must contain the following parameters :

```
casServerUrlPrefix=https://{FQDN}/sso
casServerLoginUrl=https://{FQDN}/sso/login
casLogoutUrl=https://{FQDN}/sso/logout
service=https://{FQDN}/transformation
```

/!\ In the common file.properties, there is a serverName attribute which cannot be used at the same time with service, which explains why it is not 
used anymore in this gazelle-transformation.properties file.

## Deploy the application

1. Download the .ear from the latest version of the tool (transformation-ear-X.X.X.ear): [https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22transformation-ear%22](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22transformation-ear%22)

```bash
sudo wget -nv -O transformation-ear-X.X.X.ear https://gazelle.ihe.net/nexus/service/local/repositories/releases/content/net/ihe/gazelle/transformation-ear/X.X.X/transformation-ear-X.X.X.ear
```
2. **WARNING** : From version 2.1.0, datasources have been extracted from the **ear**. The template file can be found in /src/main/application/datasource in the sources or in the file transformation-X.X.X-datasource.zip from the nexus.
For more informations about how to manage that externalization, please refer to [general considerations for JBoss7](https://gazelle.ihe.net/gazelle-documentation/General/jboss7.html).

Datasource name : transformationDS

Database name : transformation

3. Move the .ear to Jboss 8 server deployment folder:

```bash
sudo mv /tmp/transformation-ear-X.X.X.ear /usr/local/jboss8/standalone/deployments/transformation.ear
```

Wait until the application has been completely deployed:

```bash
sudo ls /usr/local/jboss8/standalone/deployments/
```
And make sure to have the .ear of deployed:

```
transformation.ear.deployed
```

Logs can be viewed here:

```bash
tail -f /var/log/jboss8/server.log
```
<!--
# Migration from a previous version

If an old version of Gazelle Transformation Service was already installed (< 2.0.0), jboss8 need to be modified before deploying the application :

1. Edit to /usr/local/jboss8/standalone/configuration/standalone.xml

2. Remove in `<subsystem xmlns="urn:jboss:domain:datasources:2.0">` the following lines
```xml
<datasources>
	<datasource jndi-name="java:jboss/datasources/ExampleDS" pool-name="ExampleDS" enabled="true" use-java-context="true">
		<connection-url>jdbc:postgresql://db/daffodil-transformation-dev</connection-url>
		<driver>postgres</driver>
		<security>
			<user-name>gazelle</user-name>
			<password>gazelle</password>
		</security>
	</datasource>
	<drivers>
		<driver name="postgres" module="org.postgres">
			<driver-class>org.postgresql.Driver</driver-class>
		</driver>
	</drivers>
</datasources>
```
and replace by
```xml
            <datasources>
                <drivers>
                    <driver name="postgresql" module="org.postgresql">
                        <driver-class>org.postgresql.Driver</driver-class>
                    </driver>
                </drivers>
            </datasources>
```
Remove the following lines too
```xml
<default-bindings context-service="java:jboss/ee/concurrency/context/default" datasource="java:jboss/datasources/ExampleDS" jms-connection-factory="java:jboss/DefaultJMSConnectionFactory" managed-executor-service="java:jboss/ee/concurrency/executor/default" managed-scheduled-executor-service="java:jboss/ee/concurrency/scheduler/default" managed-thread-factory="java:jboss/ee/concurrency/factory/default"/>
```

3. Edit /usr/local/jboss8/standalone/configuration/standalone.xml and replace
```xml
<subsystem xmlns="urn:jboss:domain:weld:2.0" require-bean-descriptor="true" />
```

by

```xml
<subsystem xmlns="urn:jboss:domain:weld:2.0"/>
```

4. Edit the following 'module.xml' file in '/usr/local/jboss8/modules/org/postgres/main' directory:

```xml
<?xml version="1.0" ?>
<module xmlns="urn:jboss:module:1.1" name="org.postgresql">
   <resources>
       <resource-root path="postgresql-42.2.1.jar"/>
   </resources>
   <dependencies>
       <module name="javax.api"/>
       <module name="javax.transaction.api"/>
   </dependencies>
</module>
```

5. Rename the directory :
```bash
mv /usr/local/jboss8/modules/org/postgres/ /usr/local/jboss8/modules/org/postgresql/
```

6. Download the postgresql driver from: [https://gazelle.ihe.net/jboss8/postgresql-42.2.1.jar](https://gazelle.ihe.net/jboss8/postgresql-42.2.1.jar)

``` bash
wget -nv -O /tmp/postgresql-42.2.1.jar https://gazelle.ihe.net/jboss8/postgresql-42.2.1.jar
sudo mv /tmp/postgresql-9.4.1212.jar /usr/local/jboss8/modules/org/postgresql/main
```
7. Delete the old postgresql driver (postgresql-9.4.1212.jar).
-->

# Application configuration

Use the Administration menu, you will find a sub-menu entitled "Configure application preferences". The following preferences must be updated according to the configuration of your system. The table below summarizes the variables used by the Gazelle Transformation Service tool.

|           variable            |           description    |          default value       |
|-------------------|--------------------------------------------------------------------------------|-----------------------------------|
| application_admin_email       | Email of the administrator of the tool                                         | technical.manager@ihe-europe.net |
| application_admin_name        | Administrator's name                                                           | Eric Poiseau|
| application_admin_title       | Administrator's function                                                       | Application Administrator|
| application_name              | Name of the application to display on the home page                            | Gazelle Transformation|
| application_release_notes_url | URL to the release note of the tool                                            | https://gazelle.ihe.net/jira/projects/TRANSFOSRV?selectedItem=com.atlassian.jira.jira-projects-plugin:release-page&status=released|
| application_url               | Complete URL of the tool                                                       | http://localhost:8580/transformation|
| application_url_basename      | URL first resource to reach the tool                                           | /transformation|
| bin_path                      | File system path used by Gazelle Transformation                                | /opt/DaffodilTransformation/dfdl|
| documentation_url             | URL of the documentation                                                       | https://gazelle.ihe.net/gazelle-documentation/Gazelle-Transformation-Service/user.html|
| NUMBER_OF_ITEMS_PER_PAGE      | Number of rows displayed by table pagination                                   | 20|
| time_zone                     | Time Zone for display                                                          | Europe/Paris|

# SSO Configuration

There are additional preferences to configure the SSO authentication.

| Preference name      | Description                                                             | Example of value |
| ----------------     | ----------------------------------------------------------------------  | ---------------- |
| **cas_enabled**      | Enable or disable the CAS authentication.                               | true             |
| **ip_login**         | Enable authentication by IP address matching `ip_login_admin` regex.    | false            |
| **ip_login_admin**   | Regex to authorize ip authentication if CAS authentication is disabled. |  .*              |

For more documentation about SSO configurations, follow the link [here](https://gitlab.inria.fr/gazelle/public/framework/sso-client-v7/-/blob/master/cas-client-v7/README.md).