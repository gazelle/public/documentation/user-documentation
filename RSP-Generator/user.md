---
subtitle:  RSP Generator
title: User Manual
author: Claude Lusseau
date: 02/11/2024
toolversion: 1.0.0
function: Software Developer
version: 0.01
status: Draft
---

# Introduction

The RSP Generator is a tool that allows generation of RSP and ARL files. The ARL file is a logical acknowledgment that confirms the receipt of a file, and the RSP file is a response to a request.
The UI is a form that allows the user to custom some fields and generate the files.


# Usage
The UI is divided in three parts:
- The fist part is the selection of the ROC version context.
- The second part is the selection of which file you want to generate, ARL Negative, ARL Positive and RSP 578 or ARL Positive and RSP 908.
- The third part is the customization of the generated file by bringing various information 
- The last part is the download of the generated file.

For the first part, you have to select the option you want to generate:

![SELECT_CONTEXT](./media/select_context.png)

For the second part, you have to select the option you want to generate:

![GENERATION_OPTION](./media/generation_option.png)

The third part depends on the file you want to generate, here is the form for the ARL Negative:

![ARL NEGATIVE FORM](./media/ARL_NEGATIVE_V2.png)

And here is the form for the ARL Positive and RSP 578:

![ARL POSITIVE AND RSP 578 FORM](./media/RSP578ARL_V2.png)

And here is the form for the ARL Positive and RSP 908:

![ARL POSITIVE AND RSP 908 FORM](./media/RSP908ARL_V2.png)


For RSP Files you have to upload a DRE-ES, since the DRE file is complex it will be validated before being used, if the DRE is not valid you will have an error message and have to upload a new one.

In this form, all fields are mandatory, if all the fields are not filled with data, it is not possible to generate files.

The last part is the download of the generated file, you have to click on the download button and a zip file will be downloaded with the generated files.  
The window also displays the result that will be downloaded.

![DOWNLOAD BUTTON](./media/downloads_files_V2.png)