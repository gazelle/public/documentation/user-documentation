---
title: Release note
subtitle: Gazelle STS
toolversion: 1.3.1
releasedate: 2020-08-27
author: Anne-Gaëlle BERGE
function: Software Engineer
customer: IHE Europe
reference: KER1-RNO-IHE-GAZELLE_STS
---
# 1.3.1
_Release date: 2020-08-27_

__Bug__
* \[[STS-40](https://gazelle.ihe.net/jira/browse/STS-40)\] Issue related to subject-id NameFormat

# 1.3.0
_Release date: 2020-06-09_

__Bug__
* \[[STS-30](https://gazelle.ihe.net/jira/browse/STS-30)\] NullPointerException on Sequoia validation

__Improvement__
* \[[STS-25](https://gazelle.ihe.net/jira/browse/STS-25)\] Change the value of the Windows Domain Qualified name for the invalid use case
* \[[STS-28](https://gazelle.ihe.net/jira/browse/STS-28)\] Externalize properties files

# 1.2.0
_Release date: 2019-09-19_

__Bug__

* \[[STS-21](https://gazelle.ihe.net/jira/browse/STS-21)\] Missing SUBJECT element on ASSERTION in AuthzDecisionStatement element

__Improvement__

* \[[STS-24](https://gazelle.ihe.net/jira/browse/STS-24)\] Allow signature validation of assertion based on RSA Key
* \[[STS-26](https://gazelle.ihe.net/jira/browse/STS-26)\] Allow the deactivation of the trust validation

# 1.1.2
_Release date: 2018-06-22_

__Bug__

* \[[STS-23](https://gazelle.ihe.net/jira/browse/STS-23)\] AttributeStatement.Authzconsent.Value is not a correct OID

# 1.1.0
_Release date: 2017-06-29_

__Technical task__
* \[[STS-11](https://gazelle.ihe.net/jira/browse/STS-11)\] Request instance attributes are stored/accessed through global variables
* \[[STS-12](https://gazelle.ihe.net/jira/browse/STS-12)\] Refactor STS to allow different token provider configuration
* \[[STS-14](https://gazelle.ihe.net/jira/browse/STS-14)\] Add specific SubjectConfirmation for sequoia

__Bug__
* \[[STS-2](https://gazelle.ihe.net/jira/browse/STS-2)\] STS wrong subject-id attribute name
* \[[STS-13](https://gazelle.ihe.net/jira/browse/STS-13)\] x509Certificate in Signature element is truncated at 1598th char during parsing

__Story__
* \[[STS-8](https://gazelle.ihe.net/jira/browse/STS-8)\] [Sequoia] Allow creation of various assertions for the needs of Sequoia testing

# 1.0.0
_Release date: 2017-04-19_

__Bug__
* \[[STS-1](https://gazelle.ihe.net/jira/browse/STS-1)\] STS : Perform proper trusted issuer validation of assertion

__Story__
* \[[STS-4](https://gazelle.ihe.net/jira/browse/STS-4)\] STS support for validation of token provided by different issuers
* \[[STS-6](https://gazelle.ihe.net/jira/browse/STS-6)\] STS war is not deployed in nexus.

# Rev 56011

_Release date: 2017-02-16_

__Bug__
* \[[GSS-412](https://gazelle.ihe.net/jira/browse/GSS-412)\] STS : Unable to use another issuer than IHE-Europe CA
