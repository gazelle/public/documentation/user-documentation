---
title:  User Manual
subtitle: Gazelle FHIR Validator
author: Nicolas BAILLIET
releasedate: 2024-11-06
toolversion: 4.1.8
function: Engineer
version: 1.03
status: Approved document
reference: KER1-MAN-IHE-GAZELLE_FHIR_VALIDATOR_USER
customer: IHE-EUROPE
---
# Introduction

This tool owns the validator for the messages defined in the IHE profiles based on FHIR. Two types of messages are validated:

* The requests (URLs) are parsed and validating using an in-house algorithm, based on a template defined by the test designer from the user interface.
* The FHIR resources are validated in three steps:
  * Is the message a valid XML or JSON document
  * Does the message complies with the FHIR requirements
  * Does the message complies with IHE requirements (uses the structure definitions published by IHE or other domains)

# Accessing the validation service

The validation service is offered as a SOAP API. You can validate your messages from [EVSClient](../../EVSClient) or using the [SOAP interface](../../FhirValidator-ejb//ModelBasedValidationWSService/ModelBasedValidationWS?wsdl)

# Using the validation service

A SoapUI project is available in the directory of the tool on Inria's forge. [SoapUI](https://gitlab.inria.fr/gazelle/public/validation/fhir-validator/-/tree/master/FhirValidator-ear/src/main/resources/soapui)

# Graphical user interface

Non-logging users are allowed to access the list of validators owned by the tool. Especially, they can access the IHE customer structure definition (snaphot and differential views) and the constraints for the URLs.

Guidance on how to manage the validators by administrator of the tool is given in the installation manual of the tool.
