---
title: Release note
subtitle: Gazelle FHIR Validator
releasedate: 2024-11-06
toolversion: 4.1.8
author: Nicolas BAILLIET
function: Software Engineer
customer: IHE Europe
reference: KER1-RNO-IHE-GAZELLE_FHIR_VALIDATOR
---

# 4.1.8
_Release date: 2024-11-06_

__Bogue__
* \[[FHIRVAL-163](https://gazelle.ihe.net/jira/browse/FHIRVAL-163)\] Date Regex for URL Validation is not up to date and allowing '+' characters

# 4.1.7
_Release date: 2022-01-28_

__Improvement__
* \[[FHIRVAL-161](https://gazelle.ihe.net/jira/browse/FHIRVAL-161)\] Improve logs provided when reloading Structure Definitions

# 4.1.6
_Release date: 2021-10-07_

## Bug
* \[[FHIRVAL-140](https://gazelle.ihe.net/jira/browse/FHIRVAL-140)\] Gazelle FHIR Validator does not recognize the content type with JSON files

# 4.1.5
_Release date: 2021-09-21_

## Bug
* \[[FHIRVAL-134](https://gazelle.ihe.net/jira/browse/FHIRVAL-134)\] Format detection doesn't work when Byte Order Mark is present
* \[[FHIRVAL-135](https://gazelle.ihe.net/jira/browse/FHIRVAL-135)\] StandardCharset used when sending the message to IG Server is not valid

## Task
* \[[FHIRVAL-133](https://gazelle.ihe.net/jira/browse/FHIRVAL-133)\] Disable Schema Validation when using the FHIR Server

# 4.1.4
_Release date: 2020-11-27_

__Bug__
* \[[FHIRVAL-127](https://gazelle.ihe.net/jira/browse/FHIRVAL-127)\] Catch Exception when Document type can not be recognized
* \[[FHIRVAL-128](https://gazelle.ihe.net/jira/browse/FHIRVAL-128)\] FHIR validators are not redirecting on errors in the XML documents using lines with the IG Server

# 4.1.3
_Release date 2020-11-10_

* \[[GT-143]](https://gazelle.ihe.net/jira/browse/GT-143)\] Improvement des validateurs d'url 

# 4.1.2
_Release date: 2020-09-23_

__Bug__
* \[[FHIRVAL-125](https://gazelle.ihe.net/jira/browse/FHIRVAL-125)\] Validation report not returned when error is raised by IG Server

# 4.1.1
_Release date: 2020-09-15_

__Bug__
* \[[FHIRVAL-122](https://gazelle.ihe.net/jira/browse/FHIRVAL-122)\] POST does not contain Content-Type header when calling IG Server


# 4.1.0
_Release date: 2020-09-11_

__Story__
* \[[FHIRVAL-113](https://gazelle.ihe.net/jira/browse/FHIRVAL-113)\] Integration with HAPI Implementation Guide JPA Server

# 4.0.4
_Release date: 2020-07-17_

__Remarks__
* Release Fhir-util 3.0.1 to 3.0.2
* Upgrade HAPI library 4.1.0 to 4.2.0

__Bug__
* \[[FHIRVAL-98](https://gazelle.ihe.net/jira/browse/FHIRVAL-98)\] Value sets are not validated
* \[[FHIRVAL-108](https://gazelle.ihe.net/jira/browse/FHIRVAL-108)\] FhirValidator does not load base structure definition from fhir.ch

__Task__
* \[[FHIRVAL-102](https://gazelle.ihe.net/jira/browse/FHIRVAL-102)\] Create a build profile for eHealthSuisse in order to modify the context root using a variable


# 4.0.3
_Release date: 2020-06-19_

__Bug__

* \[[FHIRVAL-92](https://gazelle.ihe.net/jira/browse/FHIRVAL-92)\] When schematron validation crashes, other validation logs are not returned to the user


# 4.0.2
_Release date: 2019-12-01_

__Remarks__
Correction of known issues
Miscellaneous improvements to improve user experience

__Amélioration__

* \[[FHIRVAL-70](https://gazelle.ihe.net/jira/browse/FHIRVAL-70)\] StructureDefinition is not taken into account
* \[[FHIRVAL-69](https://gazelle.ihe.net/jira/browse/FHIRVAL-69)\] Differential and snapshot seems identical
* \[[FHIRVAL-68](https://gazelle.ihe.net/jira/browse/FHIRVAL-68)\] Internal Server Error is thrown when validator OID is duplicated
* \[[FHIRVAL-66](https://gazelle.ihe.net/jira/browse/FHIRVAL-66)\] URL validation for CH:ATC Request return a JSON error
* \[[FHIRVAL-65](https://gazelle.ihe.net/jira/browse/FHIRVAL-65)\] Rest service return wrong version of the tool
* \[[FHIRVAL-30](https://gazelle.ihe.net/jira/browse/FHIRVAL-30)\] Parameters are not duplicated when I duplicate a URL validator

# 4.0.1
_Release date: 2019-11-08_

__Remarks__
Project Architecture has been modified.
Only FHIR R4 Profiles are compatible now.

__Amélioration__

* \[[FHIRVAL-61](https://gazelle.ihe.net/jira/browse/FHIRVAL-61)\] Update PDQm and PIXm validators to support the new version of the profile

# 3.0.3
_Release date: 2019-09-02_

__Bug__

* \[[FHIRVAL-49](https://gazelle.ihe.net/jira/browse/FHIRVAL-49)\] Display is not consistent
* \[[FHIRVAL-53](https://gazelle.ihe.net/jira/browse/FHIRVAL-53)\] Validation with slices fails to detect slice type
* \[[FHIRVAL-54](https://gazelle.ihe.net/jira/browse/FHIRVAL-54)\] When json is malformed, validation crashes and the report is empty

# 3.0.2
_Release date: 2019-03-26_

__Bug__

* \[[FHIRVAL-48](https://gazelle.ihe.net/jira/browse/FHIRVAL-48)\] Prefix is optional, we should be able to leave the field empty
* \[[FHIRVAL-51](https://gazelle.ihe.net/jira/browse/FHIRVAL-51)\] Regex value is not rendered when you choose a datatype

# 3.0.1
_Release date: 2019-02-19_

__Bug__

* \[[FHIRVAL-47](https://gazelle.ihe.net/jira/browse/FHIRVAL-47)\] Validation using schematrons is broken

# 3.0.0
_Release date: 2019-02-14_

__Remarks__

From version 3.0.0, the Gazelle Fhir Validator runs on Wildfly10 using Java 8. See Installation Manual for details.     
The Datasource have also been extracted from the project. See Installation Manual for details.

__Task__

* \[[FHIRVAL-33](https://gazelle.ihe.net/jira/browse/FHIRVAL-33)\] Removal of seam dependencies and replacement of missing component S14
* \[[FHIRVAL-36](https://gazelle.ihe.net/jira/browse/FHIRVAL-36)\] [POC] Replace parent pom.xml by dependencies 
* \[[FHIRVAL-37](https://gazelle.ihe.net/jira/browse/FHIRVAL-37)\] [POC] Work on identity management and security

__Improvement__

* \[[FHIRVAL-41](https://gazelle.ihe.net/jira/browse/FHIRVAL-41)\] Extract datasource configuration
* \[[FHIRVAL-42](https://gazelle.ihe.net/jira/browse/FHIRVAL-42)\] Update SQL scripts archive


# 2.2.0
_Release date: 2018-09-10_

__Remarks__

The resource validator is now able to distinguish whether the resource is formatted as Json or XML. Only one version of each validator will remain. Tools making an hard reference to those validators shall be updated to use the new names.


__Bug__

* \[[FHIRVAL-19](https://gazelle.ihe.net/jira/browse/FHIRVAL-19)\] FHIR Validator must read ValueSets AND CodeSystems, files differ
* \[[FHIRVAL-20](https://gazelle.ihe.net/jira/browse/FHIRVAL-20)\] There is no need for anonymous users to access weight and custom structure definition location

__Story__

* \[[FHIRVAL-23](https://gazelle.ihe.net/jira/browse/FHIRVAL-23)\] ITI-81 Structure Definition

__Improvement__

* \[[FHIRVAL-22](https://gazelle.ihe.net/jira/browse/FHIRVAL-22)\] Implement a validator to verify the conformance of the ITI-81 requests
* \[[FHIRVAL-24](https://gazelle.ihe.net/jira/browse/FHIRVAL-24)\] The validator shall be able to detect whether the message is XML or JSON or an URL


# 2.1.0
_Release date: 2018-04-27_

__Story__

* \[[FHIRVAL-17](https://gazelle.ihe.net/jira/browse/FHIRVAL-17)\] Display the Structure Definition file to the user

__Improvement__

* \[[FHIRVAL-16](https://gazelle.ihe.net/jira/browse/FHIRVAL-16)\] Add the possibility to test resources based on structure definition files

# 2.0.1

__Bug__

* \[[FHIRVAL-14](https://gazelle.ihe.net/jira/browse/FHIRVAL-14)\] Update postgresql driver to version 42.2.1-jre7

__Story__

* \[[FHIRVAL-10](https://gazelle.ihe.net/jira/browse/FHIRVAL-10)\] Add a validator for the DSTU3 resources

__Improvement__

* \[[FHIRVAL-11](https://gazelle.ihe.net/jira/browse/FHIRVAL-11)\] Move to new apereo CAS
* \[[FHIRVAL-12](https://gazelle.ihe.net/jira/browse/FHIRVAL-12)\] Validate with custom structure definition

# 2.0.0
_Release date: 2017-11-30_

__Remarks__

This version of the tool is no more compliant with Patient Manager from its 9.8.0 version.
Note that the Validation API (SOAP web service) has changed.

__Technical task__

* \[[FHIRVAL-5](https://gazelle.ihe.net/jira/browse/FHIRVAL-5)\] [PDQm/PDC] Update the validator for PDQm / PDC actor
* \[[FHIRVAL-6](https://gazelle.ihe.net/jira/browse/FHIRVAL-6)\] [PDQm/PDS] Update the validator for PDQm / PDS actor
* \[[FHIRVAL-7](https://gazelle.ihe.net/jira/browse/FHIRVAL-7)\] [PIXm/PDC] Update the validator for PIXm / PDC actor
* \[[FHIRVAL-8](https://gazelle.ihe.net/jira/browse/FHIRVAL-8)\] [PIXm/Manager] Update the validator for PIXm / Manager actor

__Improvement__

* \[[FHIRVAL-1](https://gazelle.ihe.net/jira/browse/FHIRVAL-1)\] Validator shall be updated to the latest version of TF

