---
title:  User Manual
subtitle: ATC Simulator
author: Wylem BARS
function: Software Engineer
date: 06/11/2019
toolversion: 2.0.X
version: 1.01
status: Reviewed
reference: KER1-MAN-IHE-ATC_SIMULATOR_USER
customer: IHE-EUROPE
---

# Introduction

This simulator is developed in conformance with Swiss Integration profiles and IHE Technical Framework. 
This simulator emulates actor from CH:ATC (Audit Trail Consumption) profile. This profile defines the audit trail consumption requirements a community 
has to provide for a patient's audit trail. The profile CH:ATC defines and precises the actors and transaction [ITI-81] of the IHE IT Infrastructure 
Technical Framework Supplement Add RESTful Query to ATNA 1 and defines the content of the Audit Messages. The different types of the Audit Messages are 
based on the requirements for Document and Policy Access management in order to achieve the Swiss regulation needs on the audit trail access by patients.
Therefore it can act as a Patient Audit Consumer or as a Patient Audit Record Repository.

As a Patient Audit Consumer, this simulator is aimed to send messages to a Patient Audit Record Repository. Consequently, if your system (named SUT or System Under Test) is reachable from the Internet, you will be able to receive messages from the simulator.

As a Patient Audit Record Repository, your system can request Audit Events from the simulator.


# Patient Audit Consumer

When acting as a Patient Audit Consumer, this simulator is able to send multiple requests to your SUT to test the handling of parameters :

* date
* entity.identifier
* entity-role
* subtype
* entity-type
* _format

It can also send bad requests (missing requested parameters, parameters providing no matches, etc) to test the behavior of the SUT against erroneous requests.

## How to access the Simulator ?

The simulator is accessible via Gazelle Webservice Tester. If the simulator is available on the instance of the tool, it should appear in the project list with the name **ATC_Repository**.
This name may also be followed by a version number.

![ATC Simulator for Patient Audit Consumer](./media/atc_repo_gwt.png)


## How to send messages to an SUT ?

If the simulator is available on the instance of Gazelle Webservice Tester you are using, it can be launched like any other project from the tools.

Go to **Run**. Select the **ATC_Repository** project. You will have two Test suite available. The **NormalCases** test suite will send a request to test each parameter listed earlier. 
The **ErrorCases** test suite will focus on error cases.

![Run the ATC Repository Project](./media/atc_repo_run.png)

You can choose if you want to execute one specific test suite or the entire project.
You will then be able to choose the endpoint the simulator will use, as well as the parameters from the requests that will be sent to your SUT.

![Form defining parameters to use to execute NormalCases test suite](./media/atc_repo_normal_cases.png)

Once you defined all parameters for the Simulator to send requests, simply press **Run** and it will send messages to your SUT and keep track of exchanges in an **Execution**.
The details for this Execution can be retrieved at anytime in the **Execution List** menu.


# Patient Audit Record Repository

The ATC Simulator can also simulate a CH:ATC Patient Audit Record Repository.
A **Patient Audit Consumer** queries a **Patient Audit Record Repository** for Audit Events defined by this profile.
The Patient Audit Consumer uses entity-id (Patient ID) and date before/after as parameters to asks the Audit Record Repository.
In all, there are only three mandatory parameters.
There are also optional parameters : entity-type, entity-role, source, type, user, subtype, outcome and address.
Each request sent must have a security token in the header, this part is used to check access rights. In this token, two elements will be checked : 

* Is the assertion valid in time ? (Time not Before in the past and Time not After in the future)
* Does the mock know the patient ID used as a **resource-id** attribute ? If not, the mock will tell you that you do not have the rights to acces the information. The list of available patient ID is displayed below.

## Data set

### Token Security

Here are the ID to put in the SAML token in **resource-id** attribute. 
If you query any other patient id, the mock will respond with an error message saying that you are not authorized to access information related to the given patient ID.

|**resource-id**|
|-------------|
|761337610430891416^^^SPID&amp;2.16.756.5.30.1.127.3.10.3&amp;ISO|
|761337610423590456^^^SPID&amp;2.16.756.5.30.1.127.3.10.3&amp;ISO|
|761337610435209810^^^SPID&amp;2.16.756.5.30.1.127.3.10.3&amp;ISO|
|761337610436974489^^^SPID&amp;2.16.756.5.30.1.127.3.10.3&amp;ISO|

### AuditEvent

Here are all couples of Dates/PatientID to effectively access audit events. 
However, using optional parameters can lead you to no Audit event returned, even with these dates and patient IDs.
Indeed, if the events does not match even one of the optional parameters, the audit event will not be returned by the ATC Mock.


|**Patient ID**|**Dates**|
|--------------|----------|
|urn:oid:2.16.756.5.30.1.127.3.10.3&vert;761337610430891416| ge2015-01-01 / le2020-01-01|
|urn:oid:2.16.756.5.30.1.127.3.10.3&vert;761337610435209810|ge2015-01-01 / le2017-01-01|
|urn:oid:2.16.756.5.30.1.127.3.10.3&vert;761337610436974489|ge2017-01-01 / le2019-01-01|
	
## End Point

Here are the endpoints to use to send your request to the Patient Audit Record Repository simulator : 

* [http://ehealthsuisse.ihe-europe.net:8096/atc-record-repository?wadl](https://ehealthsuisse.ihe-europe.net/atc-record-repository.wadl)
* [https://ehealthsuisse.ihe-europe.net:10443/atc-record-repository?wadl](https://ehealthsuisse.ihe-europe.net/simu/atc-record-repository.wadl)
        
It requires TLS mutual authentication with testing certificate (from GSS PKI).

### ATC URI example

Here is an example URI for a requets on patient with ID **761337610430891416^^^SPID&amp;2.16.756.5.30.1.127.3.10.3&amp;ISO** looking for audit events between
**2015-01-01** and **2020-01-01**

```
http://ehealthsuisse.ihe-europe.net:8096/atc-record-repository/ARRservice/AuditEvent?date=ge2015-01-00&date=le2020-01-00&entity-id=urn:oid:2.16.756.5.30.1.127.3.10.3|761337610430891416
```

## Mock messages on GWT

Messages sent to the simulator can be found in Mock Messages feature of Gazelle Webservice Tester.
This feature is documented at : [https://ehealthsuisse.ihe-europe.net/gazelle-documentation/Gazelle-Webservice-Tester/user.html](https://ehealthsuisse.ihe-europe.net/gazelle-documentation/Gazelle-Webservice-Tester/user.html).

Messages exchanged with EPR-ATC-Simulator can be found by filtering with the actor CH:PATIENT_AUDIT_RECORD_REPOSITORY.
