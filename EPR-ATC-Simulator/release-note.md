---
title: Release note
subtitle: ATC Simulator
toolversion: 2.0.1
releasedate: 2023-12-07
author: Youn Cadoret
function: Software Engineer
customer: IHE Europe
reference: KER1-RNO-IHE-ATC_SIMULATOR
---

# 2.0.1
_Release date: 2023-12-07_
__Bug__
* \[[ATCREPO-23](https://gazelle.ihe.net/jira/browse/ATCREPO-23)\] ATC Repository data need to be updated to be conform to the specification

# 2.0.0
_Release date: 2023-06-08_
__Task__
* \[[ATCREPO-22](https://gazelle.ihe.net/jira/browse/ATCREPO-22)\] Migrate from STU3 to R4


# 1.3.4
_Release date: 2020-10-08_

__Bug__
* \[[ATCREPO-21](https://gazelle.ihe.net/jira/browse/ATCREPO-21)\] CH:ATC Simulator - Invalid EprDocumentTypeCode in Response


# 1.3.3
_Release date: 2020-03-18_

__Bug__

* \[[ATCREPO-20](https://gazelle.ihe.net/jira/browse/ATCREPO-20)\] NO-BREAK SPACE is json reponse can cause error

# 1.3.2
_Release date: 2020-03-18_

__Bug__

* \[[ATCREPO-19](https://gazelle.ihe.net/jira/browse/ATCREPO-19)\] Recorded date does not match in test data Patient2-read-AE


# 1.3.1
_Release date: 2020-01-13_

__Bug__

* \[[ATCREPO-18](https://gazelle.ihe.net/jira/browse/ATCREPO-18)\] ATC simulator does not check hours and time zone

__Improvement__

* \[[ATCREPO-17](https://gazelle.ihe.net/jira/browse/ATCREPO-17)\] Update the way ATC Mock parse request for dates



# 1.3.0
_Release date: 06/11/2019_

__Improvement__

* \[[ATCREPO-16](https://gazelle.ihe.net/jira/browse/ATCREPO-16)\] Use nwittwerchristen for all test cases in ATC Consumer test cases


# 1.2.3
_Release date: 02/09/2019_

__Bug__

* \[[ATCREPO-14](https://gazelle.ihe.net/jira/browse/ATCREPO-14)\] IDs in Mock sample documents should mathc '[A-Za-z0-9\-\.]{1,64}'

# 1.2.2
_Release date: 2019-08-27_

__Task__

* \[[ATCREPO-12](https://gazelle.ihe.net/jira/browse/ATCREPO-12)\] [ATC] Create groovy script to record mock transaction


# 1.2.1
_Release date: 2019-08-02_

__Bug__

* \[[ATCREPO-13](https://gazelle.ihe.net/jira/browse/ATCREPO-13)\] Failure of the test cases due to invalid time period

# 1.2.0
_Release date: 2019-07-01_

__Improvement__

* \[[ATCREPO-11](https://gazelle.ihe.net/jira/browse/ATCREPO-11)\] Update ATC mock to be compliant with EPR 1.9 Specification

# 1.1.2
_Release date: 2019-04-10_

__Bug__

* \[[ATCREPO-7](https://gazelle.ihe.net/jira/browse/ATCREPO-7)\] ATC Client simulator send "IHE- SAML" instead of "IHE-SAML"
* \[[ATCREPO-8](https://gazelle.ihe.net/jira/browse/ATCREPO-8)\] Patient Audit Record Repository response contains not all request parameters
* \[[ATCREPO-9](https://gazelle.ihe.net/jira/browse/ATCREPO-9)\] ATC Consumer endpoint include /ARRService and shouldn't
* \[[ATCREPO-10](https://gazelle.ihe.net/jira/browse/ATCREPO-10)\] & should not be escaped inside date parameter

# 1.1.1
_Release date: 2019-04-03_

__Bug__

* \[[ATCREPO-5](https://gazelle.ihe.net/jira/browse/ATCREPO-5)\] Handling of parameter "date=ge
* \[[ATCREPO-6](https://gazelle.ihe.net/jira/browse/ATCREPO-6)\] ATC - Simulator ignores _format parameter, only responding with XML


# 1.1.0
_Release date: 2019-03-27_

__Story__

* \[[ATCREPO-1](https://gazelle.ihe.net/jira/browse/ATCREPO-1)\] Update the mock to comply with the EPR 1.8 specification

__Improvement__

* \[[ATCREPO-2](https://gazelle.ihe.net/jira/browse/ATCREPO-2)\] Update the AuditMessage and AuditEvent messages


# 1.0.2
_Release date: 2019-02-15_

__Bug__

* \[[ATCREPO-3](https://gazelle.ihe.net/jira/browse/ATCREPO-3)\] Mock not correctly handle request parameters

__Improvement__

* \[[ATCREPO-4](https://gazelle.ihe.net/jira/browse/ATCREPO-4)\] Replace regex with XMLHolder in assertion reading
