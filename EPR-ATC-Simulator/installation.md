---
title:  Installation Manual
subtitle: ATC Simulator
author: Wylem BARS
function: Software Engineer
date: 06/11/2019
toolversion: 2.0.x
version: 1.01
status: Reviewed
reference: KER1-MAN-IHE-ATC_SIMULATOR_INSTALLATION-1_01
customer: IHE-EUROPE
---

# ATC Simulator - Installation & configuration

## Source Code

Sources of the tool are available on Inria's Gitlab in the Gazelle project: 
[EPR > atc-repository](https://gitlab.inria.fr/gazelle/specific-tools/epr/atc-repository).

## Patient Audit Consumer

This project can be loaded on a Gazelle WebService tester instance. This way you will be able to launch a suite of requests from the simulator emulating 
the Patient Audit Consumer actor of the CH:ATC profile.

First you need to load the project in Gazelle WebService Tester. Then, check :
* The entire project
* The NormalCases test suite
* The ErrorCases test suite 

This will allow the user to either launch a specific test suite or to launch both using the entire project. 

Then click on the Project name in the tree and check the __endpoint__ and __enPointAssertions__ properties. This will be useful to specify where your 
system is so the simulator can request it for Audit Events. 
The click on each test case from the project and check all parameters. Then the user will be able to modify the request parameters send by the simulator to match 
audit event from his Patient Audit Record Repository.

## Patient Audit Record Repository

First step to set up the Patient Audit Record Repository actor from the simulator is to checkout sources.
You can checkout the entire ATC simulator sources from the url [here](https://gitlab.inria.fr/gazelle/specific-tools/epr/atc-repository).
The full project for released version can also been found on a ZIP archive format on nexus.
For the Patient Audit Record Repository actor you will need three things : 
* The SoapUI project __ATC/soapui/ATC-MockService-soapui-project.xml__
* The folder __ATC/test_data/AuditEvent__ that contains all Audit Events known by the simulator.
* The script __ATC/init.d/atcRecordRepositoryMock__ that will allow you to start/stop the mock project as a service on your platform

Once you downloaded the ATC folder, you have all the data you need to run the Patient Audit Record Repository Mockup.

Simply modify __atcRecordRepositoryMock__ script properties :


| **Property**                                 | **Description**                                       | **Example Value**                                                                              |
|----------------------------------------------|---------------------------------------------------------|----------------------------------------------------------------------------------------------|
| SOAPUI_PATH                    | Path to the SoapUI folder installed on your platform| /usr/local/SmartBear/SoapUI-5.3.0|
| SOAPUI_PROJECT_PATH            | Path to the SoapUI project checked out from Inria's Gforge| /opt/simulators/ATC/soapui/ATC-MockService-soapui-project.xml|
| SOAPUI_MOCK_NAME               | Name of the mock | PatientAuditRecordRepository|
| SOAPUI_MOCK_PORT               | Port that will be used to access the Mock| 8096|
| SOAPUI_MOCK_ENDPOINT           | Endpoint that will be used to reach the Mock| /atc-record-repository |
| SOAPUI_MOCK_LOG                | Path to the log file for the Mock | /var/log/soapui/epr-atc-record-repository.log|


When the properties are modified to fit your platform, you can copy the script or create a symbolic link in __/etc/init.d__ folder to be able to use start 
and stop the mock as a service.
You will then be able to use following commands to respectively start the Mock service, see its status and stop the service : 

```bash
 sudo service atcRecordRepositoryMock start
 sudo service atcRecordRepositoryMock status
 sudo service atcRecordRepositoryMock stop
```
