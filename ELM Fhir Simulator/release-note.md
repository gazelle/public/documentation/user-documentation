#---
#title: Release note
#subtitle: ELM FHIR Simulator 
#toolversion: 1.0.1
#releasedate: 2024-11-19
#authors: Achraf ACHKARI-BEGDOURI / Alexandre POCINHO
#function: Software Engineer
#customer: Switzerland Minister of Heath
#reference:
#---

# 1.0.1
_Release date: 2024-11-19_

__Bug__
* \[[CHELM-12](https://gazelle.ihe.net/jira/browse/CHELM-12)\] Wrong response in PublishDocumentReferenceResponse

# 1.0.0
_Release date: 2024-08-09_

__Epic__
* \[[CHELM-1](https://gazelle.ihe.net/jira/browse/CHELM-1)\] Implement CH:ELM Simulator Server

__Story__
* \[[CHELM-3](https://gazelle.ihe.net/jira/browse/CHELM-3)\] Implement Send Report 
* \[[CHELM-5](https://gazelle.ihe.net/jira/browse/CHELM-5)\] Implement Read
* \[[CHELM-6](https://gazelle.ihe.net/jira/browse/CHELM-6)\] Implement Search

__Task__
* \[[CHELM-7](https://gazelle.ihe.net/jira/browse/CHELM-7)\] Documentation for CH: ELM Server Simulator


