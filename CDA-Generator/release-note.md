---
title: Release note
subtitle: CDA Generator
toolversion: 3.0.0
releasedate: 2024-02-05
author: Anne-Gaëlle BERGE
function: Software Engineer
customer: IHE Europe
reference: KER1-RNO-IHE-CDA_GENERATOR
---
# 3.0.0
_Release date: 2024-02-05_

__Improvement__
* \[[CDAGEN-820](https://gazelle.ihe.net/jira/browse/CDAGEN-820)\] Integrate new sso-client-v7
* \[[CDAGEN-821](https://gazelle.ihe.net/jira/browse/CDAGEN-821)\] Remove username display in GUI

# 2.3.0
_Release date: 2024-02-06_

Context : Gazelle User Management Renovation step 2

__Task__

* \[[CDAGEN-820](https://gazelle.ihe.net/jira/browse/CDAGEN-820)\] Integrate new sso-client-v7

# 2.2.6
_Release date: 2022-01-24_

__Bug__

* \[[CDAGEN-814](https://gazelle.ihe.net/jira/browse/CDAGEN-814)\] Remote Validation is not working anymore following EVSClient previous release

# 2.2.5
_Release date: 2021-12-02_

__Bug__

* \[[CDAGEN-816](https://gazelle.ihe.net/jira/browse/CDAGEN-816)\] Hibernate connection issue in release profile

# 2.2.4
_Release date: 2021-11-30_

__Improvement__

* \[[CDAGEN-812](https://gazelle.ihe.net/jira/browse/CDAGEN-812)\] Add support of Java 11 for CDA MBVal validation


# 2.2.3
_Release date: 2021-10-01_

__Bug__

* \[[CDAGEN-815](https://gazelle.ihe.net/jira/browse/CDAGEN-815)\] CDAGen not responding: EVSClient stalls on template display


# 2.2.2
_Release date: 2020-04-23_

__Bug__

* \[[CDAGEN-779](https://gazelle.ihe.net/jira/browse/CDAGEN-779)\] Ip login doesn't work


# 2.2.1
_Release date: 2018-07-04_

__Bug__

* \[[CDAGEN-631](https://gazelle.ihe.net/jira/browse/CDAGEN-631)\] When generating an author => transaction failed
* \[[CDAGEN-708](https://gazelle.ihe.net/jira/browse/CDAGEN-708)\] /CDA Generator/System testing/CDA Edition (automatically generated from req. spec.)/CDAGEN-19:User SHALL be able to acce
* \[[CDAGEN-764](https://gazelle.ihe.net/jira/browse/CDAGEN-764)\] Add params to BRR
* \[[CDAGEN-766](https://gazelle.ihe.net/jira/browse/CDAGEN-766)\] spaces are not allowed in BBR version
* \[[CDAGEN-767](https://gazelle.ihe.net/jira/browse/CDAGEN-767)\] Validator version is not displayed in detailed result
* \[[CDAGEN-771](https://gazelle.ihe.net/jira/browse/CDAGEN-771)\] Refresh button in BBR list doesn't work

__Improvement__

* \[[CDAGEN-750](https://gazelle.ihe.net/jira/browse/CDAGEN-750)\] Add page access rules
* \[[CDAGEN-752](https://gazelle.ihe.net/jira/browse/CDAGEN-752)\] Doc deployement 

# 2.2.0
_Release date: 2018-05-04_

__Remarks__

A change in the API for the computation of the scorecard implies that CDA Generator will only works with EVS Client versions greater than 5.6.0. If the scorecard feature is not used, there is no immediate need to upgrade EVSClient.
This version is only compatible with the new Gazelle SSO.

__Improvement__
* \[[CDAGEN-739](https://gazelle.ihe.net/jira/browse/CDAGEN-739)\] Use new CAS SSO

# 2.1.22

__Story__
* \[[CDAGEN-744](https://gazelle.ihe.net/jira/browse/CDAGEN-744)\] Update validation tools for PHARM

# 2.1.21

__Story__
* \[[CDAGEN-734](https://gazelle.ihe.net/jira/browse/CDAGEN-734)\] Include cdabasic 1.1.0

# 2.1.20

__Improvement__
* \[[CDAGEN-732](https://gazelle.ihe.net/jira/browse/CDAGEN-732)\] Add the possibility to override the XSD in the BBR definition

# 2.1.19

__Bug__
* \[[CDAGEN-730](https://gazelle.ihe.net/jira/browse/CDAGEN-730)\] Problem with the output of the validator ws when the validator is not present

# 2.1.18

__Bug__
* \[[CDAGEN-724](https://gazelle.ihe.net/jira/browse/CDAGEN-724)\] The inactive BBR shall not be proposed during validation

__Bug__
* \[[CDAGEN-722](https://gazelle.ihe.net/jira/browse/CDAGEN-722)\] The validation of IVL_REAL SHALL ignore the check about the nullFlavor
* \[[CDAGEN-723](https://gazelle.ihe.net/jira/browse/CDAGEN-723)\] Problem with the validation of telecom of eD

# 2.1.14

__Bug__
* \[[CDAGEN-712](https://gazelle.ihe.net/jira/browse/CDAGEN-712)\] disactivate Scanned Document validator for epSOS
* \[[CDAGEN-713](https://gazelle.ihe.net/jira/browse/CDAGEN-713)\] Bug in templates calculation when the document is not valid
* \[[CDAGEN-719](https://gazelle.ihe.net/jira/browse/CDAGEN-719)\] the validation based on the BBR defined in CDAgenerator is not multithreading

# 2.1.8

__Improvement__
* \[[GOC-101](https://gazelle.ihe.net/jira/browse/GOC-101)\] Update the scorecard stylesheet (XSL) to allow the user to filter on errors
* \[[GOC-88](https://gazelle.ihe.net/jira/browse/GOC-88)\] \[Scorecard\] It would be interested to have access to the constraint's type
* \[[GOC-90](https://gazelle.ihe.net/jira/browse/GOC-89)] \[Scorecard\] Report assertionId and idScheme for each constraint


# 2.1.6
_Release date: 2017-05-02_

__Bug__
* \[[CDAGEN-616](https://gazelle.ihe.net/jira/browse/CDAGEN-616)\] Can't edit application preferences
* \[[CDAGEN-637](https://gazelle.ihe.net/jira/browse/CDAGEN-637)\] Cannot create/update application preferences

# 2.1.4

__Bug__
* \[[CDAGEN-652](https://gazelle.ihe.net/jira/browse/CDAGEN-652)\] Validator created from BBR failed to perform the XSD validation

# 2.1.2

__Bug__
* \[[CDAGEN-636](https://gazelle.ihe.net/jira/browse/CDAGEN-636)\] Cannot deploy CDAGenerator 2.1.1 in ovh2
* \[[CDAGEN-639](https://gazelle.ihe.net/jira/browse/CDAGEN-639)\] Add the possibility to select a versionLabel from the templates in the BBR

__Story__
* \[[CDAGEN-622](https://gazelle.ihe.net/jira/browse/CDAGEN-622)\] Add tests related to BBR and validatorDescriber in testlink

# 2.1.1

__Bug__
* \[[CDAGEN-619](https://gazelle.ihe.net/jira/browse/CDAGEN-619)\] Impossible to create a validator association if the value in the dropdown contains accented characters
* \[[CDAGEN-621](https://gazelle.ihe.net/jira/browse/CDAGEN-621)\] Can't delete a validatorDescriber if this validator is the parent of others validators describers

__Story__
* \[[CDAGEN-615](https://gazelle.ihe.net/jira/browse/CDAGEN-615)\] Release CDAGenerator 2.1.0

__Improvement__
* \[[CDAGEN-646](https://gazelle.ihe.net/jira/browse/CDAGEN-646)\] Folder for BBR compilation are not created by process
