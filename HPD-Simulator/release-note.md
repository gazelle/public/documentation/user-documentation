---
title: Release note
subtitle: HPDSimulator
toolversion: 3.0.1
releasedate: 2024-02-02
author: Malo TOUDIC
function: Software Developer
customer: IHE Europe
reference: KER1-RNO-IHE-HPD_SIMULATOR
---

# 3.0.1
_Release date: 2024-12-02_

__Bogue__
* \[[HPD-223](https://gazelle.ihe.net/jira/browse/HPD-223)\] [HPD Simu] Can't access to "Permanent link to test report"
* \[[HPD-224](https://gazelle.ihe.net/jira/browse/HPD-224)\] [HPD Simu] Difficult to access to "LDAP Server" & "Validation Tool" submenus
* \[[HPD-225](https://gazelle.ihe.net/jira/browse/HPD-225)\] ITI-58 requestID in Response does not correspond to Request

# 3.0.0
_Release date: 2024-02-02_

__Task__
* \[[HPD-218](https://gazelle.ihe.net/jira/browse/HPD-218)\] Integrate new sso-client-v7
* \[[HPD-219](https://gazelle.ihe.net/jira/browse/HPD-219)\] Remove username display in GUI

# 2.4.5
_Release date: 2023-12-08_

__Bug__
* \[[HPD-221](https://gazelle.ihe.net/jira/browse/HPD-221)\] [CH] Wrong value set IDs in HPD validation constraint

# 2.4.4
_Release date: 2023-12-07_

__Bug__
* \[[HPD-220](https://gazelle.ihe.net/jira/browse/HPD-220)\] [CH:HPD] UID Format: . and _ need to be added to the list of special characters that don't need to be escaped

# 2.4.3
_Release date: 2023-06-12_

__Bug__
* \[[HPD-217](https://gazelle.ihe.net/jira/browse/HPD-217)\] Timestamp validator RegEx error

# 2.4.2
_Release date: 2023-06-06_

# All changes

__Bug__
* \[[HPD-215](https://gazelle.ihe.net/jira/browse/HPD-215)\] ch_hpd_014_Metadata_attributes is using the wrong value set OID

__Improvement__
* \[[HPD-214](https://gazelle.ihe.net/jira/browse/HPD-214)\] Update data & schema in CPI Simulator
* \[[HPD-216](https://gazelle.ihe.net/jira/browse/HPD-216)\] [CH] Add validation contraint for the uid format

# 2.4.1
_Release date: 2022-02-07_

__Bug__
* \[[HPD-210](https://gazelle.ihe.net/jira/browse/HPD-210)\] [CH:CPI] Allow non integer requestID in batchRequest
* \[[HPD-211](https://gazelle.ihe.net/jira/browse/HPD-211)\] [CH:CPI] When receiving a CIDD request, date parameters in the message were not save and displayed
* \[[HPD-212](https://gazelle.ihe.net/jira/browse/HPD-212)\] [CH:CPI] GUI to manage CIDD message is broken

# 2.4.0
_Release date: 2022-01-13_

__Story__
* \[[HPD-204](https://gazelle.ihe.net/jira/browse/HPD-204)\] [CH:CPI] CH:CIDD simulator

# 2.3.0
_Release date: 2021-09-10_

__Remarks__

In a Swiss environment, update JbossWS (see installation manual) so that the SoapFault of the CH:CIQ transaction described in the CH:CPI specification works as expected.

__Story__
* \[[HPD-203](https://gazelle.ihe.net/jira/browse/HPD-203)\] [CH:CPI] Create CH:CIQ server simulator


# 2.2.2
_Release date: 2020-09-14_

__Bug__
* \[[HPD-199](https://gazelle.ihe.net/jira/browse/HPD-199)\] [HPD] The "description" attribute in ITI-59 is retired and should not be considered as mandatory in the validator

# 2.2.1
_Release date: 2019-10-10_

__Bug__

* \[[HPD-197](https://gazelle.ihe.net/jira/browse/HPD-197)\] [HPDSimulator] When registering a SUT, attributes order should not matter in the baseDN


# 2.2.0
_Release date: 2019-03-14_

__Remarks__

Datasource management has changed, refers to the installation manual to correctly update your instance of the tool.

__Improvement__

* \[[HPD-192](https://gazelle.ihe.net/jira/browse/HPD-192)\] Extract datasource configuration
* \[[HPD-193](https://gazelle.ihe.net/jira/browse/HPD-193)\] Update SQL scripts archive

# 2.1.2
_Release date: 2018-08-23_


__Bug__

* \[[HPD-187](https://gazelle.ihe.net/jira/browse/HPD-187)\] Transaction instance data are not correctly recorded
* \[[HPD-189](https://gazelle.ihe.net/jira/browse/HPD-189)\] Null pointer exception when validating CH CIDD request or response
* \[[HPD-190](https://gazelle.ihe.net/jira/browse/HPD-190)\] Remove constraint constraint_hpd_modifyRequest_businessCategoryCardValue

# 2.1.1
_Release date: 2018-04-10_

__Story__

* \[[HPD-184](https://gazelle.ihe.net/jira/browse/HPD-184)\] Conflict between the swiss and IHE specification
* \[[HPD-185](https://gazelle.ihe.net/jira/browse/HPD-185)\] Create validator for CH:CIDD

__Task__

* \[[HPD-176](https://gazelle.ihe.net/jira/browse/HPD-176)\] New ZAD Version 1.21.x issued
* \[[HPD-180](https://gazelle.ihe.net/jira/browse/HPD-180)\] Implementierung ZAD Stand 23.01.2018 update

# 2.1.0
_Release date: 2018-03-09_

__Remarks__

This version of the HPDSimulator tool uses the jdbc driver provided by the Jboss7 AS server.


__Bug__

* \[[HPD-165](https://gazelle.ihe.net/jira/browse/HPD-165)\] HPD ObjectClass "top" is not necessary in all requests
* \[[HPD-174](https://gazelle.ihe.net/jira/browse/HPD-174)\] Update postgresql driver to version 42.2.1-jre7

__Story__

* \[[HPD-169](https://gazelle.ihe.net/jira/browse/HPD-169)\] SOAP envelop handling in HPD Validator

__Improvement__

* \[[HPD-170](https://gazelle.ihe.net/jira/browse/HPD-170)\] Integrate HPD Simulator with the new Gazelle SSO
* \[[HPD-173](https://gazelle.ihe.net/jira/browse/HPD-173)\] Use new CAS apereo

# 2.0.5
_Release date: 2017-10-24_


__Bug__

* \[[HPD-162](https://gazelle.ihe.net/jira/browse/HPD-162)\] HPD SOAP Action inconsistency
* \[[HPD-167](https://gazelle.ihe.net/jira/browse/HPD-167)\] [HPDSimulator] Constraint on hcIdentifier is to restrictive

# 2.0.4
_Release date: 2017-09-27_

__Bug__

* \[[HPD-159](https://gazelle.ihe.net/jira/browse/HPD-159)\] message from CPI validation mentions CHgateway not CHEndpoint

__Improvement__

* \[[HPD-161](https://gazelle.ihe.net/jira/browse/HPD-161)\] BIT updated documents HPD, CPI, WSDL, LDAP schema 21.09.2017

# 2.0.3
_Release date: 2017-09-18_


__Technical task__

* \[[HPD-151](https://gazelle.ihe.net/jira/browse/HPD-151)\] relax rules for IHEHPD-037 from error to warning
* \[[HPD-154](https://gazelle.ihe.net/jira/browse/HPD-154)\] [CH:HPD] Change in description of hcIdentifier attribute
* \[[HPD-155](https://gazelle.ihe.net/jira/browse/HPD-155)\] [CH:HPD] New object classes for community gateway information
* \[[HPD-156](https://gazelle.ihe.net/jira/browse/HPD-156)\] [CH:HPD] Changes in description of CHCommunity object class
* \[[HPD-157](https://gazelle.ihe.net/jira/browse/HPD-157)\] [CH:HPD] New organizational unit available for CPI
* \[[HPD-158](https://gazelle.ihe.net/jira/browse/HPD-158)\] [CH:HPD] Update cardinality and optionality's attributes

__Bug__

* \[[HPD-146](https://gazelle.ihe.net/jira/browse/HPD-146)\] Issues reported by eHealthSuisse HPD users
* \[[HPD-149](https://gazelle.ihe.net/jira/browse/HPD-149)\] Case Sensitivity HPD in dn's too strict
* \[[HPD-153](https://gazelle.ihe.net/jira/browse/HPD-153)\] The cardinality of the attributes owner and member in Relationship are incorrect

__Improvement__

* \[[HPD-152](https://gazelle.ihe.net/jira/browse/HPD-152)\] BIT updated documents HPD, CPI 13.09.2017

# 2.0.1
_Release date: 2017-09-11_

__Technical task__

* \[[HPD-143](https://gazelle.ihe.net/jira/browse/HPD-143)\] checks on address shall be case insensitive
* \[[HPD-144](https://gazelle.ihe.net/jira/browse/HPD-144)\] Check should be case insenstive for hcIdentifier, hcSpecialisation ...
* \[[HPD-145](https://gazelle.ihe.net/jira/browse/HPD-145)\] validation on binary fields fails
* \[[HPD-148](https://gazelle.ihe.net/jira/browse/HPD-148)\] assertion CH-HPD-010 is too restrictive

__Bug__

* \[[HPD-141](https://gazelle.ihe.net/jira/browse/HPD-141)\] Case sensitivity in request and response validations too strict
* \[[HPD-147](https://gazelle.ihe.net/jira/browse/HPD-147)\] businessCategory should be multi-valued

# 2.0.0
_Release date: 2017-09-04_

__Remarks__

This new release of HPDSimulator runs under Jboss 7.2.0 (previous versions were designed for Jboss5). It also includes the validator and simulator features for the EPR (eHealthSuisse) project.


__Technical task__

* \[[HPD-94](https://gazelle.ihe.net/jira/browse/HPD-94)\] [CH-HPD] Add validation rules for CH-HPD Provider Information Feed request
* \[[HPD-95](https://gazelle.ihe.net/jira/browse/HPD-95)\] [CH-HPD] Create samples for CH-HPD Provider Information Feed request
* \[[HPD-96](https://gazelle.ihe.net/jira/browse/HPD-96)\] [CH-HPD] Unit testing of Provider Information Feed request validation rules
* \[[HPD-101](https://gazelle.ihe.net/jira/browse/HPD-101)\] [CH-HPD] Integrate CH:HPD validators into HPDSimulator
* \[[HPD-102](https://gazelle.ihe.net/jira/browse/HPD-102)\] [CH-HPD] Integration testing
* \[[HPD-104](https://gazelle.ihe.net/jira/browse/HPD-104)\] [CH-HPD] Implement the validator for PIDD request
* \[[HPD-105](https://gazelle.ihe.net/jira/browse/HPD-105)\] [CH-HPD] Implement the validator for PIDD Response
* \[[HPD-106](https://gazelle.ihe.net/jira/browse/HPD-106)\] [CH-HPD] Create samples of PIDD requests and responses
* \[[HPD-107](https://gazelle.ihe.net/jira/browse/HPD-107)\] [CH-HPD] Unit testing of PIDD validators
* \[[HPD-142](https://gazelle.ihe.net/jira/browse/HPD-142)\] Fix case sensitivity in CPI validator

__Bug__

* \[[HPD-64](https://gazelle.ihe.net/jira/browse/HPD-64)\] When updating to gazelle-tools:3.0.25 or higher, fix dependencies to asm
* \[[HPD-124](https://gazelle.ihe.net/jira/browse/HPD-124)\] The content of the addRequest form is not taken into account
* \[[HPD-125](https://gazelle.ihe.net/jira/browse/HPD-125)\] Error in checking format of hcIdentifier attribute
* \[[HPD-128](https://gazelle.ihe.net/jira/browse/HPD-128)\] Cannot parse DSML response from DSMLv2Engine when it returns errorMessage
* \[[HPD-132](https://gazelle.ihe.net/jira/browse/HPD-132)\] Creation date for LDAP partitions does not reflect the correct date

__Epic__

* \[[HPD-67](https://gazelle.ihe.net/jira/browse/HPD-67)\] Migrate the HPD Simulator to Jboss7

__Story__

* \[[HPD-69](https://gazelle.ihe.net/jira/browse/HPD-69)\] [MIG HPDSimulator] Run the migration script
* \[[HPD-70](https://gazelle.ihe.net/jira/browse/HPD-70)\] [MIG HPDSimulator] Rework the entities
* \[[HPD-71](https://gazelle.ihe.net/jira/browse/HPD-71)\] [MIG HPDSimulator] Configure the project to use gazelle-assets
* \[[HPD-72](https://gazelle.ihe.net/jira/browse/HPD-72)\] [MIG HPDSimulator] Rework page for simulator as Prov Info Consumer
* \[[HPD-73](https://gazelle.ihe.net/jira/browse/HPD-73)\] [MIG HPDSimulator] Rework pages for simulator as Prov Info Directory
* \[[HPD-75](https://gazelle.ihe.net/jira/browse/HPD-75)\] [MIG HPDSimulator] Rework the page for registering SUT
* \[[HPD-76](https://gazelle.ihe.net/jira/browse/HPD-76)\] [MIG HPDSimulator] Rework the page for browsing the transaction instance
* \[[HPD-77](https://gazelle.ihe.net/jira/browse/HPD-77)\] [MIG HPDSimulator] Rework administration pages
* \[[HPD-78](https://gazelle.ihe.net/jira/browse/HPD-78)\] [MIG HPDSimulator] Create test plan in Testlink
* \[[HPD-103](https://gazelle.ihe.net/jira/browse/HPD-103)\] [CH-HPD] Generate Java classes for PIDD option
* \[[HPD-109](https://gazelle.ihe.net/jira/browse/HPD-109)\] [CH-HPD] Create projects for HPD CH validator
* \[[HPD-113](https://gazelle.ihe.net/jira/browse/HPD-113)\] Release HPD Simulator - 2.0.0
* \[[HPD-126](https://gazelle.ihe.net/jira/browse/HPD-126)\] Issue on modifyRequest edition panel
* \[[HPD-127](https://gazelle.ihe.net/jira/browse/HPD-127)\] modDNRequest message is not correctly built

__Improvement__

* \[[HPD-86](https://gazelle.ihe.net/jira/browse/HPD-86)\] [CH-HPD] Implement validators for PIDD option
* \[[HPD-87](https://gazelle.ihe.net/jira/browse/HPD-87)\] [CH-HPD] Implement the PIDD option for the Provider Information Consumer

# 1.0.5
_Release date: 2015-01-21_

__Bug__

* \[[HPD-60](https://gazelle.ihe.net/jira/browse/HPD-60)\] Typo in wsdl

# 1.0.4
_Release date: 2014-07-18_


__Bug__

* \[[HPD-57](https://gazelle.ihe.net/jira/browse/HPD-57)\] Problem to delete and to regenerate all the documentation of the constraints

# 1.0.3
_Release date: 2014-07-18_


__Bug__

* \[[HPD-54](https://gazelle.ihe.net/jira/browse/HPD-54)\] [CP-Saudi-eHealth-91-V4] The following constraints need to be reviewed KHPD-020, KHPD-043 and KHPD-026
* \[[HPD-55](https://gazelle.ihe.net/jira/browse/HPD-55)\] [CP-Saudi-eHealth-92-V6] Update the following constraints : KHPD-019, KHPD-023 and KHPD-032
* \[[HPD-56](https://gazelle.ihe.net/jira/browse/HPD-56)\] Update providers and organizations to match the new rules

# 1.0.2
_Release date: 2014-07-02_


__Bug__

* \[[HPD-51](https://gazelle.ihe.net/jira/browse/HPD-51)\] constraint khpd_020_2 is not correct
* \[[HPD-52](https://gazelle.ihe.net/jira/browse/HPD-52)\] constraint khpd_019 is not correct
* \[[HPD-53](https://gazelle.ihe.net/jira/browse/HPD-53)\] constraints khpd_014 and khpd_015 are not correct

# 1.0.1
_Release date: 2014-06-25_


__Bug__

* \[[HPD-49](https://gazelle.ihe.net/jira/browse/HPD-49)\] We need to be able to override the URL of the SVS repository

# 1.0.0
_Release date: 2014-06-20_


__Technical task__

* \[[HPD-2](https://gazelle.ihe.net/jira/browse/HPD-2)\] [ITI-58] Validator for Provider Information Query Request
* \[[HPD-3](https://gazelle.ihe.net/jira/browse/HPD-3)\] [ITI-58] Validator for Provider Information Query Response
* \[[HPD-4](https://gazelle.ihe.net/jira/browse/HPD-4)\] [ITI-59] Validator for Provider Information Feed
* \[[HPD-5](https://gazelle.ihe.net/jira/browse/HPD-5)\] [ITI-59] validator for Provider Information Feed Acknowledgement
* \[[HPD-12](https://gazelle.ihe.net/jira/browse/HPD-12)\] investigate on a open source LDAP server
* \[[HPD-13](https://gazelle.ihe.net/jira/browse/HPD-13)\] Define the schema to be used
* \[[HPD-14](https://gazelle.ihe.net/jira/browse/HPD-14)\] Populate the LDAP server with some data
* \[[HPD-15](https://gazelle.ihe.net/jira/browse/HPD-15)\] Forward DSML requests received by the simulator (in SOAP envelope) to the LDAP server
* \[[HPD-16](https://gazelle.ihe.net/jira/browse/HPD-16)\] implement SOAP webservice
* \[[HPD-17](https://gazelle.ihe.net/jira/browse/HPD-17)\] Add security checks
* \[[HPD-19](https://gazelle.ihe.net/jira/browse/HPD-19)\] Log information received from the LDAP directory
* \[[HPD-21](https://gazelle.ihe.net/jira/browse/HPD-21)\] create basic validator for DSMLv2
* \[[HPD-29](https://gazelle.ihe.net/jira/browse/HPD-29)\] mock-up of the user interface for create requests sent in the context of ITI-59
* \[[HPD-30](https://gazelle.ihe.net/jira/browse/HPD-30)\] SOAP WS Client
* \[[HPD-31](https://gazelle.ihe.net/jira/browse/HPD-31)\] User Interface - XHTML template for consumer and source actors
* \[[HPD-32](https://gazelle.ihe.net/jira/browse/HPD-32)\] Provider Information Source - User interface
* \[[HPD-33](https://gazelle.ihe.net/jira/browse/HPD-33)\] Provider Information Consumer - User interface
* \[[HPD-34](https://gazelle.ihe.net/jira/browse/HPD-34)\] Display the configuration of the Directory to the user

__Bug__

* \[[HPD-38](https://gazelle.ihe.net/jira/browse/HPD-38)\] 'false negatives' reported on HPD validator

__Story__

* \[[HPD-1](https://gazelle.ihe.net/jira/browse/HPD-1)\] Integrate a model-based validation service for HPD exchanged in the context of HPD profile
* \[[HPD-6](https://gazelle.ihe.net/jira/browse/HPD-6)\] Project specification and planning
* \[[HPD-7](https://gazelle.ihe.net/jira/browse/HPD-7)\] Create and configure the Maven project for HPDSimulator
* \[[HPD-9](https://gazelle.ihe.net/jira/browse/HPD-9)\] validator crashes when validating an address with line breaks
* \[[HPD-10](https://gazelle.ihe.net/jira/browse/HPD-10)\] Implement Provider Information Directory actor
* \[[HPD-11](https://gazelle.ihe.net/jira/browse/HPD-11)\] Set up a LDAP server
* \[[HPD-22](https://gazelle.ihe.net/jira/browse/HPD-22)\] Define a model to be used for building request from the GUI
* \[[HPD-23](https://gazelle.ihe.net/jira/browse/HPD-23)\] Display messages exchanged with the simulator
* \[[HPD-24](https://gazelle.ihe.net/jira/browse/HPD-24)\] Allow users to store the configuration of their SUT (PROV_INFO_DIR case)
* \[[HPD-25](https://gazelle.ihe.net/jira/browse/HPD-25)\] Management of LDAP Partitions
* \[[HPD-26](https://gazelle.ihe.net/jira/browse/HPD-26)\] Document how to install the simulator and how it works
* \[[HPD-27](https://gazelle.ihe.net/jira/browse/HPD-27)\] Implement Provider Information Consumer Actor
* \[[HPD-28](https://gazelle.ihe.net/jira/browse/HPD-28)\] Implement the Provider Information Source
* \[[HPD-37](https://gazelle.ihe.net/jira/browse/HPD-37)\] Review K-project specification about HPD use case
* \[[HPD-39](https://gazelle.ihe.net/jira/browse/HPD-39)\] A production report is expected
* \[[HPD-40](https://gazelle.ihe.net/jira/browse/HPD-40)\] Update the HPD Simulator tool to test the KHPD use case
* \[[HPD-41](https://gazelle.ihe.net/jira/browse/HPD-41)\] A validator for messages exchanged in the context of the KHPD use case is needed
* \[[HPD-42](https://gazelle.ihe.net/jira/browse/HPD-42)\] A production report is expected for the hpd validator
* \[[HPD-47](https://gazelle.ihe.net/jira/browse/HPD-47)\] Installation manual

__Improvement__

* \[[HPD-43](https://gazelle.ihe.net/jira/browse/HPD-43)\] Configure the project to provide a REST interface to the AssertionManager tool
* \[[HPD-44](https://gazelle.ihe.net/jira/browse/HPD-44)\] Configure the project to expose the version REST web service
