---
title: Release note
subtitle: XDS XCA Simulator
version: 1.0.1
releasedate: 2020-01-14
author: Nicolas Bailliet
function: Software Engineer
customer: eHealth Suisse
reference: KER1-RNO-IHE-XDS-XCA-SIMULATOR
---
# 1.0.1
_Release date: 2020-01-14_

__Bug__

* \[[CHXDS-2](https://gazelle.ihe.net/jira/browse/CHXDS-2)\] Actors in GWT Mock message browser have wrong names
* \[[CHXDS-4](https://gazelle.ihe.net/jira/browse/CHXDS-4)\] XDS XCA mock is no transfering the HTTP Header

# 1.0.0
_Release date: 2019-08-27_

__Story__

* \[[CHXDS-1](https://gazelle.ihe.net/jira/browse/CHXDS-1)\] CH:XDS/XCA 1.9
