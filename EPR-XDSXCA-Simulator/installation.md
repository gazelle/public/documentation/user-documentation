---
title:  Installation Manual
subtitle: XDS XCA Simulator
author: ycadoret
function: tester
releasedate: 16/09/2019
toolversion: 1.0.0
version: 1.02
status: REVIEWED
reference: KER1-MAN-IHE-XDS_XCA_SIMULATOR_INSTALLATION-1_02
customer: eHealth Suisse
---

# EPR XCA Responding gateway MockUp

## Overview

The EPR XCA Responding gateway MockUp is a SoapUI webservice (mock).

* default_wsdl_url: https://ehealthsuisse.ihe-europe.net/RespondingGateway?wsdl
* default_path: /RespondingGateway
* default_port: 8099
* default_mock_name: RespondingGateway
* default_mock_path: /opt/simulators/xds-xca-simulator
* default_soapui_path: /usr/local/SmartBear/SoapUI-5.3.0/
* default_soapui_mock_log: /var/log/soapui/respondingGateway.log
* default_init.d: /etc/init.d/xcaRespondingGateway 
* default_keystore_path: /opt/gazelle/cert/jboss.jks


## Install SoapUI

[https://www.soapui.org/](https://www.soapui.org/)

## Install EPR XCA MockUp

### Get the Subversion project

```bash
git clone https://gitlab.inria.fr/gazelle/specific-tools/epr/xds-xca-simulator.git $EPR_XDSXCA_MOCK_DIR
```

### Install libraries required by SoapUI

Copy the external jars (esapi, velocity and postgresql)

```bash
cp $EPR_XDSXCA_MOCK_DIR/external_jar/esapi-2.1.0.1.jar $SOAPUI_INSTALL_DIR/lib/
cp $EPR_XDSXCA_MOCK_DIR/external_jar/velocity-1.7.jar $SOAPUI_INSTALL_DIR/lib/
cp $EPR_XDSXCA_MOCK_DIR/external_jar/postgresql-9.3-1102.jdbc4.jar $SOAPUI_INSTALL_DIR/lib/
```

## Mock as a service

### Prepare the init.d script

Edit the init.d script `$EPR_XDSXCA_MOCK_DIR/init.d/xcaRespondingGateway` and set the following environment variables

* SOAPUI_PATH => Path of SoapUI folder
* SOAPUI_PROJECT_PATH => Path of SoapUI project script
* SOAPUI_MOCK_NAME => Name of the SoapUI mock
* SOAPUI_MOCK_PORT => Port of the SoapUI mock
* SOAPUI_MOCK_ENDPOINT => Path of the SoapUI mock
* SOAPUI_MOCK_LOG => Path where to publish log file

### Declare the service

Type the following commands register the init.d script as service

```bash
sudo cp $EPR_ADR_MOCK_DIR/init.d/xcaRespondingGateway /etc/init.d/xcaRespondingGateway
sudo chmod u+x /etc/init.d/xcaRespondingGateway
sudo chmod 775 /etc/init.d/xcaRespondingGateway
```

If you want the service to start at each machine start up

```bash
sudo update-rc.d xcaRespondingGateway defaults
```

Be careful to user rights to allow the service to write logs into your target directory. As example

```bash
sudo mkdir /var/log/soapui
sudo chmod 775 /var/log/soapui
```

### Start the mock

To run the mock

```bash
sudo /etc/init.d/xcaRespondingGateway start
```

To stop the mock

```bash
sudo /etc/init.d/xcaRespondingGateway stop
```

To get status of the mock

```bash
sudo /etc/init.d/xcaRespondingGateway status
```


## Troubleshouting

You might need to install those following packets

```bash
sudo apt-get install -y libxrender1 libxtst6 libxi6
```


# EPR XDS Registry MockUp

## Overview

The EPR XDS Registry MockUp is a SoapUI webservice (mock).

* default_wsdl_url: http://ehealthsuisse.ihe-europe.net/DocumentRegistry?wsdl
* default_path: /DocumentRegistry
* default_port: 8098
* default_mock_name: DocumentRegistry
* default_mock_path: /opt/simulators/xds-xca-simulator
* default_soapui_path: /usr/local/SmartBear/SoapUI-5.3.0/
* default_soapui_mock_log: /var/log/soapui/documentRegistry.log
* default_init.d: /etc/init.d/DocumentRegistry 
* default_keystore_path: /opt/gazelle/cert/jboss.jks


## Install SoapUI

[https://www.soapui.org/](https://www.soapui.org/)

## Install EPR XDS Registry MockUp

### Get the Subversion project

```bash
git clone https://gitlab.inria.fr/gazelle/specific-tools/epr/xds-xca-simulator.git $EPR_XDSXCA_MOCK_DIR
```

### Install libraries required by SoapUI

Copy the external jars (esapi, velocity and postgresql)

```bash
cp $EPR_XDSXCA_MOCK_DIR/external_jar/esapi-2.1.0.1.jar $SOAPUI_INSTALL_DIR/lib/
cp $EPR_XDSXCA_MOCK_DIR/external_jar/velocity-1.7.jar $SOAPUI_INSTALL_DIR/lib/
cp $EPR_XDSXCA_MOCK_DIR/external_jar/postgresql-9.3-1102.jdbc4.jar $SOAPUI_INSTALL_DIR/lib/
```

## Mock as a service

### Prepare the init.d script

Edit the init.d script `$EPR_XDSXCA_MOCK_DIR/init.d/DocumentRegistry` and set the following environment variables

* SOAPUI_PATH => Path of SoapUI folder
* SOAPUI_PROJECT_PATH => Path of SoapUI project script
* SOAPUI_MOCK_NAME => Name of the SoapUI mock
* SOAPUI_MOCK_PORT => Port of the SoapUI mock
* SOAPUI_MOCK_ENDPOINT => Path of the SoapUI mock
* SOAPUI_MOCK_LOG => Path where to publish log file

### Declare the service

Type the following commands register the init.d script as service

```bash
sudo cp $EPR_ADR_MOCK_DIR/init.d/DocumentRegistry /etc/init.d/DocumentRegistry
sudo chmod u+x /etc/init.d/DocumentRegistry
sudo chmod 775 /etc/init.d/DocumentRegistry
```

If you want the service to start at each machine start up

```bash
sudo update-rc.d DocumentRegistry defaults
```

Be careful to allow the service to write logs into your target directory. As example

```bash
sudo mkdir /var/log/soapui
sudo chmod 775 /var/log/soapui
```

### Start the mock

To run the mock

```bash
sudo /etc/init.d/DocumentRegistry start
```

To stop the mock

```bash
sudo /etc/init.d/DocumentRegistry stop
```

To get status of the mock

```bash
sudo /etc/init.d/DocumentRegistry status
```


## Troubleshouting

You might need to install those following packets

```bash
sudo apt-get install -y libxrender1 libxtst6 libxi6
```
