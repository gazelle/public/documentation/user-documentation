---
title:  User Manual
subtitle: Schematron Validator
author: Anne-Gaëlle BERGE
date: 26/02/2024
toolversion: 2.X.X
function: Software Engineer
version: 1.04
status: Approved document
reference: KER1-MAN-IHE-SCHEMATRON_VALIDATOR_USER-1_04
customer: IHE-EUROPE
---

# Project Overview

This application is part of the External Validation Services provides by the Gazelle project. This project is made of two parts: a Web interface and a Web Service.

The Web interface [*Schematron Validator*](https://gazelle.ihe.net/SchematronValidator) enables the administrator of the application to register new schematrons and update schematrons version:

![](./media/image19-1.png)

Click on "Manage Schematrons" button to display the list of registered schematrons:
![](./media/image19-2.png)

Then click on the green button in the bottom left hand corner to create a new schematron:
![](./media/image19-3.png)


The Web Service [*Schematron Validator*](https://gazelle.ihe.net/SchematronValidator-ejb/SchematronValidatorService/SchematronValidator?wsdl) enables the user and other Gazelle applications to validate objects using those schematrons.

By now, schematrons have been written for the following kinds of documents:

* CDA documents (epSOS, IHE and other regional projects)
* HL7v3 messages (epSOS and IHE)
* SAML Assertions (epSOS)
* ATNA logging messages (epSOS)
* FHIR messages

One can access the webservice using the [*EVS Client Front-end*](https://gazelle.ihe.net/EVSClient), access to the schematrons used for the validation of documents is available from this same application.

Unless a user would like to perform mass document validation using the webservice functionality of that tool, the schematron validation should be performed using the GUI provided by the [*EVS Client Front-end*](https://gazelle.ihe.net/EVSClient).

The next section is mainly dedicated to users interested in learning more about the validation process and the methodology to call the webservice.

# Validation based on Schematron

The validation based on schematron can be performed for any kind of XML files (CDA, HL7v3 messages, SAML Assertions and so on). The XML document is processed three times before the tool can give the validation result.

1.  We first check the document is a well-formed XML. If it is not the case, the validation stopped and the FAILED result is sent back.

2.  Then, if the invoked schematron is linked to an XSD schema, we check the document is valid according to the schema. If it is not the case, the validation will go on but the result will be FAILED. Concerning CDA documents, we distinguish IHE CDA from epSOS CDA. At XSD validation step, the first ones are validated against CDA.xsd and the second ones against CDA\_extended.xsd.

3.  Then, if the document is of type CDA, we validate against the abstract CDA model specification ([*CDA validation details*](https://gazelle.ihe.net/content/cda-document-validation)).

4.  Finally, the XML document is validated against the selected schematron. Validation is performed using Saxon 9HE.

Note that, in some cases, no schematron has been provided and the validation process stops after the XSD validation.

# Web Service

The wsdl describing the service is available at [*Schematron Validator*](https://gazelle.ihe.net/SchematronValidator-ejb/SchematronValidatorService/SchematronValidator?wsdl).

To register soapUI XML project file, click on link above and save page displayed as **SchematronValidatorWS-SchematronValidator-soapui-project_0.xml** in your working repository.

SoapUI is available for download [*here*](https://www.soapui.org).

After SoapUi installation, run it and create schematron validation project by importing the downloaded XML file.

# Features

Schematron-based Validator implements various web service methods which are:

***about***: returns information about the webservice.

***getListOfValidators***: returns the list of all Schematron objects stored in the database. See javadoc for more information about the Schematron object attributes.

***getSchematronByName***: returns a Base64-encoded String reprensenting the content of the schematron file selected by its name.
The value of parameter **schematronName** in ***getSchematronByName*** request, shown below, is available at
[*Manage Schematrons*](https://gazelle.ihe.net/SchematronValidator/schematrons/manageSchematrons.seam).
```xml
 <schematronName>epSOS - ePrescription - Friendly</schematronName>
```  
The **schematronName** is **Label**:
![](./media/image19-6.png)

***getSchematronForAGivenType*** : returns the list of schematrons which are linked to the given object type (CDA, HL7v3 ...).
The value of parameter **fileType** in ***getSchematronForAGivenType*** request, shown below, is available at
[*Object Type Management*](https://gazelle.ihe.net/SchematronValidator/schematrons/manageObjectTypes.seam).
```xml
<fileType>CDA-epSOS</fileType>
```  
The **fileType** is **Keyword**:
![](./media/image19-7.png)

***validateObject*** : validate the given XML document against the given schematron.
The value of parameter **xmlMetadata** in ***validateObject*** request, shown below, is schematron name (Label):
```xml
   <xmlMetadata>epSOS - ePrescription - Friendly</xmlMetadata>
```  
***getAllAvailableObjectTypes*** : returns the list of object type that can be validated using this validator.

Validation results are formatted as an XML document which can be downloaded from [*EVS Client Front-end*](https://gazelle.ihe.net/EVSClient) after validation process:
![](./media/image19-8.png)

The validation results are also displayed:
![](./media/image19-9.png)


# Static WS Client for Schematron-based Validator Web Service

We have generated a Static Web Service client using Axis 2. This related jar is stored in our maven repository [*Nexus Repository Manager*](https://gazelle.ihe.net/nexus) and is easy to use. You only have to make a dependency to the artifact as shown below.

```xml
<dependency>
  <groupId>net.ihe.gazelle.modules</groupId>
  <artifactId>gazelle-ws-clients</artifactId>
  <version>2.1.3</version>
</dependency>
```                                            

# Administration

__The tasks introduced below are only accessible to users with admin_role permission and with access to the hosting system__

The functioning of the Schematron Validator tool is based on two main concepts:

* Object types are used to type the schematron and thus allow the filtering on the client side
* Schematron stands for the description of a schematron file with metadata used by the tool to properly process it

Being logged as administrator will give you access to the following features:

* Edit, copy, delete schematron definitions
* Edit, delete object types

## Management of object types

An object type is used to help the clients of the validator to retrieve a subset of schematrons. That means that an object type is linked to one or more schematron. The attributes of the object types are:

* **Name:** what is displayed in the user interface of the schematron section
* **Keyword:** uniquely identified an object type, it is the key used by the client to designate an object type
* **Description:** helps the user with understanding the purpose of the object type.

As a convention, the keyword of the object type is made of the short name of the standard the schematron has been developed for followed by a dash (-) and the affinity domain/extension (IHE, epSOS, ASIP ...). This is how EVSClient builds the keyword to retrieve the list of schematrons.

### Edition

Any object type can be edited by using the pencil icon.

### Deletion

Object types which are not bound to a schematron can be deleted thanks to the trash icon.

## Management of Schematrons

For each schematron file, you need to create a schematron entry in the tool. The attributes of a schematron are the following:

* **ID:** automatically given by the tool. Useful to retrieve the compiled XSL related to your schematron
* **Name**: the name of your schematron which will be used in the webservice to tell which schematron to use. Also used by EVSClient to display the list of schematrons to the user.
* **Keyword:** uniquely identified the schematron
* **Version:** to keep track of your versions of schematrons. Reported in the validation
* **Author:** Who developed the schematron report so that users can see if results differ because the schematron has changed.
* **Type:**
  * **ART-DECOR:** for schematrons generated by Art-Decor, or also schematrons that include other schematrons.
  * **Standard:** for any other schematron.
* **Description:** Free text field to describe the purpose of the schematron
* **Last changed:** updated each time you modified the entry
* **Path:** Relative path to the schematron file. Base path is $bin_path$/schematron where $bin_path$ is the value of the application preference named 'bin_path'.
* **This schematron uses relative paths ?** Check this box if your schematron contains paths to other files which are given in a relative way. In that case, the base path used for absolute path computation is the directory where the schematron is stored.
* **XSD Path:** Relative path to the XSD to be used for checking that the document is correctly formatted. Base path is $gazelle_home_path$/xsd where $gazelle_home_path$ is the value of the application preference named 'gazelle_home_path'.
* **XSD Version:** Schematron validator supports both 1.0 and 1.1
* **Object Type:** The type of object which is checked by this schematron
* **Need daffodil transformation:** whether or not the file shall be processed by Daffodil before being validated with the schematron
* **DFDL schema keyword:** If previous attribute is set to true, the keyword of the Daffodil schema as defined in Gazelle transformation service
  * **Json validation:** If you want to validate Fhir Json Bundle, please put "fhirJsonToXML" as keyword of the Daffodil schema so that will call the hapi fhir Json to XML transformation feature and validate the XML against your Schematron file.
* **Available:** whether or not the schematron shall be made available to the user
* **Need report generation transformation:** if the type Art-Decor is used, this box need to be checked to get all the check in the validation report.
* **Process Unknown check type as an error?:** If the output of the <assert> element is not typed, whether or not to report them as error


### File management

Currently, the only way to push files on the server is to copy them directly in the file system using SCP.

Schematrons shall be stored in the folder $bin_path$/schematron where $bin_path$ is the value of the application preference named 'bin_path' (usually /opt/Schematron_Validator_Prod/bin/).

XSD shall be stored in the folder $gazelle_home_path$/xsd where $gazelle_home_path$ is the value of the application preference named 'gazelle_home_path' (usually /opt/Schematron_Validator_Prod/).

### Edition

Any entry in the list of schematrons can be edited by using the pencil icon. Content of the Schematron itself shall be updated offline and then updated on the server.

### Deletion

Any entry in the list of schematrons can be deleted using the trash icon. This operation does not delete the referenced schematron file from the system.

### Copy

You can duplicate an entry by using the copy icon. All the attributes of the schematron will be copied in a new entry. The newly created schematron will have ".copy" appended at the end of its name and keyword.

### Reload XSD

For better performance, XSD factories are created only once. If the XSD file changes on disk, you need to click on the "reload XSD" to have the changes taken into account.

### Compile

The Schematron files are compiled to be processed by the tool. If you look for the resulting XSL, you will find it in TOOL_DIRECTORY/bin/compilations/ID.xsl. The compilation is performed each time the file changes on disk. You can force the compilation by clicking on the green play icon.
