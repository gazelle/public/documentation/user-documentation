---
title: Release note
subtitle: HL7V2 Simulators
author: Romuald DUBOURG, Claude Lusseau
date: 30/01/2024
toolversion: 1.3.2
function: Software Engineer
customer: ANS
---

# 1.3.2
_Release date: 2024-11-13_

__Bug__
* \[[HLVSIM-47](https://gazelle.ihe.net/jira/browse/HLVSIM-47)\] ACK non envoyé si erreur durant l'envoie des ZAM

# 1.3.1
_Release date: 2024-04-29_

__Tâche__
* \[[HLVSIM-46](https://gazelle.ihe.net/jira/browse/HLVSIM-46)\] HL7v2 simu - Plage de port sur page d'accueil

# 1.3.0
_Release date: 2024-04-09_

__IT Help__
* \[[HLVSIM-44](https://gazelle.ihe.net/jira/browse/HLVSIM-44)\] MAJ Simulateur HL7V2

# 1.2.1
_Release date: 2024-02-08_

__Improvement__
* \[[HLVSIM-42](https://gazelle.ihe.net/jira/browse/HLVSIM-42)\] Modification IHM

Remove ISO8859-1 ser ver and configuration
Remove serverside colum for SUTConfiguration

# 1.2.0
_Release date: 2024-01-30_

__Improvement__
* \[[HLVSIM-29](https://gazelle.ihe.net/jira/browse/HLVSIM-29)\] Traduction text and docker image
* \[[HLVSIM-30](https://gazelle.ihe.net/jira/browse/HLVSIM-30)\] Docker image can't write file
* \[[HLVSIM-38](https://gazelle.ihe.net/jira/browse/HLVSIM-38)\] Release & Déploiement

__Bug__
* \[[HLVSIM-39](https://gazelle.ihe.net/jira/browse/HLVSIM-39)\] Problems with flywaydb migration

# 1.0.0
_Release date: 2023-7-11_

__Story__
* \[[HLVSIM-1](https://gazelle.ihe.net/jira/browse/HLVSIM-1)\] Create Hl7V2 Simulators
