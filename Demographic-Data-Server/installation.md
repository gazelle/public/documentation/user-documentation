---
title:  Installation Manual
subtitle: Demographic Data Server
author: Maxime ARIF
date: 05/02/2024
toolversion: 4.X.X
function: Engineer
version: 1.02
status: Approved
reference: KER1-MAN-IHE-DEMOGRAPHIC_DATA_SERVER_INSTALLATION-1_02
customer: IHE-EUROPE
---
# Sources and package

Demographic Data Server is a Maven project. Sources are available on the INRIA's Gitlab project at the following URL :

[*https://gitlab.inria.fr/gazelle/public/processing/demographic-data-server.git*](https://gitlab.inria.fr/gazelle/public/processing/demographic-data-server.git)

If you would like to checkout the sources on your system you might want to use the following git command, provided git is installed on your system.

```bash
git clone https://gitlab.inria.fr/gazelle/public/processing/demographic-data-server.git
```

Note that the latest executable can be extracted from our nexus repository as well at :
[*Nexus Repository Manager*](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22DemographicDataServer-ear%22).

**Important**, if you download the **ear** from Nexus, it will have a name such as `DemographicDataServer-ear-X.X.X.ear`. Then be sure to rename it to `DemographicDataServer.ear` otherwise the deployment will fail.

# Installation

This tool has to be deployed  using a JBoss 7.2 application server and runs with a PostgreSQL database.

If you do not have yet a JBoss server installed in your environment, proceed as described in [General consideration page](https://gazelle.ihe.net/gazelle-documentation/General/jboss7.html).

## Database creation

Your database must have a user named **gazelle**.
1. Download the SQL scripts archive from [*Nexus Repository Manager*](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22DemographicDataServer-ear%22) (search for **DemographicDataServer-ear-X.X.X-sql.zip**).
2. Unzip the archive into a folder `DemographicDataServer-sql`
3. Connect to your database :
```bash
psql -U gazelle
```
4. Execute the SQL statement to create the database.
```sql
CREATE DATABASE "demographic-data-server" OWNER gazelle ENCODING UTF8 ;
```
5. Initialize database by running :
```bash
psql -U gazelle demographic-data-server < DemographicDataServer-sql/init-x.x.x.sql
```

## Deployment
To deploy Demographic Data Server:
1. Paste the archive **DemographicDataServer.ear** in the JBoss deployment directory `${JBOSS7\_HOME}/standalone/deployments/`
2. Display JBoss server logs, start JBoss and wait for **ear** deployment.
3. The application can be browsed at http://localhost:8080/DDS. *Port could also be different whether you have modified the JBoss configuration*.
4. **WARNING** : From version 4.2.0, datasources have been extracted from the **ear**. The template file can be found in /src/main/application/datasource in the source or in the file DemographicDataServer-X.X.X-datasource.zip from the nexus.
For more informations about how to manage that externalization, please refer to [general considerations for JBoss7](https://gazelle.ihe.net/gazelle-documentation/General/jboss7.html).

Datasource name : DemographicDataServerDS

Database name : demographic-data-server

# Update from a previous version

1. Download the new **ear** and its associated update SQL script from our [*Nexus Repository Manager*](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22DemographicDataServer-ear%22) (search for **DemographicDataServer-X.X.X.ear** and **DemographicDataServer-X.X.X-sql.zip**). Not each version has an update sql script to execute.

1. Unzip the SQL archive.

1. Execute SQL statements. Check that the database is correctly updated.

1. Deploy the new **ear**

**Due to the update mechanism of the database, it is important to do not skip any version of the application in an overall update. You cannot go directly from 2.0.0 to 2.3.0 : you will have to repeat the process from 2.0.0 to 2.0.1, 2.0.2, 2.0.3 and so on.**

# Configuration

| Preference name | Description | Default value |
|-----------------|-----------------------------------------|---------------------|
|application_admin_email         | An email address. Administrator of the tool might received messages when errors occur in the application such as compilation failure of schematron                                        | -                    |
|application_admin_name   |Application admin name   |-                    |
|application_admin_title    | Application admin title     |Application Administrator|
|application_release_notes_url |   |https://gazelle.ihe.net/jira/browse/DDS#selectedTab=com.atlassian.jira.plugin.system.project%3Achangelog-panel|
|application_documentation         |     Link to the user guide                                   |  https://gazelle.ihe.net/gazelle-documentation/Demographic-Data-Server/user.html                  |
|application_mode         |                   |  full    |
|application_url         |  URL to access the application  |  https://FQDN/DDS    |
|DDS_domain         |    |  DDS   |
|DDS_OID         |  Unique identifier of the tool instance  |  1.1.1.1.1.1.1.1.1.1.1.1   |
|hl7v3_sender_timeout         |   |  15000   |
|issue_tracker_url         |  Link to the issue reporter (JIRA) |  https://gazelle.ihe.net/jira/browse/DDS   |
|time_zone         |  Used to display date/time in the appropriate time zone |  Europe/Paris   |

### SSO Configuration

There are additional preferences to configure the SSO authentication.

| Preference name      | Description                                                             | Example of value |
| ----------------     | ----------------------------------------------------------------------  | ---------------- |
| **cas_enabled**      | Enable or disable the CAS authentication.                               | true             |
| **ip_login**         | Enable authentication by IP address matching `ip_login_admin` regex.    | false            |
| **ip_login_admin**   | Regex to authorize ip authentication if CAS authentication is disabled. |  .*              |

For more documentation about SSO configurations, follow the link [here](https://gitlab.inria.fr/gazelle/public/framework/sso-client-v7/-/blob/master/cas-client-v7/README.md).