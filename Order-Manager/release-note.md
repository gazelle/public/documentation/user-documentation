---
title: Release note
subtitle: Order Manager
toolversion: 6.0.2
releasedate: 2024-05-24
author: Anne-Gaëlle BERGE
function: Software Engineer
customer: IHE Europe
reference: KER1-RNO-IHE-ORDER_MANAGER
---

# 6.0.2
_Release date: 2024-06-04_

__Bug__
* \[[OM-371](https://gazelle.ihe.net/jira/browse/OM-371)\] Cannot open Data Browser > Orders


# 6.0.1
_Release date: 2024-05-24_

__Bug__
* \[[OM-370](https://gazelle.ihe.net/jira/browse/OM-370)\] Order Manager's worklist does not return all the matching and thus users are blocked in their testing process

# 6.0.0
_Release date: 2024-02-05_

Context : Gazelle User Management Renovation step 2

__Task__
* \[[OM-367](https://gazelle.ihe.net/jira/browse/OM-367)\] Integrate new sso-client-v7
* \[[OM-368](https://gazelle.ihe.net/jira/browse/OM-368)\] Remove username display in GUI

# 5.3.2
_Release date: 2021-11-23_

__Bug__
* \[[OM-365](https://gazelle.ihe.net/jira/browse/OM-365)\] DICOM Remote Validation is leading to an empty page
* \[[OM-366](https://gazelle.ihe.net/jira/browse/OM-366)\] Patient Generation gives an error when accessing the country menu

__Remarks__

# 5.3.1
_Release date: 2019-05-23_

__Bug__

* \[[OM-352](https://gazelle.ihe.net/jira/browse/OM-352)\] The list of orders is empty when you want to create a new procedure from an existing order

# 5.3.0
_Release date: 2019-03-15_

__Remarks__

Datasource management has changed, refers to the installation manual to correctly update your instance of the tool.

__Improvement__

* \[[OM-342](https://gazelle.ihe.net/jira/browse/OM-342)\] Extract datasource configuration
* \[[OM-343](https://gazelle.ihe.net/jira/browse/OM-343)\] Update SQL scripts archive

# 5.2.3
_Release date: 2018-10-15_


__Bug__

* \[[OM-331](https://gazelle.ihe.net/jira/browse/OM-331)\] Move to HL7v2.5.1 for ADT messages
* \[[OM-332](https://gazelle.ihe.net/jira/browse/OM-332)\] Display the XML dump of the worklist
* \[[OM-334](https://gazelle.ihe.net/jira/browse/OM-334)\] DICOM worklist are not correctly encoded

__Improvement__

* \[[OM-318](https://gazelle.ihe.net/jira/browse/OM-318)\] Fix javadoc to be compliant with java8 requirements

# 5.2.1
_Release date: 2018-03-14_


__Bug__

* \[[OM-330](https://gazelle.ihe.net/jira/browse/OM-330)\] The tool does not select the accurate HL7 message profile for ACK^O19^ACK messages

# 5.2.0
_Release date: 2018-03-09_

__Remarks__

This version of the Order Manager tool uses the jdbc driver provided by the Jboss7 AS server.


__Improvement__

* \[[OM-329](https://gazelle.ihe.net/jira/browse/OM-329)\] Update postgresql driver to version 42.2.1-jre7

# 5.1.1
_Release date: 2018-01-30_


__Bug__

* \[[OM-327](https://gazelle.ihe.net/jira/browse/OM-327)\] Cannot create a procedure for a new patient

# 5.1.0
_Release date: 2018-01-22_

__Remarks__

From version 5.1.0, Order Manager uses the apereo version of Gazelle SSO.


__Bug__

* \[[OM-323](https://gazelle.ihe.net/jira/browse/OM-323)\] Review critical bugs reported by sonar and fix them

__Improvement__

* \[[OM-325](https://gazelle.ihe.net/jira/browse/OM-325)\] Switch to new version of Gazelle SSO
* \[[OM-326](https://gazelle.ihe.net/jira/browse/OM-326)\] Allow user to define patient's last name/first name

# 5.0.3
_Release date: 2017-11-03_


__Bug__

* \[[OM-320](https://gazelle.ihe.net/jira/browse/OM-320)\] File underConstruction.xhtml is not found

__Story__

* \[[OM-319](https://gazelle.ihe.net/jira/browse/OM-319)\] Integrate latest version of PatientGeneration

__Improvement__

* \[[OM-321](https://gazelle.ihe.net/jira/browse/OM-321)\] modality worklist log reader is starting a process that uses 100% CPU

# 5.0.2
_Release date: 2017-03-30_


__Bug__

* \[[OM-262](https://gazelle.ihe.net/jira/browse/OM-262)\] SWF/SWF.b
* \[[OM-308](https://gazelle.ihe.net/jira/browse/OM-308)\] Order Manager cannot display C-FIND MWL Response
* \[[OM-313](https://gazelle.ihe.net/jira/browse/OM-313)\] [RAD-4/RAD-13] Order Manager is sending "accesionIdentifer" in IPC-1 instead of the value of the field

__Story__

* \[[OM-311](https://gazelle.ihe.net/jira/browse/OM-311)\] New Radiology codes to add to support REM-NM testing

__Improvement__

* \[[OM-276](https://gazelle.ihe.net/jira/browse/OM-276)\] Make use of evs-client-connector to validate DICOM messages
* \[[OM-312](https://gazelle.ihe.net/jira/browse/OM-312)\] Add two new return keys support REM-NM requirements for MWL (RAD-5)

# 5.0.0
_Release date: 2016-10-28_

__Remarks__
gazelle-proxy-netty is a pre-release which was built for the needs of another project, there is no need to use this version in the Order Manager.
This new version of the Order Manager tools runs under Jboss AS 7.2.0. A huge migration of the database shall be performed on existing installation using SQL script update-5.0.0.sql, available in the directory of the project.


__Technical task__

* \[[OM-249](https://gazelle.ihe.net/jira/browse/OM-249)\] Fix the style of AWOS Query page
* \[[OM-250](https://gazelle.ihe.net/jira/browse/OM-250)\] Fix the style of the LAB-27 page for Analyzer Manager
* \[[OM-253](https://gazelle.ihe.net/jira/browse/OM-253)\] Fix the layout of orderSender page
* \[[OM-254](https://gazelle.ihe.net/jira/browse/OM-254)\] New RAD/EYE order creation
* \[[OM-255](https://gazelle.ihe.net/jira/browse/OM-255)\] RAD/EYE order cancellation
* \[[OM-256](https://gazelle.ihe.net/jira/browse/OM-256)\] RAD/EYE order status update
* \[[OM-258](https://gazelle.ihe.net/jira/browse/OM-258)\] Procedure management: review style
* \[[OM-259](https://gazelle.ihe.net/jira/browse/OM-259)\] Procedure management: creation
* \[[OM-260](https://gazelle.ihe.net/jira/browse/OM-260)\] Procedure management : update

__Bug__

* \[[OM-122](https://gazelle.ihe.net/jira/browse/OM-122)\] A awos with this placer group number already exists (group span over multiple orders)
* \[[OM-187](https://gazelle.ihe.net/jira/browse/OM-187)\] Some connection id values appear to be negative integer
* \[[OM-188](https://gazelle.ihe.net/jira/browse/OM-188)\] Placer/Filler order number are not correctly built if values are missing
* \[[OM-214](https://gazelle.ihe.net/jira/browse/OM-214)\] LAB-28 Cancel AWOS
* \[[OM-261](https://gazelle.ihe.net/jira/browse/OM-261)\] LAB-27/received queries: Popup is not updated when clicking on the refresh icon
* \[[OM-284](https://gazelle.ihe.net/jira/browse/OM-284)\] The tool should create labels based on the content of the LAB-62 response
* \[[OM-286](https://gazelle.ihe.net/jira/browse/OM-286)\] OF/LAB-4 (O35) - removing a container from the list does not refresh the view
* \[[OM-287](https://gazelle.ihe.net/jira/browse/OM-287)\] Strange behaviour on observation creation
* \[[OM-289](https://gazelle.ihe.net/jira/browse/OM-289)\] OM-67:User shall be able to browse DICOM messages
* \[[OM-291](https://gazelle.ihe.net/jira/browse/OM-291)\] OM-48:User shall be able to access details on AWOS
* \[[OM-292](https://gazelle.ihe.net/jira/browse/OM-292)\] OM-49: "target blank" links open in the same browser tab
* \[[OM-293](https://gazelle.ihe.net/jira/browse/OM-293)\] OM-50: for objects received within a OML_O35 message, there is a link to the order which does not work (nothing happens)
* \[[OM-294](https://gazelle.ihe.net/jira/browse/OM-294)\] OM-119: DICOM worklist handles the first query and then does not respond to the next ones
* \[[OM-295](https://gazelle.ihe.net/jira/browse/OM-295)\] OM-223: message is rejected by the OF with "This visit number is already used for another patient"
* \[[OM-297](https://gazelle.ihe.net/jira/browse/OM-297)\] OM-171: Tool crashes upon receipt of AWOS status update message
* \[[OM-299](https://gazelle.ihe.net/jira/browse/OM-299)\] OM-225: Cannot create OUL_R23 message on Automation Manager

__Epic__

* \[[OM-200](https://gazelle.ihe.net/jira/browse/OM-200)\] Migrate Order Manager to Jboss 7
* \[[OM-207](https://gazelle.ihe.net/jira/browse/OM-207)\] Merge LBL simulator into Order Manager

__Story__

* \[[OM-230](https://gazelle.ihe.net/jira/browse/OM-230)\] Reuse features coming from HL7Common
* \[[OM-231](https://gazelle.ihe.net/jira/browse/OM-231)\] Review and fix DataBrowser style
* \[[OM-232](https://gazelle.ihe.net/jira/browse/OM-232)\] Review and fix Rad/Eye order management style
* \[[OM-233](https://gazelle.ihe.net/jira/browse/OM-233)\] Review and fix Worklist creation style
* \[[OM-234](https://gazelle.ihe.net/jira/browse/OM-234)\] Review and fix Lab order management style
* \[[OM-235](https://gazelle.ihe.net/jira/browse/OM-235)\] Review and fix Lab result management style
* \[[OM-236](https://gazelle.ihe.net/jira/browse/OM-236)\] Review and fix LAB-27 pages
* \[[OM-237](https://gazelle.ihe.net/jira/browse/OM-237)\] Review and fix LAB-29 page
* \[[OM-238](https://gazelle.ihe.net/jira/browse/OM-238)\] Review and fix RAD-5 page
* \[[OM-239](https://gazelle.ihe.net/jira/browse/OM-239)\] Review and fix DICOM SCP configuration page
* \[[OM-240](https://gazelle.ihe.net/jira/browse/OM-240)\] Review and fix Manage issuers page
* \[[OM-241](https://gazelle.ihe.net/jira/browse/OM-241)\] Review and fix Manage worklist page
* \[[OM-243](https://gazelle.ihe.net/jira/browse/OM-243)\] Review and fix User preferences page
* \[[OM-244](https://gazelle.ihe.net/jira/browse/OM-244)\] Review and fix dispatcher page
* \[[OM-246](https://gazelle.ihe.net/jira/browse/OM-246)\] Review the application template and menu
* \[[OM-248](https://gazelle.ihe.net/jira/browse/OM-248)\] Get rid of gif and png incons
* \[[OM-263](https://gazelle.ihe.net/jira/browse/OM-263)\] Review and fix LAB-28 page
* \[[OM-264](https://gazelle.ihe.net/jira/browse/OM-264)\] As an implementer of a LBL/LIP actor, I need to send LAB-62 messages to test the behaviour of my system
* \[[OM-265](https://gazelle.ihe.net/jira/browse/OM-265)\] As an implementer of a LBL/LIP actor, I need to send LAB-63 messages to test the behaviour of my system
* \[[OM-266](https://gazelle.ihe.net/jira/browse/OM-266)\] As an implementer of a LBL/LB actor, I need to send LAB-61 messages to test the behaviour of my system
* \[[OM-267](https://gazelle.ihe.net/jira/browse/OM-267)\] As an implementer of a LBL/LIP actor, I need the simulator to be able to acknowledge LAB-61 messages to test the conformance of my system
* \[[OM-268](https://gazelle.ihe.net/jira/browse/OM-268)\] As an implementer of a LBL/LB actor, I need the simulator to be able to acknowledge LAB-62 messages to test the conformance of my system
* \[[OM-269](https://gazelle.ihe.net/jira/browse/OM-269)\] As an implementer of a LBL/LB actor, I need the simulator to be able to acknowledge LAB-63 messages to test the conformance of my system
* \[[OM-275](https://gazelle.ihe.net/jira/browse/OM-275)\] Static code analysis with sonar

__Improvement__

* \[[OM-199](https://gazelle.ihe.net/jira/browse/OM-199)\] Order Manager
* \[[OM-277](https://gazelle.ihe.net/jira/browse/OM-277)\] Improve the way to display the wlmscpfs logs
* \[[OM-283](https://gazelle.ihe.net/jira/browse/OM-283)\] [LAB-61] Allow the user to edit the attributes of the additional labels
* \[[OM-285](https://gazelle.ihe.net/jira/browse/OM-285)\] [LAB-63] Allow the user to send several specimen groups at once

# 4.2.5
_Release date: 2016-04-01_


__Bug__

* \[[OM-221](https://gazelle.ihe.net/jira/browse/OM-221)\] DICOM Worklist / wlmscpfs is unstable
* \[[OM-225](https://gazelle.ihe.net/jira/browse/OM-225)\] LAW does not use MSH-21 to transmit the list of options supported by the system
* \[[OM-227](https://gazelle.ihe.net/jira/browse/OM-227)\] Update c3p0 configuration

# 4.2.3
_Release date: 2016-01-11_


__Bug__

* \[[OM-217](https://gazelle.ihe.net/jira/browse/OM-217)\] Order Filler received message response error Problem

# 4.2.2
_Release date: 2016-01-11_


__Bug__

* \[[OM-215](https://gazelle.ihe.net/jira/browse/OM-215)\] Automation Manager Validation Problem
* \[[OM-216](https://gazelle.ihe.net/jira/browse/OM-216)\] OUL^R22^OUL_R22 message Missing required field

# 4.2.1
_Release date: 2015-07-02_


__Bug__

* \[[OM-208](https://gazelle.ihe.net/jira/browse/OM-208)\] [ANALYZER_MGR] NPE when OBX-14 is not sent in OUL message
* \[[OM-210](https://gazelle.ihe.net/jira/browse/OM-210)\] Analyzer systematically rejects CANCEL requests

# 4.2.0
_Release date: 2015-06-17_

__Technical task__

* \[[OM-193](https://gazelle.ihe.net/jira/browse/OM-193)\] Gap analysis
* \[[OM-194](https://gazelle.ihe.net/jira/browse/OM-194)\] [ANALYZER][LAB-27] Query by isolate
* \[[OM-195](https://gazelle.ihe.net/jira/browse/OM-195)\] [LAW] list the options defined by the profile for used in the application pages
* \[[OM-196](https://gazelle.ihe.net/jira/browse/OM-196)\] [ANALYZER][LAB-27] Update OUL^R22^OUL_R22 message
* \[[OM-197](https://gazelle.ihe.net/jira/browse/OM-197)\] Update LAW Query List on ANALYZER_MGR side
* \[[OM-198](https://gazelle.ihe.net/jira/browse/OM-198)\] LAB-28 acknowledgement is now using ORL_O42 structure
* \[[OM-205](https://gazelle.ihe.net/jira/browse/OM-205)\] Decode ORL_O42 message on analyzer manager side

__Bug__

* \[[OM-142](https://gazelle.ihe.net/jira/browse/OM-142)\] Cannot change the type of action on order management page
* \[[OM-151](https://gazelle.ihe.net/jira/browse/OM-151)\] Hello, while testing the LAB-29 transaction I enco...
* \[[OM-175](https://gazelle.ihe.net/jira/browse/OM-175)\] Update the TRO part of the tool to take into account CP-Saudi-eHealth-51-V7
* \[[OM-177](https://gazelle.ihe.net/jira/browse/OM-177)\] Sending/Receiving application shall be populated with the URL of the endpoint
* \[[OM-178](https://gazelle.ihe.net/jira/browse/OM-178)\] [CP-Saudi-eHealth-9XX-V1] ERR segment
* \[[OM-179](https://gazelle.ihe.net/jira/browse/OM-179)\] SWF.b : "Unable to parse the message to send !" is raised when sending a message with option 2.5.1 selected
* \[[OM-180](https://gazelle.ihe.net/jira/browse/OM-180)\] NPE when sending a RAD-4 message for v2.5.1
* \[[OM-181](https://gazelle.ihe.net/jira/browse/OM-181)\] Error rendering allUsers.seam
* \[[OM-182](https://gazelle.ihe.net/jira/browse/OM-182)\] Disable the use of the worklist in SeHE mode
* \[[OM-183](https://gazelle.ihe.net/jira/browse/OM-183)\] Non-ASCII parameter from the URL are not correctly decoded
* \[[OM-184](https://gazelle.ihe.net/jira/browse/OM-184)\] Update dependencies to PatientGeneration:1.18.2
* \[[OM-185](https://gazelle.ihe.net/jira/browse/OM-185)\] Cannot send lab order. When selecting existing visit application crash with error in the logs
* \[[OM-191](https://gazelle.ihe.net/jira/browse/OM-191)\] Some pages for laboratory are broken
* \[[OM-192](https://gazelle.ihe.net/jira/browse/OM-192)\] Unable to sent notification
* \[[OM-204](https://gazelle.ihe.net/jira/browse/OM-204)\] form is emptied when selecting the specimen type

__Story__

* \[[OM-135](https://gazelle.ihe.net/jira/browse/OM-135)\] TQ1-9 Allows Codes Other Than R (Routine) and S (Stat) in OML^O33
* \[[OM-176](https://gazelle.ihe.net/jira/browse/OM-176)\] Aborted reason shall be specified when sending order status = DC
* \[[OM-186](https://gazelle.ihe.net/jira/browse/OM-186)\] After jboss crashed, when OrderManager restarts and wlmscpfs is still running, OrderManager tries anyhow to restart it. Then CPU runs  at 100%
* \[[OM-189](https://gazelle.ihe.net/jira/browse/OM-189)\] Update LAW actors according to LAW 1.4 supplement

# 4.1.0
_Release date: 2014-06-26_

__Bug__

* \[[OM-145](https://gazelle.ihe.net/jira/browse/OM-145)\] Dispatcher for Laboratory orders leads to Rad orders creation

__Improvement__

* \[[OM-170](https://gazelle.ihe.net/jira/browse/OM-170)\] Add a page which allows the users to browse the tele-radiology orders owned by the creator and the fulfiller
* \[[OM-171](https://gazelle.ihe.net/jira/browse/OM-171)\] Add the possibility to create a tele-radiology order from the dispatcher page
* \[[OM-172](https://gazelle.ihe.net/jira/browse/OM-172)\] Update to HL7Common:4.0.1

# 4.0.0
_Release date: 2014-06-20_

__Technical task__

* \[[OM-146](https://gazelle.ihe.net/jira/browse/OM-146)\] LAW - in OML/OUL messages, PID segment is optional
* \[[OM-147](https://gazelle.ihe.net/jira/browse/OM-147)\] LAW - workflow in LAW profile is based on containers

__Bug__

* \[[OM-114](https://gazelle.ihe.net/jira/browse/OM-114)\] HL7 ORM sent to Ordermanager Order Placer can't be validated
* \[[OM-134](https://gazelle.ihe.net/jira/browse/OM-134)\] ACK is not accurate when a wrong receiver application is received in the context of LAB-27
* \[[OM-164](https://gazelle.ihe.net/jira/browse/OM-164)\] Connection information about TRO actors are not displayed
* \[[OM-165](https://gazelle.ihe.net/jira/browse/OM-165)\] Procedure code is not sent (but is required by the specification)
* \[[OM-166](https://gazelle.ihe.net/jira/browse/OM-166)\] Placer Field 2 is not sent (but is required by the specification)
* \[[OM-167](https://gazelle.ihe.net/jira/browse/OM-167)\] NTE segment is not sent (but is required by the specification)

__Story__

* \[[OM-133](https://gazelle.ihe.net/jira/browse/OM-133)\] Simulator Does Not Recognize LAB-29 OUL^R22
* \[[OM-140](https://gazelle.ihe.net/jira/browse/OM-140)\] Sequence diagram is missing for LAB-4
* \[[OM-143](https://gazelle.ihe.net/jira/browse/OM-143)\] Use latest version of HL7 Common and update application accordingly
* \[[OM-157](https://gazelle.ihe.net/jira/browse/OM-157)\] Study how to include the simulator for the TRO scenario
* \[[OM-160](https://gazelle.ihe.net/jira/browse/OM-160)\] Implement the Tele-Radiology Order Forwarder use case actor
* \[[OM-161](https://gazelle.ihe.net/jira/browse/OM-161)\] Implement the Tele-Radiology Order creator use case actor
* \[[OM-162](https://gazelle.ihe.net/jira/browse/OM-162)\] Implement the Tele-Radiology Order fulfiller use case actor
* \[[OM-163](https://gazelle.ihe.net/jira/browse/OM-163)\] A production report is expected

__Improvement__

* \[[OM-136](https://gazelle.ihe.net/jira/browse/OM-136)\] Integrate the latest version of PatientGeneration module
* \[[OM-137](https://gazelle.ihe.net/jira/browse/OM-137)\] Harmonize the display of dates and timestamp through the application
* \[[OM-141](https://gazelle.ihe.net/jira/browse/OM-141)\] Some improvements required concerning LAW
* \[[OM-150](https://gazelle.ihe.net/jira/browse/OM-150)\] Code clean up
* \[[OM-154](https://gazelle.ihe.net/jira/browse/OM-154)\] Configure project to use version API
* \[[OM-155](https://gazelle.ihe.net/jira/browse/OM-155)\] Log the IP address of the SUT sending/receiving the HL7v2 message
* \[[OM-156](https://gazelle.ihe.net/jira/browse/OM-156)\] Upgrade to latest version of HL7Common
* \[[OM-159](https://gazelle.ihe.net/jira/browse/OM-159)\] Implement the HL7v2.5.1 option of the SWF profile for RAD-2, RAD-3, RAD-4 and RAD-13
* \[[OM-168](https://gazelle.ihe.net/jira/browse/OM-168)\] Sequence diagrams have to be drawn for the TRO use case actors
* \[[OM-169](https://gazelle.ihe.net/jira/browse/OM-169)\] Some values can be populated from value sets, add the binding to the SVSSimulator for those fields

# 3.3-GA
_Release date: 2013-07-08_


__Technical task__

* \[[OM-124](https://gazelle.ihe.net/jira/browse/OM-124)\] Update Analyzer side for LAB-27 transaction
* \[[OM-125](https://gazelle.ihe.net/jira/browse/OM-125)\] Update Analyzer Mgr side for LAB-27 transaction
* \[[OM-126](https://gazelle.ihe.net/jira/browse/OM-126)\] Update Analyzer side for LAB-28 transaction
* \[[OM-127](https://gazelle.ihe.net/jira/browse/OM-127)\] Update Analyzer Mgr side for LAB-28 transaction
* \[[OM-128](https://gazelle.ihe.net/jira/browse/OM-128)\] Update Analyzer side for LAB-29 transaction
* \[[OM-129](https://gazelle.ihe.net/jira/browse/OM-129)\] Update Analyzer Manager side for LAB-29 transaction

__Bug__

* \[[OM-118](https://gazelle.ihe.net/jira/browse/OM-118)\] Creating new patient and encounter fails
* \[[OM-120](https://gazelle.ihe.net/jira/browse/OM-120)\] An order with this filler order number already exists, no matter what order is sent to the simulator
* \[[OM-121](https://gazelle.ihe.net/jira/browse/OM-121)\] OF Simulator wrongly fills ORL^O34 OBR.fillerordernumber by cloning OBR.placerordernumber into it

__Story__

* \[[OM-93](https://gazelle.ihe.net/jira/browse/OM-93)\] Review LAW supplement and check the simulator is up-to-date
* \[[OM-131](https://gazelle.ihe.net/jira/browse/OM-131)\] reduce the number of calls to SVSSimulator

# 3.2-GA
_Release date: 2013-03-18_

__Bug__

* \[[OM-116](https://gazelle.ihe.net/jira/browse/OM-116)\] Issue with transaction LAB2
* \[[OM-117](https://gazelle.ihe.net/jira/browse/OM-117)\] When adding observations to an Order:  Observation identifier* globalForm:j_id180:j_id3048:j_id3081:obx3:j_id3114: Validation Error: Value is required.

__Story__

* \[[OM-107](https://gazelle.ihe.net/jira/browse/OM-107)\] Update user manual according to the changes done in the GUI
* \[[OM-109](https://gazelle.ihe.net/jira/browse/OM-109)\] Error in page /common/procedureDetails.xhtml
* \[[OM-110](https://gazelle.ihe.net/jira/browse/OM-110)\] Add a section to test the capabilities of the OF to support RAD-5

# 3.1-GA
_Release date: 2012-11-27_


__Bug__

* \[[OM-28](https://gazelle.ihe.net/jira/browse/OM-28)\] Not all messages received by OP/OF are displayed on the page gathering the actor configuration and received messages
* \[[OM-43](https://gazelle.ihe.net/jira/browse/OM-43)\] All data are cleared when the user tries to send a message with no SUT selected
* \[[OM-76](https://gazelle.ihe.net/jira/browse/OM-76)\] LAB-29: unable to create a message in the simulator
* \[[OM-81](https://gazelle.ihe.net/jira/browse/OM-81)\] Error during message generation when trying to perform pre connectathon test 30006

__Story__

* \[[OM-92](https://gazelle.ihe.net/jira/browse/OM-92)\] When we are aware of multiple patient IDs, use the "Other Patient IDs sequence" in the worklist in order to propagate the entire list of patient IDs 
* \[[OM-95](https://gazelle.ihe.net/jira/browse/OM-95)\] TQ-4 component is required but not sent by the simulator (RAD-3)
* \[[OM-96](https://gazelle.ihe.net/jira/browse/OM-96)\] Error page is displayed when trying to access the page "configuration of a simulated actor"
* \[[OM-103](https://gazelle.ihe.net/jira/browse/OM-103)\] Add a page to manage issuers of identifiers/numbers used in the tool
* \[[OM-104](https://gazelle.ihe.net/jira/browse/OM-104)\] Users should be allowed to update patient's demographics and identifiers
* \[[OM-105](https://gazelle.ihe.net/jira/browse/OM-105)\] Allow admin users to configure number/UID generator
* \[[OM-106](https://gazelle.ihe.net/jira/browse/OM-106)\] Add a page to manage value sets

__Improvement__

* \[[OM-34](https://gazelle.ihe.net/jira/browse/OM-34)\] Add a mean to import the logs of the mwlscp tool in the Order Manager
* \[[OM-39](https://gazelle.ihe.net/jira/browse/OM-39)\] [Specimen] add filtering on filler/placer assigned identifier
* \[[OM-97](https://gazelle.ihe.net/jira/browse/OM-97)\] 4 additional codes needed for Order Manager for Eye Care
* \[[OM-98](https://gazelle.ihe.net/jira/browse/OM-98)\] Some instances of the simulator will not be linked to the CAS server, we need a way to give users access to the administration pages
* \[[OM-99](https://gazelle.ihe.net/jira/browse/OM-99)\] Refactor menu bar
* \[[OM-100](https://gazelle.ihe.net/jira/browse/OM-100)\] Refactor the data browser feature
* \[[OM-101](https://gazelle.ihe.net/jira/browse/OM-101)\] standardize sequence diagram
* \[[OM-102](https://gazelle.ihe.net/jira/browse/OM-102)\] Update HL7 responders

# 3.0-GA
_Release date: 2012-10-15 _

__Technical task__

* \[[OM-86](https://gazelle.ihe.net/jira/browse/OM-86)\] Update the HL7 Message Profiles of the LAW Profile.
* \[[OM-87](https://gazelle.ihe.net/jira/browse/OM-87)\] Update the LAW Simulator to take account the change proposals of the CP-176.

__Bug__

* \[[OM-27](https://gazelle.ihe.net/jira/browse/OM-27)\] Validation issues for messages of type ORL_O34
* \[[OM-89](https://gazelle.ihe.net/jira/browse/OM-89)\] In the ACK^R22^ACK response message, the sending application and receiving application are reversed. Same thing for the sending and receiving facility.
* \[[OM-91](https://gazelle.ihe.net/jira/browse/OM-91)\] Problem with the ORL_O34 ack message (for HL7v2.5.1). This bug appears when the OrderManager try to get the ack information and to update the contained orders.

__Story__

* \[[OM-1](https://gazelle.ihe.net/jira/browse/OM-1)\] Add Support for EYECARE domain
* \[[OM-64](https://gazelle.ihe.net/jira/browse/OM-64)\] Add a admin page in which we can configure some of the application preferences
* \[[OM-82](https://gazelle.ihe.net/jira/browse/OM-82)\] ORL^O34 fails validation
* \[[OM-85](https://gazelle.ihe.net/jira/browse/OM-85)\] Update the LAW Simulator with the change proposal CP-LAB-176 published by the Laboratory committee. See the joint file.
* \[[OM-90](https://gazelle.ihe.net/jira/browse/OM-90)\] Error in validation of OML^O21 message
* \[[OM-94](https://gazelle.ihe.net/jira/browse/OM-94)\] Page is not rendered when some entities need to be converted

__Improvement__

* \[[OM-88](https://gazelle.ihe.net/jira/browse/OM-88)\] Update DICOM proxy to latest version

# 2.2
_Release date: 2012-05-02_


__Technical task__

* \[[OM-73](https://gazelle.ihe.net/jira/browse/OM-73)\] OBR-7 of LAB-3 messages (ORU^R01^ORU_R01) is conditional field
* \[[OM-77](https://gazelle.ihe.net/jira/browse/OM-77)\] OM-21 not populated in LAB-28
* \[[OM-80](https://gazelle.ihe.net/jira/browse/OM-80)\] LAB-2. ACK Message - Empty ORC4 and wrong value in OBR3

__Story__

* \[[OM-19](https://gazelle.ihe.net/jira/browse/OM-19)\] Enable users to copy an existing worklist and to modify some of the fields
* \[[OM-72](https://gazelle.ihe.net/jira/browse/OM-72)\] Review all message profiles used in the tool
* \[[OM-84](https://gazelle.ihe.net/jira/browse/OM-84)\] Update the HL7Common version used by the Order Manager Simulator to 2.7.

# 2.1-GA
_Release date: 2012-04-23_

__Technical task__

* \[[OM-70](https://gazelle.ihe.net/jira/browse/OM-70)\] Problem with pre connectathon test 30002

__Story__

* \[[OM-60](https://gazelle.ihe.net/jira/browse/OM-60)\] Add support for RAD-4 and RAD-13 transactions (HL7v2.3.1)
* \[[OM-69](https://gazelle.ihe.net/jira/browse/OM-69)\] Refine the way the worklists are generated according the new architecture of the RAD order/procedure

__Improvement__

* \[[OM-38](https://gazelle.ihe.net/jira/browse/OM-38)\] Add a second "fill randomly" button at the top of the form

# 2.0-GA
_Release date: 2012-03-30_

__Technical task__

* \[[OM-4](https://gazelle.ihe.net/jira/browse/OM-4)\] Implement LAB-1 and LAB-2 transactions
* \[[OM-8](https://gazelle.ihe.net/jira/browse/OM-8)\] Work order creation for laboratory domain (LTW integration profile) - LAB-4 transaction
* \[[OM-24](https://gazelle.ihe.net/jira/browse/OM-24)\] implement initiator part of result management for LTW (LAB-3 and LAB-5)
* \[[OM-44](https://gazelle.ihe.net/jira/browse/OM-44)\] implement responder part of result management for LTW (LAB-3 and LAB-5)
* \[[OM-61](https://gazelle.ihe.net/jira/browse/OM-61)\] Cancel Order message of type ORL^O34^ORL_O34 is answered by a Application Internal Error
* \[[OM-68](https://gazelle.ihe.net/jira/browse/OM-68)\] LabOrders with null order status are not shown for cancellation nor update
* \[[OM-71](https://gazelle.ihe.net/jira/browse/OM-71)\] LTW-TRM: LAB-3: OBX-8, OBX-18 and OBX-19 fields raise errors

__Bug__

* \[[OM-13](https://gazelle.ihe.net/jira/browse/OM-13)\] Update message profile for OML_O21 and OML_O33 messages
* \[[OM-16](https://gazelle.ihe.net/jira/browse/OM-16)\] When using the dispatch for creating a new worklist for an imported patient, there is no way to configure the order
* \[[OM-17](https://gazelle.ihe.net/jira/browse/OM-17)\] When selected a patient for creating a new worklist, the patient's demographics are not displayed
* \[[OM-23](https://gazelle.ihe.net/jira/browse/OM-23)\] DDS panel appears but should not
* \[[OM-29](https://gazelle.ihe.net/jira/browse/OM-29)\] Consider adding the domain keyword in the parametric URL for orders
* \[[OM-30](https://gazelle.ihe.net/jira/browse/OM-30)\] The selected domain is not taken into account when creating a new order for a patient imported from TM or PAM
* \[[OM-35](https://gazelle.ihe.net/jira/browse/OM-35)\] Add an attribute for storing the placer group number
* \[[OM-36](https://gazelle.ihe.net/jira/browse/OM-36)\] Supports for addition of LAW actors within the simulator
* \[[OM-37](https://gazelle.ihe.net/jira/browse/OM-37)\] Error occurs when trying to cancel a work order
* \[[OM-41](https://gazelle.ihe.net/jira/browse/OM-41)\] cannot access the permanent page for a dicom message from the list of messages
* \[[OM-46](https://gazelle.ihe.net/jira/browse/OM-46)\] Order Manager returns a ack with MSA-1=AE when the PATIENT group is missing in OML^O* whereas this group is set as RE in TF
* \[[OM-57](https://gazelle.ihe.net/jira/browse/OM-57)\] Problem with the Initiator class of Hapi for the ORL_O34 HL7 v2.5.1 message.
* \[[OM-58](https://gazelle.ihe.net/jira/browse/OM-58)\] Bugs appear after to have merge the branch into the trunk. See the description for further details.
* \[[OM-59](https://gazelle.ihe.net/jira/browse/OM-59)\] The selected SUT configuration needs to be updated when the user go to an other transaction page.

__Story__

* \[[OM-3](https://gazelle.ihe.net/jira/browse/OM-3)\] Need documentation page for the project
* \[[OM-15](https://gazelle.ihe.net/jira/browse/OM-15)\] DICOM Worklist: add a mean to delete them
* \[[OM-47](https://gazelle.ihe.net/jira/browse/OM-47)\] Enables the user to create a test/order result from scratch
* \[[OM-55](https://gazelle.ihe.net/jira/browse/OM-55)\] Merge work done on LAW with the trunk
* \[[OM-56](https://gazelle.ihe.net/jira/browse/OM-56)\] Add a field orderResultStatus (OBR-25) in the LabOrder entity in order to enable the user to fill it out as he/she needs
* \[[OM-62](https://gazelle.ihe.net/jira/browse/OM-62)\] Support during Sprint 5
* \[[OM-63](https://gazelle.ihe.net/jira/browse/OM-63)\] Update the documentation of Order Manager Simulator on gazelle.ihe.net with the LAW Simulator documentation.

__Improvement__

* \[[OM-9](https://gazelle.ihe.net/jira/browse/OM-9)\] The purpose of the OrderManager is to emulate OF, OP, AM and to create DICOM worklist
* \[[OM-14](https://gazelle.ihe.net/jira/browse/OM-14)\] Permanent link to the worklist is missing in the creation page
* \[[OM-31](https://gazelle.ihe.net/jira/browse/OM-31)\] Dispatcher should offer a new option: only import patient
* \[[OM-32](https://gazelle.ihe.net/jira/browse/OM-32)\] Add filters and sorting on tables
* \[[OM-33](https://gazelle.ihe.net/jira/browse/OM-33)\] Internationalization of the GUI and FacesMessages
* \[[OM-40](https://gazelle.ihe.net/jira/browse/OM-40)\] [Specimen] Add a way to retrieve only specimen-centric or only "container-centric" specimens
* \[[OM-42](https://gazelle.ihe.net/jira/browse/OM-42)\] add the list of observations related to a specimen/order in the permanent page of those objects
* \[[OM-45](https://gazelle.ihe.net/jira/browse/OM-45)\] Enables the user to create a "result" from an existing order/work order
* \[[OM-67](https://gazelle.ihe.net/jira/browse/OM-67)\] Update the trigger event of the HL7 messages (QBP_Q11 and RSP_K11) of the LAB-27 transaction of the LAW TF Supplement.

# 1.0-GA
_Release date: 2012-01-26_

__Technical task__

* \[[OM-21](https://gazelle.ihe.net/jira/browse/OM-21)\] Implement RAD-2 and RAD-3 transactions
* \[[OM-22](https://gazelle.ihe.net/jira/browse/OM-22)\] Creation of DICOM worklist for RAD and CARD domain

__Bug__

* \[[OM-10](https://gazelle.ihe.net/jira/browse/OM-10)\] images are missing on pages /hl7Responders/configurationAndMessages.seam?transaction=RAD-2&actor=OF and /hl7Responders/configurationAndMessages.seam?transaction=RAD-3&actor=OP
* \[[OM-11](https://gazelle.ihe.net/jira/browse/OM-11)\] XML version of the received/sent data set is not available the first time the user accesses the page of the message
* \[[OM-12](https://gazelle.ihe.net/jira/browse/OM-12)\] Dates are displayed with time zone = UTC+00 !!

__Story__

* \[[OM-2](https://gazelle.ihe.net/jira/browse/OM-2)\] Link the OrderManager to the SVS simulator
