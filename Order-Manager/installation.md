---
title:  Installation Manual
subtitle: Order Manager
author: Anne-Gaëlle BERGE
function: Engineer
date: 24/05/2024
toolversion: 6.X.X
version: 1.02
status: Approved document
reference: KER1-MAN-IHE-ORDER_MANAGER_INSTALLATION
customer: IHE-EUROPE
---
# Sources & binaries

To get the name of the latest release, visit the Order Manager project in [*JIRA*](https://gazelle.ihe.net/jira/browse/OM) and consult the Relese secion.

A maven artifact is published in our Nexus repository each time we release the application. You can use it, but be aware that the link to the database is hardly coded within the artifact so you will have to use the same database name, owner (and password).

To get the artifact on Nexus browse: [*https://gazelle.ihe.net/nexus/index.html\#nexus-search;gav~~OrderManager-ear~~~*](https://gazelle.ihe.net/nexus/index.html#nexus-search;gav%7E%7EOrderManager-ear%7E%7E%7E) and download the latest version.

If you rather want to build the project by yourself, you must checkout the latest tag and package it. You may want to create a new Maven profile to customize your build.

Checkout the latest tag available on Inria’s gitlab:

```bash
git clone https://gitlab.inria.fr/gazelle/public/simulation/order-manager.git
cd order-manager
git checkout <version>
```

1.  \[Optional\] Edit the pom.xml file and create a new profile

2.  Package the application: `mvn \[-P profile\] clean package`

3.  The EAR is available at OrderManager/OrderManager-ear/target/OrderManager.ear

# Dcmtk

## Installation

The OrderManager tool makes use of the OFFIS Dicom toolkit to manage its DICOM worklist. You need to locally installed the latest version of dcmtk in your environment. If you run a Debian-like operating system, execute:
```bash
sudo apt-get install dcmtk
```
We recommand to use version 3.6.0 of the toolkit, you can verify the version with the following command line:
```bash
wlmscpfs --version
```
## Configuration

DCMTK operation file-based, it thus requires the creation of a folder to store and retrieve the worklists. The path to the root folder can be configured in the database (see Application Configuration section), the sub folders shall be created as follows

* $ROOT = /opt/worklists

* Sub-folders: RAD\_OF, EYE\_OF, \_DISABLED, exchanged

* To enable the worklist to look into directories, you also have to create empty files named “lockfile” in RAD\_OF and EYE\_OF folders.

* Finally, change owner and give writing access to jboss:

  * sudo chown -R dcmtk:jboss-admin worklists

  * sudo chmod -R g+w worklists

# Deploy the application
## Database configuration

If you use the artifact available on Nexus or if you have not change this parameter in the pom.xml file, create a database named **order-manager**, owned by the user gazelle.

createdb -U gazelle -E UTF8 order-manager

## JBoss deployment
1.  Copy EAR to the deployment folder of JBoss7 (do not forget to change its name to OrderManager.ear)

2.  Start Jboss

3.  sudo service jboss7 start

4.  Wait until the application has been completly deployed and configure the database running.

5. **WARNING** : From version 5.3.0, datasources have been extracted from the **ear**. The template file can be found in /src/main/application/datasource in the source or in the file XDWSimulator-X.X.X-datasource.zip from the nexus.
For more informations about how to manage that externalization, please refer to [general considerations for JBoss7](https://gazelle.ihe.net/gazelle-documentation/General/jboss7.html).

Datasource name : OrderManagerDS

Database name : order-manager

## Initialize the application

You first need to initialize the database with some data available in a SQL script. If you have checked out the project, the script is available in OrderManager-ear/src/main/sql/import.sql

Before executing the script, open the file and checked the various preferences to be inserted in the app\_configuration table, especially the application\_url and other preferences relative to the user authentication (see Application configuration section)

Finally, execute the script:
```bash
psql -U gazelle order-manager &lt; import.sql
```

To take those parameters into account, you need to restart either the whole Jboss (```$&gt;sudo service jboss restart```), either only the application (```$&gt;touch OrderManager.ear``` in the deployment folder of Jboss)

## Application configuration
### Preferences

Use the Administration menu, you will find a sub-menu entitied "Configure application preferences". The following preferences must be updated according to the configuration of your system. The table below summarizes the variables used by the OrderManager tool.

| Variable                          | Description                                                                                                                                                     | Default value|
|--------------------------------------------------|-------------------------------------------|-------------------------------------------|
| analyzer\_serial\_number          | OID used to populate OBX-18 in the messages sent by the analyzer in the context of the LAW profile                                                              | OID formatted string                                                                                                                           |
| application\_issue\_tracker\_url                  | URL of the helpdesk or project for Order Manager in your bug tracker tool         | [*Link to jira*](https://gazelle.ihe.net/jira/browse/OM)|
| application\_release\_note\_url                  | URL to the release note in JIRA (or elsewhere)         | [*Link to JIRA*](https://gazelle.ihe.net/jira/browse/OM)|
| application\_url                  | The URL used by any user to access the tool. The application needs it to build permanent links inside the tool| http://publicUrlOfJboss/OrderManager |
| contact\_email                  | The email address of the person to be contacted by the user in case help is needed         | -                                                                                                       |
| contact\_name                  | The name of the person to be contacted by the user in case help is needed         | -                                                                                                       |
| contact\_title                  | The title of the person to be contacted by the user in case help is needed         | -                                                                                                       |
| dcm\_data\_set\_basedir           | where to store the DICOM dataset exchanged between the SUT and the simulator                                                                                    | /opt/worklists/exchanged                                                                                                                       |
| dds\_mode           | How demographics are generated when calling DDS | minimal |
| dds\_ws\_endpoint                 | Location of the Demographic Data server WSDL                                                                                                                    | [*DDS WS on Gazelle*](https://gazelle.ihe.net/DemographicDataServer-ejb/DemographicDataServerService/DemographicDataServer?wsdl) |
| dicom\_proxy\_ip                | Is only used for display to the user |Public IP of the tool                                                        |  
| dicom\_proxy\_port                | The port on which the Order Manager listens to to forward the worklist queries to dcmtk |- |
| documentation\_url                  | Link to the user manual of the tool                                                                                     | [Gazelle website](https://gazelle.ihe.net/content/order-manager)|
|eyecare_enabled | Enabled/Disabled the Eyecare entry in the menu | true
| eye\_order\_hierarchy\_location   | Location of the XML file used to perform the matching between orders and procedures/protocols in the context of the Eyecare workflow profile                    | [*Default file*](https://gazelle.ihe.net/examples/orderHierarchy-EYE2012.xml)|
| gazelle\_hl7v2\_validator\_url    | URL of the Gazelle HL7 Validator tool                                                                                                                           | [Gazelle HL7 Validator](https://gazelle.ihe.net/GazelleHL7Validator)|
| hl7v2\_xsl\_location                     | URL to access the XML stylesheet used to display HL7v2.x validation results| [*XSL location*](https://gazelle.ihe.net/xsl/hl7Validation/resultStylesheet.xsl)|
|laboratory_enabled | Enabled/Disabled the Laboratory entry in the menu | true |
|message_permanent_link | Permanent link to message details | http://publicUrlOfJboss/OrderManager/messages/MessageDisplay.seam?id=
|NUMBER_OF_ITEMS_PER_PAGE | Default number of rows to be displayed in tables | 20 |
|number_of_segments_to_display| Use to truncate HL7 message in display | 40
| order\_hierarchy\_location        | Location of the XML file used to perform the matching between orders and procedures/protocols in the context of the scheduled workflow profile| [*Default file*](https://gazelle.ihe.net/examples/Bern2012-orderHierarchy.xml)|
| pam\_encounter\_generation\_url   | Patients and Encounters are generated by a called to the PatientManager application, this preference precises the REST endpoint                                 | [*Gazelle Patient Manager*](https://gazelle.ihe.net/PatientManager/rest/GenerateRandomData)|
|proxy_port_low_limit| When opening a channel to a DICOM SCU, the proxy will start searching for a free port from this port number | 10001 |
|radiology_enabled | Enabled/Disabled the Radiology entry in the menu | true
|SeHE_mode_enabled |Enabled/Disabled the SeHE entry in the menu | false
| svs\_repository\_url              | URL of the Sharing Value Set Repository actor of the SVSSimulator | [*Gazelle SVS Repository*](https://gazelle.ihe.net) |
| timeout\_for\_receiving\_messages | How long must the HL7 initiator wait for a response (in ms)| 10000 |
| time\_zone                        | Defines which time zone to use to display dates and timestamps | Europe/Paris |
| url\_EVSC\_ws                     | URL of the Gazelle HL7 Validator wsdl (the one for HL7v2.x validation)                                                                                          | [*GazelleHL7Validator WS on Gazelle*](https://gazelle.ihe.net/GazelleHL7v2Validator-ejb/gazelleHL7v2ValidationWSService/gazelleHL7v2ValidationWS?wsdl) |
| wlmscpfs\_host                    | where to contact the worklist | localhost|
| wlmscpfs\_pid                    | Process ID used by the tool | Managed by the tool, do not edit|
| wlmscpfs\_port                    | the port on which the worklist listens to | 12345  |
| worklists\_basedir                | where to store worklists for retrieve by dcmtk                                                                                                                  |/opt/worklists|

### SSO Configuration

There are additional preferences to configure the SSO authentication.

| Preference name      | Description                                                             | Example of value |
| ----------------     | ----------------------------------------------------------------------  | ---------------- |
| **cas_enabled**      | Enable or disable the CAS authentication.                               | true             |
| **ip_login**         | Enable authentication by IP address matching `ip_login_admin` regex.    | false            |
| **ip_login_admin**   | Regex to authorize ip authentication if CAS authentication is disabled. |  .*              |

For more documentation about SSO configurations, follow the link [here](https://gitlab.inria.fr/gazelle/public/framework/sso-client-v7/-/blob/master/cas-client-v7/README.md).

### HL7v2.x responders

From the Administration &gt; HL7 Responders configuration page, you will be able to configure each actor of the tool playing the role of a responder in a HL7-based transaction. An entry in this table consists in the receiving application and facility and the port on which it listens to incoming messages. You can also configure the encoding for receiving message (always ER7 for IHE) as weel as the transport protocol to be used (always MLLP for IHE). If you are using HL7 over HTTP, you will be asked to provide the URL of the endpoint instead of the IP address/port couple.

The IP address is not used by the server but must be set properly so that the users can configure their systems under test to communicate with the tool. DO NOT update the other parameters, it would prevent the tool from working correctly.

Note: When you update a configuration, do not forget to restart it.

### Home page

The first time you access the application, you may notice that the home page of the tool is not configured. To set a title and a welcome message, log into the application with admin rights.

Note that you will have to set up this page for all the languages supported by the application.
