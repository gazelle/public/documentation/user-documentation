---
title:  Installation Manual
subtitle: Gazelle Proxy
author: Nicolas BAILLIET
date: 02/09/2024
toolversion: 6.X.X
function: Engineer
version: 2.2
status: Approved document
reference: KER1-MAN-IHE-PROXY_INSTALLATION-2_01
customer: IHE-EUROPE
---

# Introduction

Proxy is the part of the Gazelle testbed which is used to capture the messages exchanged between two systems under test.
This tool is also bind to the EVSClient in order to validate the messages stored in the Proxy in a very simple way.

As for the other tools, the proxy is an open source project and its sources are available at 
[*https://gitlab.inria.fr/gazelle/public/core/proxy-v7.git*](https://gitlab.inria.fr/gazelle/public/core/proxy-v7.git).  
You can download the latest gazelle-proxy.ear in nexus 
[*http://gazelle.ihe.net/nexus/index.html#nexus-search;quick~gazelle-proxy.ear*](http://gazelle.ihe.net/nexus/index.html#nexus-search;quick~gazelle-proxy.ear)

# Channel Socket Service (Version 6.0.0 and higher)

## Introduction

The Proxy 6 has externalized the low level operations of the channels in a dedicated service based on Netty 4.
This service is called the Channel Socket Service.

This service needs to be installed on the same machine as the Proxy and must be started before the Proxy.

Channel Socket Service is withing the Proxy V17 projects.

## Installation

### Prerequisites

1. Java 17
2. Maven 3.8.x
3. Docker
4. Optional: Mandrel 17 (for native compilation)

### Build

1. Clone the project:
    ```shell
    git clone [PROXY_V17_URL]
    ```
2. Build the project:
    ```shell
    mvn clean install -DskipTests
    ```
### Run

We can run the Channel Socket Service using the following command:
```shell
java -jar proxy-quarkus/target/quarkus-app/quarkus-run.jar 
```

### Configuration

The Channel Socket Service is configured using JVM properties. The following table describes the properties that can be used to configure the Channel Socket Service.

| **Variable name**                              | **Description**                                                                          | **Default value**                       |
|------------------------------------------------|------------------------------------------------------------------------------------------|-----------------------------------------|
| **datahouse.url**                              | The URL used to communicate with datahouse, to record messages and connections           | http://localhost:8080/datahouse/rest/v1 |
| **quarkus.http.port**                          | The port of the service                                                                  | 8081                                    |
| **quarkus.log.level**                          | The log level                                                                            | INFO                                    |
| **proxy.fallback.pipeline.enabled**            | Enable/disable fallback record                                                           | true                                    |
| **proxy.fallback.record.service.url**          | Recording service URL for the fallback pipeline (alternative to datahouse, ex: proxy v7) | http://localhost:8380/proxy/rest        |
| **service.ip.address**                         | Ip of the host machine of proxy. Used in HTTP rewrite                                    | 127.0.0.1                               |
| **handler.logger.enabled**                     | Enable/disable logger handler to log incoming/outgoing messages                          | false                                   |
| **quarkus.swagger-ui.always-include**          | Enable/disable swagger graphical interface                                               | true                                    |
| **quarkus.otel.exporter.otlp.traces.endpoint** | Service URL to export opentelemtry spans, if not set, telemetry will be disabled         | http://localhost:4317                   |



# Compilation and installation

Gazelle testbed tools are built using Maven 3, when you have download the sources, go to the gazelle-proxy folder and execute

```bash
mvn -P public clean package
```

You will get an EAR in the gazelle-proxy-ear/target folder.

Then, follow the instructions below:

## 1.  Create database

In your database (postgresql 9.1 or higher) create a database named "gazelle-proxy", using UTF-8 encoding and owned by the user gazelle
```bash
createdb -U gazelle -E UTF8 gazelle-proxy
```

## 2. File system 

### 2.1 Version 6.0.0 and higher
For persistent channels, create a file `/opt/proxy/proxyPersistentChannels.json` and make sure that the user running the application has the right to write in it.
On your file system, create a directory `/opt/proxy/DICOM`.
```bash
sudo mkdir -p /opt/proxy/DICOM
sudo touch /opt/proxy/proxyPersistentChannels.json
sudo chown -R jboss:jboss-admin /opt/proxy
sudo chmod -R 775 /opt/proxy
```


### 2.2 Version 5.0.0 and lower
For persistent channels, create a file `/opt/proxy/proxyPersistentChannels.csv` and make sure that the user running the application has the right to write in it.
On your file system, create a directory `/opt/proxy/DICOM`.

```bash
sudo mkdir -p /opt/proxy/DICOM
sudo touch /opt/proxy/proxyPersistentChannels.csv
sudo chown -R jboss:jboss-admin /opt/proxy
sudo chmod -R 775 /opt/proxy
```

## 3. Put the ear in the deployment folder of your Jboss AS

```bash
cp gazelle-proxy-ear/target/gazelle-proxy.ear /usr/local/jboss7/standalone/deployments/gazelle-proxy.ear
```

## 4.  Manage datasource

[WARNING] : From version 4.7.0, datasources have been extracted from the **ear**. The template file can be found in 
/src/main/application/datasource in the source or in the file gazelle-proxy-X.X.X-datasource.zip from the nexus.
For more information about how to manage that externalization, please refer to 
[general considerations for JBoss7](https://gazelle.ihe.net/gazelle-documentation/General/jboss7.html).

* Datasource name : gazelle-proxyDS

* Database name : gazelle-proxy

## 5. Configure SSO registration

Since version **5.1.0**, Proxy can register itself as a client of a SSO server. This is done by giving some environment variables to the application.

| **Variable name** | **Description**                                                  | **Example of value**           |
|-------------------|------------------------------------------------------------------|--------------------------------|
| GZL_PROXY_K8S_ID  | Describes the instance ID and the replica ID of the application. | gazelle-proxy-6dfeeb5595-tl29k |

[WARNING] There are additional required variables for registration of the proxy to the SSO server. Please refer to the
[README.md in sso-client-v7](https://gitlab.inria.fr/gazelle/public/framework/sso-client-v7#client-registration).

## 6.  Start Jboss AS 7

```bash
sudo /etc/init.d/jboss7 start
```

## 7. Execute the sql script available in your workspace at gazelle-proxy-ear/src/main/sql/schema-X.X.X.sql

```bash
psql -U gazelle gazelle-proxy < schema-X.X.X.sql
```

## 8.  Execute the sql script available in your workspace at gazelle-proxy-ear/src/main/sql/init-5.0.0.sql

```bash
psql -U gazelle gazelle-proxy < init-X.X.X.sql
```

## 9. Browse the application

Open a browser and go to [*http://yourServer:8080/proxy*](http://yourServer:8080/proxy)

## 10. The proxy is now up and running, see the next section for information on the configuration.

This new instance of the proxy is running without the CAS feature, that means that anyone accessing the tool has the administrator privileges.

If you rather want to use a single-sign one authentication, configure the application in this way. Edit the preference 
**cas\_enabled** to set it to **false.**

## Called tools

Check that **dcmtk** is installed on the machine. Actually, the proxy uses dcmdump to render the dicom files.

```bash
sudo apt get install dcmtk
```

# Configuration

There is a set of properties that you can configure on the Configuration page, the table below describes the various properties defined and their default values.

| **Property name**                       | **Description**                                                                                                                                                                                                                                                                                | **Default value**                                                                                                                         |
|-----------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------|
| **application\_documentation**          | The link to the user manual.                                                                                                                                                                                                                   Link to this page                               |                                                                                                                                           |
| **application\_issue\_tracker**         | The link to the section of the issue tracker where to report issues about the Gazelle Proxy tool                                                                                                                                                                                               | [https://gazelle.ihe.net/jra/browse/PROXY](https://gazelle.ihe.net/jra/browse/PROXY)                                                      |
| **application\_release\_notes**         | The link to the application release notes of the tool                                                                                                                                                                                                                                          | [https://gazelle.ihe.net/jira](https://gazelle.ihe.net/jira)                                                                              |
| **application\_url**                    | The URL used by any user to access the tool. The application needs it to build permanent links inside the tool                                                                                                                                                                                 | [http://yourASURL/proxy](http://yourASURL/proxy)                                                                                          |
| **dcmdump_path**                        | Path to dcmdump                                                                                                                                                                                                                                                                                | /usr/bin/dcmdump                                                                                                                          |
| **evs\_client\_url**                    | The URL of the EVSClient application. This is required to validate the messages captured by the proxy. If you install your own instance of the proxy, you also need your own instance of the EVSClient tool. (Do not forget the tailing slash)                                                 | [https://gazelle.ihe.net/EVSClient/](https://gazelle.ihe.net/EVSClient/)                                                                  |
| **jms_communication_is_enabled**        | Enable jms communication                                                                                                                                                                                                                                                                       | false                                                                                                                                     |
| **max\_proxy\_port**                    | Specifies the high limit for the opened ports                                                                                                                                                                                                                                                  | 11000                                                                                                                                     |
| **min\_proxy\_port**                    | Specifies the low limit for the opened ports                                                                                                                                                                                                                                                   | 10000                                                                                                                                     |
| **NUMBER_OF_ITEMS_PER_PAGE**            | Number of items to display in datatable                                                                                                                                                                                                                                                        | 20                                                                                                                                        |
| **proxy\_ip\_addresses**                | This property is used to inform the users of the IP address(es) to use to contact the proxy                                                                                                                                                                                                    | 131.254.209.16 (kujira.irisa.fr), 131.254.209.17 (kujira1.irisa.fr), 131.254.209.18 (kujira2.irisa.fr), 131.254.209.19 (kujira3.irisa.fr) |
| **proxy\_oid**                          | For each tool, we need an OID which uniquely identify the instance of the tool and the URL used to send back results.                                                                                                                                                                          | 1.1.1.1.1                                                                                                                                 |
| **storage\_dicom**                      | Absolute path to the system folder used to store the DICOM datasets                                                                                                                                                                                                                            | /opt/proxy/DICOM                                                                                                                          |
| **time\_zone**                          | The time zone used to display the timestamps                                                                                                                                                                                                                                                   | Europe/Paris                                                                                                                              |
| **admin_only_mode**                     | This preference is used to enable/disable the Admin Only mode. This mode restricts the access to messages list and messages details to admin only. Connection can however be shared by an administrator to allow users knowing the connection privacy key to see messages from the connection. | false                                                                                                                                     |
| **proxy_persistent_channels_file_path** | This file path where persistent channels are written. The file must be created by server admin                                                                                                                                                                                                 | /opt/proxy/proxyPersistentChannels.csv                                                                                                    |
| **datahouse_ui_url**                    | URL to datahouse UI for the new messages list                                                                                                                                                                                                                                                  | http://localhost:3000/datahouse-ui                                                                                                        |

# SSO Configuration

There are additional preferences to configure the SSO authentication.

| Preference name      | Description                                                             | Example of value |
| ----------------     | ----------------------------------------------------------------------  | ---------------- |
| **cas_enabled**      | Enable or disable the CAS authentication.                               | true             |
| **ip_login**         | Enable authentication by IP address matching `ip_login_admin` regex.    | false            |
| **ip_login_admin**   | Regex to authorize ip authentication if CAS authentication is disabled. |  .*              |

For more documentation about SSO configurations, follow the link [here](https://gitlab.inria.fr/gazelle/public/framework/sso-client-v7/-/blob/master/cas-client-v7/README.md).