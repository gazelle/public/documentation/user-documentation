---
title: Release note
subtitle: XDStarClient
toolversion: 3.1.0
releasedate: 2025-01-24
author: Anne-Gaëlle BERGE
function: Software Engineer
customer: IHE Europe
reference: KER1-RNO-IHE-XDSTARCLIENT
---

# 3.1.0
_Release date: 2025-01-24_

Context : GUM Step 4 renovation

__Task__
* \[[XDSTAR-789](https://gazelle.ihe.net/jira/browse/XDSTAR-789)\] Update sso-client-v7 to version 5.0.0


# 3.0.4
_Release date: 2024-12-20_

__Bug__
* \[[XDSTAR-784](https://gazelle.ihe.net/jira/browse/XDSTAR-784)\] CHFormatCodeIsDICOMManifestConstraint contraint in XD* validations needs to modified to remove check on DICOM Manifest value
* \[[XDSTAR-785](https://gazelle.ihe.net/jira/browse/XDSTAR-785)\] ITI-41: SubmissionSet.Author.AuthorRole should not be required
* \[[XDSTAR-786](https://gazelle.ihe.net/jira/browse/XDSTAR-786)\] XDMFR - change authorInstitutionComponent regex
* \[[XDSTAR-787](https://gazelle.ihe.net/jira/browse/XDSTAR-787)\] PHARM1 validator should allow urn:ihe:pharm:pml:2013,urn:ihe:pharm:mtp:2015 and urn:ihe:pharm:cma:2017 formatCodes


# 3.0.3
_Release date: 2024-08-30_

__Bug__
* \[[XDSTAR-773](https://gazelle.ihe.net/jira/browse/XDSTAR-773)\] Correct XTN detection function to allow Internet value for XTN 2 in authorTelecommunication
* \[[XDSTAR-774](https://gazelle.ihe.net/jira/browse/XDSTAR-774)\] Association type with value urn:ihe:iti:2010:AssociationType:UpdateAvailabilityStatus shall be allowed for XDSMU ITI-57 transaction
* \[[XDSTAR-782](https://gazelle.ihe.net/jira/browse/XDSTAR-782)\] [XDMFR] authorInstitution validation constraint is not valid
* \[[XDSTAR-783](https://gazelle.ihe.net/jira/browse/XDSTAR-783)\] XCA validator returns FAILED with XDS Unavailable Community

# 3.0.2
_Release date: 2024-06-26_

__Story__
* \[[XDSTAR-779](https://gazelle.ihe.net/jira/browse/XDSTAR-779)\] RAD68 - XD* don't reuse IssuerOfPatientIDQualifiersSequence
* \[[XDSTAR-780](https://gazelle.ihe.net/jira/browse/XDSTAR-780)\] RAD68 - Can't add XDSDocumentEntry.practiceSettingCode if deleted
* \[[XDSTAR-781](https://gazelle.ihe.net/jira/browse/XDSTAR-781)\] RAD-68 - Handle X-User Assertion

# 3.0.1
_Release date: 2024-02-26_

__Bug__
* \[[XDSTAR-771](https://gazelle.ihe.net/jira/browse/XDSTAR-771)\] Gazelle TM instance hardcoded in SUT Configuration page

__Task__
* \[[XDSTAR-778](https://gazelle.ihe.net/jira/browse/XDSTAR-778)\] Update rules for xdmfr_

# 3.0.0
_Release date: 2024-02-06_

Context : Gazelle User Management Renovation step 2

__Task__
* \[[XDSTAR-770](https://gazelle.ihe.net/jira/browse/XDSTAR-770)\] Integrate new sso-client-v7
* \[[XDSTAR-772](https://gazelle.ihe.net/jira/browse/XDSTAR-772)\] Remove username display in GUI

# 2.5.14
_Release date: 2023-07-04_

__Bug__
* \[[XDSTAR-769](https://gazelle.ihe.net/jira/browse/XDSTAR-769)\] Delete empty transactions

# 2.5.13
_Release date: 2023-06-27_

__Task__
* \[[XDSTAR-768](https://gazelle.ihe.net/jira/browse/XDSTAR-768)\] Implement new CH Ordinance 15042023 for XDS.b and XDS.I.b

# 2.5.12
_Release date: 2023-05-25_

__Improvement__
* \[[XDSTAR-767](https://gazelle.ihe.net/jira/browse/XDSTAR-767)\] Add Xehealth constraints

# 2.5.11
_Release date: 2022-11-23_

__Bug__
* \[[XDSTAR-766](https://gazelle.ihe.net/jira/browse/XDSTAR-766)\] [ANS] wrong constraint in XDStarClient
* Update the wrong constraint in xds-validator-jar
* Update pom in XDStar-modules and XDStarClient
* Migration from svn to gitlab

# 2.5.10
_Release date: 2022-03-03_

__Bug__
* \[[XDSTAR-765](https://gazelle.ihe.net/jira/browse/XDSTAR-765 )\] Problem with XD* Validator: eHDSI DispensationService:discard - request
* Change ValueSeParser in xds-code-jar library
* Update all pom dependencies related to xds-code-jar and xds-ehdsi-validator-jar

# 2.5.9
_Release date: 2022-02-24_

__Story__
* \[[XDSTAR-764](https://gazelle.ihe.net/jira/browse/XDSTAR-764 )\] Update validators for eHDSI wave5

# 2.5.8
_Release date: 2021-09-19_

__Bug__
* \[[XDSTAR-775](https://gazelle.ihe.net/jira/browse/XDSTAR-775)\] [eHS][XDS.b] ITI-18 validator - $MetadataLevel shall equal to 1
* \[[XDSTAR-776](https://gazelle.ihe.net/jira/browse/XDSTAR-776)\] [eHS][XDS.b] ITI-41 validator - DocumentEntry.Title should be mandatory

__Story__
* \[[XDSTAR-763](https://gazelle.ihe.net/jira/browse/XDSTAR-763 )\] Swiss adaptations 3.1

__Remarks__

This release includes the releases of the following sub-modules :

* XDStar-module 1.1.23
* chxds-validator-jar 1.0.0
* xds-validator-jar 1.0.13
* xds-code-jar 0.0.23

# 2.5.7
_Release date: 2021-01-28_

__Bug__
* \[[XDSTAR-760](https://gazelle.ihe.net/jira/browse/XDSTAR-760)\] Update the constraint "constraintSamePatientId" in the validator ASIP XDM ITI-32 


# 2.5.6
_Release date: 2021-01-11_

__Bug__
* \[[XDSTAR-759](https://gazelle.ihe.net/jira/browse/XDSTAR-759)\] Disbale the constraint "constraintSamePatientId" in the validator ASIP XDM ITI-32 


# 2.5.5
_Release date: 2020-06-15_

__Remarks__

This release includes the releases of the following sub-modules :

* XDStar-module 1.1.20

__Bug__

* \[[XDSTAR-751](https://gazelle.ihe.net/jira/browse/XDSTAR-751)\] Metadata codes check with XDS valitator does not work for AdhocQueryResponse





# 2.5.4
_Release date: 2020-05-15_

__Remarks__

This release includes the releases of the following sub-modules :

* xds-validator-jar 1.0.10
* XDStar-module 1.1.19

__Bug__

* \[[XDSTAR-747](https://gazelle.ihe.net/jira/browse/XDSTAR-747)\] Validator for ITI-57 transaction fails validation with error codes specific to XDS-MU



# 2.5.3
_Release date: 2020-02-18_

__Remarks__

This release includes the releases of the following sub-modules :

* XDStar-modules in 1.1.18

__Bug__

* \[[XDSTAR-745](https://gazelle.ihe.net/jira/browse/XDSTAR-745)\] Problem with XDStarclient XCA (ITI-39)


# 2.5.2
_Release date: 2020-01-17_

__Remarks__
This release includes the releases of the following sub-modules :

* XDStar-modules in 1.1.17
* xds-validator-jar in 1.0.9

__Bug__

* \[[XDSTAR-742](https://gazelle.ihe.net/jira/browse/XDSTAR-742)\] ITI-32 validator regex for SourcePatientInfo is wrong

__Improvement__

* \[[XDSTAR-743](https://gazelle.ihe.net/jira/browse/XDSTAR-743)\] Add PI as identifier type for PatientId in ITI-32 FR Validator
* \[[XDSTAR-744](https://gazelle.ihe.net/jira/browse/XDSTAR-744)\] Make ServiceStartTime mandatory  for ITI-32 FR validator

# 2.5.1
_Release date: 2019-10-31_

__Bug__

* \[[XDSTAR-736](https://gazelle.ihe.net/jira/browse/XDSTAR-736)\] The function checking if the DataType DTM is well formed is too restrictive
* \[[XDSTAR-738](https://gazelle.ihe.net/jira/browse/XDSTAR-738)\] Remove value check on eventCode 

__Story__

* \[[XDSTAR-734](https://gazelle.ihe.net/jira/browse/XDSTAR-734)\] Nextval setting is missing in init-metadata.sql for some tables
* \[[XDSTAR-737](https://gazelle.ihe.net/jira/browse/XDSTAR-737)\] ITI-32 FR Validateur must define serviceStartTime as Mandatory

# 2.5.0
_Release date: 2019-10-14_

__Remarks__
This release includes sub-modules releases of XDStarModule in 1.1.15, xds-validator-jar in 1.0.7 and chxdsi-validator-jar in 1.0.0. 

__Improvement__

* \[[XDSTAR-729](https://gazelle.ihe.net/jira/browse/XDSTAR-729)\] Create CH:XDS-I validator
* \[[XDSTAR-735](https://gazelle.ihe.net/jira/browse/XDSTAR-735)\] Create XDM ITI-32 FR Validator

# 2.4.0
_Release date: 2019-03-21_

__Remarks__

Datasource management has changed, refers to the installation manual to correctly update your instance of the tool.

__Improvement__

* \[[XDSTAR-718](https://gazelle.ihe.net/jira/browse/XDSTAR-718)\] Extract datasource configuration
* \[[XDSTAR-719](https://gazelle.ihe.net/jira/browse/XDSTAR-719)\] Update SQL scripts archive

# 2.3.12
_Release date: 2019-02-19_


__Bug__

* \[[XDSTAR-724](https://gazelle.ihe.net/jira/browse/XDSTAR-724)\] For XDS Metadata update and RMU, the attribute DocumentEntry entryUUID shall be editable
* \[[XDSTAR-728](https://gazelle.ihe.net/jira/browse/XDSTAR-728)\] XD*Client - XDS-MU initiator uses wrong SOAP Action

# 2.3.11
_Release date: 2019-02-19_


__Bug__

* \[[XDSTAR-723](https://gazelle.ihe.net/jira/browse/XDSTAR-723)\] Validator "epSOS DispensationService:initialize - response" is too restrictive regarding codeContext values

__Story__

* \[[XDSTAR-716](https://gazelle.ihe.net/jira/browse/XDSTAR-716)\] create calibration for RMU for ehealthsuisse

# 2.3.10
_Release date: 2018-11-09_


__Bug__

* \[[XDSTAR-608](https://gazelle.ihe.net/jira/browse/XDSTAR-608)\] The validation of messages through the test plan executed is not working
* \[[XDSTAR-711](https://gazelle.ihe.net/jira/browse/XDSTAR-711)\] Cannot create crowdin translation keys in DSUBSimulator module
* \[[XDSTAR-714](https://gazelle.ihe.net/jira/browse/XDSTAR-714)\] The list of transactions displayed for Update Responder Configuration is not coherent with the context

# 2.3.9
_Release date: 2018-10-15_


__Bug__

* \[[XDSTAR-699](https://gazelle.ihe.net/jira/browse/XDSTAR-699)\] Cannot download a validator pack class
* \[[XDSTAR-704](https://gazelle.ihe.net/jira/browse/XDSTAR-704)\] Add "Update Responder" as configuration type for RMU profile
* \[[XDSTAR-706](https://gazelle.ihe.net/jira/browse/XDSTAR-706)\] The homeCommunityId is not a required element for ITI-43 transaction
* \[[XDSTAR-707](https://gazelle.ihe.net/jira/browse/XDSTAR-707)\] Some admin pages are accessible without being logged
* \[[XDSTAR-710](https://gazelle.ihe.net/jira/browse/XDSTAR-710)\] We shall not have information about the tool in the debug page on production

__Task__

* \[[XDSTAR-712](https://gazelle.ihe.net/jira/browse/XDSTAR-712)\] Release 2.3.9

# 2.3.8
_Release date: 2018-09-07_


__Bug__

* \[[XDSTAR-703](https://gazelle.ihe.net/jira/browse/XDSTAR-703)\] Missing "<>" in the http header start element of RAD-69

# 2.3.7
_Release date: 2018-08-30_


__Story__

* \[[XDSTAR-702](https://gazelle.ihe.net/jira/browse/XDSTAR-702)\] update RMU Simulator to august 2018 TI version

# 2.3.6
_Release date: 2018-07-20_


__Story__

* \[[XDSTAR-701](https://gazelle.ihe.net/jira/browse/XDSTAR-701)\] Add PreviousVersion slot to UpdateMetadata message

# 2.3.5
_Release date: 2018-07-19_

__Bug__

* \[[XDSTAR-655](https://gazelle.ihe.net/jira/browse/XDSTAR-655)\] Review catch exception in the JAVA code, the user should be notified if there is error and where it is

__Story__

* \[[XDSTAR-700](https://gazelle.ihe.net/jira/browse/XDSTAR-700)\] Replace XCMU by RMU

# 2.3.4
_Release date: 2018-06-27_


__Bug__

* \[[XDSTAR-596](https://gazelle.ihe.net/jira/browse/XDSTAR-596)\] Duplicate a Registry Package should also duplicate the extra cosntraints
* \[[XDSTAR-597](https://gazelle.ihe.net/jira/browse/XDSTAR-597)\] When duplicating a AdhocQuery, we expect all the fields to be populated
* \[[XDSTAR-598](https://gazelle.ihe.net/jira/browse/XDSTAR-598)\] not all Validator Pack Associations can be deleted
* \[[XDSTAR-690](https://gazelle.ihe.net/jira/browse/XDSTAR-690)\] When duplicating Registry Package, usage is empty and create empty usages

__Improvement__

* \[[XDSTAR-698](https://gazelle.ihe.net/jira/browse/XDSTAR-698)\] RAD-69

# 2.3.3
_Release date: 2018-04-04_


__Bug__

* \[[XDSTAR-570](https://gazelle.ihe.net/jira/browse/XDSTAR-570)\] Cannot validate messages in Simulator as responder from the message details page
* \[[XDSTAR-590](https://gazelle.ihe.net/jira/browse/XDSTAR-590)\] Run test from test edition page raises a NPE
* \[[XDSTAR-593](https://gazelle.ihe.net/jira/browse/XDSTAR-593)\] RAD-55 : Preview button is not working
* \[[XDSTAR-672](https://gazelle.ihe.net/jira/browse/XDSTAR-672)\] NPE is thrown when jks configuration is wrong
* \[[XDSTAR-689](https://gazelle.ihe.net/jira/browse/XDSTAR-689)\] After logging, the parameters are not passed in the URL
* \[[XDSTAR-692](https://gazelle.ihe.net/jira/browse/XDSTAR-692)\] Update ExternalIdentifier related constraints

# 2.3.2
_Release date: 2018-03-20_


__Bug__

* \[[XDSTAR-595](https://gazelle.ihe.net/jira/browse/XDSTAR-595)\] Cannot validate request and response message in the test report page
* \[[XDSTAR-601](https://gazelle.ihe.net/jira/browse/XDSTAR-601)\] Documentation link is not working
* \[[XDSTAR-687](https://gazelle.ihe.net/jira/browse/XDSTAR-687)\] [RAD-55] Issue with httpClient dependency


# 2.3.1
_Release date: 2018-02-22_


__Bug__

* \[[XDSTAR-681](https://gazelle.ihe.net/jira/browse/XDSTAR-681)\] [XCMU] Cannot add SAMLAssertion in the request

__Story__

* \[[XDSTAR-657](https://gazelle.ihe.net/jira/browse/XDSTAR-657)\] Crowdin translation

# 2.3.0
_Release date: 2018-01-31_

__Remarks__

From version 2.3.0, XDStar Client shall be integrated with the new Gazelle SSO based on Apereo.


__Bug__

* \[[XDSTAR-573](https://gazelle.ihe.net/jira/browse/XDSTAR-573)\] Missing content when clicking on play icon from the XCPD Init page
* \[[XDSTAR-623](https://gazelle.ihe.net/jira/browse/XDSTAR-623)\] In XDS.b Provide and Register Document Set - epSOS affinity domain (and all the pages), when the validation is not performed, we still have an icon saying that the validation pass
* \[[XDSTAR-624](https://gazelle.ihe.net/jira/browse/XDSTAR-624)\] Regression between the prod version and the sake version about the epSOS-1 OrderService:list operation
* \[[XDSTAR-674](https://gazelle.ihe.net/jira/browse/XDSTAR-674)\] The value of the slot homeCommunityId shall start with "urn:oid"

__Improvement__

* \[[XDSTAR-600](https://gazelle.ihe.net/jira/browse/XDSTAR-600)\] Migration to Apereo CAS

# 2.2.1
_Release date: 2018-01-15_


__Bug__

* \[[XDSTAR-615](https://gazelle.ihe.net/jira/browse/XDSTAR-615)\] DispensationService:Discard() page is not working with a problem to select the configuration
* \[[XDSTAR-620](https://gazelle.ihe.net/jira/browse/XDSTAR-620)\] ConsentService:Discard() has te same problem as DispensationService:Discard()
* \[[XDSTAR-626](https://gazelle.ihe.net/jira/browse/XDSTAR-626)\] epSOS-1 transaction ignore XUA parameter
* \[[XDSTAR-673](https://gazelle.ihe.net/jira/browse/XDSTAR-673)\] AUTHER PERSON 

# 2.2.0
_Release date: 2017-12-26_


__Bug__

* \[[XDSTAR-564](https://gazelle.ihe.net/jira/browse/XDSTAR-564)\] User with admin_role cannot delete a SUT configuration

__Improvement__

* \[[XDSTAR-669](https://gazelle.ihe.net/jira/browse/XDSTAR-669)\] Create ITI-57 Document Administrator Simulator
* \[[XDSTAR-670](https://gazelle.ihe.net/jira/browse/XDSTAR-670)\] Create XCMU Initiating gateway simulator

# 2.1.1
_Release date: 2017-11-29_


__Bug__

* \[[XDSTAR-603](https://gazelle.ihe.net/jira/browse/XDSTAR-603)\] Jira link is not provided
* \[[XDSTAR-604](https://gazelle.ihe.net/jira/browse/XDSTAR-604)\] Gazelle Content Documentation is opening in the same tab
* \[[XDSTAR-616](https://gazelle.ihe.net/jira/browse/XDSTAR-616)\] When you try to validate a document through EVSClient, it is validated in the same tab, it should open a new window for that
* \[[XDSTAR-625](https://gazelle.ihe.net/jira/browse/XDSTAR-625)\] When  I add a new XDSDocumentEntryClassCode to epSOS-1 transaction, the GUI is not friendly

__Story__

* \[[XDSTAR-538](https://gazelle.ihe.net/jira/browse/XDSTAR-538)\] Add Xpath constraint for Validator Pack Association and Registry Object List

__Improvement__

* \[[XDSTAR-660](https://gazelle.ihe.net/jira/browse/XDSTAR-660)\] Enable admin to add Extra Constraints to Validator Pack Association

# 2.1.0
_Release date: 2017-11-23_


__Technical task__

* \[[XDSTAR-495](https://gazelle.ihe.net/jira/browse/XDSTAR-495)\] Migration of XDSMetadatas
* \[[XDSTAR-496](https://gazelle.ihe.net/jira/browse/XDSTAR-496)\] Migration of SUT Configurations
* \[[XDSTAR-542](https://gazelle.ihe.net/jira/browse/XDSTAR-542)\] INIT Simulator : XDS.b
* \[[XDSTAR-543](https://gazelle.ihe.net/jira/browse/XDSTAR-543)\] INIT Simulator : XCA
* \[[XDSTAR-544](https://gazelle.ihe.net/jira/browse/XDSTAR-544)\] INIT Simulator : XDR
* \[[XDSTAR-545](https://gazelle.ihe.net/jira/browse/XDSTAR-545)\] INIT Simulator : XDW
* \[[XDSTAR-546](https://gazelle.ihe.net/jira/browse/XDSTAR-546)\] INIT Simulator : XCF
* \[[XDSTAR-547](https://gazelle.ihe.net/jira/browse/XDSTAR-547)\] INIT Simulator : MPQ
* \[[XDSTAR-548](https://gazelle.ihe.net/jira/browse/XDSTAR-548)\] INIT Simulator : DSUB
* \[[XDSTAR-549](https://gazelle.ihe.net/jira/browse/XDSTAR-549)\] INIT Simulator : XCPD
* \[[XDSTAR-550](https://gazelle.ihe.net/jira/browse/XDSTAR-550)\] INIT Simulator : CT
* \[[XDSTAR-551](https://gazelle.ihe.net/jira/browse/XDSTAR-551)\] INIT Simulator : PHARM-1
* \[[XDSTAR-552](https://gazelle.ihe.net/jira/browse/XDSTAR-552)\] INIT Simulator : XDSi.b
* \[[XDSTAR-553](https://gazelle.ihe.net/jira/browse/XDSTAR-553)\] INIT Simulator : XCA-I

__Bug__

* \[[XDSTAR-476](https://gazelle.ihe.net/jira/browse/XDSTAR-476)\] The export button of AdhocQuery is public
* \[[XDSTAR-477](https://gazelle.ihe.net/jira/browse/XDSTAR-477)\] When uploading a new AdhocMetadata, there is no message of success
* \[[XDSTAR-490](https://gazelle.ihe.net/jira/browse/XDSTAR-490)\] Error in web service validation in the prod 
* \[[XDSTAR-510](https://gazelle.ihe.net/jira/browse/XDSTAR-510)\] When updating to gazelle-tools:3.0.25 or higher, fix dependencies to asm
* \[[XDSTAR-569](https://gazelle.ihe.net/jira/browse/XDSTAR-569)\] Filters on Message Transactions for Simu as responder do not work
* \[[XDSTAR-571](https://gazelle.ihe.net/jira/browse/XDSTAR-571)\] Issues on reporting the result of XCPD ITI-55 transaction when initiating from the tool
* \[[XDSTAR-572](https://gazelle.ihe.net/jira/browse/XDSTAR-572)\] ITI-41 request/response -  validation popup panel is empty
* \[[XDSTAR-582](https://gazelle.ihe.net/jira/browse/XDSTAR-582)\] ITI-61 request -  mimeType is required
* \[[XDSTAR-584](https://gazelle.ihe.net/jira/browse/XDSTAR-584)\] ITI-39 request/response -  home CommunityId is required
* \[[XDSTAR-586](https://gazelle.ihe.net/jira/browse/XDSTAR-586)\] SubmissionSet panel, patientId  added is lost when clicking in Random fulfil button in DocumentEntry panel
* \[[XDSTAR-587](https://gazelle.ihe.net/jira/browse/XDSTAR-587)\] ITI-41 request (XDW) issue with metadatas randomly fulfil
* \[[XDSTAR-588](https://gazelle.ihe.net/jira/browse/XDSTAR-588)\] ITI-51 request/response -  issue with XDSDocumentEntryStatus
* \[[XDSTAR-591](https://gazelle.ihe.net/jira/browse/XDSTAR-591)\] RAD-69 request : all the fields are required to execute the transaction
* \[[XDSTAR-599](https://gazelle.ihe.net/jira/browse/XDSTAR-599)\] Usage drop down menu shows empty labels
* \[[XDSTAR-613](https://gazelle.ihe.net/jira/browse/XDSTAR-613)\] epSOS - DispensationService:initialize() :  when we click on the button Execute, it reset the autor value
* \[[XDSTAR-614](https://gazelle.ihe.net/jira/browse/XDSTAR-614)\] Problem with XUA parameter, I checked it , but it is not taken into account
* \[[XDSTAR-617](https://gazelle.ihe.net/jira/browse/XDSTAR-617)\] When you sent consent, using epSOS - ConsentService:put(), you have a new button "Delete file", this is a bug
* \[[XDSTAR-618](https://gazelle.ihe.net/jira/browse/XDSTAR-618)\] XUA inclusion is not working for epSOS - ConsentService:put() page
* \[[XDSTAR-621](https://gazelle.ihe.net/jira/browse/XDSTAR-621)\] The icon used in XDS.b Provide and Register Document Set - epSOS affinity domain is not good for document
* \[[XDSTAR-622](https://gazelle.ihe.net/jira/browse/XDSTAR-622)\] In XDS.b Provide and Register Document Set - epSOS affinity domain, the color of the button random fulfill is not the same as it is in the other PnR pages
* \[[XDSTAR-631](https://gazelle.ihe.net/jira/browse/XDSTAR-631)\] /XD* Client/System testing/simulators/xcf/XDSTAR-41:execute ITI-63 request -  Executed ON (ISO FORMAT): 2017-11-15 22:16
* \[[XDSTAR-646](https://gazelle.ihe.net/jira/browse/XDSTAR-646)\] /XD* Client/System testing/simulators/epsos-2/Provide Data Service/XDSTAR-215:XUA -  Executed ON (ISO FORMAT): 2017-11-1

__Epic__

* \[[XDSTAR-530](https://gazelle.ihe.net/jira/browse/XDSTAR-530)\] Migration of XD* Client style

__Story__

* \[[XDSTAR-494](https://gazelle.ihe.net/jira/browse/XDSTAR-494)\] Migrate metadata and SUT conf
* \[[XDSTAR-497](https://gazelle.ihe.net/jira/browse/XDSTAR-497)\] Migration of SIMU-Initiators
* \[[XDSTAR-498](https://gazelle.ihe.net/jira/browse/XDSTAR-498)\] Migration of patient management module
* \[[XDSTAR-499](https://gazelle.ihe.net/jira/browse/XDSTAR-499)\] migration of messages
* \[[XDSTAR-531](https://gazelle.ihe.net/jira/browse/XDSTAR-531)\] Test day for XD* Client
* \[[XDSTAR-539](https://gazelle.ihe.net/jira/browse/XDSTAR-539)\] Migration of SIMU-Responders
* \[[XDSTAR-540](https://gazelle.ihe.net/jira/browse/XDSTAR-540)\] Migration of XDS Documentation
* \[[XDSTAR-541](https://gazelle.ihe.net/jira/browse/XDSTAR-541)\] Migrate the Home page, menu and footer
* \[[XDSTAR-561](https://gazelle.ihe.net/jira/browse/XDSTAR-561)\] Use new CAS SSO

__Improvement__

* \[[XDSTAR-233](https://gazelle.ihe.net/jira/browse/XDSTAR-233)\] review of sonar for XDStar-modules
* \[[XDSTAR-500](https://gazelle.ihe.net/jira/browse/XDSTAR-500)\] migration of administration menu

# 2.0.12
_Release date: 2016-02-16_


__Technical task__

* \[[XDSTAR-478](https://gazelle.ihe.net/jira/browse/XDSTAR-478)\] upload of XML structure of metadata is not working

__Bug__

* \[[XDSTAR-464](https://gazelle.ihe.net/jira/browse/XDSTAR-464)\] Fault error related to the validation of DocumentEntryType
* \[[XDSTAR-465](https://gazelle.ihe.net/jira/browse/XDSTAR-465)\] bug in the validation of ITI-18
* \[[XDSTAR-491](https://gazelle.ihe.net/jira/browse/XDSTAR-491)\] bug in HE XDS.b ITI-41 Provide and Register Set-b - request schema validation

# 2.0.11
_Release date: 2016-02-15_

__Bug__

* \[[XDSTAR-471](https://gazelle.ihe.net/jira/browse/XDSTAR-471)\] Review why the DSUB validation was not working in CAT US
* \[[XDSTAR-474](https://gazelle.ihe.net/jira/browse/XDSTAR-474)\] Wado validation does not work in EVSClient

# 2.0.10
_Release date: 2016-02-09_

__Bug__

* \[[XDSTAR-469](https://gazelle.ihe.net/jira/browse/XDSTAR-469)\] Unit tests are not working anymore for XDSValidator module
* \[[XDSTAR-470](https://gazelle.ihe.net/jira/browse/XDSTAR-470)\] ITI-41 Response - EVSClient incorrectly reports an error

# 2.0.8
_Release date: 2015-11-18_


__Bug__

* \[[XDSTAR-278](https://gazelle.ihe.net/jira/browse/XDSTAR-278)\] problem on RAd-69, for generating from manifest
* \[[XDSTAR-391](https://gazelle.ihe.net/jira/browse/XDSTAR-391)\] References to the standard paragraph for the class ExternalIdentifierSpec are wrong
* \[[XDSTAR-462](https://gazelle.ihe.net/jira/browse/XDSTAR-462)\] constraintCreationTime_DTM is not working fine

__Improvement__

* \[[XDSTAR-369](https://gazelle.ihe.net/jira/browse/XDSTAR-369)\] in WADO simulator, set the html as a link, and execute it when you click on it
* \[[XDSTAR-370](https://gazelle.ihe.net/jira/browse/XDSTAR-370)\] in CT simulator, the GUI is not intuitive, add a table with the headers server, client, and differences

# 2.0.7
_Release date: 2015-11-04_


__Bug__

* \[[XDSTAR-435](https://gazelle.ihe.net/jira/browse/XDSTAR-435)\] Problem to view the filter of allMessages
* \[[XDSTAR-441](https://gazelle.ihe.net/jira/browse/XDSTAR-441)\] ITI-61 is not working fine

# 2.0.6
_Release date: 2015-10-28_


__Bug__

* \[[XDSTAR-459](https://gazelle.ihe.net/jira/browse/XDSTAR-459)\] epSOS validation is not processed in the messages displayed
* \[[XDSTAR-461](https://gazelle.ihe.net/jira/browse/XDSTAR-461)\] Bug in the validation of epSOS OrderService:list(), PatientService:list() and IdentificationService

# 2.0.5
_Release date: 2015-10-28_

__Bug__

* \[[XDSTAR-458](https://gazelle.ihe.net/jira/browse/XDSTAR-458)\] external validator are not treated when they are needed

# 2.0.4
_Release date: 2015-10-23_


__Bug__

* \[[XDSTAR-389](https://gazelle.ihe.net/jira/browse/XDSTAR-389)\] error in http://gazelle.ihe.net/XDStarClient/docum/constraints/constraintDescriptor.seam?name=constraintEntryUUID_UUID&class=XDSSubmissionSetConstraints&pack=rimihe
* \[[XDSTAR-390](https://gazelle.ihe.net/jira/browse/XDSTAR-390)\] The description of http://gazelle.ihe.net/XDStarClient/docum/constraints/constraintDescriptor.seam?name=cnstraintSubmissionSetTime&class=XDSSubmissionSetConstraints&pack=rimihe is wrong
* \[[XDSTAR-392](https://gazelle.ihe.net/jira/browse/XDSTAR-392)\] http://gazelle.ihe.net/XDStarClient/docum/constraints/constraintDescriptor.seam?name=constraintSlot&class=SlotPackageSpec&pack=rimihe SHALL be related to requirement IHEXDS-117
* \[[XDSTAR-413](https://gazelle.ihe.net/jira/browse/XDSTAR-413)\] constraintSubmissionSet_allowed_slot is wrong assertion
* \[[XDSTAR-452](https://gazelle.ihe.net/jira/browse/XDSTAR-452)\] The responder is not viewed in the list of all message of initiators
* \[[XDSTAR-457](https://gazelle.ihe.net/jira/browse/XDSTAR-457)\] Problem in the validation using nist jar

# 2.0.0
_Release date: 2015-09-08_


__Story__

* \[[XDSTAR-394](https://gazelle.ihe.net/jira/browse/XDSTAR-394)\] Migration to jboss7 of XDStarClient


# 1.1.2
_Release date: No release date_


__Technical task__

* \[[XDSTAR-241](https://gazelle.ihe.net/jira/browse/XDSTAR-241)\] update the description of pre-cat tests
* \[[XDSTAR-388](https://gazelle.ihe.net/jira/browse/XDSTAR-388)\] The view of validation result does not appear anymore in XDStarClient

__Bug__

* \[[XDSTAR-237](https://gazelle.ihe.net/jira/browse/XDSTAR-237)\] fix the problem of the missing  association between PS L1 and PS L3
* \[[XDSTAR-238](https://gazelle.ihe.net/jira/browse/XDSTAR-238)\] [XDS.b Provide and Register Document Set - epSOS affinity domain] not working well
* \[[XDSTAR-255](https://gazelle.ihe.net/jira/browse/XDSTAR-255)\] Add trim in patientId for PnR transaction
* \[[XDSTAR-262](https://gazelle.ihe.net/jira/browse/XDSTAR-262)\] RAD-75 : the transfert syntax is badly used to create the request !
* \[[XDSTAR-277](https://gazelle.ihe.net/jira/browse/XDSTAR-277)\] Problem on the xsd validation of XDS metadatas
* \[[XDSTAR-348](https://gazelle.ihe.net/jira/browse/XDSTAR-348)\] Missing validation of the valuesset of SubmissionSet.contentType on the model based validation of KXDS
* \[[XDSTAR-371](https://gazelle.ihe.net/jira/browse/XDSTAR-371)\] Wado validation does not work.
* \[[XDSTAR-387](https://gazelle.ihe.net/jira/browse/XDSTAR-387)\] bug in constraintIntendedRecipienStruc requirements

# 1.1.1
_Release date: 2015-01-08_


__Bug__

* \[[XDSTAR-284](https://gazelle.ihe.net/jira/browse/XDSTAR-284)\] There are no page.xml for the audit edition
* \[[XDSTAR-292](https://gazelle.ihe.net/jira/browse/XDSTAR-292)\] The codingScheme of urn:ihe:rad:PDF shall be 1.3.6.1.4.1.19376.1.2.3 (for RAD-68)
* \[[XDSTAR-385](https://gazelle.ihe.net/jira/browse/XDSTAR-385)\] Bug in the publish DSUB

# 1.1.0
_Release date: 2015-01-08_

__Technical task__

* \[[XDSTAR-363](https://gazelle.ihe.net/jira/browse/XDSTAR-363)\] Implement delete() [ITI-62] in xdstools2
* \[[XDSTAR-382](https://gazelle.ihe.net/jira/browse/XDSTAR-382)\] Fix the validation of publish transaction
* \[[XDSTAR-383](https://gazelle.ihe.net/jira/browse/XDSTAR-383)\] Fix the simulator of publish transaction

__Bug__

* \[[XDSTAR-295](https://gazelle.ihe.net/jira/browse/XDSTAR-295)\] Update the validator of DSUB because it is not useful anymore after the last CP. The simulator is OK.
* \[[XDSTAR-380](https://gazelle.ihe.net/jira/browse/XDSTAR-380)\] Problem in the picklist of usage of list configurations
* \[[XDSTAR-381](https://gazelle.ihe.net/jira/browse/XDSTAR-381)\] Fix the error in the validation of the publish transaction

__Story__

* \[[XDSTAR-303](https://gazelle.ihe.net/jira/browse/XDSTAR-303)\] Configuration of xdstools for KSA project
* \[[XDSTAR-378](https://gazelle.ihe.net/jira/browse/XDSTAR-378)\] add SubmissionSet and Folder metadata attributes to ITI-52 in XDStarClient
* \[[XDSTAR-379](https://gazelle.ihe.net/jira/browse/XDSTAR-379)\] NA2015: Some oddities with testing ITI-62 in XDStarClient
* \[[XDSTAR-384](https://gazelle.ihe.net/jira/browse/XDSTAR-384)\] Add the simulator of notify transaction

# 1.0.4
_Release date: 2014-12-04_

__Technical task__

* \[[XDSTAR-305](https://gazelle.ihe.net/jira/browse/XDSTAR-305)\] configure the XDStarClient, to have a client for KSA domain
* \[[XDSTAR-306](https://gazelle.ihe.net/jira/browse/XDSTAR-306)\] write testlink requirements and test cases

__Bug__

* \[[XDSTAR-291](https://gazelle.ihe.net/jira/browse/XDSTAR-291)\] The identifier of the template of DSUB subscribe is wrong !!
* \[[XDSTAR-345](https://gazelle.ihe.net/jira/browse/XDSTAR-345)\] fix the generation of the request WADO
* \[[XDSTAR-359](https://gazelle.ihe.net/jira/browse/XDSTAR-359)\] Error on th page.xml, there are a redirection to login.xhtml, which does not exist any more
* \[[XDSTAR-377](https://gazelle.ihe.net/jira/browse/XDSTAR-377)\] The DSUB validation does not take care of the ihe:FolderMetadata and ihe:submissionSetMetadata

# 1.0.3
_Release date: 2014-07-18_


__Bug__

* \[[XDSTAR-362](https://gazelle.ihe.net/jira/browse/XDSTAR-362)\] The list of assertions is not reachable from AssertionManager

# 1.0.1
_Release date: No release date_

__Story__

* \[[XDSTAR-360](https://gazelle.ihe.net/jira/browse/XDSTAR-360)\] Create a validator for K-BPPC Sharing document

# 1.0.0
_Release date: 2014-05-28_

__Technical task__

* \[[XDSTAR-310](https://gazelle.ihe.net/jira/browse/XDSTAR-310)\] Add the PurposeOfUse for the ATNA messages validation (ITI TF V2b, 3.40.1.2.3.1)
* \[[XDSTAR-311](https://gazelle.ihe.net/jira/browse/XDSTAR-311)\] Add the specifications of ATNA logging (ITI V2b, 3.40.4.2) for the UserName
* \[[XDSTAR-324](https://gazelle.ihe.net/jira/browse/XDSTAR-324)\] relate the configurations of KSA directly to the RESTful services of gazelle

__Bug__

* \[[XDSTAR-343](https://gazelle.ihe.net/jira/browse/XDSTAR-343)\] problem on the web.xml for the retful services

__Story__

* \[[XDSTAR-275](https://gazelle.ihe.net/jira/browse/XDSTAR-275)\]  KSA - Create validator for SAML assertions
* \[[XDSTAR-276](https://gazelle.ihe.net/jira/browse/XDSTAR-276)\]  KSA - Create validator for DSUB ITI-53 notification transaction
* \[[XDSTAR-309](https://gazelle.ihe.net/jira/browse/XDSTAR-309)\] Create validator for AuditMessages KSA
* \[[XDSTAR-312](https://gazelle.ihe.net/jira/browse/XDSTAR-312)\] Update the xdstools2 to support DSUB messages for K-Project

# 0.57.8
_Release date: 2014-04-04_


__Technical task__

* \[[XDSTAR-313](https://gazelle.ihe.net/jira/browse/XDSTAR-313)\] Create responder to store the notification --> a notify recipient actor

__Bug__

* \[[XDSTAR-268](https://gazelle.ihe.net/jira/browse/XDSTAR-268)\] KSA - Create validator for XDS.b
* \[[XDSTAR-308](https://gazelle.ihe.net/jira/browse/XDSTAR-308)\]  entryUUID shall have the structure of UUID (IHE_ITI_TF V3, 4.2.3.3.5)
* \[[XDSTAR-340](https://gazelle.ihe.net/jira/browse/XDSTAR-340)\]  RAD-75 webservice call from XDStarclient. The Action tag has an extra colon and space versus what should be generated
* \[[XDSTAR-341](https://gazelle.ihe.net/jira/browse/XDSTAR-341)\] Problem to generate the RAD-68 for KSA

__Improvement__

* \[[XDSTAR-342](https://gazelle.ihe.net/jira/browse/XDSTAR-342)\] Add restful request for the version of the simulator

# 0.57.7
_Release date: 2014-04-01_


__Technical task__

* \[[XDSTAR-267](https://gazelle.ihe.net/jira/browse/XDSTAR-267)\] KSA - Create validator for XDS-I.b RAD-55 WADO request
* \[[XDSTAR-322](https://gazelle.ihe.net/jira/browse/XDSTAR-322)\] problem on the encoding of XDStools
* \[[XDSTAR-323](https://gazelle.ihe.net/jira/browse/XDSTAR-323)\] problem on the parsing of the mtom

__Bug__

* \[[XDSTAR-314](https://gazelle.ihe.net/jira/browse/XDSTAR-314)\] Problem encoding xml file
* \[[XDSTAR-315](https://gazelle.ihe.net/jira/browse/XDSTAR-315)\] Problem on the DSUB Recipient endpoint

__Story__

* \[[XDSTAR-263](https://gazelle.ihe.net/jira/browse/XDSTAR-263)\] KSA - Create validator for XDS-I.b
* \[[XDSTAR-321](https://gazelle.ihe.net/jira/browse/XDSTAR-321)\] Create validators for KSA DSUB

# 0.57.2
_Release date: No release date_

__Bug__

* \[[XDSTAR-316](https://gazelle.ihe.net/jira/browse/XDSTAR-316)\] Update of the schema of RF3881 to be conform to PurposeOfUse option

# 0.57.1
_Release date: No release date_


__Technical task__

* \[[XDSTAR-264](https://gazelle.ihe.net/jira/browse/XDSTAR-264)\] KSA - Create validator for XDS-I.b RAD-68 SIIR manifest upload
* \[[XDSTAR-265](https://gazelle.ihe.net/jira/browse/XDSTAR-265)\] KSA - Create validator for XDS-I.b RAD-68 SIIR report upload
* \[[XDSTAR-266](https://gazelle.ihe.net/jira/browse/XDSTAR-266)\] KSA - Create validator for XDS-I.b RAD-69 retrieve
* \[[XDSTAR-271](https://gazelle.ihe.net/jira/browse/XDSTAR-271)\] KSA - Create validator for XDS.b ITI-41 upload
* \[[XDSTAR-272](https://gazelle.ihe.net/jira/browse/XDSTAR-272)\] KSA - Create validator for XDS.b ITI-41 lab report upload
* \[[XDSTAR-273](https://gazelle.ihe.net/jira/browse/XDSTAR-273)\] KSA - Create validator for XDS.b ITI-41 lab order upload
* \[[XDSTAR-274](https://gazelle.ihe.net/jira/browse/XDSTAR-274)\] KSA - Create validator for XDS.b ITI-41 radiology report upload
* \[[XDSTAR-301](https://gazelle.ihe.net/jira/browse/XDSTAR-301)\] system testing of KSA validators
* \[[XDSTAR-302](https://gazelle.ihe.net/jira/browse/XDSTAR-302)\] Prepare report of delivery
* \[[XDSTAR-304](https://gazelle.ihe.net/jira/browse/XDSTAR-304)\] configure the codes.xml

__Story__

* \[[XDSTAR-259](https://gazelle.ihe.net/jira/browse/XDSTAR-259)\] Add the possibility to hide epSOS menus from XDStarClient

# 0.57.0
_Release date: 2014-02-18_


__Technical task__

* \[[XDSTAR-297](https://gazelle.ihe.net/jira/browse/XDSTAR-297)\] Update the rules
* \[[XDSTAR-299](https://gazelle.ihe.net/jira/browse/XDSTAR-299)\] unit testing of the rules
* \[[XDSTAR-300](https://gazelle.ihe.net/jira/browse/XDSTAR-300)\] Integration testing of the new validators

__Bug__

* \[[XDSTAR-296](https://gazelle.ihe.net/jira/browse/XDSTAR-296)\] Write CP about DSUB profile

# 0.56.4
_Release date: 2014-01-23_

__Technical task__

* \[[XDSTAR-269](https://gazelle.ihe.net/jira/browse/XDSTAR-269)\] KSA - Create validator for XDS.b ITI-18 request
* \[[XDSTAR-270](https://gazelle.ihe.net/jira/browse/XDSTAR-270)\] KSA - Create validator for XDS.b ITI-43 request

__Bug__

* \[[XDSTAR-257](https://gazelle.ihe.net/jira/browse/XDSTAR-257)\] Resolve logs about serialization of beans
* \[[XDSTAR-261](https://gazelle.ihe.net/jira/browse/XDSTAR-261)\] There are missing transfert syntax uuid
* \[[XDSTAR-285](https://gazelle.ihe.net/jira/browse/XDSTAR-285)\] Attach the file uploaded by RAD-69 to the message saved
* \[[XDSTAR-286](https://gazelle.ihe.net/jira/browse/XDSTAR-286)\] Fix the mtom attachement when it is a dicom file / binary

# 0.56.2
_Release date: 2013-12-03_

__Bug__

* \[[XDSTAR-260](https://gazelle.ihe.net/jira/browse/XDSTAR-260)\] Problem on configuring the TLS

# 0.56.1
_Release date: 2013-11-29_


__Bug__

* \[[XDSTAR-258](https://gazelle.ihe.net/jira/browse/XDSTAR-258)\] Problem to delete object from XDStarClient

# 0.56.0
_Release date: 2013-11-28_


__Technical task__

* \[[XDSTAR-251](https://gazelle.ihe.net/jira/browse/XDSTAR-251)\] Update validators using the valueSet validation
* \[[XDSTAR-252](https://gazelle.ihe.net/jira/browse/XDSTAR-252)\] Update the simulators using the valueSet

__Story__

* \[[XDSTAR-249](https://gazelle.ihe.net/jira/browse/XDSTAR-249)\] Merge the validator / simulator's modules to the new version of SVSSimulator
* \[[XDSTAR-256](https://gazelle.ihe.net/jira/browse/XDSTAR-256)\] Update the use of the cas that it will be used for china installation

# 0.51
_Release date: No release date_

__Technical task__

* \[[XDSTAR-250](https://gazelle.ihe.net/jira/browse/XDSTAR-250)\] update SVSValueSet used by XDStarClient

__Bug__

* \[[XDSTAR-247](https://gazelle.ihe.net/jira/browse/XDSTAR-247)\] Problem on the cas configuration

__Story__

* \[[XDSTAR-244](https://gazelle.ihe.net/jira/browse/XDSTAR-244)\] Add the version of the nist tool validation into the result of the validation 
* \[[XDSTAR-248](https://gazelle.ihe.net/jira/browse/XDSTAR-248)\] review response message part of audit messages of epSOS

# 0.50
_Release date: 2013-09-25_


__Technical task__

* \[[XDSTAR-245](https://gazelle.ihe.net/jira/browse/XDSTAR-245)\] Add the retrieve responding operation to the webservice
* \[[XDSTAR-246](https://gazelle.ihe.net/jira/browse/XDSTAR-246)\] Add unit tests for the retrieve validator

# 0.49
_Release date: 2013-09-20_


__Story__

* \[[XDSTAR-243](https://gazelle.ihe.net/jira/browse/XDSTAR-243)\] add GE profile

# 0.48
_Release date: No release date_


__Technical task__

* \[[XDSTAR-236](https://gazelle.ihe.net/jira/browse/XDSTAR-236)\] Add unit testing for the validator
* \[[XDSTAR-239](https://gazelle.ihe.net/jira/browse/XDSTAR-239)\] Add a page to capture all message treated by the XCAResponding Gateway
* \[[XDSTAR-242](https://gazelle.ihe.net/jira/browse/XDSTAR-242)\] verify that TLS is working fine

__Story__

* \[[XDSTAR-234](https://gazelle.ihe.net/jira/browse/XDSTAR-234)\] Add the generation of the RAD-69 (Retrieve Imaging Document Set) from the KOS manifest
* \[[XDSTAR-235](https://gazelle.ihe.net/jira/browse/XDSTAR-235)\] Add validator of RetrieveImagingDocumentSetRequest by reference to its KOS manifest

# 0.47
_Release date: 2013-09-05_


__Bug__

* \[[XDSTAR-232](https://gazelle.ihe.net/jira/browse/XDSTAR-232)\] Add management of constraints on the admin menu

# 0.46
_Release date: 2013-09-04_


__Technical task__

* \[[XDSTAR-104](https://gazelle.ihe.net/jira/browse/XDSTAR-104)\] Pages of the tools are not enough documented. There are no instructions to users.
* \[[XDSTAR-227](https://gazelle.ihe.net/jira/browse/XDSTAR-227)\] add page to configure application preference on XDStarClient
* \[[XDSTAR-228](https://gazelle.ihe.net/jira/browse/XDSTAR-228)\] update gazelle-tool to support multiple kind of application preference --> allow security on simulator-common
* \[[XDSTAR-229](https://gazelle.ihe.net/jira/browse/XDSTAR-229)\] update simulator common with security management
* \[[XDSTAR-230](https://gazelle.ihe.net/jira/browse/XDSTAR-230)\] write documentation about simulator security

__Bug__

* \[[XDSTAR-225](https://gazelle.ihe.net/jira/browse/XDSTAR-225)\] problem on the start http header, on the retrieve document set

__Story__

* \[[XDSTAR-155](https://gazelle.ihe.net/jira/browse/XDSTAR-155)\] Add the documentation of templates/ constraints as it was done for CDA
* \[[XDSTAR-170](https://gazelle.ihe.net/jira/browse/XDSTAR-170)\] Add security check as it was done for EVSClient

# 0.45
_Release date: 2013-08-27_


__Improvement__

* \[[XDSTAR-150](https://gazelle.ihe.net/jira/browse/XDSTAR-150)\] Add validation version to the result of validation for XDS and auditMessages validation

# 0.44
_Release date: 2013-08-26_


__Technical task__

* \[[XDSTAR-215](https://gazelle.ihe.net/jira/browse/XDSTAR-215)\] Add description on teststeps
* \[[XDSTAR-216](https://gazelle.ihe.net/jira/browse/XDSTAR-216)\] add copy of templates
* \[[XDSTAR-217](https://gazelle.ihe.net/jira/browse/XDSTAR-217)\] add copy of Test description
* \[[XDSTAR-218](https://gazelle.ihe.net/jira/browse/XDSTAR-218)\] Add the validation of rules
* \[[XDSTAR-220](https://gazelle.ihe.net/jira/browse/XDSTAR-220)\] Add sequence diagram

__Bug__

* \[[XDSTAR-213](https://gazelle.ihe.net/jira/browse/XDSTAR-213)\] Problem on the constraint constraint_RegistryObjectList and constraintStatus of the validators epSOS OrderService:list - request (V2.2 XCF) / epSOS PatientService:list - request (V2.2 XCF)
* \[[XDSTAR-214](https://gazelle.ihe.net/jira/browse/XDSTAR-214)\] Problem on epSOS2 validator of metadatas

__Improvement__

* \[[XDSTAR-210](https://gazelle.ihe.net/jira/browse/XDSTAR-210)\] Add button to refresh automatically the timer

# 0.42
_Release date: 2013-07-12_


__Technical task__

* \[[XDSTAR-172](https://gazelle.ihe.net/jira/browse/XDSTAR-172)\] add manual check on the page of configurations
* \[[XDSTAR-201](https://gazelle.ihe.net/jira/browse/XDSTAR-201)\] Upgrade of auditMessages of XCPD req/resp
* \[[XDSTAR-202](https://gazelle.ihe.net/jira/browse/XDSTAR-202)\] Upgrade of the simulator of the client

__Bug__

* \[[XDSTAR-207](https://gazelle.ihe.net/jira/browse/XDSTAR-207)\] Problem on the validation of uploaded document CDA on ProvideDataService
* \[[XDSTAR-209](https://gazelle.ihe.net/jira/browse/XDSTAR-209)\] Problem on MTOM message generation

__Story__

* \[[XDSTAR-200](https://gazelle.ihe.net/jira/browse/XDSTAR-200)\] Upgrade of Identification Service to epSOS-2
* \[[XDSTAR-204](https://gazelle.ihe.net/jira/browse/XDSTAR-204)\] configure the c3p0 of XDStarClient
* \[[XDSTAR-206](https://gazelle.ihe.net/jira/browse/XDSTAR-206)\] Add the possibility to hide configuration (private configuration)

__Improvement__

* \[[XDSTAR-208](https://gazelle.ihe.net/jira/browse/XDSTAR-208)\] Add the ability to associate a configuration to a user account as it is possible in the other simulators.

# 0.41
_Release date: 2013-07-05_


__Technical task__

* \[[XDSTAR-197](https://gazelle.ihe.net/jira/browse/XDSTAR-197)\] create simulator for Provide Data Service
* \[[XDSTAR-198](https://gazelle.ihe.net/jira/browse/XDSTAR-198)\] create validator for ProvideDataService (request + response)
* \[[XDSTAR-199](https://gazelle.ihe.net/jira/browse/XDSTAR-199)\] create validators for auditMessages

__Story__

* \[[XDSTAR-196](https://gazelle.ihe.net/jira/browse/XDSTAR-196)\] Simulator and validator for epSOS-2 XDR-binding

# 0.40
_Release date: 2013-07-04_


__Improvement__

* \[[XDSTAR-195](https://gazelle.ihe.net/jira/browse/XDSTAR-195)\] Add download button of XML metadatas

# 0.39
_Release date: 2013-07-02_


__Technical task__

* \[[XDSTAR-186](https://gazelle.ihe.net/jira/browse/XDSTAR-186)\] Update the validator of ITI-18
* \[[XDSTAR-187](https://gazelle.ihe.net/jira/browse/XDSTAR-187)\] Update the validator of ITI-43
* \[[XDSTAR-188](https://gazelle.ihe.net/jira/browse/XDSTAR-188)\] Update the validator of ITI-38
* \[[XDSTAR-189](https://gazelle.ihe.net/jira/browse/XDSTAR-189)\] Update the validator of ITI-39
* \[[XDSTAR-190](https://gazelle.ihe.net/jira/browse/XDSTAR-190)\] Update the simulator of ITI-18
* \[[XDSTAR-191](https://gazelle.ihe.net/jira/browse/XDSTAR-191)\] Update the simulator of ITI-38
* \[[XDSTAR-192](https://gazelle.ihe.net/jira/browse/XDSTAR-192)\] Add validator for ITI-61
* \[[XDSTAR-193](https://gazelle.ihe.net/jira/browse/XDSTAR-193)\] Add simulator for ITI-61
* \[[XDSTAR-194](https://gazelle.ihe.net/jira/browse/XDSTAR-194)\] Add auditMessage validator for On-Demand Document transaction

__Bug__

* \[[XDSTAR-159](https://gazelle.ihe.net/jira/browse/XDSTAR-159)\] Name of table is too long !

__Story__

* \[[XDSTAR-185](https://gazelle.ihe.net/jira/browse/XDSTAR-185)\] Add On Demand Documents option to XDS / XCA profiles

__Improvement__

* \[[XDSTAR-169](https://gazelle.ihe.net/jira/browse/XDSTAR-169)\] Missing documentation of the tool DSUB simulator

# 0.38
_Release date: 2013-06-19_

__Bug__

* \[[XDSTAR-167](https://gazelle.ihe.net/jira/browse/XDSTAR-167)\] Problem on the validator of publish notify message
* \[[XDSTAR-168](https://gazelle.ihe.net/jira/browse/XDSTAR-168)\] Problem on the response kind, of publish dsub transaction

# 0.37
_Release date: 2013-06-19_


__Bug__

* \[[XDSTAR-182](https://gazelle.ihe.net/jira/browse/XDSTAR-182)\] Deploy the possibiliy t link the result of validation of AuditMessage with the documentation

# 0.36
_Release date: 2013-06-03_


__Bug__

* \[[XDSTAR-151](https://gazelle.ihe.net/jira/browse/XDSTAR-151)\] Error occure when accessing to list Usages
* \[[XDSTAR-158](https://gazelle.ihe.net/jira/browse/XDSTAR-158)\] Update pom informations, to fix links on sonar

__Story__

* \[[XDSTAR-157](https://gazelle.ihe.net/jira/browse/XDSTAR-157)\] Add coupling between the validation of AuditMessage and the documentation generated from the model
* \[[XDSTAR-160](https://gazelle.ihe.net/jira/browse/XDSTAR-160)\] Internationalization of XDStarClient
* \[[XDSTAR-164](https://gazelle.ihe.net/jira/browse/XDSTAR-164)\] Create Subscription Client for DSUB profile (ITI-52)
* \[[XDSTAR-166](https://gazelle.ihe.net/jira/browse/XDSTAR-166)\] Create publish Client for DSUB profile (ITI-54)

# 0.35
_Release date: 2013-05-14_


__Bug__

* \[[XDSTAR-154](https://gazelle.ihe.net/jira/browse/XDSTAR-154)\] Problem on validation soap envelop for DSUB validator

# 0.33
_Release date: 2013-05-14_


__Bug__

* \[[XDSTAR-130](https://gazelle.ihe.net/jira/browse/XDSTAR-130)\] Problem on the creation of the request ITI-41, the nist  validation does not pass when the request contains a document to submit

__Story__

* \[[XDSTAR-153](https://gazelle.ihe.net/jira/browse/XDSTAR-153)\] Validator for DSub transactions

# 0.31
_Release date: 2013-03-28_

__Bug__

* \[[XDSTAR-129](https://gazelle.ihe.net/jira/browse/XDSTAR-129)\] log on the deployment of XDStarClient
* \[[XDSTAR-146](https://gazelle.ihe.net/jira/browse/XDSTAR-146)\] Problem to access the list of all messages

__Improvement__

* \[[XDSTAR-145](https://gazelle.ihe.net/jira/browse/XDSTAR-145)\] Add statistics to Webservice validation

# 0.30
_Release date: 2013-03-26_

__Bug__

* \[[XDSTAR-125](https://gazelle.ihe.net/jira/browse/XDSTAR-125)\] Problem on delete a document from a submissionSet
* \[[XDSTAR-131](https://gazelle.ihe.net/jira/browse/XDSTAR-131)\] Improvement of the editor of AuditMessage description
* \[[XDSTAR-134](https://gazelle.ihe.net/jira/browse/XDSTAR-134)\] Some logs appears on the server of production

__Story__

* \[[XDSTAR-126](https://gazelle.ihe.net/jira/browse/XDSTAR-126)\] Add audit-message specification edition
* \[[XDSTAR-132](https://gazelle.ihe.net/jira/browse/XDSTAR-132)\] Add the validation of epSOS AuditMessage

# 0.29
_Release date: 2013-01-18_

__Bug__

* \[[XDSTAR-133](https://gazelle.ihe.net/jira/browse/XDSTAR-133)\] Problem on the validation of XCN on metadatas

__Story__

* \[[XDSTAR-128](https://gazelle.ihe.net/jira/browse/XDSTAR-128)\] Create the system of check of rules for AuditMessages

# 0.28
_Release date: 2013-01-15_


__Story__

* \[[XDSTAR-127](https://gazelle.ihe.net/jira/browse/XDSTAR-127)\] Create the base of rules for audit messages

# 0.27
_Release date: 2013-01-08_


__Bug__

* \[[XDSTAR-123](https://gazelle.ihe.net/jira/browse/XDSTAR-123)\] problem on the validation of the response of  DispensationService:initialize(), there are a log that shall not be there
* \[[XDSTAR-124](https://gazelle.ihe.net/jira/browse/XDSTAR-124)\] Problem on the validation of XDS-I RAD-69 response 

# 0.26
_Release date: 2013-01-07_


__Story__

* \[[XDSTAR-120](https://gazelle.ihe.net/jira/browse/XDSTAR-120)\] Add the management of patient of openempi, remove this component from PDQPDS

# 0.25
_Release date: 2013-01-04_


__Bug__

* \[[XDSTAR-109](https://gazelle.ihe.net/jira/browse/XDSTAR-109)\] Need additional field in the GUI to built the XUA assertions in the simulator

__Improvement__

* \[[XDSTAR-122](https://gazelle.ihe.net/jira/browse/XDSTAR-122)\] remove the cid attribute from the menu link

# 0.24
_Release date: 2012-12-28_


__Bug__

* \[[XDSTAR-94](https://gazelle.ihe.net/jira/browse/XDSTAR-94)\] on the transaction XCPD, when selecting a configuration, the homeCommunityId receiver SHALL be auto fulfill
* \[[XDSTAR-99](https://gazelle.ihe.net/jira/browse/XDSTAR-99)\] XDSTAR - Problem with random fulfil of metadata on PnR, sometimes metadatas are left blank
* \[[XDSTAR-101](https://gazelle.ihe.net/jira/browse/XDSTAR-101)\] Problem to submit document to xdstools of epSOS due to the change of the codes related to ISO 3166-1 alpha-2
* \[[XDSTAR-102](https://gazelle.ihe.net/jira/browse/XDSTAR-102)\] On the configurations, view context by display and not by keyword
* \[[XDSTAR-103](https://gazelle.ihe.net/jira/browse/XDSTAR-103)\] on editClassificationMetadata, reorder affinityDomain/transaction when selecting the usage of classification
* \[[XDSTAR-105](https://gazelle.ihe.net/jira/browse/XDSTAR-105)\] Problem on the birthTime value for XCPD (IHE) profile
* \[[XDSTAR-108](https://gazelle.ihe.net/jira/browse/XDSTAR-108)\] Sort and Filtering in table that contains all the messages does not work !
* \[[XDSTAR-109](https://gazelle.ihe.net/jira/browse/XDSTAR-109)\] Need additional field in the GUI to built the XUA assertions in the simulator
* \[[XDSTAR-110](https://gazelle.ihe.net/jira/browse/XDSTAR-110)\] Log errors on XDStarClient sake
* \[[XDSTAR-111](https://gazelle.ihe.net/jira/browse/XDSTAR-111)\] Add TRC choose on PatientIdentification for epSOS
* \[[XDSTAR-116](https://gazelle.ihe.net/jira/browse/XDSTAR-116)\] When the nist server is down, we are not able to validate metadatas 

__Story__

* \[[XDSTAR-98](https://gazelle.ihe.net/jira/browse/XDSTAR-98)\] Add support of eprescription on XDStools of epSOS

__Improvement__

* \[[XDSTAR-107](https://gazelle.ihe.net/jira/browse/XDSTAR-107)\] use the schema validation with sax instead of dom valiadtion for metadata validation
* \[[XDSTAR-115](https://gazelle.ihe.net/jira/browse/XDSTAR-115)\] Improve the birthtime attribute specification for PatientIdentification / epSOS

# 0.23
_Release date: 2012-12-17 00:00:00.0 _

__Remarks__


__Bug__

* \[[XDSTAR-100](https://gazelle.ihe.net/jira/browse/XDSTAR-100)\] Problem on the nist validation of ITI-41 request
* \[[XDSTAR-106](https://gazelle.ihe.net/jira/browse/XDSTAR-106)\] Problem on the validation of RetrieveDocumentSet response

# 0.22
_Release date: 2012-11-26_


__Technical task__

* \[[XDSTAR-75](https://gazelle.ihe.net/jira/browse/XDSTAR-75)\] documentation of how to add constraints to models

__Story__

* \[[XDSTAR-69](https://gazelle.ihe.net/jira/browse/XDSTAR-69)\] Add XUA support for IHE transactions
* \[[XDSTAR-71](https://gazelle.ihe.net/jira/browse/XDSTAR-71)\] Documentation of the generation of models based validation
* \[[XDSTAR-82](https://gazelle.ihe.net/jira/browse/XDSTAR-82)\] Add transaction RAD-68
* \[[XDSTAR-88](https://gazelle.ihe.net/jira/browse/XDSTAR-88)\] Add a IHE validator for KOS dicom attributes
* \[[XDSTAR-95](https://gazelle.ihe.net/jira/browse/XDSTAR-95)\] RAD-55 WADO Retrieve

# 0.21
_Release date: 2012-11-21_


__Bug__

* \[[XDSTAR-92](https://gazelle.ihe.net/jira/browse/XDSTAR-92)\] bugs on testing
* \[[XDSTAR-93](https://gazelle.ihe.net/jira/browse/XDSTAR-93)\] Bug on the validator of xdsi RAD-68

# 0.20
_Release date: 2012-11-19_


__Story__

* \[[XDSTAR-84](https://gazelle.ihe.net/jira/browse/XDSTAR-84)\] Add transaction RAD-75
* \[[XDSTAR-90](https://gazelle.ihe.net/jira/browse/XDSTAR-90)\] RAD-69 TRansaction XCA-I domain, from DOC_IMG_CONS to INIt_IMG_GW
* \[[XDSTAR-91](https://gazelle.ihe.net/jira/browse/XDSTAR-91)\] RAD-69 TRansaction XCA-I domain, from RESP_IMG_GW to IMG_DOC_SRC

# 0.19
_Release date: 2012-11-15_


__Technical task__

* \[[XDSTAR-86](https://gazelle.ihe.net/jira/browse/XDSTAR-86)\] create validator for CDA XDS-SD RAD
* \[[XDSTAR-87](https://gazelle.ihe.net/jira/browse/XDSTAR-87)\] auto fulfil the metadatas related to the CDA document or to the DICOM KOS
* \[[XDSTAR-89](https://gazelle.ihe.net/jira/browse/XDSTAR-89)\] create validator for the RetrieveImaginDocumentSet request / response

__Story__

* \[[XDSTAR-26](https://gazelle.ihe.net/jira/browse/XDSTAR-26)\] Add replace  /transform document for ITI-41 (used especially for XDW)
* \[[XDSTAR-83](https://gazelle.ihe.net/jira/browse/XDSTAR-83)\] Add transaction RAD-69

# 0.18
_Release date: 2012-11-05_


__Technical task__

* \[[XDSTAR-72](https://gazelle.ihe.net/jira/browse/XDSTAR-72)\] documentation of generation of models from schemas
* \[[XDSTAR-73](https://gazelle.ihe.net/jira/browse/XDSTAR-73)\] documentation of OCL / dresden
* \[[XDSTAR-74](https://gazelle.ihe.net/jira/browse/XDSTAR-74)\] documentation of CDA models
* \[[XDSTAR-76](https://gazelle.ihe.net/jira/browse/XDSTAR-76)\] documentation of the generation of documentation
* \[[XDSTAR-77](https://gazelle.ihe.net/jira/browse/XDSTAR-77)\] documentation of the generation of editors
* \[[XDSTAR-78](https://gazelle.ihe.net/jira/browse/XDSTAR-78)\] documentation of the constraint-viewer module

__Bug__

* \[[XDSTAR-80](https://gazelle.ihe.net/jira/browse/XDSTAR-80)\] Fix problems on the transaction PHARM-1

__Story__

* \[[XDSTAR-81](https://gazelle.ihe.net/jira/browse/XDSTAR-81)\] Documentation about XDS-I.b
* \[[XDSTAR-85](https://gazelle.ihe.net/jira/browse/XDSTAR-85)\] add validator for RAD-68 metadatas (request and response)

# 0.17
_Release date: 2012-10-26_

__Bug__

* \[[XDSTAR-79](https://gazelle.ihe.net/jira/browse/XDSTAR-79)\] problem on the name of the domain PHARM (change PHARMA to PHARM) 

# 0.16
_Release date: 2012-10-24 00:00:00.0 _

__Story__

* \[[XDSTAR-55](https://gazelle.ihe.net/jira/browse/XDSTAR-55)\] add the constraint search of xds constraints
* \[[XDSTAR-66](https://gazelle.ihe.net/jira/browse/XDSTAR-66)\] Add simulator for the profile MPQ
* \[[XDSTAR-67](https://gazelle.ihe.net/jira/browse/XDSTAR-67)\] Add validator for the transaction ITI-51 (request and response of MPQ)

# 0.15
_Release date: 2012-10-19_


__Bug__

* \[[XDSTAR-60](https://gazelle.ihe.net/jira/browse/XDSTAR-60)\] merge to crowdin

__Story__

* \[[XDSTAR-61](https://gazelle.ihe.net/jira/browse/XDSTAR-61)\] Add transaction DispensationService:Discard()
* \[[XDSTAR-62](https://gazelle.ihe.net/jira/browse/XDSTAR-62)\] Add transaction ConsnetService:Discard()
* \[[XDSTAR-63](https://gazelle.ihe.net/jira/browse/XDSTAR-63)\] Add transaction Delete Document Set (ITI-62)
* \[[XDSTAR-64](https://gazelle.ihe.net/jira/browse/XDSTAR-64)\] Add validator for The transaction ITI-62

# 0.14
_Release date: 2012-10-18_

__Story__

* \[[XDSTAR-59](https://gazelle.ihe.net/jira/browse/XDSTAR-59)\] Add validator for CMPD - Metadatas

# 0.13
_Release date: 2012-10-16_


__Bug__

* \[[XDSTAR-54](https://gazelle.ihe.net/jira/browse/XDSTAR-54)\] Improve the GUI of Usages
* \[[XDSTAR-56](https://gazelle.ihe.net/jira/browse/XDSTAR-56)\] XDR requests cannot be parsed as DOM

__Story__

* \[[XDSTAR-58](https://gazelle.ihe.net/jira/browse/XDSTAR-58)\] Add validator for XCF messages

# 0.12
_Release date: No release date_


__Bug__

* \[[XDSTAR-50](https://gazelle.ihe.net/jira/browse/XDSTAR-50)\] Add XCF/IHE profile

# 0.11
_Release date: 2012-10-11_


__Bug__

* \[[XDSTAR-57](https://gazelle.ihe.net/jira/browse/XDSTAR-57)\] epSOS XCA simulator does not work

# 0.10
_Release date: 2012-10-09_


__Bug__

* \[[XDSTAR-49](https://gazelle.ihe.net/jira/browse/XDSTAR-49)\] Add transaction PHARMA-1
* \[[XDSTAR-51](https://gazelle.ihe.net/jira/browse/XDSTAR-51)\] Update the WS of XDStarClient to be conform to the modelBased ws

# 0.9
_Release date: No release date_


__Technical task__

* \[[XDSTAR-34](https://gazelle.ihe.net/jira/browse/XDSTAR-34)\] Add validation of the request before the send of the request
* \[[XDSTAR-38](https://gazelle.ihe.net/jira/browse/XDSTAR-38)\] Add documentation of the validator (by template)

__Bug__

* \[[XDSTAR-21](https://gazelle.ihe.net/jira/browse/XDSTAR-21)\] Integrate XCPD into XDStarClient

__Story__

* \[[XDSTAR-52](https://gazelle.ihe.net/jira/browse/XDSTAR-52)\] Add XCPD ITI-55 as an IHE simulator
* \[[XDSTAR-53](https://gazelle.ihe.net/jira/browse/XDSTAR-53)\] Add XCPD ITI-56 as a new transaction on IHE domain

# 0.8
_Release date: No release date_


__Bug__

* \[[XDSTAR-48](https://gazelle.ihe.net/jira/browse/XDSTAR-48)\] bug on the documentation of XDS

# 0.7
_Release date: No release date_


__Technical task__

* \[[XDSTAR-45](https://gazelle.ihe.net/jira/browse/XDSTAR-45)\] Fix a bug related to the use of TRC for RSQ / XGQ
* \[[XDSTAR-47](https://gazelle.ihe.net/jira/browse/XDSTAR-47)\] Add formatting of the response

# 0.6
_Release date: No release date_


__Story__

* \[[XDSTAR-4](https://gazelle.ihe.net/jira/browse/XDSTAR-4)\] Integration of XDSbSimulator to XDStarClient, with new metadata spec
* \[[XDSTAR-6](https://gazelle.ihe.net/jira/browse/XDSTAR-6)\] Integration of XDWSimulator to XDStarClient, with new metadata spec
* \[[XDSTAR-46](https://gazelle.ihe.net/jira/browse/XDSTAR-46)\] Add description of the response of adhocQuery on the GUI

# 0.5
_Release date: No release date_

__Technical task__

* \[[XDSTAR-43](https://gazelle.ihe.net/jira/browse/XDSTAR-43)\] Add validation of metadata for RSQ when there are metadatas on the response
* \[[XDSTAR-44](https://gazelle.ihe.net/jira/browse/XDSTAR-44)\] Fix a bug when validating XGQ message with the nist tool

__Bug__

* \[[XDSTAR-42](https://gazelle.ihe.net/jira/browse/XDSTAR-42)\] Error in the validation of the XDS data type XON  

__Story__

* \[[XDSTAR-19](https://gazelle.ihe.net/jira/browse/XDSTAR-19)\] Add validation of Registry Document Set-b on the webservice

# 0.3
_Release date: No release date_


__Bug__

* \[[XDSTAR-41](https://gazelle.ihe.net/jira/browse/XDSTAR-41)\] Problem on permissions of DispensationService, and ConsentService

# 0.2
_Release date: No release date_


__Technical task__

* \[[XDSTAR-30](https://gazelle.ihe.net/jira/browse/XDSTAR-30)\] Improve the result of validation of messages
* \[[XDSTAR-37](https://gazelle.ihe.net/jira/browse/XDSTAR-37)\] Add a button to add a new configuration on the simulator main page
* \[[XDSTAR-39](https://gazelle.ihe.net/jira/browse/XDSTAR-39)\] Add random fullFill of metadatas of PnR transaction
* \[[XDSTAR-40](https://gazelle.ihe.net/jira/browse/XDSTAR-40)\] Add the ability to select optional metadata for XDSFolder objects

# 0.1
_Release date: No release date_


__Technical task__

* \[[XDSTAR-29](https://gazelle.ihe.net/jira/browse/XDSTAR-29)\] Improve the footer (release note / documentation)
* \[[XDSTAR-32](https://gazelle.ihe.net/jira/browse/XDSTAR-32)\] Add validation result on the permanent link of messages transactions
* \[[XDSTAR-33](https://gazelle.ihe.net/jira/browse/XDSTAR-33)\] When clicking on the validation of the response, go directly to the response Validation
* \[[XDSTAR-35](https://gazelle.ihe.net/jira/browse/XDSTAR-35)\] Review of the xslt of result validation
* \[[XDSTAR-36](https://gazelle.ihe.net/jira/browse/XDSTAR-36)\] move the message report page to scope page

__Bug__

* \[[XDSTAR-31](https://gazelle.ihe.net/jira/browse/XDSTAR-31)\] Bug on the deletion of AdhocQuery

# 0.0.9
_Release date: No release date_


__Technical task__

* \[[XDSTAR-22](https://gazelle.ihe.net/jira/browse/XDSTAR-22)\] Improve retrieve Document Set
* \[[XDSTAR-24](https://gazelle.ihe.net/jira/browse/XDSTAR-24)\] Add remove of folder / document on ITI-41
* \[[XDSTAR-28](https://gazelle.ihe.net/jira/browse/XDSTAR-28)\] Improve Registry Stored Query

# 0.0.4
_Release date: 2012-05-11_


__Story__

* \[[XDSTAR-3](https://gazelle.ihe.net/jira/browse/XDSTAR-3)\] Integration of XDRSRCSimulator to XDStarClient, with new metadata spec
* \[[XDSTAR-5](https://gazelle.ihe.net/jira/browse/XDSTAR-5)\] Integration of XCAINITSimulator to XDStarClient, with new metadata spec
* \[[XDSTAR-7](https://gazelle.ihe.net/jira/browse/XDSTAR-7)\] Create a WS validator for XDSMetadata

__Improvement__

* \[[XDSTAR-18](https://gazelle.ihe.net/jira/browse/XDSTAR-18)\] add delete button for administrator to delete configurations

# 0.0.3
_Release date: 2012-05-07_

__Bug__

* \[[XDSTAR-17](https://gazelle.ihe.net/jira/browse/XDSTAR-17)\] problem when adding new registry configuration

# 0.0.2
_Release date: No release date_


__Bug__

* \[[XDSTAR-16](https://gazelle.ihe.net/jira/browse/XDSTAR-16)\] Problem when releasing the version 0.0.1 due to java heap space

# 0.0.1
_Release date: No release date_


__Bug__

* \[[XDSTAR-14](https://gazelle.ihe.net/jira/browse/XDSTAR-14)\] some problem when testing the module epSOS, DispensationService

__Story__

* \[[XDSTAR-1](https://gazelle.ihe.net/jira/browse/XDSTAR-1)\] creation of the project, the architecture of XDStarClient
* \[[XDSTAR-2](https://gazelle.ihe.net/jira/browse/XDSTAR-2)\] Add edition of metadata specifications
