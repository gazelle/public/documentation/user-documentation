---
title:  User Manual
subtitle: XDSMU Simulator
author: Youn Cadoret
function: Developer
releasedate: 28/06/2019
toolversion: 1.2.0
version: 1.02
status: Approved Document
reference: KER1-MAN-IHE-XDSMU_SIMULATOR_USER
customer: IHE-EUROPE
---


eHealthSuisse XDS Metadata Update MockUp
-----------------------------------------

ehealthSuisse XDS Metadata Update MockUp simulates a Document Recipient.

The XDS Metadata Update profile provides a capability to change selected metadata attributes of documents registered.

The actors involved in this profile are : Document Administrator and Document Registry/recipient.

*   The Document Administrator sends Update Document Set \[ITI-57\] request to the Responder specifying the patientID.
*   The Document Registry/recipient checks if the request is valid, if not, the transaction fails. If the request is valid, the Document Registry responds if the update has been done or not.
*   To build correctly \[ITI-57\] request, we need to retrieve the last good version of document registry. So first of all, it's important to execute Registry Stored Query \[ITI-18\] request; indeed this request will return the last version Approved of the document registry. And with this response we can build an adapted \[ITI-57\] request



### Data Set

#### Responder data


| PatientID | SourceID | UniqueID | HomeCommunityID |
|--------|:---:|-----:|------|
| CHPAM4489^^^CHPAM&1.3.6.1.4.1.12559.11.20.1&ISO       | 1.3.6.1.4.1.12559.11.13.2.5    | 1.3.6.1.4.1.12559.11.13.2.6.1218     | urn:oid:1.2.3.4.5.6.2.333.23     |
| CHPAM1416^^^CHPAM&1.3.6.1.4.1.12559.11.20.1&ISO     | 1.3.6.1.4.1.12559.11.13.2.5    | 1.3.6.1.4.1.12559.11.13.2.6.1229     | urn:oid:1.2.3.4.5.6.2.333.23     |
| CHPAM0456^^^CHPAM&1.3.6.1.4.1.12559.11.20.1&ISO     | 1.3.6.1.4.1.12559.11.13.2.5    | 1.3.6.1.4.1.12559.11.13.2.6.1232     | urn:oid:1.2.3.4.5.6.2.333.23     |
| CHPAM9127^^^CHPAM&1.3.6.1.4.1.12559.11.20.1&ISO     | 1.3.6.1.4.1.12559.11.13.2.5    | 1.3.6.1.4.1.12559.11.13.2.6.1111     | urn:oid:1.2.3.4.5.6.2.333.23     |
| CHPAM2987^^^CHPAM&1.3.6.1.4.1.12559.11.20.1&ISO     | 1.3.6.1.4.1.12559.11.13.2.5    | 1.3.6.1.4.1.12559.11.13.2.6.1112     | urn:oid:1.2.3.4.5.6.2.333.23     |
| CHPAM7647^^^CHPAM&1.3.6.1.4.1.12559.11.20.1&ISO     | 1.3.6.1.4.1.12559.11.13.2.5    | 1.3.6.1.4.1.12559.11.13.2.6.1113     | urn:oid:1.2.3.4.5.6.2.333.23     |
| CHPAM1945^^^CHPAM&1.3.6.1.4.1.12559.11.20.1&ISO     | 1.3.6.1.4.1.12559.11.13.2.5    | 1.3.6.1.4.1.12559.11.13.2.6.1114     | urn:oid:1.2.3.4.5.6.2.333.23     |
| CHPAM5763^^^CHPAM&1.3.6.1.4.1.12559.11.20.1&ISO     | 1.3.6.1.4.1.12559.11.13.2.5    | 1.3.6.1.4.1.12559.11.13.2.6.1115     | urn:oid:1.2.3.4.5.6.2.333.23     |
| CHPAM3946^^^CHPAM&1.3.6.1.4.1.12559.11.20.1&ISO     | 1.3.6.1.4.1.12559.11.13.2.5    | 1.3.6.1.4.1.12559.11.13.2.6.1116     | urn:oid:1.2.3.4.5.6.2.333.23     |
| 761337610435209810^^^SPID&2.16.756.5.30.1.127.3.10.3&ISO     | 1.3.6.1.4.1.12559.11.13.2.5    | 1.3.6.1.4.1.12559.11.13.2.6.1117     | urn:oid:1.2.3.4.5.6.2.333.23     |



### EndPoint

*   [RMU : http://ehealthsuisse.ihe-europe.net/restricted-metadata-update?wsdl](http://ehealthsuisse.ihe-europe.net/restricted-metadata-update?wsdl)
*   [ITI-57 : http://ehealthsuisse.ihe-europe.net/update-document-set?wsdl](http://ehealthsuisse.ihe-europe.net/update-document-set?wsdl)
*   [ITI-18 : http://ehealthsuisse.ihe-europe.net/registry-stored-query?wsdl](http://ehealthsuisse.ihe-europe.net/registry-stored-query?wsdl)
*   It requires TLS mutual authentication with testing certificate (from GSS PKI). [The wsdl can be browsed here](https://ehealthsuisse.ihe-europe.net:10443/xcmu-responding-gateway?wsdl)


### XDS Metadata Update \[ITI-57\] Request example

```xml
<soap12:Envelope xmlns:wsa="http://www.w3.org/2005/08/addressing" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
   <soap12:Header>
      <wsa:Action soap12:mustUnderstand="1">urn:ihe:iti:2010:UpdateDocumentSet</wsa:Action>
      <wsa:MessageID>urn:uuid:7f24a8b1-05f5-430f-8e2d-87b8ef91dcc0</wsa:MessageID>
      <wsa:ReplyTo>
         <wsa:Address>http://www.w3.org/2005/08/addressing/anonymous</wsa:Address>
      </wsa:ReplyTo>
      <wsa:To/>
      <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"/>
  </soap12:Header>
   <soap12:Body>

      <lcm:SubmitObjectsRequest xmlns:lcm="urn:oasis:names:tc:ebxml-regrep:xsd:lcm:3.0" xmlns:rim="urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0" xmlns:rs="urn:oasis:names:tc:ebxml-regrep:xsd:rs:3.0" xmlns:xdsb="urn:ihe:iti:xds-b:2007" xmlns:xop="http://www.w3.org/2004/08/xop/include">
         <rim:RegistryObjectList>

            <rim:RegistryPackage id="urn:uuid:fd22545f-d7f6-4ca1-8ff1-d8bae7364ac1" objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:RegistryPackage" home="urn:oid:1.2.3.4.5.6.2.333.23">
               <rim:Slot name="submissionTime">
                  <rim:ValueList>
                     <rim:Value>20180115120619</rim:Value>
                  </rim:ValueList>
               </rim:Slot>
               <rim:Name>
                  <rim:LocalizedString value="XDS Submission Set"/>
               </rim:Name>
               <rim:Classification classificationScheme="urn:uuid:aa543740-bdda-424e-8c96-df4873be8500" classifiedObject="urn:uuid:0216e18f-55d4-4593-b73d-3d23d2bab035" id="urn:uuid:05a39635-af04-4802-8c8a-7640dfde61da" nodeRepresentation="419891008" objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification">
                  <rim:Slot name="codingScheme">
                     <rim:ValueList>
                        <rim:Value>2.16.840.1.113883.6.96</rim:Value>
                     </rim:ValueList>
                  </rim:Slot>
                  <rim:Name>
                     <rim:LocalizedString value="Other Composition"/>
                  </rim:Name>
               </rim:Classification>
               <rim:Classification classificationNode="urn:uuid:a54d6aa5-d40d-43f9-88c5-b4633d873bdd" classifiedObject="urn:uuid:fd22545f-d7f6-4ca1-8ff1-d8bae7364ac1" id="urn:uuid:d2947dde-7346-437b-87d5-6f4812f68509" objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification"/>
               <rim:ExternalIdentifier id="urn:uuid:c8f14902-d156-43a4-8513-f3f67d2f5dce" identificationScheme="urn:uuid:554ac39e-e3fe-47fe-b233-965d2a147832" objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:ExternalIdentifier" registryObject="urn:uuid:fd22545f-d7f6-4ca1-8ff1-d8bae7364ac1" value="1.3.6.1.4.1.21367.2012.2.1.4">
                  <rim:Name>
                     <rim:LocalizedString value="XDSSubmissionSet.sourceId"/>
                  </rim:Name>
               </rim:ExternalIdentifier>
               <rim:ExternalIdentifier id="urn:uuid:f602bd54-7716-4318-9c69-70e65b4e458e" identificationScheme="urn:uuid:96fdda7c-d067-4183-912e-bf5ee74998a8" objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:ExternalIdentifier" registryObject="urn:uuid:fd22545f-d7f6-4ca1-8ff1-d8bae7364ac1" value="1.3.6.1.4.1.12559.11.1.2.2.1.1.4.101633">
                  <rim:Name>
                     <rim:LocalizedString value="XDSSubmissionSet.uniqueId"/>
                  </rim:Name>
               </rim:ExternalIdentifier>
               <rim:ExternalIdentifier id="c5a27d7a-069a-43c9-b3d5-495110b61575" identificationScheme="urn:uuid:6b5aea1a-874d-4603-a4bc-96a0a7b38446" objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:ExternalIdentifier" registryObject="urn:uuid:fd22545f-d7f6-4ca1-8ff1-d8bae7364ac1" value="CHPAM34^^^CHPAM&amp;1.3.6.1.4.1.12559.11.20.1&amp;ISO">
                  <rim:Name>
                     <rim:LocalizedString value="XDSSubmissionSet.patientId"/>
                  </rim:Name>
               </rim:ExternalIdentifier>
            </rim:RegistryPackage>
            <!--==== /Submission Part ====-->

            <!--==== Document Entry Part ====-->
            <rim:ExtrinsicObject id="urn:uuid:811da817-e516-4331-b9e2-f649086a5481" mimeType="text/xml" objectType="urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1" status="urn:oasis:names:tc:ebxml-regrep:StatusType:Approved" lid="urn:uuid:811da817-e516-4331-b9e2-f649086a5481" home="urn:oid:1.2.3.4.5.6.2.333.23">
               <rim:Slot name="repositoryUniqueId">
                  <rim:ValueList>
                     <rim:Value>1.3.6.1.4.1.12559.11.25.1.11.2</rim:Value>
                  </rim:ValueList>
               </rim:Slot>
               <rim:Slot name="hash">
                  <rim:ValueList>
                     <rim:Value>e564e98733d1ecdff0255fe0b50a33075940015c</rim:Value>
                  </rim:ValueList>
               </rim:Slot>
               <rim:Slot name="size">
                  <rim:ValueList>
                     <rim:Value>9995</rim:Value>
                  </rim:ValueList>
               </rim:Slot>
               <rim:Slot name="languageCode">
                  <rim:ValueList>
                     <rim:Value>en-us</rim:Value>
                  </rim:ValueList>
               </rim:Slot>
               <rim:Slot name="creationTime">
                  <rim:ValueList>
                     <rim:Value>20180712030836</rim:Value>
                  </rim:ValueList>
               </rim:Slot>
               <rim:Slot name="sourcePatientId">
                  <rim:ValueList>
                     <rim:Value>CHPAM34^^^CHPAM&amp;1.3.6.1.4.1.12559.11.20.1&amp;ISO</rim:Value>
                  </rim:ValueList>
               </rim:Slot>
               <rim:Name>
                  <rim:LocalizedString value="XDSDocument Entry 1"/>
               </rim:Name>
               <rim:VersionInfo versionName="1"/>
               <rim:Classification classificationScheme="urn:uuid:41a5887f-8865-4c09-adf7-e362475b143a" classifiedObject="urn:uuid:811da817-e516-4331-b9e2-f649086a5481" id="urn:uuid:6e9c5cff-a1db-4e62-93fd-a257fccc83d5" nodeRepresentation="1241000195103" objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification">
                  <rim:Slot name="codingScheme">
                     <rim:ValueList>
                        <rim:Value>2.16.840.1.113883.6.96</rim:Value>
                     </rim:ValueList>
                  </rim:Slot>
                  <rim:Name>
                     <rim:LocalizedString value="Note on Procedure (record artifact)"/>
                  </rim:Name>
                  <rim:VersionInfo versionName="1"/>
               </rim:Classification>
               <rim:Classification classificationScheme="urn:uuid:f4f85eac-e6cb-4883-b524-f2705394840f" classifiedObject="urn:uuid:811da817-e516-4331-b9e2-f649086a5481" id="urn:uuid:de59c2f8-75a2-4d1b-80ed-c491666b30b3" nodeRepresentation="1131000195104" objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification">
                  <rim:Slot name="codingScheme">
                     <rim:ValueList>
                        <rim:Value>2.16.840.1.113883.6.96</rim:Value>
                     </rim:ValueList>
                  </rim:Slot>
                  <rim:Name>
                     <rim:LocalizedString value="eingeschrenkt"/>
                  </rim:Name>
                  <rim:VersionInfo versionName="1"/>
               </rim:Classification>
               <rim:Classification classificationScheme="urn:uuid:a09d5840-386c-46f2-b5ad-9c3699a4309d" classifiedObject="urn:uuid:811da817-e516-4331-b9e2-f649086a5481" id="urn:uuid:32641cd9-b238-4d56-99ed-1a97d7c85aeb" nodeRepresentation="\*" objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification">
                  <rim:Slot name="codingScheme">
                     <rim:ValueList>
                        <rim:Value>2.16.756.5.30.1.127.77.10.5.2</rim:Value>
                     </rim:ValueList>
                  </rim:Slot>
                  <rim:Name>
                     <rim:LocalizedString value="\*"/>
                  </rim:Name>
                  <rim:VersionInfo versionName="1"/>
               </rim:Classification>
               <rim:Classification classificationScheme="urn:uuid:f33fb8ac-18af-42cc-ae0e-ed0b0bdb91e1" classifiedObject="urn:uuid:811da817-e516-4331-b9e2-f649086a5481" id="urn:uuid:0e941bd5-e24b-418f-b527-92e0db255fff" nodeRepresentation="394778007" objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification">
                  <rim:Slot name="codingScheme">
                     <rim:ValueList>
                        <rim:Value>2.16.840.1.113883.6.96</rim:Value>
                     </rim:ValueList>
                  </rim:Slot>
                  <rim:Name>
                     <rim:LocalizedString value="Client's or patient's home (environment)"/>
                  </rim:Name>
                  <rim:VersionInfo versionName="1"/>
               </rim:Classification>
               <rim:Classification classificationScheme="urn:uuid:cccf5598-8b07-4b77-a05e-ae952c785ead" classifiedObject="urn:uuid:811da817-e516-4331-b9e2-f649086a5481" id="urn:uuid:dc659cbe-b5b5-4235-a9b0-fbb9a2de2746" nodeRepresentation="8918198" objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification">
                  <rim:Slot name="codingScheme">
                     <rim:ValueList>
                        <rim:Value>2.16.840.1.113883.6.96</rim:Value>
                     </rim:ValueList>
                  </rim:Slot>
                  <rim:Name>
                     <rim:LocalizedString value="Community Medication Treatment Plan"/>
                  </rim:Name>
                  <rim:VersionInfo versionName="1"/>
               </rim:Classification>
               <rim:Classification classificationScheme="urn:uuid:f0306f51-975f-434e-a61c-c59651d33983" classifiedObject="urn:uuid:811da817-e516-4331-b9e2-f649086a5481" id="urn:uuid:b6e6d57d-eae0-4391-9146-90aabaff52f9" nodeRepresentation="721912009" objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification">
                  <rim:Slot name="codingScheme">
                     <rim:ValueList>
                        <rim:Value>2.16.840.1.113883.6.96</rim:Value>
                     </rim:ValueList>
                  </rim:Slot>
                  <rim:Name>
                     <rim:LocalizedString value="Medication summary document (record artifact)"/>
                  </rim:Name>
                  <rim:VersionInfo versionName="1"/>
               </rim:Classification>
               <rim:ExternalIdentifier id="urn:uuid:7d976a2b-8784-41fc-a939-af0b0d71cbcd" identificationScheme="urn:uuid:2e82c1f6-a085-4c72-9da3-8640a32e42ab" objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:ExternalIdentifier" registryObject="urn:uuid:811da817-e516-4331-b9e2-f649086a5481" value="1.3.6.1.4.1.12559.11.25.1.16.2.20171010072756850">
                  <rim:Name>
                     <rim:LocalizedString value="XDSDocumentEntry.uniqueId"/>
                  </rim:Name>
                  <rim:VersionInfo versionName="1"/>
               </rim:ExternalIdentifier>
               <rim:ExternalIdentifier id="urn:uuid:355feb7f-a3d2-41f5-9e4e-698ed3ffc4bd" identificationScheme="urn:uuid:58a6f841-87b3-4a3e-92fd-a8ffeff98427" objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:ExternalIdentifier" registryObject="urn:uuid:811da817-e516-4331-b9e2-f649086a5481" value="CHPAM34^^^CHPAM&amp;1.3.6.1.4.1.12559.11.20.1&amp;ISO">
                  <rim:Name>
                     <rim:LocalizedString value="XDSDocumentEntry.patientId"/>
                  </rim:Name>
                  <rim:VersionInfo versionName="1"/>
               </rim:ExternalIdentifier>
            </rim:ExtrinsicObject>
            <!--==== /Document Entry Part ====-->

            <rim:Association associationType="urn:oasis:names:tc:ebxml-regrep:AssociationType:HasMember" id="98359cee-59bb-4f9d-a199-151e933eeb66" sourceObject="urn:uuid:fd22545f-d7f6-4ca1-8ff1-d8bae7364ac1" targetObject="urn:uuid:e72a86d7-9d50-4b24-9c8c-2f57f6560ff8">
               <rim:Slot name="SubmissionSetStatus">
                  <rim:ValueList>
                     <rim:Value>Original</rim:Value>
                  </rim:ValueList>
               </rim:Slot>
               <rim:Slot name="PreviousVersion">
                  <rim:ValueList>
                     <rim:Value>1</rim:Value>
                  </rim:ValueList>
               </rim:Slot>
            </rim:Association>

         </rim:RegistryObjectList>
      </lcm:SubmitObjectsRequest>
     </soap12:Body>
    </soap12:Envelope>
```


### XDS Metadata Update \[ITI-57\] Success response example
```xml
<soap12:Envelope xmlns:soap12="http://www.w3.org/2003/05/soap-envelope" xmlns:wsa="http://www.w3.org/2005/08/addressing">
   <soap12:Header>
      <wsa:Action xmlns="http://www.w3.org/2005/08/addressing">urn:ihe:iti:2010:UpdateDocumentSetResponse</wsa:Action>
      <wsa:MessageID>urn:uuid:247d1b0a-477b-4539-ad61-18833df5b327</wsa:MessageID>
      <wsa:To>http://www.w3.org/2005/08/addressing/anonymous</wsa:To>
      <wsa:RelatesTo>urn:uuid:43182b89-24d6-43cf-bc86-da4d710650e0</wsa:RelatesTo>
   </soap12:Header>
   <soap12:Body>
      <rs:RegistryResponse status="urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success" xmlns:rs="urn:oasis:names:tc:ebxml-regrep:xsd:rs:3.0"/>
   </soap12:Body>
</soap12:Envelope>
```



### Restricted Update Document Set \[ITI-92\] Request example

```xml
<soap12:Envelope xmlns:wsa="http://www.w3.org/2005/08/addressing" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
   <soap12:Header>
      <wsa:Action soap12:mustUnderstand="1">urn:ihe:iti:2018:RestrictedUpdateDocumentSet</wsa:Action>
      <wsa:MessageID>urn:uuid:7f24a8b1-05f5-430f-8e2d-87b8ef91dcc0</wsa:MessageID>
      <wsa:ReplyTo>
         <wsa:Address>http://www.w3.org/2005/08/addressing/anonymous</wsa:Address>
      </wsa:ReplyTo>
      <wsa:To/>
      <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"/>
  </soap12:Header>
   <soap12:Body>

      <lcm:SubmitObjectsRequest xmlns:lcm="urn:oasis:names:tc:ebxml-regrep:xsd:lcm:3.0" xmlns:rim="urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0" xmlns:rs="urn:oasis:names:tc:ebxml-regrep:xsd:rs:3.0" xmlns:xdsb="urn:ihe:iti:xds-b:2007" xmlns:xop="http://www.w3.org/2004/08/xop/include">
         <rim:RegistryObjectList>

            <rim:RegistryPackage id="urn:uuid:fd22545f-d7f6-4ca1-8ff1-d8bae7364ac1" objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:RegistryPackage" home="urn:oid:1.2.3.4.5.6.2.333.23">
               <rim:Slot name="submissionTime">
                  <rim:ValueList>
                     <rim:Value>20180115120619</rim:Value>
                  </rim:ValueList>
               </rim:Slot>
               <rim:Name>
                  <rim:LocalizedString value="XDS Submission Set"/>
               </rim:Name>
               <rim:Classification classificationScheme="urn:uuid:aa543740-bdda-424e-8c96-df4873be8500" classifiedObject="urn:uuid:0216e18f-55d4-4593-b73d-3d23d2bab035" id="urn:uuid:05a39635-af04-4802-8c8a-7640dfde61da" nodeRepresentation="419891008" objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification">
                  <rim:Slot name="codingScheme">
                     <rim:ValueList>
                        <rim:Value>2.16.840.1.113883.6.96</rim:Value>
                     </rim:ValueList>
                  </rim:Slot>
                  <rim:Name>
                     <rim:LocalizedString value="Other Composition"/>
                  </rim:Name>
               </rim:Classification>
               <rim:Classification classificationNode="urn:uuid:a54d6aa5-d40d-43f9-88c5-b4633d873bdd" classifiedObject="urn:uuid:fd22545f-d7f6-4ca1-8ff1-d8bae7364ac1" id="urn:uuid:d2947dde-7346-437b-87d5-6f4812f68509" objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification"/>
               <rim:ExternalIdentifier id="urn:uuid:c8f14902-d156-43a4-8513-f3f67d2f5dce" identificationScheme="urn:uuid:554ac39e-e3fe-47fe-b233-965d2a147832" objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:ExternalIdentifier" registryObject="urn:uuid:fd22545f-d7f6-4ca1-8ff1-d8bae7364ac1" value="1.3.6.1.4.1.21367.2012.2.1.4">
                  <rim:Name>
                     <rim:LocalizedString value="XDSSubmissionSet.sourceId"/>
                  </rim:Name>
               </rim:ExternalIdentifier>
               <rim:ExternalIdentifier id="urn:uuid:f602bd54-7716-4318-9c69-70e65b4e458e" identificationScheme="urn:uuid:96fdda7c-d067-4183-912e-bf5ee74998a8" objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:ExternalIdentifier" registryObject="urn:uuid:fd22545f-d7f6-4ca1-8ff1-d8bae7364ac1" value="1.3.6.1.4.1.12559.11.1.2.2.1.1.4.101633">
                  <rim:Name>
                     <rim:LocalizedString value="XDSSubmissionSet.uniqueId"/>
                  </rim:Name>
               </rim:ExternalIdentifier>
               <rim:ExternalIdentifier id="c5a27d7a-069a-43c9-b3d5-495110b61575" identificationScheme="urn:uuid:6b5aea1a-874d-4603-a4bc-96a0a7b38446" objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:ExternalIdentifier" registryObject="urn:uuid:fd22545f-d7f6-4ca1-8ff1-d8bae7364ac1" value="CHPAM34^^^CHPAM&amp;1.3.6.1.4.1.12559.11.20.1&amp;ISO">
                  <rim:Name>
                     <rim:LocalizedString value="XDSSubmissionSet.patientId"/>
                  </rim:Name>
               </rim:ExternalIdentifier>
            </rim:RegistryPackage>
            <!--==== /Submission Part ====-->

            <!--==== Document Entry Part ====-->
            <rim:ExtrinsicObject id="urn:uuid:811da817-e516-4331-b9e2-f649086a5481" mimeType="text/xml" objectType="urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1" status="urn:oasis:names:tc:ebxml-regrep:StatusType:Approved" lid="urn:uuid:811da817-e516-4331-b9e2-f649086a5481" home="urn:oid:1.2.3.4.5.6.2.333.23">
               <rim:Slot name="repositoryUniqueId">
                  <rim:ValueList>
                     <rim:Value>1.3.6.1.4.1.12559.11.25.1.11.2</rim:Value>
                  </rim:ValueList>
               </rim:Slot>
               <rim:Slot name="hash">
                  <rim:ValueList>
                     <rim:Value>e564e98733d1ecdff0255fe0b50a33075940015c</rim:Value>
                  </rim:ValueList>
               </rim:Slot>
               <rim:Slot name="size">
                  <rim:ValueList>
                     <rim:Value>9995</rim:Value>
                  </rim:ValueList>
               </rim:Slot>
               <rim:Slot name="languageCode">
                  <rim:ValueList>
                     <rim:Value>en-us</rim:Value>
                  </rim:ValueList>
               </rim:Slot>
               <rim:Slot name="creationTime">
                  <rim:ValueList>
                     <rim:Value>20180712030836</rim:Value>
                  </rim:ValueList>
               </rim:Slot>
               <rim:Slot name="sourcePatientId">
                  <rim:ValueList>
                     <rim:Value>CHPAM34^^^CHPAM&amp;1.3.6.1.4.1.12559.11.20.1&amp;ISO</rim:Value>
                  </rim:ValueList>
               </rim:Slot>
               <rim:Name>
                  <rim:LocalizedString value="XDSDocument Entry 1"/>
               </rim:Name>
               <rim:VersionInfo versionName="1"/>
               <rim:Classification classificationScheme="urn:uuid:41a5887f-8865-4c09-adf7-e362475b143a" classifiedObject="urn:uuid:811da817-e516-4331-b9e2-f649086a5481" id="urn:uuid:6e9c5cff-a1db-4e62-93fd-a257fccc83d5" nodeRepresentation="1241000195103" objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification">
                  <rim:Slot name="codingScheme">
                     <rim:ValueList>
                        <rim:Value>2.16.840.1.113883.6.96</rim:Value>
                     </rim:ValueList>
                  </rim:Slot>
                  <rim:Name>
                     <rim:LocalizedString value="Note on Procedure (record artifact)"/>
                  </rim:Name>
                  <rim:VersionInfo versionName="1"/>
               </rim:Classification>
               <rim:Classification classificationScheme="urn:uuid:f4f85eac-e6cb-4883-b524-f2705394840f" classifiedObject="urn:uuid:811da817-e516-4331-b9e2-f649086a5481" id="urn:uuid:de59c2f8-75a2-4d1b-80ed-c491666b30b3" nodeRepresentation="1131000195104" objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification">
                  <rim:Slot name="codingScheme">
                     <rim:ValueList>
                        <rim:Value>2.16.840.1.113883.6.96</rim:Value>
                     </rim:ValueList>
                  </rim:Slot>
                  <rim:Name>
                     <rim:LocalizedString value="eingeschrenkt"/>
                  </rim:Name>
                  <rim:VersionInfo versionName="1"/>
               </rim:Classification>
               <rim:Classification classificationScheme="urn:uuid:a09d5840-386c-46f2-b5ad-9c3699a4309d" classifiedObject="urn:uuid:811da817-e516-4331-b9e2-f649086a5481" id="urn:uuid:32641cd9-b238-4d56-99ed-1a97d7c85aeb" nodeRepresentation="\*" objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification">
                  <rim:Slot name="codingScheme">
                     <rim:ValueList>
                        <rim:Value>2.16.756.5.30.1.127.77.10.5.2</rim:Value>
                     </rim:ValueList>
                  </rim:Slot>
                  <rim:Name>
                     <rim:LocalizedString value="\*"/>
                  </rim:Name>
                  <rim:VersionInfo versionName="1"/>
               </rim:Classification>
               <rim:Classification classificationScheme="urn:uuid:f33fb8ac-18af-42cc-ae0e-ed0b0bdb91e1" classifiedObject="urn:uuid:811da817-e516-4331-b9e2-f649086a5481" id="urn:uuid:0e941bd5-e24b-418f-b527-92e0db255fff" nodeRepresentation="394778007" objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification">
                  <rim:Slot name="codingScheme">
                     <rim:ValueList>
                        <rim:Value>2.16.840.1.113883.6.96</rim:Value>
                     </rim:ValueList>
                  </rim:Slot>
                  <rim:Name>
                     <rim:LocalizedString value="Client's or patient's home (environment)"/>
                  </rim:Name>
                  <rim:VersionInfo versionName="1"/>
               </rim:Classification>
               <rim:Classification classificationScheme="urn:uuid:cccf5598-8b07-4b77-a05e-ae952c785ead" classifiedObject="urn:uuid:811da817-e516-4331-b9e2-f649086a5481" id="urn:uuid:dc659cbe-b5b5-4235-a9b0-fbb9a2de2746" nodeRepresentation="8918198" objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification">
                  <rim:Slot name="codingScheme">
                     <rim:ValueList>
                        <rim:Value>2.16.840.1.113883.6.96</rim:Value>
                     </rim:ValueList>
                  </rim:Slot>
                  <rim:Name>
                     <rim:LocalizedString value="Community Medication Treatment Plan"/>
                  </rim:Name>
                  <rim:VersionInfo versionName="1"/>
               </rim:Classification>
               <rim:Classification classificationScheme="urn:uuid:f0306f51-975f-434e-a61c-c59651d33983" classifiedObject="urn:uuid:811da817-e516-4331-b9e2-f649086a5481" id="urn:uuid:b6e6d57d-eae0-4391-9146-90aabaff52f9" nodeRepresentation="721912009" objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification">
                  <rim:Slot name="codingScheme">
                     <rim:ValueList>
                        <rim:Value>2.16.840.1.113883.6.96</rim:Value>
                     </rim:ValueList>
                  </rim:Slot>
                  <rim:Name>
                     <rim:LocalizedString value="Medication summary document (record artifact)"/>
                  </rim:Name>
                  <rim:VersionInfo versionName="1"/>
               </rim:Classification>
               <rim:ExternalIdentifier id="urn:uuid:7d976a2b-8784-41fc-a939-af0b0d71cbcd" identificationScheme="urn:uuid:2e82c1f6-a085-4c72-9da3-8640a32e42ab" objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:ExternalIdentifier" registryObject="urn:uuid:811da817-e516-4331-b9e2-f649086a5481" value="1.3.6.1.4.1.12559.11.25.1.16.2.20171010072756850">
                  <rim:Name>
                     <rim:LocalizedString value="XDSDocumentEntry.uniqueId"/>
                  </rim:Name>
                  <rim:VersionInfo versionName="1"/>
               </rim:ExternalIdentifier>
               <rim:ExternalIdentifier id="urn:uuid:355feb7f-a3d2-41f5-9e4e-698ed3ffc4bd" identificationScheme="urn:uuid:58a6f841-87b3-4a3e-92fd-a8ffeff98427" objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:ExternalIdentifier" registryObject="urn:uuid:811da817-e516-4331-b9e2-f649086a5481" value="CHPAM34^^^CHPAM&amp;1.3.6.1.4.1.12559.11.20.1&amp;ISO">
                  <rim:Name>
                     <rim:LocalizedString value="XDSDocumentEntry.patientId"/>
                  </rim:Name>
                  <rim:VersionInfo versionName="1"/>
               </rim:ExternalIdentifier>
            </rim:ExtrinsicObject>
            <!--==== /Document Entry Part ====-->

            <rim:Association associationType="urn:oasis:names:tc:ebxml-regrep:AssociationType:HasMember" id="98359cee-59bb-4f9d-a199-151e933eeb66" sourceObject="urn:uuid:fd22545f-d7f6-4ca1-8ff1-d8bae7364ac1" targetObject="urn:uuid:e72a86d7-9d50-4b24-9c8c-2f57f6560ff8">
               <rim:Slot name="SubmissionSetStatus">
                  <rim:ValueList>
                     <rim:Value>Original</rim:Value>
                  </rim:ValueList>
               </rim:Slot>
               <rim:Slot name="PreviousVersion">
                  <rim:ValueList>
                     <rim:Value>1</rim:Value>
                  </rim:ValueList>
               </rim:Slot>
            </rim:Association>

         </rim:RegistryObjectList>
      </lcm:SubmitObjectsRequest>
     </soap12:Body>
    </soap12:Envelope>
```


### Restricted Update Document Set \[ITI-92\] Success response example
```xml
<soap12:Envelope xmlns:soap12="http://www.w3.org/2003/05/soap-envelope" xmlns:wsa="http://www.w3.org/2005/08/addressing">
   <soap12:Header>
      <wsa:Action xmlns="http://www.w3.org/2005/08/addressing">urn:ihe:iti:2018:RestrictedUpdateDocumentSetResponse</wsa:Action>
      <wsa:MessageID>urn:uuid:247d1b0a-477b-4539-ad61-18833df5b327</wsa:MessageID>
      <wsa:To>http://www.w3.org/2005/08/addressing/anonymous</wsa:To>
      <wsa:RelatesTo>urn:uuid:43182b89-24d6-43cf-bc86-da4d710650e0</wsa:RelatesTo>
   </soap12:Header>
   <soap12:Body>
      <rs:RegistryResponse status="urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success" xmlns:rs="urn:oasis:names:tc:ebxml-regrep:xsd:rs:3.0"/>
   </soap12:Body>
</soap12:Envelope>
```


### Restricted Update Document Set \[ITI-92\] Failed response example
```xml
<soap12:Envelope xmlns:soap12="http://www.w3.org/2003/05/soap-envelope" xmlns:wsa="http://www.w3.org/2005/08/addressing">
   <soap12:Header>
      <wsa:Action>urn:ihe:iti:2018:RestrictedUpdateDocumentSetResponse</wsa:Action>
      <wsa:MessageID>urn:uuid:986c0b7a-7fe4-467e-886d-46993ab7fcea</wsa:MessageID>
      <wsa:To>http://www.w3.org/2005/08/addressing/anonymous</wsa:To>
      <wsa:RelatesTo xmlns="http://www.w3.org/2005/08/addressing">urn:uuid:ae6d7767-c766-475e-85fc-395270831912</wsa:RelatesTo>
   </soap12:Header>
   <soap12:Body>
      <rs:RegistryResponse status="urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure" xmlns:rs="urn:oasis:names:tc:ebxml-regrep:xsd:rs:3.0">
      	<rs:RegistryErrorList>
      		<rs:RegistryError severity="urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error" errorCode="XDSUnknownCommunity" codeContext="A value for the homeCommunityId is not recognize ()" />
      	</rs:RegistryErrorList>
      </rs:RegistryResponse>
   </soap12:Body>
</soap12:Envelope>
```


### Registry Stored Query \[ITI-18\] Request example
```xml
<s:Envelope xmlns:a="http://www.w3.org/2005/08/addressing" xmlns:s="http://www.w3.org/2003/05/soap-envelope">
   <s:Header>
      <a:Action s:mustUnderstand="1">urn:ihe:iti:2007:RegistryStoredQuery</a:Action>
      <a:MessageID>urn:uuid:3cce0135-cedb-4a26-ba00-8698ee8dde04</a:MessageID>
      <a:ReplyTo>
         <a:Address>http://www.w3.org/2005/08/addressing/anonymous</a:Address>
      </a:ReplyTo>
      <a:To s:mustUnderstand="1"/>
   </s:Header>
   <s:Body>
      <query:AdhocQueryRequest xmlns:lcm="urn:oasis:names:tc:ebxml-regrep:xsd:lcm:3.0"
 			       xmlns:query="urn:oasis:names:tc:ebxml-regrep:xsd:query:3.0"
			       xmlns:rim="urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0"
			       xmlns:rs="urn:oasis:names:tc:ebxml-regrep:xsd:rs:3.0"
			       xmlns:xdsb="urn:ihe:iti:xds-b:2007"
			       xmlns:xop="http://www.w3.org/2004/08/xop/include">
         <query:ResponseOption returnComposedObjects="true" returnType="LeafClass"/>
            <rim:AdhocQuery home="urn:oid:1.2.3.4.5.6.2.333.23" id="urn:uuid:14d4debf-8f97-4251-9a74-a90016b0af0d">
            <rim:Slot name="$XDSDocumentEntryPatientId">
               <rim:ValueList>
            <rim:Value>'CHPAM34^^^CHPAM&amp;1.3.6.1.4.1.12559.11.20.1&amp;ISO'</rim:Value>
               </rim:ValueList>
            </rim:Slot>
            <rim:Slot name="$XDSDocumentEntryStatus">
               <rim:ValueList>
                     <rim:Value>('urn:oasis:names:tc:ebxml-regrep:StatusType:Approved')</rim:Value>
               </rim:ValueList>
            </rim:Slot>
         </rim:AdhocQuery>
      </query:AdhocQueryRequest>
   </s:Body>
</s:Envelope>
```


### Registry Stored Query \[ITI-18\] Success response example
```xml
<S:Envelope xmlns:S="http://www.w3.org/2003/05/soap-envelope">
   <S:Header>
      <wsa:Action s:mustUnderstand="1" xmlns:s="http://www.w3.org/2003/05/soap-envelope" xmlns:wsa="http://www.w3.org/2005/08/addressing">urn:ihe:iti:2007:RegistryStoredQueryResponse</wsa:Action>
      <wsa:RelatesTo xmlns:wsa="http://www.w3.org/2005/08/addressing">urn:uuid:03630d05-2fe8-4832-9e6f-345e53d4338a</wsa:RelatesTo>
   </S:Header>
   <S:Body>
      <query:AdhocQueryResponse status="urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success"
                                    xmlns:query="urn:oasis:names:tc:ebxml-regrep:xsd:query:3.0">
         <rim:RegistryObjectList xmlns:rim="urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0">
            <rim:ExtrinsicObject id="urn:uuid:e72a86d7-9d50-4b24-9c8c-2f57f6560ff8"
                                    mimeType="text" objectType="urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1"
                                    status="urn:oasis:names:tc:ebxml-regrep:StatusType:Approved"
                                    lid="urn:uuid:e72a86d7-9d50-4b24-9c8c-2f57f6560ff8">
               <rim:Slot name="languageCode">
                  <rim:ValueList>
                     <rim:Value>en-us</rim:Value>
                  </rim:ValueList>
               </rim:Slot>
               <rim:Slot name="creationTime">
                  <rim:ValueList>
                     <rim:Value>20180115120556</rim:Value>
                  </rim:ValueList>
               </rim:Slot>
               <rim:Slot name="sourcePatientId">
                  <rim:ValueList>
                     <rim:Value>CHPAM34^^^CHPAM&amp;1.3.6.1.4.1.12559.11.20.1&amp;ISO</rim:Value>
                  </rim:ValueList>
               </rim:Slot>
               <rim:Name>
                  <rim:LocalizedString value="XDSDocument Entry 1"/>
               </rim:Name>
               <rim:VersionInfo versionName="1"/>
               <rim:Classification classificationScheme="urn:uuid:41a5887f-8865-4c09-adf7-e362475b143a"
                                        classifiedObject="urn:uuid:811da817-e516-4331-b9e2-f649086a5481"
                                        id="urn:uuid:6e9c5cff-a1db-4e62-93fd-a257fccc83d5"
                                        nodeRepresentation="\*"
                                        objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification">
                  <rim:Slot name="codingScheme">
                     <rim:ValueList>
                        <rim:Value>2.16.840.1.113883.6.96</rim:Value>
                     </rim:ValueList>
                  </rim:Slot>
                  <rim:Name>
                     <rim:LocalizedString value="\*"/>
                  </rim:Name>
                  <rim:VersionInfo versionName="1"/>
               </rim:Classification>
               <rim:Classification classificationScheme="urn:uuid:f4f85eac-e6cb-4883-b524-f2705394840f"
                                        classifiedObject="urn:uuid:811da817-e516-4331-b9e2-f649086a5481"
                                        id="urn:uuid:de59c2f8-75a2-4d1b-80ed-c491666b30b3"
                                        nodeRepresentation="1131000195104"
                                        objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification">
                  <rim:Slot name="codingScheme">
                     <rim:ValueList>
                        <rim:Value>2.16.840.1.113883.6.96</rim:Value>
                     </rim:ValueList>
                  </rim:Slot>
                  <rim:Name>
                     <rim:LocalizedString value="eingeschr?nkt"/>
                  </rim:Name>
                  <rim:VersionInfo versionName="1"/>
               </rim:Classification>
               <rim:Classification classificationScheme="urn:uuid:a09d5840-386c-46f2-b5ad-9c3699a4309d"
                                        classifiedObject="urn:uuid:811da817-e516-4331-b9e2-f649086a5481"
                                        id="urn:uuid:32641cd9-b238-4d56-99ed-1a97d7c85aeb"
                                        nodeRepresentation="urn:ihe:rad:1.2.840.10008.5.1.4.1.1.88.59"
                                        objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification">
                  <rim:Slot name="codingScheme">
                     <rim:ValueList>
                        <rim:Value>2.16.756.5.30.1.127.77.10.5.2</rim:Value>
                     </rim:ValueList>
                  </rim:Slot>
                  <rim:Name>
                     <rim:LocalizedString value="KOS Dokument"/>
                  </rim:Name>
                  <rim:VersionInfo versionName="1"/>
               </rim:Classification>
               <rim:Classification classificationScheme="urn:uuid:f33fb8ac-18af-42cc-ae0e-ed0b0bdb91e1"
                                        classifiedObject="urn:uuid:811da817-e516-4331-b9e2-f649086a5481"
                                        id="urn:uuid:0e941bd5-e24b-418f-b527-92e0db255fff"
                                        nodeRepresentation="394778007"
                                        objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification">
                  <rim:Slot name="codingScheme">
                     <rim:ValueList>
                        <rim:Value>2.16.840.1.113883.6.96</rim:Value>
                     </rim:ValueList>
                  </rim:Slot>
                  <rim:Name>
                     <rim:LocalizedString value="Client's or patient's home (environment)"/>
                  </rim:Name>
                  <rim:VersionInfo versionName="1"/>
               </rim:Classification>
               <rim:Classification classificationScheme="urn:uuid:cccf5598-8b07-4b77-a05e-ae952c785ead"
                                        classifiedObject="urn:uuid:811da817-e516-4331-b9e2-f649086a5481"
                                        id="urn:uuid:dc659cbe-b5b5-4235-a9b0-fbb9a2de2746"
                                        nodeRepresentation="394810000"
                                        objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification">
                  <rim:Slot name="codingScheme">
                     <rim:ValueList>
                        <rim:Value>2.16.840.1.113883.6.96</rim:Value>
                     </rim:ValueList>
                  </rim:Slot>
                  <rim:Name>
                     <rim:LocalizedString value="Rheumatology (qualifier value)"/>
                  </rim:Name>
                  <rim:VersionInfo versionName="1"/>
               </rim:Classification>
               <rim:Classification classificationScheme="urn:uuid:f0306f51-975f-434e-a61c-c59651d33983"
                                        classifiedObject="urn:uuid:811da817-e516-4331-b9e2-f649086a5481"
                                        id="urn:uuid:b6e6d57d-eae0-4391-9146-90aabaff52f9"
                                        nodeRepresentation="721912009"
                                        objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification">
                  <rim:Slot name="codingScheme">
                     <rim:ValueList>
                        <rim:Value>2.16.840.1.113883.6.96</rim:Value>
                     </rim:ValueList>
                  </rim:Slot>
                  <rim:Name>
                     <rim:LocalizedString value="Medication summary document (record artifact)"/>
                  </rim:Name>
                  <rim:VersionInfo versionName="1"/>
               </rim:Classification>
               <rim:ExternalIdentifier id="urn:uuid:b21fc0dd-3ad6-4bb5-95d5-b730ced3b984"
                                            identificationScheme="urn:uuid:2e82c1f6-a085-4c72-9da3-8640a32e42ab"
                                            objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:ExternalIdentifier"
                                            registryObject="urn:uuid:e72a86d7-9d50-4b24-9c8c-2f57f6560ff8"
                                            value="1.3.6.1.4.1.12559.11.1.2.2.1.1.4.101633">
                  <rim:Name>
                     <rim:LocalizedString value="XDSDocumentEntry.uniqueId"/>
                  </rim:Name>
               </rim:ExternalIdentifier>
               <rim:ExternalIdentifier id="2617f4ec-599b-41c1-9ffe-9039178d9eb5"
                                            identificationScheme="urn:uuid:58a6f841-87b3-4a3e-92fd-a8ffeff98427"
                                            objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:ExternalIdentifier"
                                            registryObject="urn:uuid:e72a86d7-9d50-4b24-9c8c-2f57f6560ff8"
                                            value="CHPAM34^^^CHPAM&amp;1.3.6.1.4.1.12559.11.20.1&amp;ISO">
                  <rim:Name>
                     <rim:LocalizedString value="XDSDocumentEntry.patientId"/>
                  </rim:Name>
               </rim:ExternalIdentifier>
            </rim:ExtrinsicObject>
         </rim:RegistryObjectList>
      </query:AdhocQueryResponse>
   </S:Body>
</S:Envelope>
```


### Registry Stored Query \[ITI-18\] Failed response example
```xml
<S:Envelope xmlns:S="http://www.w3.org/2003/05/soap-envelope">
   <S:Header>
      <wsa:Action s:mustUnderstand="1" xmlns:s="http://www.w3.org/2003/05/soap-envelope" xmlns:wsa="http://www.w3.org/2005/08/addressing">urn:ihe:iti:2007:RegistryStoredQueryResponse</wsa:Action>
      <wsa:RelatesTo xmlns:wsa="http://www.w3.org/2005/08/addressing">urn:uuid:88f37191-6659-494a-919e-2bd52f655303</wsa:RelatesTo>
   </S:Header>
   <S:Body>
      <query:AdhocQueryResponse status="urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failed"
                                    xmlns:query="urn:oasis:names:tc:ebxml-regrep:xsd:query:3.0">
         <Error>no documents with this PatientID 333^^^&amp;2.16.756.5.30.1.127.3.10.3&amp;ISO</Error>
      </query:AdhocQueryResponse>
   </S:Body>
</S:Envelope>
```

## Mock messages on GWT

Messages sent to the simulator can be found in Mock Messages feature of Gazelle Webservice Tester.
This feature is documented at : [https://ehealthsuisse.ihe-europe.net/gazelle-documentation/Gazelle-Webservice-Tester/user.html](https://ehealthsuisse.ihe-europe.net/gazelle-documentation/Gazelle-Webservice-Tester/user.html).

Messages exchanged with EPR-XDSMU-Simulator can be found by filtering with the actor CH:DOCUMENT_RECIPIENT.
