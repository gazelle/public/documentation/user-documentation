---
title:  Wildfly10
subtitle: Installation
author: Wylem BARS
date: 07/02/2019
toolversion:
function: Engineer
version: 0.01
status: /!\ Not validated /!\
reference: KER1-MAN-IHE-WILDFLY_10_INSTALLATION-0_01
customer: IHE-EUROPE
---
# Purpose

This page describes the prerequisite to the installation of Gazelle
applications. All the tools developed in the context of the Gazelle
testbed project are developed for JBoss (5.0.1-GA, 7.2.0.final, 8.0.0.Final or 10.0.0.Final) and
use a postgreSQL database.

We recommand to install the Gazelle tools in a Debian-like environment,
it's the environment running on IHE Europe servers so we know that it is
correctly working. Moreover, most of the installation and configuration
procedures are described for such an environment.

# PostgreSQL

We are currenlty using PostgreSQL 9.6 on most of our servers.

# Install a JVM

Our applications running on Wildfly10 are using java 8. Consider
installing
[openJDK](http://openjdk.java.net/install/ "http://openjdk.java.net/install/").

``` bash
echo 'deb http://ftp.de.debian.org/debian jessie-backports main' >> /etc/apt/sources.list
sudo apt-get update && sudo apt-get install openjdk-8-jdk
```

# Install Wildfly 10 application server

## Get package

from: [http://gazelle.ihe.net/wildfly10/wildfly-10.0.0.Final.zip](http://gazelle.ihe.net/wildfly10/wildfly-10.0.0.Final.zip "http://gazelle.ihe.net/wildfly10/wildfly-10.0.0.Final.zip")

``` bash
wget -nv -O /tmp/wildfly-10.0.0.Final.zip http://gazelle.ihe.net/wildfly10/wildfly-10.0.0.Final.zip
```

## Create init script

from: [http://gazelle.ihe.net/wildfly10/init.d_wildfly10](http://gazelle.ihe.net/wildfly10/init.d_wildfly10)

``` bash
wget -nv -O /tmp/init.d_wildfly10 https://gazelle.ihe.net/wildfly10/init.d_wildfly10
```

## Install jboss in the /usr/local folder

``` bash
cd /usr/local
sudo mv /tmp/wildfly-10.0.0.Final.zip .
sudo unzip ./wildfly-10.0.0.Final.zip
sudo ln -s wildfly-10.0.0.Final wildfly10
sudo rm -rf wildfly-10.0.0.Final.zip
sudo chown -R jboss:jboss-admin /usr/local/wildfly-10.0.0.Final
sudo chmod -R 755 /usr/local/wildfly-10.0.0.Final
sudo mkdir /var/log/wildfly10/
sudo chown -R jboss:jboss-admin /var/log/wildfly10/
sudo chmod -R g+w /var/log/wildfly10/
```

## Install the init script and make it start at system startup

``` bash
sudo mv /tmp/init.d_wildfly10 /etc/init.d/wildfly10
sudo chmod +x /etc/init.d/wildfly10
sudo chown root:root /etc/init.d/wildfly10
sudo update-rc.d wildfly10 defaults
```

## Update Wildfly 10 postgresql jdbc driver


1. Stop jboss and go to
```bash
sudo mkdir -p /usr/local/wildfly10/modules/system/layers/base/org/postgresql/main
cd /usr/local/wildfly10/modules/system/layers/base/org/postgresql/main
```

2. Download driver
```bash
sudo wget https://gazelle.ihe.net/wildfly10/postgresql-42.2.5.jar
sudo chown jboss:jboss-admin postgresql-42.2.5.jar
sudo chmod 775 postgresql-42.2.5.jar
```

3. Edit module.xml and update the version

```xml
<module xmlns="urn:jboss:module:1.1" name="org.postgresql">
     <resources>
         <resource-root path="postgresql-42.2.5.jar"/>
     </resources>
     <dependencies>
         <module name="javax.api"/>
         <module name="javax.transaction.api"/>
     </dependencies>
</module>
```

4. Clean up your jboss
```bash
sudo rm -rf /usr/local/wildfly10/standalone/tmp/
sudo rm -rf /usr/local/wildfly10/standalone/data/
```

5. Restart your jboss and deploy your ear


## Externalize datasources from tool projects

Some projects may have extracted datasources. In order to deploy those projects, you will need to include some special 
instructions in your jboss server configuration file :

Edit standalone.xml in /usr/local/YOUR_JBOSS_SERVER/standalone/configuration folder and update datasources :


```xml
<datasources>
    ...
    <datasource jndi-name="java:jboss/datasources/YOUR_TOOL_DATASOURCES" pool-name="YOUR_TOOL_DATASOURCES" enabled="true" use-java-context="true">
        <connection-url>jdbc:postgresql://localhost:5432/YOUR_TOOL_DB</connection-url>
        <driver>postgresql</driver>
        <security>
            <user-name>YOUR_USER_NAME</user-name>
            <password>YOUR_PASSWORD</password>
        </security>
    </datasource>
</datasources>
```

You may also need to add the driver to the standalone.xml file :

```xml
 <drivers>
    ...
    <driver name="postgresql" module="org.postgresql">
        <driver-class>org.postgresql.Driver</driver-class>
        <xa-datasource-class>org.postgresql.xa.PGXADataSource</xa-datasource-class>
    </driver>
    ...
</drivers>
```

**NOTE :** YOUR_TOOL_DATASOURCES can be found in TOOL-ds.xml file (/src/main/application/META-INF of your ear folder)
or in the tool user manual.

Finally, restart your Wildfly to take into account your config.
