---
title: Installation Manual (including rest api)
subtitle: EVS Client
author: Cédric Eoche-Duval
function: Software Engineer at IHE Gazelle Team
date: 2022/10/06
toolversion: 6.1.x
version: 2.01
status: Approved document
reference: KER1-MAN-IHE-EVS_CLIENT_INSTALLATION-2_01
customer: IHE-EUROPE
---

# Gazelle Token Service - Installation & Configuration

Gazelle Token Service is an open-source Java EE project used to calling various validation-services of e-Health standards.

![GazelleTokenApi architecture](./media/installation/UML_Global_GazelleTokenApi.png)


## Get Gazelle-Token-Service

### Build from sources

Sources of this project are available on the INRIA GitLab; sources are managed using git. Anonymous
access is available if you only want to retrieve the sources (read-only access). If you intend to
build the tool and to install it on your own server, we recommend you to use a tagged version.

```bash
git clone --branch "TAG_VERSION" https://gitlab.inria.fr/gazelle/applications/core/gazelle-token.git
```

To retrieve the current version of the tool, take a look into the release notes of the project in
[*Jira*](https://gazelle.ihe.net/jira/browse/EVSCLT#selectedTab=com.atlassian.jira.plugin.system.project%3Achangelog-panel)
.

The project needs JDK 11 and Maven to be compiled.

```shell
mvn clean install
```

Once the compilation is over, the deployable artifact can be found in `gazelle-token-service-ear/target/gazelle-token-service-ear.ear`.

### Or download the EAR from IHE artifact repository

Each version of EVSClient is published in
[IHE Gazelle Nexus repository](https://gazelle.ihe.net/nexus/index.html#nexus-search;gav~~gazelle-token-service-ear~~~).

`gazelle-token-service-ear-X.X.X.ear` is the artifact to download and deploy.


## Installation

### Pre-requisites

Gazelle Token Service requires **JDK 11**,  **WildFly 26.1.2.Final**+ and **PostgreSQL**
to be run. Please refere to
[General considerations for WildFly 26](https://gazelle.ihe.net/gazelle-documentation/General/wildfly26.html)
to install WildFly 26 and setup a compliant environment.

### Create and initialize the database

You still need to create the database and the user.
This initialization is done by the scripts in `gazelle-token-service-ear/src/main/resources/sql/`:
- `schema-1.0.0.sql`.

Note: Initial application preferences values _(application_url, cas_enabled, ...)_ should be provided as environment variables.
Default values could be found in `/opt/gazelle-token/preferences.properties`.</br>
If any of these variables is set, the corresponding preference in `preferences.properties` file will be overridden.

### Data source

Data sources _(configuration file that indicates how to connect to the
database)_ must be defined in the WildFly application server. More information about how to configure
data sources can be found here:
[WildFly datasources](https://gazelle.ihe.net/gazelle-documentation/General/wildfly26.html).

Gazelle Token will expect the JNDI datasource to be named: `java:jboss/datasources/GazelleTokenDS`.

A data source example and specific to Gazelle Token Service can be found in `gazelle-token-service-ear/src/main/datasource`
in the sources or in the archive `gazelle-token-service-ear-X.X.X-datasource.zip` that can be downloaded from
(https://gazelle.ihe.net/nexus/service/local/repositories/releases/content/net/ihe/gazelle/token/gazelle-token-service-ear/1.0.0/gazelle-token-service-ear-1.0.0-datasource.zip).

### File system
Files system are located in /opt/gazelle-token.
You should create the folder before launching the app.
```
sudo mkdir /opt/gazelle-token
```

### Single Sign On Installation

SSO installation is optional.

An instance of [Gazelle SSO](../Gazelle-SSO) must be deployed, the file `gazelle-token-service.properties`
shall be in `/opt/gazelle/cas` and must contain the following statements :

```properties
casServerUrlPrefix=https://<yourfqdn>/sso
casServerLoginUrl=https://<yourfqdn>/sso/login
casLogoutUrl=https://<yourfqdn>/sso/logout 
service=https://<yourfqdn>/gazelle-token-service
```

_The truststore path must be the path referenced in `/etc/init.d/wildfly26`, in the option `OPT_SSL` and `-Djavax.net.ssl.trustStore`._

## Deployment

Once you have retrieved the archive, copy it to your wildfly26
server in the `/usr/local/wildfly26/standalone/deployments` directory. The EAR copied in this folder MUST be
named `gazelle-token-service.ear`.

```bash
cp gazelle-token-service-ear-X.X.X.ear /usr/local/wildfly26/standalone/deployments/gazelle-token-service-ear.ear
sudo service wildfly26 start
```

If you did not change any default configuration, Gazelle Token Service should be accessible at
[http://localhost:8080/gazelle-token-service](http://localhost:8080/gazelle-token-service).


## Administration manual

The following elements of Gazelle Token Service can be administrated:

* The token life duration,

### Administration panel

Users with `admin\_role` role can access the admin panel section through the menu
**Administration**.

![img.png](media/installation/adminButton.png)

### _Important_

- By default, the lifetime of tokens is 365 days (maximum duration).
- Once the tokens have expired, it is not possible to reuse them.
- Each user will have to generate a token again to use the Gazelle services.
- After modification, only future tokens will have the lifespan recorded below.

To modify duration, enter a new value then click on "Modify".

![img.png](media/installation/duration.png)

### Authentication

There is no user registry in Gazelle Token Service. Gazelle Token Service is either configured to get
authentication token from a Single-Sign-On service (Apereo CAS) either configured to grant admin
rights based on IP address filtering.

#### CAS Configuration

When Central Authentication Service (CAS) is enabled, users identity and roles are provided by
[Gazelle SSO](../Gazelle-SSO).

To enable the CAS:

* Preference `cas_enabled` must be set to `true`,
* preference `ip_login` must be set to `false`,
* property file gathering SSO URLs information must be installed. See
  [Single-Sign-On Installation](#single-sign-on-installation).

#### IP Login

**WARNING, except on an isolated or secured local network, IP Login is a degraded and unsecure mode
and should only be limited to the installation or testing of EVSClient.**

If CAS is not available, there is a degraded mode using an IP address based login. If this mode is
activated, a click on the **login** button of the application menu bar will grant the role admin to
all users which have an IP address that matches the defined regular-expression.

To enable IP Login:

* Preference `cas_enabled` must be set to `false`,
* preference `ip_login` must be set to `true`,
* preference `ip_login_admin` must be defined with a regular-expression
    * `.*` any visitor can be granted admin
    * `192\.168\..*` any visitor from the network 192.168.*.* can be granted admin, etc.

