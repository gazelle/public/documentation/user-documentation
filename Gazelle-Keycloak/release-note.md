---
title: Release note
subtitle: Gazelle-Keycloak
toolversion: 18.0.0-1.2.4
releasedate: 2022-05-10
author: Gabriel Landais
function: Engineer
customer: IHE Europe
reference: KER1-RNO-IHE-KEYCLOAK
---

# 18.0.0-1.2.4

_Release date: 2022-05-10_

Initial release of Gazelle modules for Keycloak 18.0.0.

- gazelle-keycloak-cas-protocol : Allows to use Keycloak as a drop-in replacement for CAS
- gazelle-keycloak-theme : Provides a login form with IHE colors/logo
- gazelle-keycloak-provider : Allows to use Gazelle TM as a Keycloak User Federation

__Story__
* \[[SSO-15](https://gazelle.ihe.net/jira/browse/SSO-15)\] Keycloak Gazelle User Federation
* \[[SSO-16](https://gazelle.ihe.net/jira/browse/SSO-16)\] Rocket.chat configuration
* \[[SSO-17](https://gazelle.ihe.net/jira/browse/SSO-17)\] Keycloak as SSO for communication tools Docker image
* \[[SSO-18](https://gazelle.ihe.net/jira/browse/SSO-18)\] Keycloak installation documentation
* \[[SSO-19](https://gazelle.ihe.net/jira/browse/SSO-19)\] Keycloak Gazelle theme
* \[[SSO-22](https://gazelle.ihe.net/jira/browse/SSO-22)\] Handle lower/upper cases during Keycloak login
* \[[SSO-23](https://gazelle.ihe.net/jira/browse/SSO-23)\] Handle Keycloak CAS logout with session
* \[[SSO-25](https://gazelle.ihe.net/jira/browse/SSO-25)\] Replace Keycloak with CAS for legacy applications
* \[[SSO-26](https://gazelle.ihe.net/jira/browse/SSO-26)\] Provide guidelines/tooling for realm import/export

__Task__
* \[[SSO-21](https://gazelle.ihe.net/jira/browse/SSO-21)\] Release
* \[[SSO-28](https://gazelle.ihe.net/jira/browse/SSO-28)\] Documentation
