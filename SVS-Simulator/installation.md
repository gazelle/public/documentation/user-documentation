---
title:  Installation Manual
subtitle: SVS Simulator
author: Anne-Gaëlle Bergé
date: 06/02/2024
toolversion: 3.X.X
function: Quality Engineer
version: 2.02
status: Approved document
reference: KER1-MAN-IHE-SVS_SIMULATOR_INSTALLATION-2._01
customer: IHE-EUROPE
---

# SVSSimulator - Installation & Configuration

# Sources & binaries

To get the name of the latest release, visit the SVS Simulator project in [*JIRA*](https://gazelle.ihe.net/jira/browse/SVS) and consult **Releases** section at left.

A maven artifact is published in our Nexus repository each time we release the application. You can use it, but be aware that the link to the database is hardly expressed within the artifact so you will have to use the same database name, hosted in the server running Jboss, and with the same owner (and password).

To get the artifact on Nexus browse: [*SVSSimulator-ear*](https://gazelle.ihe.net/nexus/index.html#nexus-search;gav%7E%7ESVSSimulator-ear%7E%7E%7E) and download the latest version.

If you rather want to build the project by yourself, you must checkout the latest tag and package it. You may want to create a new profile to customize your build.

1-  Checkout the latest tag available on Inria’s forge: 

```bash
git clone https://gitlab.inria.fr/gazelle/public/core/svs-simulator.git
cd svs-simulator
git checkout <version>
```

2-  \[Optional\] Edit the pom.xml file and create a new profile

3-  Package the application: 

```
$>mvn [-P profile] clean package
```

4-  The EAR is available at SVSSimulator/SVSSimulator-ear/target/SVSSimulator.ear

# Database configuration

If you use the artifact available on Nexus or if you have not change this parameter in the pom.xml file, create a database named svs-simulator, owned by the user gazelle.

```
$>createdb -U gazelle -E UTF8 svs-simulator
```
 
# Deploy the application

Copy EAR to the deploy folder of JBoss (do not forget to change its name to SVSSimulator.ear) ans start Jboss:
 
```
$>sudo service jboss start
```

Wait until the application has been completely deployed and configure the database running the SQL script for set-up.

**WARNING** : From version 2.2.0, datasources have been extracted from the **ear**. The template file can be found in /src/main/application/datasource in the source or in the file SVSSimulator-X.X.X-datasource.zip from the nexus.
For more informations about how to manage that externalization, please refer to [general considerations for JBoss7](https://gazelle.ihe.net/gazelle-documentation/General/jboss7.html).

Datasource name : SVSSimulatorDS

Database name : svs-simulator

# Initialize the application

You first need to initialize the database with some data available in a SQL script. If you have checked out the project, the script is available in SVSSimulator-ear/src/main/sql/initial-import.sql

Otherwise, download it from Inria’s forge (See Sources section).

Before executing the script, open the file and checked the various preferences to be inserted in the app\_configuration table, especially the application\_url and other preferences relative to the user authentication (see Application configuration section).

Finally, execute the script to initialize the database: 

```
$>psql -U gazelle svs-simulator < initial-import.sql
```

To take those parameters into account, you need to restart either the whole Jboss:

```
$>sudo service jboss restart
```

Either only the application in the deploy folder of Jboss:

```
$>touch OrderManager.ear 
```

# Application configuration

In the Administration menu, you will find a sub-menu entitled "Application Configuration". The following preferences must be updated according to the configuration of your system. The table below summarizes the variables used by the SVS Simulator tool.

| Variable                         | Description                                                  | Default value                                                                                          |
|----------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------|
| application\_issue\_tracker\_url | The URL of the bug tracking system where to post issues                                                                                                         | https://gazelle.ihe.net/jira/browse/SVS                                                                 |
| application\_url                 | The URL used by any user to access the tool. The application needs it to build permanent links inside the tool                                                  | http://publicUrlOfJboss/SVSSimulator                                                                   |
| documentation\_url               | Where to find the user manual                                                                                                                                   | https://gazelle.ihe.net/content/svs-simulator                                                           |
| esvs\_xsd\_location              | URL of XSD schema ESVS-20100726.xsd                                                                                                                             | https://gazelle.ihe.net/xsd/svs/ESVS-20100726.xsd                                                       |
| ignore\_validation\_in\_import   | disable the XSD validation when importing value sets from XML files                                                                                             | false                                                                                                  |
| link\_repository\_http           | Endpoint to contact the SVS Repository - displayed to the user (HTTP binding)                                                                                   | https://gazelle.ihe.net/                                                                                |
| link\_repository\_soap           | Endpoint to contact the SVS Repository - displayed to the user (SOAP binding)                                                                                   | https://gazelle.ihe.net/SVSSimulator-ejb/ValueSetRepository\_Service/ValueSetRepository\_PortType?wsdl |
| message\_permanent\_link         | Page which displays message details                                                                                                                             | https://gazelle.ihe.net/SVSSimulator/messages/messageDisplay.seam?id=                                  |
| result\_xsl\_location            | URL to access the XML stylesheet used to display HL7v2.x validation results                                                                                     | [*XSL location*](https://gazelle-vm/xsl/svsDetailedResult.xsl)                                         |
| svs\_repository\_url             | URL of the Sharing Value Set Repository actor of the SVSSimulator                                                                                               | https://gazelle.ihe.net                                                                                 |
| svs\_xsd\_location               | URL of XSD schema SVS.xsd                                                                                                                                       |  https://gazelle.ihe.net/xsd/svs/SVS.xsd                                                                |

# SSO Configuration

There are additional preferences to configure the SSO authentication.

| Preference name      | Description                                                             | Example of value |
| ----------------     | ----------------------------------------------------------------------  | ---------------- |
| **cas_enabled**      | Enable or disable the CAS authentication.                               | true             |
| **ip_login**         | Enable authentication by IP address matching `ip_login_admin` regex.    | false            |
| **ip_login_admin**   | Regex to authorize ip authentication if CAS authentication is disabled. |  .*              |

For more documentation about SSO configurations, follow the link [here](https://gitlab.inria.fr/gazelle/public/framework/sso-client-v7/-/blob/master/cas-client-v7/README.md).

# Home page

The first time you access the application, you may notice that the home page of the tool is not configured. To set a title and a welcome message, log into the application with admin rights.

Note that you will have to set up this page for all the languages supported by the application.
