---
subtitle:  XDW Simulator
title: Installation Manual
author: Malo TOUDIC
date: 23/08/2016
toolversion: 2.1.0
function: Software Developer
version: 0.01
status: Draft
reference: KER1-MAN-IHE-XDW_SIMULATOR_INSTALL-1_02
customer: IHE-EUROPE
---

# Sources & binaries

To get the name of the latest release, visit the XDW Simulator project in [*JIRA*](https://gazelle.ihe.net/jira/browse/XDW) and consult the Relese secion.

A maven artifact is published in our Nexus repository each time we release the application. You can use it, but be aware that the link to the database is hardly coded within the artifact so you will have to use the same database name, owner (and password).

To get the artifact on Nexus browse: [*https://gazelle.ihe.net/nexus/index.html#nexus-search;gav\~\~XDWSimulator-ear\~\~\~*](https://gazelle.ihe.net/nexus/index.html#nexus-search;gav%7E%7EXDWSimulator-ear%7E%7E%7E) and download the latest version.

If you rather want to build the project by yourself, you must checkout the latest tag and package it. You may want to create a new Maven profile to customize your build.

Checkout the latest tag available on Inria’s gitlab:

```bash
git clone https://gitlab.inria.fr/gazelle/public/simulation/xdw-simulator
```

1.  \[Optional\] Edit the pom.xml file and create a new profile

2.  Package the application: `mvn \[-P profile\] clean package`

3.  The EAR is available at XDWSimulator/XDWSimulator-ear/target/XDWSimulator.ear

# Jboss configuration

Update your standalone.xml like below (The messaging extension can be already configured, in this case you can skip this section)

in \<extensions\> add:

```xml
<extensions>
...
<extension module="org.jboss.as.messaging"/>
...
</extensions>
```

You also need to declare an addition subsystem to manage the messaging feature:

```xml
 <subsystem xmlns="urn:jboss:domain:messaging:1.3">
            <hornetq-server>
                <persistence-enabled>true</persistence-enabled>
                <security-enabled>false</security-enabled>
                <journal-file-size>102400</journal-file-size>
                <journal-min-files>2</journal-min-files>

                <connectors>
                    <netty-connector name="netty" socket-binding="messaging"/>
                    <netty-connector name="netty-throughput" socket-binding="messaging-throughput">
                        <param key="batch-delay" value="50"/>
                    </netty-connector>
                    <in-vm-connector name="in-vm" server-id="0"/>
                </connectors>

                <acceptors>
                    <netty-acceptor name="netty" socket-binding="messaging"/>
                    <netty-acceptor name="netty-throughput" socket-binding="messaging-throughput">
                        <param key="batch-delay" value="50"/>
                        <param key="direct-deliver" value="false"/>
                    </netty-acceptor>
                    <in-vm-acceptor name="in-vm" server-id="0"/>
                </acceptors>

                <security-settings>
                    <security-setting match="#">
                        <permission type="send" roles="guest"/>
                        <permission type="consume" roles="guest"/>
                        <permission type="createNonDurableQueue" roles="guest"/>
                        <permission type="deleteNonDurableQueue" roles="guest"/>
                    </security-setting>
                </security-settings>

                <address-settings>
                    <address-setting match="#">
                        <dead-letter-address>jms.queue.DLQ</dead-letter-address>
                        <expiry-address>jms.queue.ExpiryQueue</expiry-address>
                        <redelivery-delay>0</redelivery-delay>
                        <max-size-bytes>10485760</max-size-bytes>
                        <address-full-policy>BLOCK</address-full-policy>
                        <message-counter-history-day-limit>10</message-counter-history-day-limit>
                    </address-setting>
                </address-settings>

                <jms-connection-factories>
                    <connection-factory name="InVmConnectionFactory">
                        <connectors>
                            <connector-ref connector-name="in-vm"/>
                        </connectors>
                        <entries>
                            <entry name="java:/ConnectionFactory"/>
                        </entries>
                    </connection-factory>
                    <connection-factory name="RemoteConnectionFactory">
                        <connectors>
                            <connector-ref connector-name="netty"/>
                        </connectors>
                        <entries>
                            <entry name="java:jboss/exported/jms/RemoteConnectionFactory"/>
                        </entries>
                    </connection-factory>
                    <pooled-connection-factory name="hornetq-ra">
                        <transaction mode="xa"/>
                        <connectors>
                            <connector-ref connector-name="in-vm"/>
                        </connectors>
                        <entries>
                            <entry name="java:/JmsXA"/>
                        </entries>
                    </pooled-connection-factory>
                </jms-connection-factories>
              </hornetq-server>
</subsystem>
```  

And finally, add the following to the <socket-binding-group>

```xml
        <socket-binding name="messaging" port="5445"/>
        <socket-binding name="messaging-group" port="0" multicast-address="${jboss.messaging.group.address:231.7.7.7}" multicast-port="${jboss.messaging.group.port:9876}"/>
        <socket-binding name="messaging-throughput" port="5455"/>
```

# Deploy the application
## Database configuration

If you use the artifact available on Nexus or if you have not change this parameter in the pom.xml file, create a database named **xdw-simulator**, owned by the user gazelle.

```bash
createdb -U gazelle -E UTF8 xdw-simulator
```

## JBoss deployment
1.  Copy EAR to the deployment folder of JBoss7 (do not forget to change its name to XDWSimulator.ear)

2.  Start Jboss

3.  sudo service jboss7 start

4.  Wait until the application has been completly deployed and configure the database running.

5. **WARNING** : From version 2.1.0, datasources have been extracted from the **ear**. The template file can be found in /src/main/application/datasource in the source or in the file XDWSimulator-X.X.X-datasource.zip from the nexus.
For more informations about how to manage that externalization, please refer to [general considerations for JBoss7](https://gazelle.ihe.net/gazelle-documentation/General/jboss7.html).
Moreover, it is needed to add the attribute 
```<transaction-isolation>TRANSACTION_READ_COMMITTED</transaction-isolation> ``` to the datasource in order to get XDWSimulator to start.

Datasource name : XDWSimulatorDS

Database name : xdw-simulator


## Initialize the application

You first need to initialize the database with some data available in a SQL script. If you have checked out the project, the script is available in XDWSimulator-ear/src/main/sql/import-prod.sql

Before executing the script, open the file and checked the various preferences to be inserted in the app\_configuration table, especially the application\_url and other preferences relative to the user authentication (see Application configuration section)

Finally, execute the script:
```bash
psql -U gazelle xdw-simulator < import-prod.sql
```

To take those parameters into account, you need to restart either the whole Jboss (```sudo service jboss restart```), either only the application (```touch XDWSimulator.ear``` in the deployment folder of Jboss)

## Application configuration
### Preferences

Use the Administration menu, you will find a sub-menu entitied "Configure application preferences". The following preferences must be updated according to the configuration of your system. The table below summarizes the variables used by the XDW Simulator tool.

| Variable                          | Description                                                                                                                                                     | Default value|
|--------------------------------------------------|-------------------------------------------|-------------------------------------------|
| application\_issue\_tracker\_url                  | URL of the helpdesk or project for XDW Simulator in your bug tracker tool         | [*Link to JIRA*](https://gazelle.ihe.net/jira/browse/XDW)|
| application\_release\_note\_url                  | URL to the release note in JIRA (or elsewhere)         | [*Link to JIRA*](https://gazelle.ihe.net/jira/browse/XDW)|
| application\_url                  | The URL used by any user to access the tool. The application needs it to build permanent links inside the tool| https://publicUrlOfJboss/XDWSimulator |
| contact\_email                  | The email address of the person to be contacted by the user in case help is needed         | -                                                                                                       |
| contact\_name                  | The name of the person to be contacted by the user in case help is needed         | -                                                                                                       |
| contact\_title                  | The title of the person to be contacted by the user in case help is needed         | -                                                                                                       |
| xdw_xsd | Path to the XDW xsd | /opt/XDWSimulator/xsd/XDW-2015-08-18.xsd |
| doc_path   | Location of the samples  saved in XDW Simulator                    |  /opt/XDWSimulator/samples/ |
| dot_path | | dot |
| evs_url                     | URL to EVS Client | [*EVSClient URL*](https://gazelle.ihe.net/EVSClient)|
| xdw_xslt_viewer | Location of the XSL file used to display the XDW files | [*XSL URL*] https://gazelle.ihe.net/xsl/XDW.xsl |

### SSO Configuration

There are additional preferences to configure the SSO authentication.

| Preference name      | Description                                                             | Example of value |
| ----------------     | ----------------------------------------------------------------------  | ---------------- |
| **cas_enabled**      | Enable or disable the CAS authentication.                               | true             |
| **ip_login**         | Enable authentication by IP address matching `ip_login_admin` regex.    | false            |
| **ip_login_admin**   | Regex to authorize ip authentication if CAS authentication is disabled. |  .*              |

For more documentation about SSO configurations, follow the link [here](https://gitlab.inria.fr/gazelle/public/framework/sso-client-v7/-/blob/master/cas-client-v7/README.md).

### Home page

The first time you access the application, you may notice that the home page of the tool is not configured. To set a title and a welcome message, log into the application with admin rights.

Note that you will have to set up this page for all the languages supported by the application.
