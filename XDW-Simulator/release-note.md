---
title: Release note
subtitle: XDW Simulator
toolversion: 3.0.0
releasedate: 2024-02-06
author: Anne-Gaëlle BERGE
function: Software Engineer
customer: IHE Europe
reference: KER1-RNO-IHE-XDWSimulator
---

# 3.0.0
_Release date: 2024-02-06_

Context : Gazelle User Management Renovation step 2

__Improvement__
* \[[XDW-129](https://gazelle.ihe.net/jira/browse/XDW-129)\] Integrate new sso-client-v7
* \[[XDW-130](https://gazelle.ihe.net/jira/browse/XDW-130)\] Remove username display in GUI

# 2.1.0
_Release date: 2019-03-15_

__Remarks__

Datasource management has changed, refers to the installation manual to correctly update your instance of the tool.

__Improvement__

* \[[XDW-127](https://gazelle.ihe.net/jira/browse/XDW-127)\] Extract datasource configuration
* \[[XDW-128](https://gazelle.ihe.net/jira/browse/XDW-128)\] Update SQL scripts archive

# 2.0.0
_Release date: 2018-12-18_

__Remarks__

From version 2.0.0, XDWSimulator shall be integrated with the new Gazelle SSO based on Apereo.


__Bug__

* \[[XDW-118](https://gazelle.ihe.net/jira/browse/XDW-118)\] When updating to gazelle-tools:3.0.25 or higher, fix dependencies to asm

__Improvement__

* \[[XDW-119](https://gazelle.ihe.net/jira/browse/XDW-119)\] Need to implement version module to get the application version with REST WS
* \[[XDW-122](https://gazelle.ihe.net/jira/browse/XDW-122)\] Use new CAS apereo
* \[[XDW-123](https://gazelle.ihe.net/jira/browse/XDW-123)\] Update postgresql driver to version 42.2.1-jre7
* \[[XDW-126](https://gazelle.ihe.net/jira/browse/XDW-126)\] Create datasource instead of modify jboss configuration

# 1.1.0
_Release date: 2016-06-03_


__Bug__

* \[[XDW-81](https://gazelle.ihe.net/jira/browse/XDW-81)\] The image in the render tab is not viewed in the viewMessage page
* \[[XDW-114](https://gazelle.ihe.net/jira/browse/XDW-114)\] Gazelle External Validation Service, Basic XDW model, has parameter not camel-cased, giving false positives

__Improvement__

* \[[XDW-115](https://gazelle.ihe.net/jira/browse/XDW-115)\] Migration of the style

# 1.0.1
_Release date: 2015-01-02_

__Technical task__

* \[[XDW-112](https://gazelle.ihe.net/jira/browse/XDW-112)\] Update the XDW sample for preCAT tests

__Bug__

* \[[XDW-70](https://gazelle.ihe.net/jira/browse/XDW-70)\] Bug in migration of XDWSimulator
* \[[XDW-107](https://gazelle.ihe.net/jira/browse/XDW-107)\] Fix the validation tool according to the CP in attachment

__Story__

* \[[XDW-69](https://gazelle.ihe.net/jira/browse/XDW-69)\] Initialize the tag for V5

# 1.0.0
_Release date: 2014-12-15_

__Technical task__

* \[[XDW-71](https://gazelle.ihe.net/jira/browse/XDW-71)\] page "View XDW-Doc", when you upload a document, the view button does not work
* \[[XDW-72](https://gazelle.ihe.net/jira/browse/XDW-72)\] The upload does not work in the page "Voir XDW-Doc"
* \[[XDW-73](https://gazelle.ihe.net/jira/browse/XDW-73)\] "XDW-Edition" : button validate does not work
* \[[XDW-74](https://gazelle.ihe.net/jira/browse/XDW-74)\] Problem when you edit using the editor of XDW document, when you modify an element the complete document is not updated
* \[[XDW-75](https://gazelle.ihe.net/jira/browse/XDW-75)\] Permanent link to sample saved does not work
* \[[XDW-76](https://gazelle.ihe.net/jira/browse/XDW-76)\] XDW Sample : the table contains sorted and unsorted
* \[[XDW-77](https://gazelle.ihe.net/jira/browse/XDW-77)\] XDW Templates Documentation: this page is not accessible
* \[[XDW-78](https://gazelle.ihe.net/jira/browse/XDW-78)\] Admin : Configuration: button set defualt http header does not work
* \[[XDW-79](https://gazelle.ihe.net/jira/browse/XDW-79)\] Admin: Constraint-Admin: delete of packages does not work
* \[[XDW-80](https://gazelle.ihe.net/jira/browse/XDW-80)\] The add button in /viewMessage is not in the right place
* \[[XDW-82](https://gazelle.ihe.net/jira/browse/XDW-82)\] bug on the delete of the filter
* \[[XDW-83](https://gazelle.ihe.net/jira/browse/XDW-83)\] Bug on the access to documentation html of Workflow definitions
* \[[XDW-101](https://gazelle.ihe.net/jira/browse/XDW-101)\] Bug on the home menu
* \[[XDW-102](https://gazelle.ihe.net/jira/browse/XDW-102)\] bug on the edition of enumerations
* \[[XDW-103](https://gazelle.ihe.net/jira/browse/XDW-103)\] bug on the tables of edition : the css
* \[[XDW-104](https://gazelle.ihe.net/jira/browse/XDW-104)\] Bug in viewing the home icon

__Bug__

* \[[XDW-65](https://gazelle.ihe.net/jira/browse/XDW-65)\] XDW simulator need to be updated allow setting of the CAS URL from the preferences.

__Story__

* \[[XDW-68](https://gazelle.ihe.net/jira/browse/XDW-68)\] Migration of XDWSimulator to V7

# 0.12
_Release date: 2014-11-25_


__Technical task__

* \[[XDW-66](https://gazelle.ihe.net/jira/browse/XDW-66)\] Update of XDW validator
* \[[XDW-67](https://gazelle.ihe.net/jira/browse/XDW-67)\] Update the sample XDW document

__Improvement__

* \[[XDW-64](https://gazelle.ihe.net/jira/browse/XDW-64)\] NA2015: XDW validator needs to be updated based on 2014 CPs

# 0.11
_Release date: 2014-01-28_


__Bug__

* \[[XDW-59](https://gazelle.ihe.net/jira/browse/XDW-59)\] The facesMessages are not well configured. There are no icon to indicate the kind of the message
* \[[XDW-62](https://gazelle.ihe.net/jira/browse/XDW-62)\] Update the eReferral validator with the last CP (of names)

__Story__

* \[[XDW-58](https://gazelle.ihe.net/jira/browse/XDW-58)\] Add statistics to Webservice validation

# 0.10
_Release date: 2013-09-03_


__Technical task__

* \[[XDW-54](https://gazelle.ihe.net/jira/browse/XDW-54)\] merge the XDW validator to simulator-common 2.5
* \[[XDW-55](https://gazelle.ihe.net/jira/browse/XDW-55)\] add admin configuration of the application preferences
* \[[XDW-60](https://gazelle.ihe.net/jira/browse/XDW-60)\] review the menu of XDWSimulator
* \[[XDW-61](https://gazelle.ihe.net/jira/browse/XDW-61)\] update the documentation of XDW model / XDW-WD

__Bug__

* \[[XDW-56](https://gazelle.ihe.net/jira/browse/XDW-56)\] Update the icon of XDWSimulator

__Story__

* \[[XDW-53](https://gazelle.ihe.net/jira/browse/XDW-53)\] Add security check as it was done for XDStarClient
* \[[XDW-57](https://gazelle.ihe.net/jira/browse/XDW-57)\] Add documentation of constraints as it was done for CDAGenerator

# 0.9
_Release date: 2013-06-06_


__Technical task__

* \[[XDW-38](https://gazelle.ihe.net/jira/browse/XDW-38)\] Add modifications done on the CP to the validator of XBeR WD
* \[[XDW-44](https://gazelle.ihe.net/jira/browse/XDW-44)\] Add constraint on part/@name attribute according to XBer supplement 

__Bug__

* \[[XDW-40](https://gazelle.ihe.net/jira/browse/XDW-40)\] Missing translation strings in the GUI
* \[[XDW-49](https://gazelle.ihe.net/jira/browse/XDW-49)\] The version in production do not show the internationalization. This is less than optimal and it is a bad image when presenting it to potential user.

__Story__

* \[[XDW-37](https://gazelle.ihe.net/jira/browse/XDW-37)\] Improve XBeR validator

# 0.6
_Release date: 2012-12-19_


__Story__

* \[[XDW-39](https://gazelle.ihe.net/jira/browse/XDW-39)\] Update the validator Basic XDW to the latest version, with the CP 646


# 0.5
_Release date: 2012-10-11_


__Bug__

* \[[XDW-32](https://gazelle.ihe.net/jira/browse/XDW-32)\] Add validator for XBer-WD and XTHM-WD

__Story__

* \[[XDW-36](https://gazelle.ihe.net/jira/browse/XDW-36)\] Add validator for CMPD-WD

# 0.3
_Release date: 2012-10-09_


__Bug__

* \[[XDW-35](https://gazelle.ihe.net/jira/browse/XDW-35)\] Problem on the validation of XSD

# 0.2
_Release date: 2012-10-09_


__Story__

* \[[XDW-34](https://gazelle.ihe.net/jira/browse/XDW-34)\] XDW - create validator for XBer - WD

# 0.1
_Release date: No release Date_


__Bug__

* \[[XDW-31](https://gazelle.ihe.net/jira/browse/XDW-31)\] Update XDW validator with the new schema

__Improvement__

* \[[XDW-33](https://gazelle.ihe.net/jira/browse/XDW-33)\] Improve the page of list of sample saved after has been edited by users

# 0.0.5
_Release date: 2012-04-30_


__Technical task__

* \[[XDW-8](https://gazelle.ihe.net/jira/browse/XDW-8)\] Write a documentation about technologies used on the the deployment of the validation tool
* \[[XDW-12](https://gazelle.ihe.net/jira/browse/XDW-12)\] Add validation of generated xdw document
* \[[XDW-23](https://gazelle.ihe.net/jira/browse/XDW-23)\] Add retrieve stored query for WD
* \[[XDW-24](https://gazelle.ihe.net/jira/browse/XDW-24)\] Add retrieve doc-set of XDW document
* \[[XDW-25](https://gazelle.ihe.net/jira/browse/XDW-25)\] Creation of the structure of XDSMetadata documents

__Bug__

* \[[XDW-26](https://gazelle.ihe.net/jira/browse/XDW-26)\] Bug on the xsl to view the result of validation of an XDW Document if the document is not well formatted
* \[[XDW-27](https://gazelle.ihe.net/jira/browse/XDW-27)\] Bug on the view of XDW Documnet by the xsl stylesheet, the input and output are out of date

__Story__

* \[[XDW-3](https://gazelle.ihe.net/jira/browse/XDW-3)\] Choose of technologies, deployment of a dynamic and evolutive standalone environment of validation of XDW
* \[[XDW-11](https://gazelle.ihe.net/jira/browse/XDW-11)\] Add IHM for the edition and generation of xdw document
* \[[XDW-14](https://gazelle.ihe.net/jira/browse/XDW-14)\] Write of testcase for XDW workflow
* \[[XDW-15](https://gazelle.ihe.net/jira/browse/XDW-15)\] Development of each actor participating on the workflow

# 0.0.3
_Release date: 2012-02-20_


__Story__

* \[[XDW-13](https://gazelle.ihe.net/jira/browse/XDW-13)\] Create an xsl document for standalone reading of xdw document

# 0.0.2
_Release date: 2012-02-06_

__Bug__

* \[[XDW-18](https://gazelle.ihe.net/jira/browse/XDW-18)\] Improvment of the GUI of XDW on EVSClient
* \[[XDW-19](https://gazelle.ihe.net/jira/browse/XDW-19)\] Test the efficacity of XDW Validator WS

# 0.0.1
_Release date: 2012-02-01_


__Technical task__

* \[[XDW-4](https://gazelle.ihe.net/jira/browse/XDW-4)\] Study of MDHT as a validation environment already used
* \[[XDW-5](https://gazelle.ihe.net/jira/browse/XDW-5)\] Study and search for adapted tools of modeling
* \[[XDW-6](https://gazelle.ihe.net/jira/browse/XDW-6)\] Write acceleo template for the generation of classes modeling XDW elements and attributes
* \[[XDW-7](https://gazelle.ihe.net/jira/browse/XDW-7)\] Write of the acceleo template for the validation of XDW Document using UML constraints system

__Bug__

* \[[XDW-16](https://gazelle.ihe.net/jira/browse/XDW-16)\] The webservice of validation contains the version of the project
* \[[XDW-17](https://gazelle.ihe.net/jira/browse/XDW-17)\] All webservice of sake don't work

__Story__

* \[[XDW-1](https://gazelle.ihe.net/jira/browse/XDW-1)\] Creation of Model Driven of XDW Document Xorkflow
* \[[XDW-2](https://gazelle.ihe.net/jira/browse/XDW-2)\] Documentation about XDW profile
* \[[XDW-9](https://gazelle.ihe.net/jira/browse/XDW-9)\] Write constraints of validation from the TF of XDW profile
* \[[XDW-10](https://gazelle.ihe.net/jira/browse/XDW-10)\] Add IHM validation service, and web service validation functionality