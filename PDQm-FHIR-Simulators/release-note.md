#---
#title: Release note
#subtitle: PDQm FHIR Simulator 
#toolversion: 1.0.0
#releasedate: 2024-06-25
#authors: Romuald DUBOURG
#function: Software Engineer
#customer: eHealthsuisse
#reference:
#---

# 1.0.0
_Release date: 2024-06-25_

__Story__
* \[[PDQM-5](https://gazelle.ihe.net/jira/browse/PDQM-5)\] [IHE ITI-78] Read a resource
* \[[PDQM-6](https://gazelle.ihe.net/jira/browse/PDQM-6)\] [CH ITI-78] Search a Resource
* \[[PDQM-7](https://gazelle.ihe.net/jira/browse/PDQM-7)\] Load resources at startup
* \[[PDQM-8](https://gazelle.ihe.net/jira/browse/PDQM-8)\] [CH ITI-78] Read a Resource
* \[[PDQM-9](https://gazelle.ihe.net/jira/browse/PDQM-9)\] [IHE ITI-78] Search resources
* \[[PDQM-10](https://gazelle.ihe.net/jira/browse/PDQM-10)\] Integrate HAPI Fhir with Quarkus

