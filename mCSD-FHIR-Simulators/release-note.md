#---
#title: Release note
#subtitle: mCSD FHIR Simulator 
#toolversion: 1.0.0
#releasedate: 2024-06-21
#authors: Achraf ACHKARI-BEGDOURI / Alexandre POCINHO
#function: Software Engineer
#customer: eHealthsuisse
#reference:
#---

# 1.0.0
_Release date: 2024-06-21_

__Story__
* \[[MCSD-6](https://gazelle.ihe.net/jira/browse/MCSD-6)\] [CH ITI-90] Read a Resource
* \[[MCSD-9](https://gazelle.ihe.net/jira/browse/MCSD-9)\] [CH ITI-90] Search a Resource
* \[[MCSD-15](https://gazelle.ihe.net/jira/browse/MCSD-15)\] Create Indexer and Searcher for resources
* \[[MCSD-17](https://gazelle.ihe.net/jira/browse/MCSD-17)\] Add interceptor for mcsd-simulators

__Task__
* \[[MCSD-8](https://gazelle.ihe.net/jira/browse/MCSD-8)\] Integrate HAPI Fhir with Quarkus
* \[[MCSD-10](https://gazelle.ihe.net/jira/browse/MCSD-10)\] Load resources at startup
