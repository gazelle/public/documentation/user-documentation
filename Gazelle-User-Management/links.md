---
title: Documentation
subtitle: Gazelle User Management
toolversion: 4.1.0
releasedate: 2025-02-07
author:
---

Gazelle User Management documentation is located
on [https://doc-ihe.kereval.cloud/gazelle-applications/v/gazelle-user-management/](https://doc-ihe.kereval.cloud/gazelle-applications/v/gazelle-user-management/).
