The JIRA template is used to allow the team to easily copy/paste the content of the release note in Jira in markdown
format.
When you have modified file releasenotes-html.vm, you have to copy it at
/home/jira50/jira7/atlassion/jira/atlassian-jira/WEB-INF/classes/templates/jira/project/releasenotes

and restart Jira (sudo service jira restart).