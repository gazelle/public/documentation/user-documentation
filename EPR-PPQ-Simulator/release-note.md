---
title:  Release note
subtitle: PPQ Simulator
author: Youn Cadoret
function: Developer
releasedate: 2019-12-31
toolversion: 1.3.5
reference: KER1-RNO-IHE-PPQ_SIMULATOR
customer: IHE-EUROPE
---

# 1.3.7
_Release date: 2023-02-03_

__Bogue__
* \[[PPQREPO-20](https://gazelle.ihe.net/jira/browse/PPQREPO-20)\] Wrong place of « Status » tag in PPQ Repo's' responses.

# 1.3.6
_Release date: 2022-12-07_

__Bogue__
* \[[PPQREPO-19](https://gazelle.ihe.net/jira/browse/PPQREPO-19)\] Change namespace in policyQuery Mock Failure response


# 1.3.5
_Release date: 2019-10-31_

__Bug__

* \[[PPQREPO-18](https://gazelle.ihe.net/jira/browse/PPQREPO-18)\] [PPQ] An error occurred when a HCP add policy for an other HCP


# 1.3.4
_Release date: 2019-10-09_

__Bug__

* \[[PPQREPO-17](https://gazelle.ihe.net/jira/browse/PPQREPO-17)\] Confidentiality Codes differ in Appendix 3/2.1 and published policy stack


# 1.3.3
_Release date: 2019-10-04_

__Bug__

* \[[PPQREPO-16](https://gazelle.ihe.net/jira/browse/PPQREPO-16)\] PPQ error response not valid 



# 1.3.2
_Release date: 2019-09-03_

__Bug__

* \[[PPQREPO-14](https://gazelle.ihe.net/jira/browse/PPQREPO-14)\] The validation of the message response for invalid case failed

# 1.3.1
_Release date: 2019-08-27_

__Bug__

* \[[PPQREPO-13](https://gazelle.ihe.net/jira/browse/PPQREPO-13)\] The script for record message response failed


# 1.3.0
_Release date: 2019-08-02_

__Story__

* \[[PPQREPO-12](https://gazelle.ihe.net/jira/browse/PPQREPO-12)\] [PPQ] Create groovy script to record mock transaction

# 1.2.0 (18657703)
_Release date: 2019-06-27_

__Remarks__

__Bug__

* \[[PPQREPO-10](https://gazelle.ihe.net/jira/browse/PPQREPO-10)\] ppq simulator returns incorrect date time



# 1.1 (#67135)
_Release date: 2019-03-25_

__Remarks__

__Bug__

* \[[PPQREPO-1](https://gazelle.ihe.net/jira/browse/PPQREPO-1)\] Invalid namespace of PolicySet in PPQ query response
* \[[PPQREPO-2](https://gazelle.ihe.net/jira/browse/PPQREPO-2)\] SOAP Action missmatch for PPQ_for_UpdatePolicy_Repository
* \[[PPQREPO-3](https://gazelle.ihe.net/jira/browse/PPQREPO-3)\] Wrong Response SOAP Action on PolicyQuery



# 1.0 (#66417)
_Release date: 2019-02-20_

__Remarks__

__Improvement__

* \[[PPQREPO-6](https://gazelle.ihe.net/jira/browse/PPQREPO-6)\] Update PPQ simulator [EPR 1.7] 

