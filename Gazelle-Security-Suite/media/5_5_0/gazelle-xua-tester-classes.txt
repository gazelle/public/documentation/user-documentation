@startuml
abstract SOAPHeaderPart
class Exception{
__private attributes__
String xpath
String newValue
}
enum ExceptionType{
REPLACE
REMOVE
}
Exception -- "1" ExceptionType
SOAPHeaderPart "1" -right- "*" Exception
class Assertion{
__private attributes__
String password
String username
String stsEndpoint
}
class SOAPHeaderElement{
__private attributes__
String namespaceUri
String localName
String value
boolean mustUnderstand
}
class Timestamp{
__private attributes__
Integer createdOffset
Integer duration
}
class Signature{
__private attributes__
String keystore
String keystorePassword
String privateKeyPassword
String alias
String signedElementLocalName
String signedElementNamespaceUri
}
SOAPHeaderPart <|-- Assertion
SOAPHeaderPart <|-- SOAPHeaderElement
SOAPHeaderPart <|-- Timestamp
SOAPHeaderPart <|-- Signature
class ServiceProviderTestCase{
__private attributes__
String keyword
Stirng description
String expectedResult
String lastModifier
Date lastChanged
}
ServiceProviderTestCase "1" -- "*" SOAPHeaderPart
class ServiceProviderTestInstance{
__private attributes__
String testedEndpoint
byte[] response
String testStatus
byte[] request
Date timestamp
String username
}
ServiceProviderTestInstance "*" -- "1" ServiceProviderTestCase
class AncillaryTransaction{
__private attributes__
String keyword
String name
String description
String pathToSoapBody
String soapAction
}
class MessageVariable{
String token
String defaultValue
String value
}
ServiceProviderTestInstance -- "1" AncillaryTransaction
AncillaryTransaction "1" -- "*" MessageVariable
@enduml
