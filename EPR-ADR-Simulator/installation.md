---
title:  Installation Manual
subtitle: ADR Simulator
author: Youn Cadoret
function: Developer
releasedate: 02/06/2019
toolversion: 1.3
version: 1.02
status: Approved Document
reference: KER1-MAN-IHE-ADR_SIMULATOR_INSTALLATION-1_02
customer: IHE-EUROPE
---
# EPR ADR Provider MockUp

## Overview

The EPR ADR Provider MockUp is a SoapUI webservice (mock) that provides Authorization Decisions when requested.

* default_wsdl_url: http://ehealthsuisse.ihe-europe.net/adr-provider?wsdl
* default_path: /adr-provider
* default_port: 8091
* default_mock_name: AdrDecisionQuerySoapBinding_MockService
* default_mock_path: /opt/simulators/epr-adr-provider-mockup
* default_soapui_path: /usr/local/SmartBear/SoapUI-5.3.0/
* default_soapui_mock_log: /var/log/soapui/epr-adr-provider.log
* default_init.d: /etc/init.d/adrProviderMock
* default_keystore_path: /opt/gazelle/cert/jboss.jks


## Install SoapUI

[https://www.soapui.org/](https://www.soapui.org/)

## Install EPR Assertion Provider MockUp

### Get the Subversion project

```bash
git clone https://gitlab.inria.fr/gazelle/specific-tools/epr/adr-provider.git $EPR_ADR_MOCK_DIR
```

### Install libraries required by SoapUI

Copy the external jars (esapi, velocity and postgresql)

```bash
cp $EPR_ADR_MOCK_DIR/external_jar/esapi-2.1.0.1.jar $SOAPUI_INSTALL_DIR/lib/
cp $EPR_ADR_MOCK_DIR/external_jar/velocity-1.7.jar $SOAPUI_INSTALL_DIR/lib/
cp $EPR_ADR_MOCK_DIR/external_jar/postgresql-9.3-1102.jdbc4.jar $SOAPUI_INSTALL_DIR/lib/
```

### Prepare the database

Database is not mandatory, but if not configured, it will raise log errors for each recieving request.

```bash
psql -U gazelle postgres
> CREATE DATABASE "adr" OWNER gazelle ;
> \q
psql -U gazelle adr < $EPR_ADR_MOCK_DIR/sql/adr_schema_data_1.9.sql
```

## Mock as a service

### Prepare the init.d script

Edit the init.d script `$EPR_ADR_MOCK_DIR/init.d/adrProviderMock` and set the following environment variables

* SOAPUI_PATH => Path of SoapUI folder
* SOAPUI_PROJECT_PATH => Path of SoapUI project script
* SOAPUI_MOCK_NAME => Name of the SoapUI mock
* SOAPUI_MOCK_PORT => Port of the SoapUI mock
* SOAPUI_MOCK_ENDPOINT => Path of the SoapUI mock
* SOAPUI_MOCK_LOG => Path where to publish log file

### Declare the service

Type the following commands register the init.d script as service

```bash
sudo cp $EPR_ADR_MOCK_DIR/init.d/adrProviderMock /etc/init.d/adrProviderMock
sudo chmod u+x /etc/init.d/adrProviderMock
sudo chmod 775 /etc/init.d/adrProviderMock
```

If you want the service to start at each machine start up

```bash
sudo update-rc.d adrProviderMock defaults
```

Be careful to allow the service to write logs into your target directory. As example

```bash
sudo mkdir /var/log/soapui
sudo chmod 775 /var/log/soapui
```

### Start the mock

To run the mock

```bash
sudo /etc/init.d/adrProviderMock start
```

To stop the mock

```bash
sudo /etc/init.d/adrProviderMock stop
```

To get status of the mock

```bash
sudo /etc/init.d/adrProviderMock status
```


## Troubleshouting

You might need to install those following packets

```bash
sudo apt-get install -y libxrender1 libxtst6 libxi6
```
