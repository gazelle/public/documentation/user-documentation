---
title:  Release note
subtitle: ADR Validator 
author: Youn Cadoret
function: Developer
date: 02/06/2019
toolversion: R2
version: 1.0
status: To be reviewed
reference: KER1-RNO-IHE-ADR_VALIDATOR 
customer: IHE-EUROPE
---

# R2
_Release date: 2019-03-25_

__Remarks__


__Bug__

* \[[CHADR-15](https://gazelle.ihe.net/jira/browse/CHADR-15)\] ADR request from Webservice Tester uses anyURI instead of string for subject-id-qualifier

__Improvement__

* \[[CHADR-16](https://gazelle.ihe.net/jira/browse/CHADR-16)\] Update ADR Schematrons 


# R1
_Release date: 2017-06-09_

__Remarks__


__Task__

* \[[CHADR-2](https://gazelle.ihe.net/jira/browse/CHADR-2)\] Create schematrons for ADR messages
* \[[CHADR-3](https://gazelle.ihe.net/jira/browse/CHADR-3)\] Create sample data for testing ADR schematrons 
* \[[CHADR-10](https://gazelle.ihe.net/jira/browse/CHADR-10)\] Create junit tests for ADR schematrons
