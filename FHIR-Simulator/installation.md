#---
#title: Release note
#subtitle: FHIR Simulator 
#toolversion: 1.1.0
#releasedate: 2024-06-21
#authors: Achraf ACHKARI-BEGDOURI / Romuald DUBOURG
#function: Software Engineer
#customer: IHE Europe
#reference:
#---

The FHIR Simulator is a project that gives all tools needed to implement a custom FHIR Simulator for a custom profile.
It is in v1.1.0 a library that can be imported in your pom.xml project

# Sources & binaries

- Link to project : https://gitlab.inria.fr/gazelle/public/simulation/fhir-simulators

- Declaration in your pom.xml

```xml
<dependency>
    <groupId>net.ihe.gazelle</groupId>
    <artifactId>fhir-server-simulator</artifactId>
    <version>1.1.0</version> <!-- or any version -->
    <exclusions>
        <exclusion>
            <artifactId>servlet-api</artifactId>
            <groupId>javax.servlet</groupId>
        </exclusion>
        <exclusion>
            <artifactId>xpp3</artifactId>
            <groupId>org.ogce</groupId>
        </exclusion>
    </exclusions>
</dependency>
```
