#---
#title: Release note
#subtitle: FHIR Simulator 
#toolversion: 1.1.0
#releasedate: 2024-06-21
#authors: Achraf ACHKARI-BEGDOURI / Romuald DUBOURG
#function: Software Engineer
#customer: IHE Europe
#reference:
#---

# 1.1.1

# All changes

__Bug__
* \[[FHIRSIMU-29](https://gazelle.ihe.net/jira/browse/FHIRSIMU-29)\] Double "/" in validation URL


# 1.1.0

_Release date: 2024-06-21_

# All changes

__Story__
* \[[FHIRSIMU-25](https://gazelle.ihe.net/jira/browse/FHIRSIMU-25)\] Validate Fhir Resource with HTTP Val

# Dependencies

# Remarks

