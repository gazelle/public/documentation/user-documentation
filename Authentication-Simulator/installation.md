---
title:  Installation Manual
subtitle: Authentication Simulator / ch-idp-renewal
author: Malo TOUDIC & Alexandre POCINHO
function: Developer
date: 2019-12-10
toolversion: 0.1.x (Authenticator Simulator) / 1.0.0 (ch-idp-renewal)
version: 1.0
status: Approved
reference: KER1-MAN-IHE-AUTHENTICATION_SIMULATOR_INSTALLATION
customer: eHealth Suisse
---

# Installation process for autethenticator-simulator

## Sources & binaries

Authentication Simulator is an open-source project under Apache License Version 2.0 ([https://gazelle.ihe.net/content/license](https://gazelle.ihe.net/content/license)). Sources are available via Subversion at [https://gitlab.inria.fr/gazelle/public/simulation/authentication-simulator](https://gitlab.inria.fr/gazelle/public/simulation/authentication-simulator).

The latest public packaged release can be downloaded from our Nexus repository [https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22authentication-simulator-ear%22](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22authentication-simulator-ear%22) (search for **authentication-simulator-X.X.X.ear**) for Authentication Simulator and [https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22idp-adapter-ear%22](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22idp-adapte-ear%22) (search for **idp-adapte-X.X.X.ear**) for the IDP Adapter.

If you need for any reason a more recent version (hotfix, experimental feature...), the public packaged application of our development trunk can be found at [https://gazelle.ihe.net/jenkins/job/authentication-simulator/ws/authentication-simulator-ear/target/authentication-simulator.ear](https://gazelle.ihe.net/jenkins/job/authentication-simulator/ws/authentication-simulator-ear/target/authentication-simulator.ear). This package can be unstable. The IDP Adapter ear can be found in the same directory.

## Installation

If you are installing a Gazelle tool for the first time in your environment, make sure to read carefully the [general considerations for JBoss7](/gazelle-documentation/General/jboss7.html)

### Database creation

Your database must have a user **gazelle** :

1. Connect to your database

```bash
psql -U gazelle
```

1. Execute the SQL statement to create the database.

```sql
CREATE DATABASE "authentication-simulator" OWNER gazelle ENCODING 'UTF8' ;
```

### Deployment

To deploy Authentication :

1. Download the ear file from our Nexus repository

1. Paste the archive **authentication-simulator.ear** in the JBoss deployment directory `${JBOSS7\_HOME}/standalone/deployments/`

1. Display JBoss server logs, start JBoss and wait for **ear** deployment.

1. The application can be browsed at [http://yourserver/authentication-simulator](http://yourserver/authentication-simulator)
_Port could also be different whether you have modified the JBoss server configurations or not._

Authentication Simulator needs an another ear to parse the shibboleth logs. This ear does not need a database, just put the ear in a jboss. This ear needs to be installed in the same machine as Shibboleth IDP but not necessarily in the same machine as Authentication Simulator.

### Application configuration

1. Download the SQL scripts archive from our Nexus repository [https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22authentication-simulator-ear%22](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22authentication-simulator-ear%22) (search for **authentication-simulator-X.X.X-sql.zip**)

1. Unzip the archive

1. Edit the application_url value in init.sql. You might also want to edit the idp_adapter_wsdl_endpoint configuration.

1. From the bash, update the application configuration by running :

```bash
psql -U gazelle authentication-simulator < init.sql
```

# Installation process for ch-idp-renewal

## Sources & binaries

`ch-idp-renewal` is an open-source project under Apache License Version 2.0 ([https://gazelle.ihe.net/content/license](https://gazelle.ihe.net/content/license)). Sources are available via Gitlab at [https://gitlab.inria.fr/gazelle/specific-tools/epr/idp-renewal/](https://gitlab.inria.fr/gazelle/specific-tools/epr/idp-renewal).

The latest public packaged release can be downloaded from our Nexus repository [https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22ch-idp-renewal%22](https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22ch-idp-renewal%22) (search for **ch-idp-renewal-X.X.X.war**)

## Installation

If you are installing a Gazelle tool for the first time in your environment, make sure to read carefully the [general considerations for Wildfly26](/gazelle-documentation/General/wildfly26.html)

### Configuration

In the saml metadata file of the SUT (by default in /opt/shibboleth-idp/metadata/), add the following AssertionConsumerService:

```xml
<AssertionConsumerService Binding="urn:oasis:names:tc:SAML:2.0:bindings:PAOS" Location="https://sp-clone.ihe-europe.net/Shibboleth.sso/SAML2/ECP" index="3"/>
```

In the same file, in the `<SPSSODescriptor>` element, change the `AuthnRequestsSigned` to **false** instead of **true** if present.

### Deployment

To deploy Authentication :

1. Download the war file from our Nexus repository

1. Paste the archive **ch-idp-renewal-X.X.X.war** in the JBoss deployment directory `${WILDFLY26\_HOME}/standalone/deployments/`

1. Display JBoss server logs, start JBoss and wait for **war** deployment.

1. The application can be browsed at [http(s)://yourServer/ch-idp-renewal/ws-trust?wsdl](http(s)://yourServer/ch-idp-renewal/ws-trust?wsdl)
_Port could also be different whether you have modified the Wildfly server configurations or not._
