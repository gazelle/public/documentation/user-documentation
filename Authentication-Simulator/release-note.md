---
title: Release note
subtitle: Authentication Simulator
toolversion: 0.2.0
releasedate: 2024-02-06
author: Youn Cadoret
function: Software Engineer
customer: IHE Europe
reference: KER1-RNO-IHE-AUTHENTICATION_SIMULATOR
---
# 0.2.0
_Release date: 2024-02-06_

Context : Gazelle User Management Renovation step 2

__Task__
* \[[AUTH-25](https://gazelle.ihe.net/jira/browse/AUTH-25)\] Integrate new sso-client-v7

# 0.1.2
_Release date: 2020-01-14_

__Improvement__

* \[[AUTH-19](https://gazelle.ihe.net/jira/browse/AUTH-19)\] Manage idp simulator links from app_configuration


# 0.1.1
_Release date: 2019-09-10_

__Bug__

* \[[AUTH-16](https://gazelle.ihe.net/jira/browse/AUTH-16)\] The Subject/NameID in the assertion return by the idp can be random depending on the nameID format

__Task__

* \[[AUTH-18](https://gazelle.ihe.net/jira/browse/AUTH-18)\] Update the url for the new IDP in the description

# 0.1.0
_Release date: 2018-07-27_

__Story__

* \[[AUTH-5](https://gazelle.ihe.net/jira/browse/AUTH-5)\] Display IdP and XSU simulator information
* \[[AUTH-6](https://gazelle.ihe.net/jira/browse/AUTH-6)\] List IdP Simulator authentication attempts
* \[[AUTH-7](https://gazelle.ihe.net/jira/browse/AUTH-7)\] Provide a permanent link for authentication attempts

__Bug__

* \[[AUTH-11](https://gazelle.ihe.net/jira/browse/AUTH-11)\] No more log reported by idp-adapter after log-rotate
