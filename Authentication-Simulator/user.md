---
title:  User Manual
subtitle: Authentication Simulator
author: Malo Toudic
function: Developer
releasedate: 2019-09-17
toolversion: 0.1.1
version: 1.02
status: Draft
reference: KER1-MAN-IHE-AUTHENTICATION_SIMULATOR_USER
customer: eHealth Suisse
---

The documentation is available directly on the tool : [https://ehealthsuisse.ihe-europe.net/authentication-simulator/home.seam](https://ehealthsuisse.ihe-europe.net/authentication-simulator/home.seam)
