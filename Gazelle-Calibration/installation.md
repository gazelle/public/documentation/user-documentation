---
title:  Installation Manual
subtitle: Gazelle-Calibration
author: Guillaume Thomazon
function: Engineer
date: 11/12/2019
toolversion: N/A
version: 1.02
status: Approved Document
reference: KER1-MAN-IHE-GAZELLE_CALIBRATION_INSTALLATION-1_02
customer: IHE-EUROPE
---

# Introduction

The purpose of this document is the installation of Gazelle-calibration on a VM.

# gazelle-calibration Installation

## Installation

### Install gazelle-calibration

The project can be found at [https://svn.ihe-europe.net/gazelle/Maven/gazelle-calibration/trunk/](https://svn.ihe-europe.net/gazelle/Maven/gazelle-calibration/trunk/)

If you don't already have a zip of the project, create one with :
- icons/
- scripts/
- site/
- calibrate.php
- calibrate_fork.php
- getCalibrationExecutions.php
- getCalibrationResult.php
- index.php
- installation.sh
- pom.xml
- settings.xml

Send the zip to the VM :

```
scp /path/to/the/zip.zip gazelle@<VM>/tmp/
```

Connect to the VM and extract the zip :

```
unzip /tmp/gazelle-calibration.zip -d /tmp/gazelle-calibration
```

Edit installation.sh and make sure the variable APACHE_ROOT is correct.
Execute the script :

```
sudo chmod +x installation.sh
sudo /tmp/gazelle-calibration/installation.sh
```

### Install xmllint :
```
sudo apt-get install libxml2-utils
```

### Install maven :
```
sudo apt-get install maven
```

### Install soapui :
```
cd /tmp
wget https://s3.amazonaws.com/downloads.eviware/soapuios/5.4.0/SoapUI-x64-5.4.0.sh
chmod +x SoapUI-x64-5.4.0.sh
./SoapUI-x64-5.4.0.sh
```

### Install php DOM extension

Check the php version installed on the server :
```
php --version
```
Depending on the version, the package is different :

* php-5 :
```
sudo apt-get install php5-dom
```

* php-7.0 :
```
sudo apt-get install php7.0-xml
```

* php-7.1 :
```
sudo apt-get install php7.1-xml
```

### Install curl :
```
sudo apt-get install curl
```

## Configuration

### Gazelle-calibration

Check that the values in ~/gazelle-calibration/html/scripts/calibrate.sh are correct, especially INDEX_PATH and REFERENCES_PATH.

### Htaccess

Create or add an existing htpasswd.users in /home/gazelle/gazelle-calibration/html. To add a new user type :
```
sudo htpasswd /home/gazelle/gazelle-calibration/html/htpasswd.users <username>
```

Add the following to the configuration file in /etc/apache2/site-enable :
```
<Directory /var/www/html/gazelle-calibration>
          Options +Indexes +FollowSymLinks +MultiViews
          Order allow,deny
          Allow from all
          AuthName "Calibration Access"
          AuthType Basic
          AuthUserFile /home/gazelle/gazelle-calibration/html/htpasswd.users
          Require valid-user
</Directory>
```

*<domain\>*/gazelle-calibration is now password protected.

# Administration home page
## Installation
Create a folder

```bash
sudo mkdir /var/www/html/admin
cd /var/www/html/admin
```

Connect the project from the forge :

```bash
sudo svn co https://svn.ihe-europe.net/gazelle/Maven/gazelle-calibration/trunk/admin .
cd ..
```

If base html folder is /var/www/html, create a symbolic link :

```bash
sudo ln -s admin/admin.php admin.php
```

If base html folder is /var/www/, create a symbolic link of the symbolic link :

```bash
cd ..
sudo ln -s html/admin.php admin.php
```

Go back to /var/www/html/admin and make a copy of getToolsArraysTemplate.php :

```bash
cd /var/www/html/admin
sudo cp getToolsArraysTemplate.php getToolsArrays.php
```

This file contains arrays which list tools.


Go back to /var/www/html/ and create a symbolic link of getToolsArrays.php.
If base html folder is /var/www/html, create a symbolic link :

```bash
cd /var/www/html
sudo ln -s admin/getToolsArrays.php getToolsArrays.php
```

If base html folder is /var/www/, create a symbolic link of the symbolic link :

```bash
cd ..
sudo ln -s html/getToolsArrays.php getToolsArrays.php
```

## Configuration

### Edit getToolsArrays.php to list available tools on the VM.

```bash
sudo vi /var/www/html/getToolsArrays.php
```

```bash
//    array("toolLink","toolName", tool installed (0 or 1), doc installed (0 or 1), doc link (eg. assertion-manager),toolDescription)
array("AssertionManagerGui", "Assertion Manager", 1, 1,"Assertion-Manager", "Assertion Manager tool is used to stored the requirements on which the tests and tools are based. It also allows the linkage between tests, steps, validators and requirements."),
```

* **toolLink** : link to the tool (eg: [https://gazelle.ihe.net/_**AssertionManagerGui**](https://gazelle.ihe.net/AssertionManagerGui))

Be careful, for Gazelle Test Management the link could be "EU-CAT" or "gazelle".

* **toolName** : the name of the tool display in home page.

* **tool installed** : 0=false the line about the tool will not be displayed, 1=true the line about the tool will be displayed.

* **doc installed** : 0=false the icon about the doc will not be displayed, 1=true the icon about the doc will be displayed.

* **doc link** : the tool link in the gazelle-documentation (eg: [_https://gazelle.ihe.net/gazelle-documentation/_**Assertion-Manager**_/user.html_](https://gazelle.ihe.net/gazelle-documentation/Assertion-Manager/user.html))

* **toolDescription** : the description of the tool display in the home page.

### Update Logos

```bash
sudo mkdir /var/www/html/logo
sudo cp /var/www/html/admin/customerLogo.png /var/www/html/logo
sudo cp /var/www/html/admin/providerLogo.png /var/www/html/logo
```

If base html folder is /var/www/, create a symbolic link of the symbolic link

```bash
cd /var/www/html/
sudo ln -s html/logo logo
```

If you need to update customerLogo.png and providerLogo.png

```bash
cd /var/www/html/logo
wget https://edi-tu.efs.sante.fr/LOGO_EFS.png
sudo convert LOGO_EFS.png customerLogo.png
```
