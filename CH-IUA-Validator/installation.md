---
title:  Installation Manual
subtitle: CH:IUA Validator
author: Nicolas BAILLIET
function: Software Engineer
date: 2020-09-04
toolversion: 1.0.1
version: 1.01
status: To be reviewed
reference: KER1-MAN-IHE-CH-IUA-VALIDATOR_INSTALLATION-1\_01
customer: IHE-EUROPE
---
# Purpose

Here is a guide to help you installing the CH-IUA-Validator-Service.

# Deployment

## Minimal requirements

* Debian squeeze or ubuntu 12.04 64bits or higher with an Internet access.
* Database : Postgresql 9.6+
* Java virtual machine : JDK 11
* Application server : Wildfly 18.0.1

To install those requirements you can refer to the documentation of installation of Wildfly : [*General Requirements Wildfly 18*](https://ehealthsuisse.ihe-europe.net/gazelle-documentation/General/wildfly18.html)

## Instructions

Get the chiua-validator-service.war from https://nexus.ihe-catalyst.net/#browse/search=keyword%3D%22app.validator%22 and copy  it into the "/usr/local/wildfly18/standalone/deployments" directory of your Wildfly server. Finally, start your server.

Once the application is deployed, open a browser and go to http://yourserver/chiua-validator-service/ModelBasedValidationWSImpl?wsdl in order to check of the service is available.

If the deployment is successful, you should see the WSDL page.

The sources of the projects are available on [Inria's gitlab](https://gitlab.inria.fr/gazelle/public/validation/ch_iua-validator-service.git).


# Configuration in EVSClient

**Administrator rights are required to follow the instructions below**

## Create a referenced standard

In EVSClient, go to the menu Administration > Manage referenced standards. 
If an CH:IUA standard has not been created yet, click on "Add a standard" button.
*Otherwise, you should skip the instructions to the following part.*

You can refer to the [*Add a referenced standard*](https://ehealthsuisse.ihe-europe.net/gazelle-documentation/EVS-Client/installation.html#add-a-reference-to-a-standard) part of the installation manual of EVSClient.

## Create a validation service

In EVSClient, go to the menu Administration > Manage validation services. 
If an CH:IUA Token validator service has not been created yet, click on "Create new validation service" button.
*Otherwise, you should skip the instructions to the following part.*

You can refer to the [*Adding a validation service*](https://ehealthsuisse.ihe-europe.net/gazelle-documentation/EVS-Client/installation.html#adding-a-validation-service) part of the installation manual of EVSClient.

## Configure the menu

In EVSClient, go to the menu Administration > Menu configuration. 
If the CH:IUA standard has not been assigned yet to a menu, add it by editing the corresponding menu or click on "Create a new menu group" button if there is not any suitable group menu.
*Otherwise, you should skip the instructions to the following part.*

You can refer to the [*Configuring the top bar menu*](https://ehealthsuisse.ihe-europe.net/gazelle-documentation/EVS-Client/installation.html#configuring-the-top-bar-menu) part of the installation manual of EVSClient.