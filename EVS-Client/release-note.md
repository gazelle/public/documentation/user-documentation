---
title: Release note
subtitle: EVS Client
toolversion: 7.2.3
releasedate: 2025-02-12
author: Cédric Eoche-Duval
function: Software Engineer
customer: IHE Europe
reference: KER1-RNO-IHE-EVS_CLIENT
---

# 7.2.3

_Release date: 2025-02-12_

__Bug__
* \[[EVSCLT-1213](https://gazelle.ihe.net/jira/browse/EVSCLT-1213)\] EVSClient does not display all character from validated message correctly in UTF8

__Story__
* \[[EVSCLT-1228](https://gazelle.ihe.net/jira/browse/EVSCLT-1228)\] Test execution - Add private Key to permanent link in a test instance

# 7.2.2

_Release date: 2025-01-24_

__Bogue__
* \[[EVSCLT-1229](https://gazelle.ihe.net/jira/browse/EVSCLT-1229)\] Cannot download a validated file when using validation from the Proxy

## Message-Content-Analyzer 4.0.4

_Release date: 2025-01-24_

# 7.2.1
_Release date: 2025-01-10_

__Bug__
* \[[EVSCLT-1227](https://gazelle.ihe.net/jira/browse/EVSCLT-1227)\] Wrong timeout type in schematron client

# 7.2.0
_Release date: 2024-11-29_

Context : GUM Step 4 renovation

__Task__
* \[[EVSCLT-1216](https://gazelle.ihe.net/jira/browse/EVSCLT-1216)\] [GUM04] Update SSO v7 to 5.0.0

# 7.1.1
_Release date: 2024-11-26_

__Bug__
* \[[EVSCLT-1226](https://gazelle.ihe.net/jira/browse/EVSCLT-1226)\] EVS validation API regression


# 7.1.0
_Release date: 2024-11-12_

__Story__
* \[[EVSCLT-1217](https://gazelle.ihe.net/jira/browse/EVSCLT-1217)\] Get all validation profiles with API


# 7.0.7
_Release date: 2024-10-23_

__Bug__
* \[[EVSCLT-1221](https://gazelle.ihe.net/jira/browse/EVSCLT-1221)\] Revalidating xml and switching schematron produces incoherent results
* \[[EVSCLT-1224](https://gazelle.ihe.net/jira/browse/EVSCLT-1224)\] Adapt XML validation title to do not include anymore the type - conflict with format
* \[[EVSCLT-1225](https://gazelle.ihe.net/jira/browse/EVSCLT-1225)\] Validation IHE-france HL7V2 is not working when selecting a validator from second page
* \[[EVSCLT-1206](https://gazelle.ihe.net/jira/browse/EVSCLT-1206)\] the eye function doesn't work well


# 7.0.6
_Release date: 2024-09-25_

__Bug__
* \[[EVSCLT-1218](https://gazelle.ihe.net/jira/browse/EVSCLT-1218)\] Cross-validation - Bad validation status

## Gazelle X Validator 3.0.1

__Bug__
* \[[XVAL-171](https://gazelle.ihe.net/jira/browse/XVAL-171)\] Import fails when importing a cross validator with rules dealing with DateFormatType
* \[[XVAL-190](https://gazelle.ihe.net/jira/browse/XVAL-190)\] Bad validation status

# 7.0.5
_Release date: 2024-08-01_

## Message-Content-Analyzer 4.0.3

_Release date: 2024-08-01_

__Bug__
* \[[MCA-133](https://gazelle.ihe.net/jira/browse/MCA-133)\] EVS/MCA "SAML-EPD rule" doesn't catch the saml:assertion

__Task__
* \[[MCA-134](https://gazelle.ihe.net/jira/browse/MCA-134)\] Analyseur de contenu : Icone Cross-validation grisée après cross validation

# 7.0.4
_Release date: 2024-06-25_

__Improvement__
* \[[EVSCLT-1214](https://gazelle.ihe.net/jira/browse/EVSCLT-1214)\] Upgrade sso-client-v7 library to 4.1.1

# 7.0.3
_Release date: 2024-04-10_

__Bug__
* \[[EVSCLT-1211](https://gazelle.ihe.net/jira/browse/EVSCLT-1211)\] Broken CSS rules in HTML attributes


## Message-Content-Analyzer 4.0.2

_Release date: 2024-04-10_

__Bug__
* \[[MCA-132](https://gazelle.ihe.net/jira/browse/MCA-132)\] Broken CSS rules in HTML attributes

# 7.0.2

_Release date: 2024-03-14_

__Bug__
* \[[EVSCLT-1210](https://gazelle.ihe.net/jira/browse/EVSCLT-1210)\] HL7 Message Profile Selection section is broken in hl7v2/validator.xhtml

# 7.0.1

_Release date: 2024-02-16_

__Bug__
* \[[EVSCLT-1197](https://gazelle.ihe.net/jira/browse/EVSCLT-1197)\] Cannot create nor maintain referenced standards in administration.
* \[[EVSCLT-1198](https://gazelle.ihe.net/jira/browse/EVSCLT-1198)\] NPE when trying to get user name
* \[[EVSCLT-1200](https://gazelle.ihe.net/jira/browse/EVSCLT-1200)\] Referenced Standard initialization checks services without verifying the availability
* \[[EVSCLT-1201](https://gazelle.ihe.net/jira/browse/EVSCLT-1201)\] Missing security rules for pages
* \[[EVSCLT-1202](https://gazelle.ihe.net/jira/browse/EVSCLT-1202)\] Validation services registered as DEFAULT validationType are called GITB.
* \[[EVSCLT-1203](https://gazelle.ihe.net/jira/browse/EVSCLT-1203)\] \[QA\] Renderer are breaking EVS pages
* \[[EVSCLT-1204](https://gazelle.ihe.net/jira/browse/EVSCLT-1204)\] The version of the validator is incorrect

## Message-Content-Analyzer 4.0.1

_Release date: 2024-02-16_

__Bug__
* \[[MCA-130](https://gazelle.ihe.net/jira/browse/MCA-130)\] NPE when trying to get user name
* \[[MCA-131](https://gazelle.ihe.net/jira/browse/MCA-131)\] \[QA\] Renderer are breaking EVS pages

# 7.0.0

_Release date: 2024-02-06_

Context : Gazelle User Management Renovation step 2

__Task__
* \[[EVSCLT-1190](https://gazelle.ihe.net/jira/browse/EVSCLT-1190)\] Integrate new sso-client-v7
* \[[EVSCLT-1193](https://gazelle.ihe.net/jira/browse/EVSCLT-1193)\] Remove username display in GUI

## Message-Content-Analyzer 4.0.0

_Release date: 2024-02-06_

Context : Gazelle User Management Renovation step 2

__Task__
* \[[MCA-127](https://gazelle.ihe.net/jira/browse/MCA-127)\] Integrate new sso-client-v7
* \[[MCA-128](https://gazelle.ihe.net/jira/browse/MCA-128)\] Remove username display in GUI

# 6.4.1

_Release date: 2023-12-08_

__Bug__
* \[[EVSCLT-1188](https://gazelle.ihe.net/jira/browse/EVSCLT-1188)\] Rename Pixelmed SR Dose validator
* \[[EVSCLT-1191](https://gazelle.ihe.net/jira/browse/EVSCLT-1191)\] Missing MCA Button for remote validation

# 6.4.0
_Release date: 2023-09-13_

__Story__
* \[[EVSCLT-1164](https://gazelle.ihe.net/jira/browse/EVSCLT-1164)\] Validation Service Client for EVS6

__Bug__
* \[[EVSCLT-1147](https://gazelle.ihe.net/jira/browse/EVSCLT-1147)\] Cannot download the validation report
* \[[EVSCLT-1182](https://gazelle.ihe.net/jira/browse/EVSCLT-1182)\] Title of the validation page is not relevant
* \[[EVSCLT-1183](https://gazelle.ihe.net/jira/browse/EVSCLT-1183)\] Does not display the eye icon if the user cannot be pointed to the location of the issue

# 6.3.0
_Release date: 2023-07-11_

Step 1 of Gazelle User Management renovation.

__Task__
* \[[EVSCLT-1174](https://gazelle.ihe.net/jira/browse/EVSCLT-1174)\] [GUM] Integrate new sso-client-v7 to evs

# 6.2.4 (SUPPORT)

_Release date: 2023-09-05_

__Bug__
* \[[EVSCLT-1185](https://gazelle.ihe.net/jira/browse/EVSCLT-1185)\] Bugs correction in MCA
* \[[EVSCLT-1186](https://gazelle.ihe.net/jira/browse/EVSCLT-1186)\] Bugs correction in X-val
* \[[EVSCLT-1187](https://gazelle.ihe.net/jira/browse/EVSCLT-1187)\] Error on validation report

Remarks : This version is a support version from 6.2.3. It DOES NOT support Gazelle User Management Feature and corrects some bugs presents in Message Content Analyzer (upgraded to 3.0.5 SUPPORT), Gazelle X Validation (upgraded to 2.0.5), Gazelle Assets (upgraded to 3.0.1).

## Message-Content-Analyzer 3.1.0

_Release date: 2023-07-11_

Step 1 of Gazelle User Management renovation.

Integration of new version (2.0.0) of sso-client-v7.

# 6.2.3
_Release date: 2023-06-06_

__Bug__
* \[[EVSCLT-1177](https://gazelle.ihe.net/jira/browse/EVSCLT-1177)\] Add description for HL7V2 reference standard
* \[[EVSCLT-1178](https://gazelle.ihe.net/jira/browse/EVSCLT-1178)\] X-Val version update to 2.0.3

__Story__
* \[[EVSCLT-1176](https://gazelle.ihe.net/jira/browse/EVSCLT-1176)\] validation CDA EV6

# 6.2.2
_Release date: 2023-04-20_

__Bug__
* \[[EVSCLT-1173](https://gazelle.ihe.net/jira/browse/EVSCLT-1173)\] GITB validator integration broke other validators

# 6.2.1
_Release date: 2023-04-17_

__Bug__
* \[[EVSCLT-1162](https://gazelle.ihe.net/jira/browse/EVSCLT-1162)\] Map Gazelle HL7 Profile Exception as Unexpected Error in the report

# 6.2.0
_Release date: 2023-04-06_

__Bug__
* \[[EVSCLT-1170](https://gazelle.ihe.net/jira/browse/EVSCLT-1170)\] [EVS6] Fix TOS popup

__Task__
* \[[EVSCLT-1155](https://gazelle.ihe.net/jira/browse/EVSCLT-1155)\] Create GITB REST Client

# 6.1.4
_Release date: 2023-02-07_

__Bug__
* \[[EVSCLT-1133](https://gazelle.ihe.net/jira/browse/EVSCLT-1133)\] Gazelle Cross Validator  - Encoding problem
* \[[EVSCLT-1137](https://gazelle.ihe.net/jira/browse/EVSCLT-1137)\] Home Page  - Page formatting
* \[[EVSCLT-1138](https://gazelle.ihe.net/jira/browse/EVSCLT-1138)\] Validate CDA documents  - Page formatting
* \[[EVSCLT-1139](https://gazelle.ihe.net/jira/browse/EVSCLT-1139)\] Standard Report - Remove CamelCase format
* \[[EVSCLT-1140](https://gazelle.ihe.net/jira/browse/EVSCLT-1140)\] Wrong DTD issue

# 6.1.3
_Release date: 2022-11-06_

__Bug__
* \[[EVSCLT-1131](https://gazelle.ihe.net/jira/browse/EVSCLT-1131)\] File too large slow down navigator
* \[[EVSCLT-1132](https://gazelle.ihe.net/jira/browse/EVSCLT-1132)\] Can't display Tree of the templates used in document

# 6.1.2
_Release date: 2022-10-24_

__Bug__
* \[[EVSCLT-1118](https://gazelle.ihe.net/jira/browse/EVSCLT-1118)\] Jboss ASM conflict with Json-Path  dependency

# 6.1.1
_Release date: 2022-10-20_

__Bug__
* \[[EVSCLT-1117](https://gazelle.ihe.net/jira/browse/EVSCLT-1117)\] Problem with afterMigrate script for dockerized environments

# 6.1.0
_Release date: 2022-10-19_

Notice: Please backup your configuration (cmn_application_preference table) before upgrading because it will be overridden by the preferences.properties file.

__Bug__
* \[[EVSCLT-1081](https://gazelle.ihe.net/jira/browse/EVSCLT-1081)\] the eye in a validation report have not the right redirection
* \[[EVSCLT-1090](https://gazelle.ihe.net/jira/browse/EVSCLT-1090)\] Tab - Validation Report for Schematron CDA Validator - eye icon click issue
* \[[EVSCLT-1091](https://gazelle.ihe.net/jira/browse/EVSCLT-1091)\] Pretty content after disabled - eye icon click issue
* \[[EVSCLT-1092](https://gazelle.ihe.net/jira/browse/EVSCLT-1092)\] Tree View issue - Technical Error is displayed
* \[[EVSCLT-1099](https://gazelle.ihe.net/jira/browse/EVSCLT-1099)\] Validation PDF Report 2 Button(w/out score card) issues
* \[[EVSCLT-1103](https://gazelle.ihe.net/jira/browse/EVSCLT-1103)\] Unclickable eye button with FHIR files
* \[[EVSCLT-1104](https://gazelle.ihe.net/jira/browse/EVSCLT-1104)\] styled result tab don't display results sometimes with FHIR
* \[[EVSCLT-1107](https://gazelle.ihe.net/jira/browse/EVSCLT-1107)\] [QUAL] generation of result report in pdf is not possible with few validators
* \[[EVSCLT-1112](https://gazelle.ihe.net/jira/browse/EVSCLT-1112)\] Filter JSON Path don't work when we are in JSON Tree.
* \[[EVSCLT-1113](https://gazelle.ihe.net/jira/browse/EVSCLT-1113)\] Tab and content in English on the Dev2 environment, not on the prod environment
* \[[EVSCLT-1114](https://gazelle.ihe.net/jira/browse/EVSCLT-1114)\] [QUAL] HL7v2 validator returns an undefined result
* \[[EVSCLT-1115](https://gazelle.ihe.net/jira/browse/EVSCLT-1115)\] HL7v2 - tabs disappeared when you click on the permanent link

__Improvement__
* \[[EVSCLT-1093](https://gazelle.ihe.net/jira/browse/EVSCLT-1093)\] Preview sections ordering to change
* \[[EVSCLT-1094](https://gazelle.ihe.net/jira/browse/EVSCLT-1094)\] Wording on the document or file uploaded
* \[[EVSCLT-1095](https://gazelle.ihe.net/jira/browse/EVSCLT-1095)\] Review Red closing cross behavior
* \[[EVSCLT-1096](https://gazelle.ihe.net/jira/browse/EVSCLT-1096)\] Missing tooltip on the WS Generation button
* \[[EVSCLT-1097](https://gazelle.ihe.net/jira/browse/EVSCLT-1097)\] Increase dynamically the progress bar indicator for error/warn/info to display the number
* \[[EVSCLT-1098](https://gazelle.ihe.net/jira/browse/EVSCLT-1098)\] FlywayDB enabled for Data migration
* \[[EVSCLT-1100](https://gazelle.ihe.net/jira/browse/EVSCLT-1100)\] Move the X-Validation Documentation menu at sub-menu level of X-Validation
* \[[EVSCLT-1101](https://gazelle.ihe.net/jira/browse/EVSCLT-1101)\] Style Sheet Vizualisation Application Preference
* \[[EVSCLT-1106](https://gazelle.ihe.net/jira/browse/EVSCLT-1106)\] eye in a validation report doesn't work with HPD validator

# 6.0.0
_Release date: 2022-06-09_

__Story__
* \[[EVSCLT-1041](https://gazelle.ihe.net/jira/browse/EVSCLT-1041)\] Implements Certificate service
* \[[EVSCLT-1042](https://gazelle.ihe.net/jira/browse/EVSCLT-1042)\] Implements IUA (ch-iua) service
* \[[EVSCLT-1043](https://gazelle.ihe.net/jira/browse/EVSCLT-1043)\] Implements Pdf service
* \[[EVSCLT-1044](https://gazelle.ihe.net/jira/browse/EVSCLT-1044)\] Implements digital signature validation service
* \[[EVSCLT-1045](https://gazelle.ihe.net/jira/browse/EVSCLT-1045)\] Implements Dicom validation services
* \[[EVSCLT-1046](https://gazelle.ihe.net/jira/browse/EVSCLT-1046)\] Implements missing MBV services ITs
* \[[EVSCLT-1047](https://gazelle.ihe.net/jira/browse/EVSCLT-1047)\] Implements scorecard processing

__Bug__
* \[[EVSCLT-1054](https://gazelle.ihe.net/jira/browse/EVSCLT-1054)\] [QUAL] documentation link is not up to date
* \[[EVSCLT-1066](https://gazelle.ihe.net/jira/browse/EVSCLT-1066)\] [QUAL] Metadata tabs are not functional after Validation Then Analyze
* \[[EVSCLT-1067](https://gazelle.ihe.net/jira/browse/EVSCLT-1067)\] [QUAL] Validation without choosing validator return an error null.pointer.exception
* \[[EVSCLT-1068](https://gazelle.ihe.net/jira/browse/EVSCLT-1068)\] [QUAL] error after a user story and validation of signature
* \[[EVSCLT-1069](https://gazelle.ihe.net/jira/browse/EVSCLT-1069)\] [QUAL] Desactivation of signature validation doesn't work
* \[[EVSCLT-1070](https://gazelle.ihe.net/jira/browse/EVSCLT-1070)\] [QUAL] Chip on MCA after a validation is blue
* \[[EVSCLT-1071](https://gazelle.ihe.net/jira/browse/EVSCLT-1071)\] [QUAL] pb downloading a pdf file include in CDA
* \[[EVSCLT-1072](https://gazelle.ihe.net/jira/browse/EVSCLT-1072)\] [QUAL] sharing a private analyse not working
* \[[EVSCLT-1073](https://gazelle.ihe.net/jira/browse/EVSCLT-1073)\] [QUAL] mca_part_edit_mode_enabled set to true with edition of file make an error
* \[[EVSCLT-1074](https://gazelle.ihe.net/jira/browse/EVSCLT-1074)\] [QUAL] world map stats doesn't work
* \[[EVSCLT-1075](https://gazelle.ihe.net/jira/browse/EVSCLT-1075)\] [QUAL] Display of validation report with CR BIO crash
* \[[EVSCLT-1077](https://gazelle.ihe.net/jira/browse/EVSCLT-1077)\] [QUAL] Content of file in HL7v2 is not displayed
* \[[EVSCLT-1078](https://gazelle.ihe.net/jira/browse/EVSCLT-1078)\] [QUAL] filter doesn't work on Manage validation service
* \[[EVSCLT-1080](https://gazelle.ihe.net/jira/browse/EVSCLT-1080)\] [QUAL] analyse a file via validation EVS, don't return result when logged-in
* \[[EVSCLT-1083](https://gazelle.ihe.net/jira/browse/EVSCLT-1083)\] [QUAL] Fix menus order to be Alphabetic

# 5.15.3
_Release date: 2023-04-17_

__Bug__
* \[[EVSCLT-1157](https://gazelle.ihe.net/jira/browse/EVSCLT-1157)\] [EVS5] Fix TOS popup

# 5.15.2
_Release date: 2023-02-15_

__Bug__
* \[[EVSCLT-1157](https://gazelle.ihe.net/jira/browse/EVSCLT-1157)\] [EVS5] Fix TOS popup

# 5.15.1
_Release date: 2023-02-02_

__Bug__
* \[[EVSCLT-1157](https://gazelle.ihe.net/jira/browse/EVSCLT-1157)\] [EVS5] Fix TOS popup

# 5.15.0
_Release date: 2022-12-01_
  
Notice: This version is released to support QRCode validation that is currently not supported in EVSClient version >6.x

__Bug__
* \[[EVSCLT-1130](https://gazelle.ihe.net/jira/browse/EVSCLT-1130)\] Correct bug in redirection from gazelle tm to EVSClient with QR COde validation

__Task__
* \[[EVSCLT-1129](https://gazelle.ihe.net/jira/browse/EVSCLT-1129)\] Add new interface to QR Validation

# 5.14.4
_Release date: 2022-09-07_

__Story__
* \[[EVSCLT-1087](https://gazelle.ihe.net/jira/browse/EVSCLT-1087)\] CH_IUA Validator

# 5.14.3
_Release date: 2022-08-26_

__Bogue__
* \[[EVSCLT-1082](https://gazelle.ihe.net/jira/browse/EVSCLT-1082)\] [DCC Validator] Json is not correctly displayed when there is a "<" in it
* \[[EVSCLT-1088](https://gazelle.ihe.net/jira/browse/EVSCLT-1088)\] Fix indentation issue in EVSClient

__Tâche__
* \[[EVSCLT-1089](https://gazelle.ihe.net/jira/browse/EVSCLT-1089)\] Update Pixelmed dependency through DICOMEVS-Api

# 5.14.2
_Release date: 2022-05-12_

__Bug__
* \[[EVSCLT-1065](https://gazelle.ihe.net/jira/browse/EVSCLT-1065)\] Use a URL instead of a Path for qrcode xsl

# 5.14.1
_Release date: 2022-05-11_

__Story__
* \[[EVSCLT-1064](https://gazelle.ihe.net/jira/browse/EVSCLT-1064)\] QR Code Validator - Display JSON

# 5.14.0
_Release date: 2022-04-05_

__Story__
* \[[EVSCLT-1061](https://gazelle.ihe.net/jira/browse/EVSCLT-1061)\] Support for the Digital Covid Certificate validator

# 6.0.0-RC4
_Release date: 2022-03-18_

__Story__
* \[[EVSCLT-981](https://gazelle.ihe.net/jira/browse/EVSCLT-981)\] Add GDPR banner

__Bug__
* \[[EVSCLT-1020](https://gazelle.ihe.net/jira/browse/EVSCLT-1020)\] [QUALIF] MCA - editing a file is impossible after editing with an error
* \[[EVSCLT-1030](https://gazelle.ihe.net/jira/browse/EVSCLT-1030)\] [PreProd] Validation of manual content impossible
* \[[EVSCLT-1038](https://gazelle.ihe.net/jira/browse/EVSCLT-1038)\] CDA Service Type not correctly infered
* \[[EVSCLT-1052](https://gazelle.ihe.net/jira/browse/EVSCLT-1052)\] Limit validation service usage

## Message-Content-Analyzer 3.0.0-RC4

__Bug__
* \[[MCA-113](https://gazelle.ihe.net/jira/browse/MCA-113)\] Base64 Injection of forbidden filePAth value in Query Param of MCA is not protected
* \[[MCA-117](https://gazelle.ihe.net/jira/browse/MCA-117)\] MCA crash when a file is UTF-8 BOM encoded
* \[[MCA-120](https://gazelle.ihe.net/jira/browse/MCA-120)\] Error in start/end indexes in MCA segmentation

# 6.0.0-RC3
_Release date: 2022-02-18_

__Story__
* \[[EVSCLT-928](https://gazelle.ihe.net/jira/browse/EVSCLT-928)\] [EVS API] X-Validator
* \[[EVSCLT-975](https://gazelle.ihe.net/jira/browse/EVSCLT-975)\] [EVS API] Re-integrate MCA GUI + XVal Configuration
* \[[EVSCLT-999](https://gazelle.ihe.net/jira/browse/EVSCLT-999)\] [EVS API] Ability for EVS consumers to integrate with 2 EVSClients
* \[[EVSCLT-1028](https://gazelle.ihe.net/jira/browse/EVSCLT-1028)\] [EVS API] Severity threshold on API validation report

__Bug__
* \[[EVSCLT-949](https://gazelle.ihe.net/jira/browse/EVSCLT-949)\] Wrong counters in report about warning, errors and total
* \[[EVSCLT-967](https://gazelle.ihe.net/jira/browse/EVSCLT-967)\] [QUALIF] Export button from Stats not translated in french
* \[[EVSCLT-972](https://gazelle.ihe.net/jira/browse/EVSCLT-972)\] [QUALIF] text in stats page not translate in french
* \[[EVSCLT-991](https://gazelle.ihe.net/jira/browse/EVSCLT-991)\] [QUALIF] SSO don't work with EVS6 but works with EVS5
* \[[EVSCLT-992](https://gazelle.ihe.net/jira/browse/EVSCLT-992)\] [QUALIF] inversion of 2 parameters in the report of API GET Report
* \[[EVSCLT-993](https://gazelle.ihe.net/jira/browse/EVSCLT-993)\] [QUALIF] GET Report return code 404 when request a private report without auth
* \[[EVSCLT-994](https://gazelle.ihe.net/jira/browse/EVSCLT-994)\] [QUALIF] Bad value for serviceName and validatorID with an HL7v3 validator
* \[[EVSCLT-995](https://gazelle.ihe.net/jira/browse/EVSCLT-995)\] [QUALIF] filter of the status result doesn't exist in EVSv6
* \[[EVSCLT-996](https://gazelle.ihe.net/jira/browse/EVSCLT-996)\] [QUALIF] Validation status in GUI is DONE_XXX
* \[[EVSCLT-998](https://gazelle.ihe.net/jira/browse/EVSCLT-998)\] [QUALIF] impossible to access to a private report in IHM with the WS url
* \[[EVSCLT-1001](https://gazelle.ihe.net/jira/browse/EVSCLT-1001)\] [QUALIF] validation with schematron not possible
* \[[EVSCLT-1003](https://gazelle.ihe.net/jira/browse/EVSCLT-1003)\] [QUALIF] Cross Val - revalidate do nothing
* \[[EVSCLT-1004](https://gazelle.ihe.net/jira/browse/EVSCLT-1004)\] [QUALIF] Revalidate don't work (FHIR and CDA)
* \[[EVSCLT-1005](https://gazelle.ihe.net/jira/browse/EVSCLT-1005)\] [QUALIF] CrossVal - button revalidate don't work/useless
* \[[EVSCLT-1007](https://gazelle.ihe.net/jira/browse/EVSCLT-1007)\] [QUALIF] validation link from POST don't work with private validations
* \[[EVSCLT-1008](https://gazelle.ihe.net/jira/browse/EVSCLT-1008)\] [QUALIF] world map stats doesn't work
* \[[EVSCLT-1010](https://gazelle.ihe.net/jira/browse/EVSCLT-1010)\] [QUALIF] different numbers of checks between WS and API validation on schematron
* \[[EVSCLT-1011](https://gazelle.ihe.net/jira/browse/EVSCLT-1011)\] [QUALIF] Cannot create Application Preference
* \[[EVSCLT-1016](https://gazelle.ihe.net/jira/browse/EVSCLT-1016)\] [QUALIF] MCA - Error while parsing CDA file
* \[[EVSCLT-1018](https://gazelle.ihe.net/jira/browse/EVSCLT-1018)\] Cannot create Tag Configuration / Mime Type Configuration / Content Analysis Configuration
* \[[EVSCLT-1019](https://gazelle.ihe.net/jira/browse/EVSCLT-1019)\] When creating a Tag Configuration / Mime Type Configuration / Content Analysis Configuration, choosing a value besides the default value for the field "Associated Validation Type" leads to a crash
* \[[EVSCLT-1023](https://gazelle.ihe.net/jira/browse/EVSCLT-1023)\] [QUALIF] Cannot delete Tag Config / Mime Type Config / Content Analysis Config
* \[[EVSCLT-1024](https://gazelle.ihe.net/jira/browse/EVSCLT-1024)\] [QUALIF] MCA - No error message when creating a Tag without requiered fields
* \[[EVSCLT-1026](https://gazelle.ihe.net/jira/browse/EVSCLT-1026)\] [QUALIF] MCA - pop up message after a validation don't interprete french accent caracters
* \[[EVSCLT-1027](https://gazelle.ihe.net/jira/browse/EVSCLT-1027)\] [QUALIF] MCA - MimeType not recognized

__Task__
* \[[EVSCLT-891](https://gazelle.ihe.net/jira/browse/EVSCLT-891)\] [EVS API] Mapping of validationReport
* \[[EVSCLT-962](https://gazelle.ihe.net/jira/browse/EVSCLT-962)\] Clean up and prepare RC3 for qualif
* \[[EVSCLT-964](https://gazelle.ihe.net/jira/browse/EVSCLT-964)\] Clean up : update dependency
* \[[EVSCLT-979](https://gazelle.ihe.net/jira/browse/EVSCLT-979)\] [EVS API] Remove legacy package and java classes
* \[[EVSCLT-980](https://gazelle.ihe.net/jira/browse/EVSCLT-980)\] [EVS API] JSF delete unused xhtml files

# 5.13.8
_Release date: 2022-02-08_

__Story__
* \[[EVSCLT-1006](https://gazelle.ihe.net/jira/browse/EVSCLT-1006)\] Add "Disable validation" feature


# 5.13.7
_Release date: 2022-01-24_

__Bug__
* \[[EVSCLT-982](https://gazelle.ihe.net/jira/browse/EVSCLT-982)\] Problem of SSO

# 5.13.6
_Release date: 2022-01-19_

__Story__
* \[[EVSCLT-981](https://gazelle.ihe.net/jira/browse/EVSCLT-981)\] GDPR Refacto banner in standalone library

# 6.0.0-RC2
_Release date: 2021-12-08_

__Story__
* \[[EVSCLT-886](https://gazelle.ihe.net/jira/browse/EVSCLT-886)\] [EVS API] Complete statistics for API validation

__Bug__
* \[[EVSCLT-950](https://gazelle.ihe.net/jira/browse/EVSCLT-950)\] [QUALIF] HL7v2 validation issue in GUI
* \[[EVSCLT-953](https://gazelle.ihe.net/jira/browse/EVSCLT-953)\] [QUALIF] HL7v2 message guessing is less efficient
* \[[EVSCLT-955](https://gazelle.ihe.net/jira/browse/EVSCLT-955)\] [QUALIF] HPRIM validation always return DONE_UNDEFINED
* \[[EVSCLT-956](https://gazelle.ihe.net/jira/browse/EVSCLT-956)\] [QUALIF] validation of a file with unknown encoding return Error 500

# 6.0.0-RC1

_Release date: 2021-10-18_

__Story__
* \[[EVSCLT-871](https://gazelle.ihe.net/jira/browse/EVSCLT-871)\] [EVS API] Create validated object through webservice
* \[[EVSCLT-872](https://gazelle.ihe.net/jira/browse/EVSCLT-872)\] [EVS API] Refactor current validation model
* \[[EVSCLT-875](https://gazelle.ihe.net/jira/browse/EVSCLT-875)\] [EVS API] Simplify the visibility management of validations and logs
* \[[EVSCLT-884](https://gazelle.ihe.net/jira/browse/EVSCLT-884)\] [EVS API] Create private validations through webservice
* \[[EVSCLT-885](https://gazelle.ihe.net/jira/browse/EVSCLT-885)\] [EVS API] Read private validations through web-services
* \[[EVSCLT-887](https://gazelle.ihe.net/jira/browse/EVSCLT-887)\] [EVS API] Read ValidatedObject and XML Gazelle Validation Report through webservice
* \[[EVSCLT-895](https://gazelle.ihe.net/jira/browse/EVSCLT-895)\] [EVS API] User get an API Key for webservice authentication
* \[[EVSCLT-911](https://gazelle.ihe.net/jira/browse/EVSCLT-911)\] [EVS API] Remote Validation
* \[[EVSCLT-915](https://gazelle.ihe.net/jira/browse/EVSCLT-915)\] [EVS API] HL7V2 Validator
* \[[EVSCLT-920](https://gazelle.ihe.net/jira/browse/EVSCLT-920)\] [EVS API] CDA Validator
* \[[EVSCLT-924](https://gazelle.ihe.net/jira/browse/EVSCLT-924)\] [EVS API] FHIR Validator
* \[[EVSCLT-932](https://gazelle.ihe.net/jira/browse/EVSCLT-932)\] [EVS-API] HL7V3 Validator

__Bug__
* \[[EVSCLT-937](https://gazelle.ihe.net/jira/browse/EVSCLT-937)\] [QUALIF] validation report from WS does not have the same sub-reports than in GUI
* \[[EVSCLT-938](https://gazelle.ihe.net/jira/browse/EVSCLT-938)\] [QUALIF] Wrong status code if GET /validation with another organization's API key.
* \[[EVSCLT-939](https://gazelle.ihe.net/jira/browse/EVSCLT-939)\] [QUALIF] API Key is not created if api_key_validity_days = -1
* \[[EVSCLT-940](https://gazelle.ihe.net/jira/browse/EVSCLT-940)\] [QUALIF] Can create an API key without being authentified
* \[[EVSCLT-941](https://gazelle.ihe.net/jira/browse/EVSCLT-941)\] [QUALIF] Validation done via WS is not displayed in GUI

__Task__
* \[[EVSCLT-891](https://gazelle.ihe.net/jira/browse/EVSCLT-891)\] [EVS API] Mapping of validationReport
* \[[EVSCLT-892](https://gazelle.ihe.net/jira/browse/EVSCLT-892)\] [EVS API] Internal validation service API for calling any validator
* \[[EVSCLT-912](https://gazelle.ihe.net/jira/browse/EVSCLT-912)\] [EVS API] GUI refatoring for Remote validation
* \[[EVSCLT-914](https://gazelle.ihe.net/jira/browse/EVSCLT-914)\] [EVS API] Integrations tests and unit tests for private validations trought WS
* \[[EVSCLT-916](https://gazelle.ihe.net/jira/browse/EVSCLT-916)\] [EVS API] Integrations tests and unit tests for private validations trought WS
* \[[EVSCLT-917](https://gazelle.ihe.net/jira/browse/EVSCLT-917)\] [EVS-API] GUI Refactoring for HL7v2
* \[[EVSCLT-918](https://gazelle.ihe.net/jira/browse/EVSCLT-918)\] [EVS-API] Refactor services from legacy to new API model for HL7V2
* \[[EVSCLT-919](https://gazelle.ihe.net/jira/browse/EVSCLT-919)\] [EVS-API] Integration tests and unit tests for HL7V2
* \[[EVSCLT-921](https://gazelle.ihe.net/jira/browse/EVSCLT-921)\] [EVS-API] GUI Refactoring for CDA Validator
* \[[EVSCLT-925](https://gazelle.ihe.net/jira/browse/EVSCLT-925)\] [EVS-API] GUI Refactoring for FHIR Validator
* \[[EVSCLT-926](https://gazelle.ihe.net/jira/browse/EVSCLT-926)\] [EVS-API] Refactor services from legacy to new API model for FHIR Validator
* \[[EVSCLT-927](https://gazelle.ihe.net/jira/browse/EVSCLT-927)\] [EVS-API] Integration tests and unit tests for FHIR Validator
* \[[EVSCLT-933](https://gazelle.ihe.net/jira/browse/EVSCLT-933)\] [EVS-API] GUI Refactoring for HL7V3 Validator

# 5.13.5
_Release date: 2021-10-18_

__Bug__
* \[[MCA-117](https://gazelle.ihe.net/jira/browse/MCA-117)\] MCA crash when a file is UTF-8 BOM encoded

# 5.13.4
_Release date: 2021-07-19_

__Remarks__
This version of EVSClient is only compatible with the following minimum versions of those tools:
* Gazelle Test Management: 6.1.0
* Gazelle proxy: 5.0.2
* Gazelle Webservice Tester: 1.7.4

__Bug__
* \[[MCA-113](https://gazelle.ihe.net/jira/browse/MCA-113)\] Base64 Injection of forbidden filePAth value in Query Param of MCA is not protected

# 5.13.3
_Release date: 2021-06-16_

__Improvements__
* \[[EVSCLT-903](https://gazelle.ihe.net/jira/browse/EVSCLT-903)\] - Probleme Cross-Validation
* \[[EVSCLT-904](https://gazelle.ihe.net/jira/browse/EVSCLT-904)\] - Impossible de valider une archive dans le MCA

# 5.13.2
_Release date: 2021-05-06_

__Bug__
* \[[EVSCLT-870](https://gazelle.ihe.net/jira/browse/EVSCLT-870)\] - Fix diplay issues

# 5.13.1
_Release date: 2021-04-15_

** Sub-task
* \[[EVSCLT-864](https://gazelle.ihe.net/jira/browse/EVSCLT-864)\] Change retrieve date
* \[[EVSCLT-865](https://gazelle.ihe.net/jira/browse/EVSCLT-865)\] Add validation from MCA validation service
* \[[EVSCLT-866](https://gazelle.ihe.net/jira/browse/EVSCLT-866)\] Add validation from Xval validation Service
* \[[EVSCLT-867](https://gazelle.ihe.net/jira/browse/EVSCLT-867)\] Refactor Validator Name in the case of CDA


** Story
* \[[EVSCLT-863] (https://gazelle.ihe.net/jira/browse/EVSCLT-863)\] - Indicateur ANS - Rework

# 5.13.0 
_Release date: 2021-03-24_

__Remarks__

Add function to export as CSV validation of last month. 

__Epic__
* \[[EVSCLT-849](https://gazelle.ihe.net/jira/browse/EVSCLT-849)\] EVS validation data export

__Story__
* \[[EVSCLT-853](https://gazelle.ihe.net/jira/browse/EVSCLT-853)\] Retrieve validation data CSV of last month

__Task__
* \[[EVSCLT-850](https://gazelle.ihe.net/jira/browse/EVSCLT-850)\] Validations data model and application
* \[[EVSCLT-851](https://gazelle.ihe.net/jira/browse/EVSCLT-851)\] Validation data DB query to rebuild data
* \[[EVSCLT-852](https://gazelle.ihe.net/jira/browse/EVSCLT-852)\] Validation data CSV Presenter

__Improvement__
* \[[EVSCLT-846](https://gazelle.ihe.net/jira/browse/EVSCLT-846)\] Update cas configuration to use a dedicated property file

# 5.12.5
_Release date: 2021-02-01_

__Remarks__

Include the __gazelle-tools release  3.1.3__

__Bug__
* \[[EVSCLT-845](https://gazelle.ihe.net/jira/browse/EVSCLT-845)\] Vulnerability XXE

__Improvement__
* \[[EVSCLT-847](https://gazelle.ihe.net/jira/browse/EVSCLT-847)\] File Content Section HTML Rendering XML on GUI Update Request - Replace "View Pretty XML Content" with "View XML with Stylesheet"


# 5.12.4
_Release date: 2020-11-05_

__Bug__
* \[[EVSCLT-841](https://gazelle.ihe.net/jira/browse/EVSCLT-841)\] Validation Fails because timeout to short

# 5.12.3
_Release date: 2020-10-16_

## Gazelle X Validation 1.4.3

__Bug__

* \[[XVAL-168](https://gazelle.ihe.net/jira/browse/XVAL-168)\] Unit test file uploading is not working in partitioned environment

# 5.12.2
_Release date: 2020-02-14_

__Bug__

* \[[EVSCLT-824](https://gazelle.ihe.net/jira/browse/EVSCLT-824)\] EVSClient must display messages in xml format for hl7v2 validator when xml message is input

__Improvement__

* \[[EVSCLT-822](https://gazelle.ihe.net/jira/browse/EVSCLT-822)\] Move to the latest version of dicom3tools


# 5.12.1
_Release date: 2019-11-04_

## Message Content Analyzer 2.3.2

__Bug__
* \[[MCA-99](https://gazelle.ihe.net/jira/browse/MCA-99)\] ZIP archives are downloaded as txt files

# 5.12.0
_Release date: 2019-10-11_

__Remarks__
This release uses the latest version of Gazelle X Validation module, see changes below.

__Bug__

* \[[EVSCLT-815](https://gazelle.ihe.net/jira/browse/EVSCLT-815)\] Improve the integration of the Stylesheets in XML validation results not to overwrite gazelle style

__Improvement__

* \[[EVSCLT-810](https://gazelle.ihe.net/jira/browse/EVSCLT-810)\] Externalize the configuration of the mail server

## Gazelle X Validation 1.4.2
_Release date: 2019-10-11_

__Improvement__

* \[[XVAL-161](https://gazelle.ihe.net/jira/browse/XVAL-161)\] Externalize report XSL stylesheet
* \[[XVAL-163](https://gazelle.ihe.net/jira/browse/XVAL-163)\] Add link to AssertionManager in validation results
* \[[XVAL-164](https://gazelle.ihe.net/jira/browse/XVAL-164)\] Dates comparison
* \[[XVAL-165](https://gazelle.ihe.net/jira/browse/XVAL-165)\] Enhance unit test report

# 5.11.2
_Release date: 2019-04-01_

__Remarks__

This release uses the latest version of Gazelle X Validation module, see changes below.

## Gazelle X Validation 1.4.1

__Bug__

* \[[XVAL-162](https://gazelle.ihe.net/jira/browse/XVAL-162)\] [Cross-Validator] Edition des ID

# 5.11.1
_Release date: 2019-03-15_

__Remarks__
Management of the datasource has changed. Refer to the installation manual to configure your Jboss when updating to this version.

__Improvement__
* \[[EVSCLT-793](https://gazelle.ihe.net/jira/browse/EVSCLT-793)\] Extract datasource configuration
* \[[EVSCLT-794](https://gazelle.ihe.net/jira/browse/EVSCLT-794)\] Update SQL scripts archive

## Message Content Analyzer 2.3.1

__Bug__
* \[[MCA-97](https://gazelle.ihe.net/jira/browse/MCA-97)\] [ASIP XDM] No CDA file in archive should generate an error

# 5.11.0
_Release date: 2019-03-05_

__Improvement__
* \[[EVSCLT-801](https://gazelle.ihe.net/jira/browse/EVSCLT-801)\] Add AssertionCoverageProvider API reference from X Val WS
* \[[EVSCLT-806](https://gazelle.ihe.net/jira/browse/EVSCLT-806)\] The administration menu for X validator shall be moved under add-ons

## Gazelle X Validation 1.4.0

__Story__
* \[[XVAL-157](https://gazelle.ihe.net/jira/browse/XVAL-157)\] Detect files resulting from XDM Detection in MCA and send them back

__Improvement__
* \[[XVAL-159](https://gazelle.ihe.net/jira/browse/XVAL-159)\] Review the XSL stylesheet to display the abort reason to the user

## Message Content Analyzer 2.3.0

__Bug__
* \[[MCA-84](https://gazelle.ihe.net/jira/browse/MCA-84)\] ZIP architecture not properly displaying in analysis results
* \[[MCA-85](https://gazelle.ihe.net/jira/browse/MCA-85)\] MTOM Not detected when X509Certificate xml tag in content
* \[[MCA-86](https://gazelle.ihe.net/jira/browse/MCA-86)\] The MCA looses the information on calling tools when called from remote validator
* \[[MCA-94](https://gazelle.ihe.net/jira/browse/MCA-94)\] XValidation results are not persisted in analysis results
* \[[MCA-95](https://gazelle.ihe.net/jira/browse/MCA-95)\] Prevent User from sending an archive to validation
* \[[MCA-96](https://gazelle.ihe.net/jira/browse/MCA-96)\] Not logged in User is able to filter analysis logs on users

__Story__
* \[[MCA-87](https://gazelle.ihe.net/jira/browse/MCA-87)\] Create XDM Detection
* \[[MCA-88](https://gazelle.ihe.net/jira/browse/MCA-88)\] Send Subparts to Cross-Validation
* \[[MCA-89](https://gazelle.ihe.net/jira/browse/MCA-89)\] Find analysis log from analysis subpart oid

## EVSClient-connector 1.3.0

__Bug__
* \[[EVSCONNECT-5](https://gazelle.ihe.net/jira/browse/EVSCONNECT-5)\] Add method to retrieve validator status

__Story__
* \[[EVSCONNECT-4](https://gazelle.ihe.net/jira/browse/EVSCONNECT-4)\] Manage Subparts from MCA for XDM Detection

# 5.10.3
_Release date: 2019-03-02_

## Gazelle X Validation 1.3.0

__Improvement__
* \[[XVAL-149](https://gazelle.ihe.net/jira/browse/XVAL-149)\] Add a MANIFEST in input's MessageType enumeration
* \[[XVAL-150](https://gazelle.ihe.net/jira/browse/XVAL-150)\] Manage input optionality in the cardinality


# 5.10.2
_Release date: 2019-01-02_

__Bug__
* \[[EVSCLT-797](https://gazelle.ihe.net/jira/browse/EVSCLT-797)\] No Validation Result when xml document is not well formed

# 5.10.1
_Release date: 2018-12-20_

__Remarks__
For the task EVSCLT-792, part of the work was done in Gazelle Test Management size. To benefit from the harmonization of validation results between those tools, it is required to work with Gazelle Test Management version 5.9.5 or higher. There is however no compatibility problem if another version is used.

__Sub-task__
* \[[EVSCLT-792](https://gazelle.ihe.net/jira/browse/EVSCLT-792)\] Harmonize style of the result between TM and EVS

__Bug__
* \[[EVSCLT-739](https://gazelle.ihe.net/jira/browse/EVSCLT-739)\] No status given when validating with dciovfy
* \[[EVSCLT-755](https://gazelle.ihe.net/jira/browse/EVSCLT-755)\] DICOM Validation result does not show correct Dicom Operation 
* \[[EVSCLT-776](https://gazelle.ihe.net/jira/browse/EVSCLT-776)\] Impossible to delete a tool referenced in Calling Tools menu
* \[[EVSCLT-790](https://gazelle.ihe.net/jira/browse/EVSCLT-790)\] MBValidation stub timeout is not working

# 5.10.0
_Release date: 2018-10-31_

__Remarks__

The "version" module with version 2.0.0 is only compliant with Widefly 8 and thus not applicable to this version of EVSClient.

__Bug__

* \[[EVSCLT-787](https://gazelle.ihe.net/jira/browse/EVSCLT-787)\] Missing Translation

# Message Content Analyzer 2.2.1 changes

__Bug__

* \[[MCA-80](https://gazelle.ihe.net/jira/browse/MCA-80)\] Performance issue due to overly complex regex for xml tag detection
* \[[MCA-81](https://gazelle.ihe.net/jira/browse/MCA-81)\] Missing translation from 2.1.0

# Gazelle X Validation 1.2.0 changes

__Bug__

* \[[XVAL-120](https://gazelle.ihe.net/jira/browse/XVAL-120)\] Add an input button is not working after saving a validator
* \[[XVAL-121](https://gazelle.ihe.net/jira/browse/XVAL-121)\] Problem after resetting validator during cross validator selection
* \[[XVAL-122](https://gazelle.ihe.net/jira/browse/XVAL-122)\] Fix cross validation inputs number detection in EVSClient
* \[[XVAL-123](https://gazelle.ihe.net/jira/browse/XVAL-123)\] Add a rule button is not working after saving a validator
* \[[XVAL-124](https://gazelle.ihe.net/jira/browse/XVAL-124)\] Fix the link in Edit Rule to go back to Validator Editor instead of Validator Documentation
* \[[XVAL-128](https://gazelle.ihe.net/jira/browse/XVAL-128)\] Add inherited inputs in validator inputs table when calling a validator
* \[[XVAL-129](https://gazelle.ihe.net/jira/browse/XVAL-129)\] Validation log page is empty

__Story__

* \[[XVAL-140](https://gazelle.ihe.net/jira/browse/XVAL-140)\] Hide create unit test file button when no textual expression has been created

__Improvement__

* \[[XVAL-127](https://gazelle.ihe.net/jira/browse/XVAL-127)\] Add REST method to POST validator status


__Dependencies__



# 5.9.0
_Release date: 2018-10-11_

__Remarks__

WARNING : When deploying EVSClient version 5.9.0 , make sure that EVSClient version 5.8.x has been deployed on the server and that the migration of analysis data is over. You can then deploy this version and apply the SQL script without loosing any analysis data.

## Message Content Analyzer 2.2.0

__Bug__

* \[[MCA-31](https://gazelle.ihe.net/jira/browse/MCA-31)\] Blank page when trying to access an analysis result report with unappropriated rights.
* \[[MCA-76](https://gazelle.ihe.net/jira/browse/MCA-76)\] XML part download does not show the popup + file extension

__Task__

* \[[MCA-69](https://gazelle.ihe.net/jira/browse/MCA-69)\] Add Sample field in configuration
* \[[MCA-74](https://gazelle.ihe.net/jira/browse/MCA-74)\] Improve the MCA Config page
* \[[MCA-75](https://gazelle.ihe.net/jira/browse/MCA-75)\] Add import/export functionality for MCA configurations
* \[[MCA-79](https://gazelle.ihe.net/jira/browse/MCA-79)\] Change GUI info to popovers

# 5.8.1
_Release date: 2018-10-10_

__Bug__

* \[[EVSCLT-788](https://gazelle.ihe.net/jira/browse/EVSCLT-788)\] Confusion between the integration of new SchematronValidator webservices

# 5.8.0
_Release date: 2018-10-05_

__Remarks__

The sql script "update-5.8.0.sql" shall be completed with values depending on the installation where the tool will be deployed.
This version of EVSClient is not compliant with versions of SchematronValidator prior to 2.4.0.

__Improvement__

* \[[EVSCLT-786](https://gazelle.ihe.net/jira/browse/EVSCLT-786)\] Update to be consistent with SchematronValidator 2.4.0 improvement

# 5.7.2
_Release date: 2018-10-03_

__Bug__

* \[[EVSCLT-768](https://gazelle.ihe.net/jira/browse/EVSCLT-768)\] Legend for statistics on result status is not complete
* \[[EVSCLT-777](https://gazelle.ihe.net/jira/browse/EVSCLT-777)\] Missing acces rule
* \[[EVSCLT-784](https://gazelle.ihe.net/jira/browse/EVSCLT-784)\] Missing preference causes XValidators oid to contain 'null'


# 5.7.1
_Release date: 2018-09-21_

__Bug__
* \[[EVSCLT-783](https://gazelle.ihe.net/jira/browse/EVSCLT-783)\] PDF validation log page does not display all results when arriving on the page

# 5.7.0
_Release date: 2018-09-14_

__Remarks__

A new module has been released : Gazelle Calling Tools 1.0.0.
This module is now in dependence in a jar with EVSClient and Gazelle X Validation module embedded in EVSClient.

__Bug__

* \[[EVSCLT-774](https://gazelle.ihe.net/jira/browse/EVSCLT-774)\] TM does not display the right result for PDF validation from EVSClient (sample validation)

## Message Content Analyzer 2.1.0

__Epic__

* \[[MCA-60](https://gazelle.ihe.net/jira/browse/MCA-60)\] Define and Externalize MCA configuration

## Cross Validation 1.1.0

__Story__

* \[[XVAL-19](https://gazelle.ihe.net/jira/browse/XVAL-19)\] API Implement a web service for remote validation
* \[[XVAL-104](https://gazelle.ihe.net/jira/browse/XVAL-104)\] Each X Validator shall have an OID
* \[[XVAL-105](https://gazelle.ihe.net/jira/browse/XVAL-105)\] "Tag" the inputs
* \[[XVAL-116](https://gazelle.ihe.net/jira/browse/XVAL-116)\] Remote validation - Handle the case where the validator OID is not provided
* \[[XVAL-119](https://gazelle.ihe.net/jira/browse/XVAL-119)\] Allow the user to access the XValidator documentation knowing the OID of the validator


# EVSClient 5.6.2 changes
_Release date: 2018-07-19_

__Remarks__

MCA data migration will suppress all record in database that are not linked to a file or that are linked with a file that does not exist.

__Story__
* \[[EVSCLT-772](https://gazelle.ihe.net/jira/browse/EVSCLT-772)\] Display validator version in the validation result 

## Message Content Analyzer 2.0.0

__Bug__
* \[[MCA-17](https://gazelle.ihe.net/jira/browse/MCA-17)\] FileToValidate model serialization bloc migration and model evolution

__Story__
* \[[MCA-9](https://gazelle.ihe.net/jira/browse/MCA-9)\] API improvement

## gazelle-assets 2.0.24

Added stacked icon style class "gzl-icon-stack-small-bottom-right" for MCA "Validate without known Validation Type" button.


# 5.6.1
_Release date: 2018-05-30_

__Remarks__

The sql script 'update-5.6.1.sql' allow to update the preference of the 'fhir_repository' which contains one more space in the 'update-5.3.0.sql'.

__Bug__
* \[[EVSCLT-753](https://gazelle.ihe.net/jira/browse/EVSCLT-753)\] Impossible to validate messages with FHIR validation service
* \[[EVSCLT-760](https://gazelle.ihe.net/jira/browse/EVSCLT-760)\] User is not informed that the validation result is in TM test step
* \[[EVSCLT-761](https://gazelle.ihe.net/jira/browse/EVSCLT-761)\] Error when we validate sample which is download from EVSClient
* \[[EVSCLT-763](https://gazelle.ihe.net/jira/browse/EVSCLT-763)\] File validation permanentlink is not updated


# 5.6.0
_Release date: 2018-05-28_

__Remarks__

In addition to the update_5.6.0.sql script, there is also update_5.6.0.sh to execute to migrate validated files on disk.

__Bug__
* \[[EVSCLT-736](https://gazelle.ihe.net/jira/browse/EVSCLT-736)\] The upload widget is not available on XML based validation page
* \[[EVSCLT-752](https://gazelle.ihe.net/jira/browse/EVSCLT-752)\] Cannot access all logs for PDF validation
* \[[EVSCLT-753](https://gazelle.ihe.net/jira/browse/EVSCLT-753)\] Impossible to validate messages with FHIR validation service
* \[[EVSCLT-756](https://gazelle.ihe.net/jira/browse/EVSCLT-756)\] Signature validation report is not displayed in the validation report

## Message Content Analyzer 1.0.0

__Epic__
* \[[MCA-5](https://gazelle.ihe.net/jira/browse/MCA-5)\] Isolate Content Analyzer as module

__Bug__
* \[[MCA-23](https://gazelle.ihe.net/jira/browse/MCA-23)\] Infinite loop on SAML Assertion detection in Content Analyzer

# 5.5.0
_Release date: 2018-03-31_

__Epic__
* \[[EVSCLT-705](https://gazelle.ihe.net/jira/browse/EVSCLT-705)\] [KELA] Validation of Digital Signature

__Test__
* \[[EVSCLT-708](https://gazelle.ihe.net/jira/browse/EVSCLT-708)\] Test campaign
* \[[EVSCLT-728](https://gazelle.ihe.net/jira/browse/EVSCLT-728)\] [KELA/GDPR] Test campaign

__Bug__
* \[[EVSCLT-734](https://gazelle.ihe.net/jira/browse/EVSCLT-734)\] After logging, the parameters are not passed in the URL

__Story__
* \[[EVSCLT-733](https://gazelle.ihe.net/jira/browse/EVSCLT-733)\] [KELA] Detect signature in CDA documents and validate

__Improvement__
* \[[EVSCLT-706](https://gazelle.ihe.net/jira/browse/EVSCLT-706)\] [KELA] Add a Digital Signature validation module
* \[[EVSCLT-735](https://gazelle.ihe.net/jira/browse/EVSCLT-735)\] Use of the IOUtils.closequietly in finally's in order to clean up the code

# 5.4.0
_Release date: 2018-03-02_

__Bug__
* \[[EVSCLT-698](https://gazelle.ihe.net/jira/browse/EVSCLT-698)\] When several menu entries reference the same standard type, it is impossible to access all of them

__Story__
* \[[EVSCLT-723](https://gazelle.ihe.net/jira/browse/EVSCLT-723)\] [KELA/GDPR] Execution of GDPR rules

__Improvement__
* \[[EVSCLT-696](https://gazelle.ihe.net/jira/browse/EVSCLT-696)\] Try to really apereo cas logout
* \[[EVSCLT-702](https://gazelle.ihe.net/jira/browse/EVSCLT-702)\] [KELA/GDPR] Allow the administrator of the tool to enable/disable the GDPR-checks
* \[[EVSCLT-725](https://gazelle.ihe.net/jira/browse/EVSCLT-725)\] [KELA/GDPR] Writing of GDPR rules (javascript)

# 5.3.0
_Release date: 2017-12-21_

__Remarks__

From version 5.2.10, EVSClient is making use of the new Gazelle SSO (Apereo). Configuration of the SSO is no more done in database. Make sure the configuration file is available on the server hosting EVSClient at /opt/gazelle/cas/file.properties.

__Bug__
* \[[EVSCLT-662](https://gazelle.ihe.net/jira/browse/EVSCLT-662)\] Memory leak
* \[[EVSCLT-687](https://gazelle.ihe.net/jira/browse/EVSCLT-687)\] The OID is the same after performing another validation

__Story__
* \[[EVSCLT-672](https://gazelle.ihe.net/jira/browse/EVSCLT-672)\] Description field of standard could support HTML tags
* \[[EVSCLT-673](https://gazelle.ihe.net/jira/browse/EVSCLT-673)\] Display description field of validation service on validator page

__Improvement__
* \[[EVSCLT-676](https://gazelle.ihe.net/jira/browse/EVSCLT-676)\] We shall be able to call the FHIR Validator from EVSClient

# 5.2.10

__Bug__
* \[[EVSCLT-645](https://gazelle.ihe.net/jira/browse/EVSCLT-645)\] [EFS] No specific errors when DFDL error happens
* \[[EVSCLT-659](https://gazelle.ihe.net/jira/browse/EVSCLT-659)\] Problem in the view of the XML
# 5.2.9

__Bug__
* \[[EVSCLT-655](https://gazelle.ihe.net/jira/browse/EVSCLT-655)\] [EVS Client] Pb with buttons "Make this result public" / "Share this result"
* \[[EVSCLT-657](https://gazelle.ihe.net/jira/browse/EVSCLT-657)\] Bad label for menu visibility

# 5.2.8

__Bug__
* \[[EVSCLT-637](https://gazelle.ihe.net/jira/browse/EVSCLT-637)\] Template tree has been tested, remove the "experimental" word
# 5.2.7

__Bug__
* \[[EVSCLT-617](https://gazelle.ihe.net/jira/browse/EVSCLT-617)\] When there are only one schematron in CDA validation, it is selected for validation even if you deselect it
* \[[EVSCLT-628](https://gazelle.ihe.net/jira/browse/EVSCLT-628)\] Button validate again is not working, when revalidating a CDA document
* \[[EVSCLT-641](https://gazelle.ihe.net/jira/browse/EVSCLT-641)\] Missing translation strings
* \[[EVSCLT-642](https://gazelle.ihe.net/jira/browse/EVSCLT-642)\] Unexpected entries in Model-based report (PDF version)
* \[[EVSCLT-644](https://gazelle.ihe.net/jira/browse/EVSCLT-644)\] [EFS] Revalidate button in EVS Client does not load the existing file
* \[[EVSCLT-648](https://gazelle.ihe.net/jira/browse/EVSCLT-648)\] When we validate a CDA document that contains a PDF as a <reference> and not embeded B64, we still propose to validate it using the PDF validation

__Story__
* \[[EVSCLT-646](https://gazelle.ihe.net/jira/browse/EVSCLT-646)\] [EHEALTHBEL] The XSL of CDA.xsl cannot be configured in EVSCLient

__Improvement__
* \[[EVSCLT-620](https://gazelle.ihe.net/jira/browse/EVSCLT-620)\] [Sequoia/CDA] Make the SchematronValidation exportable as PDF

# 5.2.4

__Bug__
* \[[EVSCLT-634](https://gazelle.ihe.net/jira/browse/EVSCLT-634)\] Timeout when validating documents

# 5.2.3

__Improvement__
* \[[EVSCLT-636](https://gazelle.ihe.net/jira/browse/EVSCLT-636)\] Add the ability for a menu to be only accessible to logged in users

# 5.2.2

__Improvement__
* \[[EVSCLT-623](https://gazelle.ihe.net/jira/browse/EVSCLT-623)\] Improve verbosity of mail send by EVSClient when access to External validation service does not work

# 5.2.1

__Story__
* \[[EVSCLT-630](https://gazelle.ihe.net/jira/browse/EVSCLT-630)\] URL to assertion manager is hard-coded

#  5.2.0
_Release date: 2017-06-17_

__Improvement__
* \[[EVSCLT-627](https://gazelle.ihe.net/jira/browse/EVSCLT-627)\] Update to newest version of gazelle-x-validation

#  5.1.6

__Improvement__
* \[[EVSCLT-626](https://gazelle.ihe.net/jira/browse/EVSCLT-626)\] Add a delete validation result for admin

# 5.1.5

__Bug__
* \[[EVSCLT-562](https://gazelle.ihe.net/jira/browse/EVSCLT-562)\] Cannot navigate among issues in the CDA (Firefox)
* \[[EVSCLT-563](https://gazelle.ihe.net/jira/browse/EVSCLT-563)\] Issues with some documents
* \[[EVSCLT-612](https://gazelle.ihe.net/jira/browse/EVSCLT-612)\] Unsubscribe DSUB messages are not detected in the Content Analyzer

__Story__
* \[[EVSCLT-122](https://gazelle.ihe.net/jira/browse/EVSCLT-122)\] NPE when validating a message with bad MSH-1

__Improvement__
* \[[EVSCLT-340](https://gazelle.ihe.net/jira/browse/EVSCLT-340)\] Content Analyzer should recognize MTOM even if we do not start by the MTOM section

# 5.1.4
_Release date: 2017-05-02_

__Bug__
* \[[EVSCLT-618](https://gazelle.ihe.net/jira/browse/EVSCLT-618)\] Message Content Analyser edited message not taken into account
* \[[EVSCLT-619](https://gazelle.ihe.net/jira/browse/EVSCLT-619)\] The link on the eye icon to goto the error in the code on the validation page does not work in some cases

__Story__
* \[[EVSCLT-538](https://gazelle.ihe.net/jira/browse/EVSCLT-538)\] [Sequoia/CDA] Make the report exportable as PDF

# 5.1.3
_Release date: 2017-03-30_

__Bug__
* \[[EVSCLT-604](https://gazelle.ihe.net/jira/browse/EVSCLT-604)\] Link to validation result bound to a proxy URL does not lead to a valid page
* \[[EVSCLT-606](https://gazelle.ihe.net/jira/browse/EVSCLT-606)\] Call to DCCHECK  validation service does not work

# 5.1.2
_Release date: 2017-02-08_
__Bug__
* \[[EVSCLT-602](https://gazelle.ihe.net/jira/browse/EVSCLT-602)\] GetValidation*ByExternalId does not return accurate result

# 5.1.1

__Bug__
* \[[EVSCLT-601](https://gazelle.ihe.net/jira/browse/EVSCLT-601)\] Automated message analyzer truncates some of the DSUB messages

__Improvement__
* \[[EVSCLT-600](https://gazelle.ihe.net/jira/browse/EVSCLT-600)\] File extensions of files that can be uploaded shall be configurable

# 5.1.0
_Release date: 2017-01-13_
__Remarks__

This version (5.1.0) of EVSClient will be able to communicate only with instances of Schematron Validator 2.1.0 and higher. This version also embeds the scorecard service for CDA document, the one is available in CDA Generator 2.1.1 and higher.

__Technical task__
* \[[EVSCLT-539](https://gazelle.ihe.net/jira/browse/EVSCLT-539)\] [Sequoia/CDA] Display advanced filters
* \[[EVSCLT-541](https://gazelle.ihe.net/jira/browse/EVSCLT-541)\] [Sequoia/CDA] Display statistics on template usage part of the scorecard
* \[[EVSCLT-542](https://gazelle.ihe.net/jira/browse/EVSCLT-542)\] [Sequoia/CDA] Display statistics on document part of the scorecard
* \[[EVSCLT-543](https://gazelle.ihe.net/jira/browse/EVSCLT-543)\] [Sequoia/CDA] Define how EVSClient retrieves the scorecard from Gazelle Object Checker
* \[[EVSCLT-544](https://gazelle.ihe.net/jira/browse/EVSCLT-544)\] [Sequoia/CDA] EVSClient shall retrieve the scorecard for a given CDA

__Bug__
* \[[EVSCLT-404](https://gazelle.ihe.net/jira/browse/EVSCLT-404)\] DB Script to add WS Trust is missing
* \[[EVSCLT-450](https://gazelle.ihe.net/jira/browse/EVSCLT-450)\] Allow user to perform a new validation
* \[[EVSCLT-455](https://gazelle.ihe.net/jira/browse/EVSCLT-455)\] DICOM validate again button does not work
* \[[EVSCLT-493](https://gazelle.ihe.net/jira/browse/EVSCLT-493)\] Validation result information panel is displayed differently
* \[[EVSCLT-514](https://gazelle.ihe.net/jira/browse/EVSCLT-514)\] The user is no more able to validate a new document
* \[[EVSCLT-555](https://gazelle.ihe.net/jira/browse/EVSCLT-555)\] When you show the result of validation, we don't see the documentation from CDAGenerator but we see the link to the assertion
* \[[EVSCLT-559](https://gazelle.ihe.net/jira/browse/EVSCLT-559)\] Stastitics are not displayed for the validation kind selected by the user
* \[[EVSCLT-561](https://gazelle.ihe.net/jira/browse/EVSCLT-561)\] Schematron validation results: cannot hide/unhide reports
* \[[EVSCLT-564](https://gazelle.ihe.net/jira/browse/EVSCLT-564)\] Improve display of information section
* \[[EVSCLT-566](https://gazelle.ihe.net/jira/browse/EVSCLT-566)\] "Make this result public" does not work as expected
* \[[EVSCLT-569](https://gazelle.ihe.net/jira/browse/EVSCLT-569)\] Privacy of HL7v2 validation result is not accurate
* \[[EVSCLT-570](https://gazelle.ihe.net/jira/browse/EVSCLT-570)\] HL7v2: Reset button does not work
* \[[EVSCLT-571](https://gazelle.ihe.net/jira/browse/EVSCLT-571)\] Review display of header of HL7 message profile selection table
* \[[EVSCLT-577](https://gazelle.ihe.net/jira/browse/EVSCLT-577)\] I see no difference between two methods of DICOM3TOOLS
* \[[EVSCLT-579](https://gazelle.ihe.net/jira/browse/EVSCLT-579)\] DICOM Web Validation does not work
* \[[EVSCLT-580](https://gazelle.ihe.net/jira/browse/EVSCLT-580)\] CDA Validation: Notification filter based on template tree does not work
* \[[EVSCLT-584](https://gazelle.ihe.net/jira/browse/EVSCLT-584)\] No data available in Sequoia CDA validation Staistics page
* \[[EVSCLT-585](https://gazelle.ihe.net/jira/browse/EVSCLT-585)\] [Add-ons/Schematrons] The list of schematrons : filters for label and name don't work

__Story__
* \[[EVSCLT-529](https://gazelle.ihe.net/jira/browse/EVSCLT-529)\] compute statistics is twice in EVSClient
* \[[EVSCLT-534](https://gazelle.ihe.net/jira/browse/EVSCLT-534)\] [Sequoia/CDA] Display the CDA scorecard (produced by GOC)
* \[[EVSCLT-556](https://gazelle.ihe.net/jira/browse/EVSCLT-556)\] HL7v2 : Validation result is never send to the proxy

__Improvement__
* \[[EVSCLT-565](https://gazelle.ihe.net/jira/browse/EVSCLT-565)\] Allow user to "unshare" a result
* \[[EVSCLT-572](https://gazelle.ihe.net/jira/browse/EVSCLT-572)\] Consider showing the privacy in the log table
* \[[EVSCLT-574](https://gazelle.ihe.net/jira/browse/EVSCLT-574)\] H7v2: Add a faces message or scroll down to validation report
* \[[EVSCLT-575](https://gazelle.ihe.net/jira/browse/EVSCLT-575)\] HL7v2: Filtering of notifications do not work on validation page

# 5.0.14

__Bug__
* \[[EVSCLT-408](https://gazelle.ihe.net/jira/browse/EVSCLT-408)\] In CDA validation result, if user try to sort by extension it doesn't work
* \[[EVSCLT-495](https://gazelle.ihe.net/jira/browse/EVSCLT-495)\] Gazelle Cross Validator doesn't work
* \[[EVSCLT-553](https://gazelle.ihe.net/jira/browse/EVSCLT-553)\] Admin user should see as much as the users with the monitor role

__Epic__
* \[[EVSCLT-480](https://gazelle.ihe.net/jira/browse/EVSCLT-480)\] Update validation report XSL files

__Improvement__
* \[[EVSCLT-441](https://gazelle.ihe.net/jira/browse/EVSCLT-441)\] Make sure the tool creates directories with 775 rights
* \[[EVSCLT-516](https://gazelle.ihe.net/jira/browse/EVSCLT-516)\] Allow the same feature in MBV and Schematron validation reports

# 5.0.13

__Bug__
* \[[EVSCLT-533](https://gazelle.ihe.net/jira/browse/EVSCLT-533)\] HL7v3 validator doesn't load properly the XML file.

__Story__
* \[[EVSCLT-113](https://gazelle.ihe.net/jira/browse/EVSCLT-113)\] add pretty formatting when validating XDS metadata
* \[[EVSCLT-530](https://gazelle.ihe.net/jira/browse/EVSCLT-530)\] The errors are not CSS displayed in validation of XDS metadatas

__Improvement__
* \[[EVSCLT-330](https://gazelle.ihe.net/jira/browse/EVSCLT-330)\] Harmonisation of xsl files for validators.
* \[[EVSCLT-536](https://gazelle.ihe.net/jira/browse/EVSCLT-536)\] Page for the management of CDA templates did not make it through the migration to Jboss 7
* \[[EVSCLT-537](https://gazelle.ihe.net/jira/browse/EVSCLT-537)\] Cannot sort/filter validation logs by schematron or model based used
* \[[EVSCLT-552](https://gazelle.ihe.net/jira/browse/EVSCLT-552)\] Problem rendering non ascii characters when using jhighlight

# 5.0.10

__Technical task__
* \[[EVSCLT-550](https://gazelle.ihe.net/jira/browse/EVSCLT-550)\] [Sequoia/CDA] Integrate the new report for CDA in EVSClient

# 5.0.9

__Improvement__
* \[[EVSCLT-549](https://gazelle.ihe.net/jira/browse/EVSCLT-549)\] Add a preference to hide the add-ons menu.


# 5.0.8
_Release date: 2016-11-25_

__Bug__
* \[[EVSCLT-545](https://gazelle.ihe.net/jira/browse/EVSCLT-545)\] Webservice to get last validation permanent link return last validation date
* \[[EVSCLT-222](https://gazelle.ihe.net/jira/browse/EVSCLT-222)\] DiskStorePathManager for CacheManager is set to /tmp
* \[[EVSCLT-525](https://gazelle.ihe.net/jira/browse/EVSCLT-525)\] Validation of PDF embedded in CDA does not work
* \[[EVSCLT-526](https://gazelle.ihe.net/jira/browse/EVSCLT-526)\] UserAdmin.css is not used but embedded in the EAR. Should be removed.
* \[[EVSCLT-527](https://gazelle.ihe.net/jira/browse/EVSCLT-527)\] Calling tool page : Table scrolling does not work.
* \[[EVSCLT-463](https://gazelle.ihe.net/jira/browse/EVSCLT-463)\] EVSClient - epSOS Validators - Reset button changes validators
* \[[EVSCLT-468](https://gazelle.ihe.net/jira/browse/EVSCLT-468)\] When Schematron validation is performed, the validation result is empty
* \[[EVSCLT-472](https://gazelle.ihe.net/jira/browse/EVSCLT-472)\] XD* Validation : XSL model based report has old style
* \[[EVSCLT-477](https://gazelle.ihe.net/jira/browse/EVSCLT-477)\] HL7v2 validation report : "Profile exceptions" panel has old style
* \[[EVSCLT-482](https://gazelle.ihe.net/jira/browse/EVSCLT-482)\] AuditMessage validation report xsl has old style
* \[[EVSCLT-483](https://gazelle.ihe.net/jira/browse/EVSCLT-483)\] DSUB validation report XSL has old style
* \[[EVSCLT-519](https://gazelle.ihe.net/jira/browse/EVSCLT-519)\] Missing EVSClient-dist.zip for 5.0.0 on nexus
* \[[EVSCLT-522](https://gazelle.ihe.net/jira/browse/EVSCLT-522)\] The creation of a new reference standard does not save the name of the referenced standard

__Improvement__
* \[[EVSCLT-402](https://gazelle.ihe.net/jira/browse/EVSCLT-402)\] Improved errors display

__Story__
* \[[EVSCLT-521](https://gazelle.ihe.net/jira/browse/EVSCLT-521)\] in Manage referenced standards the filtering related to extension disapeared

# 5.0.0
_Release date: 2016-09-05_

__Remarks__
This is the first release of the EVS Client application for the Jboss 7 Application Server. 
The entire design of the application has been reviewed. The application now uses the harmmonized 
layout of the Gazelle Test Bed applications.

This version does not yet include the Gazelle Cross Validator component. 

__Bug__
* \[[EVSCLT-396](https://gazelle.ihe.net/jira/browse/EVSCLT-396)\] Errors in ovh3 logs
* \[[EVSCLT-414](https://gazelle.ihe.net/jira/browse/EVSCLT-414)\] Message content analyzer fails to show re-analyze of edited message/content
* \[[EVSCLT-415](https://gazelle.ihe.net/jira/browse/EVSCLT-415)\] Revalidate doesn't work anymore for Content Analyzer
* \[[EVSCLT-423](https://gazelle.ihe.net/jira/browse/EVSCLT-423)\] Empty element not recognized 
* \[[EVSCLT-425](https://gazelle.ihe.net/jira/browse/EVSCLT-425)\] Infinite Loop when validating the WS-TRUST response file 
* \[[EVSCLT-426](https://gazelle.ihe.net/jira/browse/EVSCLT-426)\] Bug in the javascript of the list of checks
* \[[EVSCLT-451](https://gazelle.ihe.net/jira/browse/EVSCLT-451)\] Update Schematron validation report stylesheet to new style
* \[[EVSCLT-452](https://gazelle.ihe.net/jira/browse/EVSCLT-452)\] Missing translation keys
* \[[EVSCLT-453](https://gazelle.ihe.net/jira/browse/EVSCLT-453)\] CDA HTML view is not rendered
* \[[EVSCLT-454](https://gazelle.ihe.net/jira/browse/EVSCLT-454)\] DSUB content is not displayed as HTML
* \[[EVSCLT-466](https://gazelle.ihe.net/jira/browse/EVSCLT-466)\] A broken image is displayed next to the username when you are logged in with the CAS (Firefox)
* \[[EVSCLT-467](https://gazelle.ihe.net/jira/browse/EVSCLT-467)\] When a FacesMessage is displayed, it never disappear
* \[[EVSCLT-469](https://gazelle.ihe.net/jira/browse/EVSCLT-469)\] Show templates tree : no tree is displayed
* \[[EVSCLT-471](https://gazelle.ihe.net/jira/browse/EVSCLT-471)\] On permanentlink result, the font is changed (blue, with smaller pixels)
* \[[EVSCLT-473](https://gazelle.ihe.net/jira/browse/EVSCLT-473)\] User can't access the list of Schematron
* \[[EVSCLT-474](https://gazelle.ihe.net/jira/browse/EVSCLT-474)\] HL7v2 validation : Error The given Id is unknown
* \[[EVSCLT-476](https://gazelle.ihe.net/jira/browse/EVSCLT-476)\] Revalidate an HL7v2 message trigger a Null Pointer Exception
* \[[EVSCLT-479](https://gazelle.ihe.net/jira/browse/EVSCLT-479)\] PDF validation have old style
* \[[EVSCLT-481](https://gazelle.ihe.net/jira/browse/EVSCLT-481)\] In statistics if user click on compute stat the application crash
* \[[EVSCLT-485](https://gazelle.ihe.net/jira/browse/EVSCLT-485)\] HL7v3 validation report XSL has old style
* \[[EVSCLT-486](https://gazelle.ihe.net/jira/browse/EVSCLT-486)\] Assertion (SAML) validation report XSL has old style
* \[[EVSCLT-487](https://gazelle.ihe.net/jira/browse/EVSCLT-487)\] There is no check to verify that the XSL input is really an URL
* \[[EVSCLT-488](https://gazelle.ihe.net/jira/browse/EVSCLT-488)\] HPD Validation report XSL has old style
* \[[EVSCLT-489](https://gazelle.ihe.net/jira/browse/EVSCLT-489)\] SVS Validation report XSL has old style
* \[[EVSCLT-490](https://gazelle.ihe.net/jira/browse/EVSCLT-490)\] Administration -> Manage referenced standards : inconsistency with gazelle style
* \[[EVSCLT-491](https://gazelle.ihe.net/jira/browse/EVSCLT-491)\] Title of "Reuse message" button (refresh icon) displays crowdin key
* \[[EVSCLT-492](https://gazelle.ihe.net/jira/browse/EVSCLT-492)\] Change referenced standard create / edit layout
* \[[EVSCLT-494](https://gazelle.ihe.net/jira/browse/EVSCLT-494)\] CDA validation report : HTML file content XSL is inconsistent with new Gazelle style
* \[[EVSCLT-496](https://gazelle.ihe.net/jira/browse/EVSCLT-496)\] CDA Validation Report : Check "show template tree (experimental)" change layout.
* \[[EVSCLT-497](https://gazelle.ihe.net/jira/browse/EVSCLT-497)\] When editing a referenced standard, transaction failed when adding an XSL location
* \[[EVSCLT-500](https://gazelle.ihe.net/jira/browse/EVSCLT-500)\] HL7v2 validator list : "Validate with the profile" has an old-styled icon
* \[[EVSCLT-501](https://gazelle.ihe.net/jira/browse/EVSCLT-501)\] EVSClient menu : icons that are not a brand should be font-awesome icons
* \[[EVSCLT-502](https://gazelle.ihe.net/jira/browse/EVSCLT-502)\] Manage calling tools : If you create an already existing preference property, there is a transaction failed, it should instead be handled with a message saying that the property already exists
* \[[EVSCLT-503](https://gazelle.ihe.net/jira/browse/EVSCLT-503)\] Create calling tools :If you register the same tool (same label and oid) there is a transaction failed instead of a message
* \[[EVSCLT-505](https://gazelle.ihe.net/jira/browse/EVSCLT-505)\] Can't acces to previous validation in Dicom SCP Screener
* \[[EVSCLT-506](https://gazelle.ihe.net/jira/browse/EVSCLT-506)\] Menu configuration : No confirmation popup when deleting an entry
* \[[EVSCLT-508](https://gazelle.ihe.net/jira/browse/EVSCLT-508)\] Manage Validation Services -> Edit : Inconsistency with the database
* \[[EVSCLT-515](https://gazelle.ihe.net/jira/browse/EVSCLT-515)\] link to assertions in CDA result is not correct
* \[[EVSCLT-518](https://gazelle.ihe.net/jira/browse/EVSCLT-518)\] for dcmcheck we are not able anymore to edit the service in V7 version

__Epic__
* \[[EVSCLT-416](https://gazelle.ihe.net/jira/browse/EVSCLT-416)\] Migrate EVSClient to Jboss 7
* \[[EVSCLT-418](https://gazelle.ihe.net/jira/browse/EVSCLT-418)\] migrate EVSClient

__Story__
* \[[EVSCLT-517](https://gazelle.ihe.net/jira/browse/EVSCLT-517)\] test day Jboss7 : execution

__Improvement__
* \[[EVSCLT-341](https://gazelle.ihe.net/jira/browse/EVSCLT-341)\] Integrate the Gazelle X Validation module
* \[[EVSCLT-345](https://gazelle.ihe.net/jira/browse/EVSCLT-345)\] Add HTTP detection in Content Analyzer
* \[[EVSCLT-386](https://gazelle.ihe.net/jira/browse/EVSCLT-386)\] Can we have the checkbox to hide the passed requirement when we validate a HL7 V2 message?
* \[[EVSCLT-419](https://gazelle.ihe.net/jira/browse/EVSCLT-419)\] Fix GUI issues
* \[[EVSCLT-420](https://gazelle.ihe.net/jira/browse/EVSCLT-420)\] Review menu layout
* \[[EVSCLT-421](https://gazelle.ihe.net/jira/browse/EVSCLT-421)\] migrate pixelmed-jar
* \[[EVSCLT-459](https://gazelle.ihe.net/jira/browse/EVSCLT-459)\] remove Cache from results webservices
* \[[EVSCLT-511](https://gazelle.ihe.net/jira/browse/EVSCLT-511)\] As an administrator of the tool, I need to know which instance of EVSClient is reporting issue in order to manage it
* \[[EVSCLT-512](https://gazelle.ihe.net/jira/browse/EVSCLT-512)\] remove ear from EVSClient-dist.zip
